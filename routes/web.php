<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Facebook api
// EAAQhs75iutUBAPPnnky3EIPAZALwep935ZAJmMv2S9eHbqZBuhtBHv2X1s86lXvtiYU84DZAhxvdvdKXCizeayOwKZCfSOZBvZAhDphnJDqBfjILmcVsjZAZAeZAbr5q0uscC7ftGeG5g70cnZBY7sGMfpkbeMNtg3DTtBLcgnYZAjpXvAZDZD

// Route::domain('revistas.upmaxixe.mock')->group(function () {
//   // SUB DOMAIN
//   Route::get('/', 'RevistasController@index')->name('revistaseartigos');
  
// });


// Route::domain('upmaxixe.mock')->group(function () {
  // MAIN DOMAIN
  Route::get('/', 'PagesController@home')->name('home');
  Route::get('/upengenheiros', 'PagesController@upengenheiros')->name('upengenheiros');
  



  # UNIVERSIDADES TAB
  Route::get('/quemsomos', 'PagesController@quemsomos')->name('quemsomos');
  Route::get('/misseval', 'PagesController@missaoevalores')->name('missaoevalores');
    Route::get('/docentes', 'PagesController@docentes')->name('docentes');
      // Docentes --> Management
      Route::get('/docentes/manage', 'PagesController@docentesmanage')->name('docentes.manage');
      Route::get('/docentes/{id}', 'PagesController@docentesshow');
      Route::post('/docentes/manage/add', 'PagesController@addDocente');
      Route::delete('/docentes/manage/delete/{id}', 'PagesController@deleteDocente'); // delete docente
      Route::get('/docentes/manage/edit/{id}', 'PagesController@editDocente'); // show the edit form
      Route::put('/docentes/manage/edit/{id}', 'PagesController@updateDocente'); // save the edit
  Route::get('/organigrama', 'PagesController@organigrama')->name('organigrama');
  Route::get('/orgaossup', 'PagesController@orgaossuperiores')->name('orgaossuperiores');
  Route::get('/consdirect', 'PagesController@conselhodirectivo')->name('conselhodirectivo');




  # DIRECCAO TAB
  Route::get('/director', 'PagesController@odirector')->name('odirector');
  Route::get('/gabinete', 'PagesController@gabinetedodirector')->name('gabinetedodirector');
  Route::get('/secretaria', 'PagesController@secretariageral')->name('secretariageral');
  Route::get('/registoacademico', 'PagesController@registoacademico')->name('registoacademico');



  
  # COOPERAÇÃO E INTERNACIONALIZAÇÃO TAB
  Route::prefix('/cei')->group(function () { // 'cei' stands for 'cooperação e internacionalização'
    // Matches The "/cei/apresentacao" URL:
    Route::get('/apresentacao', 'CooperacaoController@apresentacao')->name('cei.apresentacao');
      // Matches The "/cei/nacional" URL:
      Route::get('cooperacao/nacional', 'CooperacaoController@cooperacaonacional')->name('cei.cooperacao.nacional');
      // Matches The "/cei/internacional" URL:
      Route::get('cooperacao/internacional', 'CooperacaoController@cooperacaointernacional')->name('cei.cooperacao.internacional');
      
      // Matches The "/cei/internacionalizacao/visiting" URL:
      Route::get('internacionalizacao/visiting', 'CooperacaoController@visiting')->name('cei.internacionalizacao.visiting');
      // Matches The "/cei/internacionalizacao/visiting" URL:
      Route::get('internacionalizacao/professoresconvidados', 'CooperacaoController@professoresconvidados')->name('cei.internacionalizacao.professoresconvidados');
      // Matches The "/cei/internacionalizacao/editais" URL:  
      Route::resource('/internacionalizacao/editais', 'IntEditaisController'); // Route::get('internacionalizacao/editais', 'CooperacaoController@editais')->name('cei.internacionalizacao.editais');
    
    // Matches The "/cei/informacao" URL:  
    Route::get('/informacao', 'CooperacaoController@informacao')->name('cei.informacao');
    // Matches The "/cei/newsletter" URL:  
    Route::get('/newsletter', 'CooperacaoController@newsletter')->name('cei.newsletter');
  });




  # FACULDADES TAB
  Route::get('/faculdades', 'PagesController@faculdades')->name('faculdades');




  # PÓS-GRADUAÇÃO, PESQUISA E EXTENSÃO TAB
  // Route::prefix('/centrosenucleos')->group(function () {
  //   Route::get('/', 'Centros_NucleosController@index')->name('centrosenucleos');
  //   Route::get('/adicionar', 'Centros_NucleosController@create')->name('centrosenucleos.create');
  //   // Route::get('/adicionar', 'Centros_NucleosController@create')->name('centrosenucleos.create');
  // });
  Route::get('/dppe', 'PosGraduacaoController@dppeapresentacao')->name('dppe.apresentacao'); 
  Route::get('/dppe/organigrama', 'PosGraduacaoController@organigrama')->name('dppe.organigrama'); 
  Route::get('/dppe/planoestrategico', 'PosGraduacaoController@planoestrategico')->name('dppe.planoestrategico'); 
  Route::get('/dppe/regulamento', 'PosGraduacaoController@regulamento')->name('dppe.regulamento'); 
  Route::get('/dppe/boletim', 'PosGraduacaoController@boletimdodppe')->name('dppe.boletimdodppe'); 

  Route::resource('/centrosenucleos', 'Centros_NucleosController');
  Route::get('/centrosenucleos/showmodal/{id}', 'Centros_NucleosController@showmodal'); // to retrieve evento's info to show a modal

  Route::get('/dppe/dpp', 'PosGraduacaoController@dppapresentacao')->name('dppe.dpp.apresentacao'); 
  Route::get('/dppe/dpp/projectos', 'PosGraduacaoController@dppprojectos')->name('dppe.dpp.projectos'); 
  Route::get('/dppe/dpp/linhasdepesquisa', 'PosGraduacaoController@dpplinhasdepesquisa')->name('dppe.dpp.linhasdepesquisa'); 

  Route::get('/dppe/dpi', 'PosGraduacaoController@dpiapresentacao')->name('dppe.dpi.apresentacao'); 
  Route::get('/dppe/dpi/projectos', 'PosGraduacaoController@dpiprojectos')->name('dppe.dpi.projectos'); 
  Route::get('/dppe/dpi/escolinhas', 'PosGraduacaoController@dpiescolinhas')->name('dppe.dpi.escolinhas'); 

  Route::get('/dppe/dfp', 'PosGraduacaoController@dfpapresentacao')->name('dppe.dfp.apresentacao'); 
  Route::get('/dppe/dfp/efectivodedocente', 'PosGraduacaoController@dfpefectivodedocente')->name('dppe.dfp.efectivodedocente'); 
  Route::get('/dppe/dfp/docentesporformacao', 'PosGraduacaoController@dfpdocentesporformacao')->name('dppe.dfp.docentesporformacao'); 

  Route::get('/revistas', 'RevistasController@index')->name('dppe.publ.revistaseartigos'); 
  Route::get('/actasdeconferencia', 'PosGraduacaoController@publactasdeconferencia')->name('dppe.publ.actasdeconferencia'); 
  Route::get('/livros', 'PosGraduacaoController@publlivros')->name('dppe.publ.livros'); 
  Route::get('/revistasdeposgraduacao', 'PosGraduacaoController@publrevistasdeposgrad')->name('dppe.publ.revistasdeposgraduacao'); 

  // Route::get('/projectos', 'PagesController@projectosdepesquisa')->name('projectosdepesquisa');
  // Route::get('/revistas', 'RevistasController@index')->name('revistas');
  // Route::get('/escolinhas', 'PagesController@escolinhas')->name('escolinhas');





  # Posts CRUD routes (create, read, upadte, delete)
  Route::resource('/noticias', 'NoticiasController'); // main crud
  Route::post('/noticias/ui/{id}', 'NoticiasController@ui'); // upload image
  Route::get('/noticias/di/{id}/{imageName}', 'NoticiasController@di'); // delete image

  # Eventos CRUD routes (create, read, upadte, delete)
  Route::resource('/eventos', 'EventosController'); // main crud
  Route::get('/eventos/ver/todos', 'EventosController@todos')->name('eventos.todos'); // show all eventos
  Route::get('/eventos/showmodal/{id}', 'EventosController@showmodal'); // to retrieve evento's info to show a modal
  Route::get('/eventos/di/{id}/{imageName}', 'EventosController@di'); // delete image

  # Dashboard Routes
  Route::prefix('/dashboard')->group(function () {
    // Matches The "/dashboard/" URL:
    Route::get('/', 'DashboardController@index')->name('dashboard.index');
    // Matches The "/dashboard/destaques" URL:
    Route::get('destaques', 'DashboardController@destaques')->name('dashboard.destaques');

    // To upload images (Matches The "/dashboard/destaques/ui/[id]" URL):
    Route::post('destaques/ui/{id}', 'DashboardController@ui'); // to upload an image
    // To save a destaque (Matches The "/dashboard/destaques/save/[id]" URL):
    Route::post('destaques/save/{id}', 'DashboardController@save'); // to save a destaque
    // To remove a destaque (Matches The "/dashboard/destaques/remove/[id]" URL):
    Route::post('destaques/delete/{id}', 'DashboardController@delete');

    // To show the dialog content
    Route::get('destaques/show/{id}', 'DashboardController@showmodal');
  });

// });


# Authentication Pages Default Routes
Auth::routes();


// Route::group(['prefix' => 'up_ficheiros', 'middleware' => ['web', 'auth']], function () {
//   \UniSharp\LaravelFilemanager\Lfm::routes();
// });


// Route::group(['prefix' => 'up_ficheiros', 'middleware' => ['web', 'auth']], function () {
//   // \UniSharp\LaravelFilemanager\Lfm::routes();
//   // return redirect('/filemanager/dialog.php');
 

// });

Route::get('/up_ficheiros', function () {
  return redirect('/filemanager/dialog.php');
})->middleware(['web', 'auth']);


