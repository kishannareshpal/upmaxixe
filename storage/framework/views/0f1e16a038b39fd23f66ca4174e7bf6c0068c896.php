<?php $__env->startSection('title'); ?>
    UP Maxixe –> Notícias
<?php $__env->stopSection(); ?>

<?php
  /*
    Must init this variable..
    this serves to know on which page the user to highlight
    the correspondent MENU (NAVBAR) item down bellow. in @section('menus')
  */

  $isActive = 'noticias';
?>



<?php $__env->startSection('menus'); ?>
  <?php echo $__env->make('inc.menus', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php echo $__env->make('inc.mobilemenus', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>



<?php $__env->startSection('navbar-right-content'); ?>
  <?php if(auth()->guard()->check()): ?>
    <?php echo $__env->make('inc.navbar-user', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('headcsslink'); ?>
  <style>
    /* Fade */
    .hvr-fade {
      display: inline-block;
      vertical-align: middle;
      -webkit-transform: perspective(1px) translateZ(0);
      transform: perspective(1px) translateZ(0);
      box-shadow: 0 0 1px rgba(0, 0, 0, 0);
      overflow: hidden;
      -webkit-transition-duration: 0.3s;
      transition-duration: 0.3s;
      -webkit-transition-property: color, background-color;
      transition-property: color, background-color;
    }
    .hvr-fade:hover, .hvr-fade:focus, .hvr-fade:active {
      background-color: #1589e9;
      color: white;
    }

    /* Makes a hover effect to make a clickable card  */
    .card-k {
      transition: 0.4s;
      cursor: pointer;
    }
    .card-k:hover{
      background-color: #f3f3f3
    }
  </style>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('body'); ?>


    <section class="uk-section uk-margin-remove-bottom uk-section-small">
        <div class="uk-container uk-container-small">

            <h1 class="uk-margin-remove-bottom uk-h1">
              <a href="<?php echo e(route('noticias.index')); ?>" class="uk-text-bold uk-link-reset"><img src="<?php echo e(asset('images/icons/newspaper.svg')); ?>" style="position: relative; bottom: 3.1px" width="30" height="30" uk-svg> Notícias</a>
              <?php if(auth()->guard()->check()): ?>
                <a href="<?php echo e(route('noticias.create')); ?>" class="hvr-grow-shadow uk-dark uk-button uk-button-danger uk-border-rounded uk-box-shadow-small uk-float-right" style="position: relative; top: 6.4px;" uk-icon='plus' uk-tooltip="title: Publicar Nova Notícia; delay: 300"></a>
              <?php endif; ?>
            </h1>
            <p style="position: relative; bottom: 10px" class="uk-margin-remove-top uk-text-bold"><small>da</small> UP Maxixe</p>
            <!--Message-->
              <?php echo $__env->make('inc.messages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <!--/Message-->

            

        </div>
    </section>

    <section class="uk-section-muted uk-section-small">
      <div class="uk-container uk-container-small">
        <div class="uk-grid" uk-grid>
          <div class="uk-width-1-1@m">
            <?php if($posts->firstItem() == $posts->currentPage()): ?>
              <h4 class="uk-heading-line uk-text-bold uk-text-danger"><span>Últimas Notícias<small></small></span></h4>
            <?php else: ?>
              <h4 class="uk-heading-line uk-text-bold uk-text-success"><i><span>Página <?php echo e($posts->currentPage()); ?> / <?php echo e($posts->lastPage()); ?></i></span></h4>
            <?php endif; ?>

            <?php if(count($posts) > 0): ?>
              <div uk-grid="masonry: true" class="uk-child-width-1-2@m uk-grid-small uk-margin-bottom">
                <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php
                    # localize month name to pt.. not the best way, i know... but whatever!
                      // retrieve raw post month
                      $monthRaw = $post->created_at->month;
                      // finally translate that shit
                      $ptmonth = $months[$monthRaw];
                  ?>

                    <div class="uk-margin-small-bottom">
                      <div class="card-k uk-border-rounded uk-text-break uk-card uk-card-default uk-card-body uk-transition-toggle">
                        <h3 class="uk-margin-remove uk-text-bold"><a title="Continuar a ler: '<?php echo e($post->title); ?>'" class="uk-link-heading" href="<?php echo e(route('noticias.index')); ?>/<?php echo e($post->id); ?>"><?php echo e($post->title); ?></a></h3>
                        <?php if(isset($post->subtitle)): ?>
                          <h5 class="uk-margin-remove uk-margin-small-bottom"><?php echo e($post->subtitle); ?></h5>
                        <?php endif; ?>
    
                        <span class="uk-comment-meta uk-margin-remove-top">
                          <small><?php echo e($post->created_at->day); ?> de <?php echo e($ptmonth); ?>, <?php echo e($post->created_at->year); ?> – <?php echo e($post->created_at->format('H:i A')); ?></small>
                          <?php if($post->created_at->diffInDays(Carbon\Carbon::now()) < 10): ?>
                            <span style="position: relative; bottom: 1px" class="uk-label uk-label-danger">
                              <small>
                                <?php switch($post->created_at->diffInDays(Carbon\Carbon::now())):
                                  case (0): ?>
                                    A algumas horas
                                    <?php break; ?>
                                  <?php case (1): ?>
                                    Ontem
                                    <?php break; ?>
                                  <?php default: ?>
                                    <?php echo e($post->created_at->diffInDays(Carbon\Carbon::now())); ?> dias atras
                                <?php endswitch; ?>
                              </small>
                            </span>
                          <?php endif; ?>
                          <p class="uk-margin-remove uk-transition-fade uk-transition-slide-right-small uk-position-bottom-right" style="top: 5px; right: 10px">
                            
                            <a class="uk-link-muted uk-text-bold" style="color: #F90F47 !important; opacity: .4" href="#">Ler a notícia <span uk-icon="arrow-right"></span></a>
                          </p>
                        </span>

                        <a href="<?php echo e(route('noticias.index')); ?>/<?php echo e($post->id); ?>"><span style="position: absolute; width: 100%; height: 100%; top: 0; left: 0; z-index: 1;" class="linkSpanner"></span></a>
    
                        <?php
                          # Extract imageurl from the string to an array of urls = $image_urls.
                          preg_match_all('#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#', $post->image_urls, $image_urls);
                          $image_urls = $image_urls[0]; // TRICK: i did this because it for some reason returns two arrays: [the first one, containing the actual image urls that i need] and [the second one, literaly an array of "g"'s]
                        ?>
    
                        <?php if(count($image_urls) > 0): ?>
                          <figure>
                            <div class="uk-height-small uk-background-cover uk-blend-darken" data-src="<?php echo e($image_urls[0] /* the first image */); ?>" uk-img>
                            </div>
                          </figure>
                        <?php endif; ?>
    
                        <div style="overflow-y: scroll">
                          <p><?php echo str_limit($post->body, $limit = 100, $end = ' [...] '); ?></p>
                        </div>
                      </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </div>
            <?php else: ?>
              <div class="uk-margin">
                <div class="uk-card uk-card-small uk-card-default uk-card-body">
                  <h6 class="uk-h7 uk-text-muted">
                    Nenhuma notícia postada até então.
                  </h6>
                </div>
              </div>
            <?php endif; ?>

            <?php echo e($posts->links()); ?>


          </div>
        </div>
      </div>
      <!--/CONTENT-->
    </section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>