<?php
  /*
    Must init this variable..
    this serves to know on which page the user to highlight
    the correspondent MENU (NAVBAR) item down bellow. in @section('menus')
  */

  $isActive = 'login';
?>

<?php $__env->startSection('title'); ?>
	UP Maxixe -> Autenticar
<?php $__env->stopSection(); ?>

<?php $__env->startSection('headcsslink'); ?>
	<style>
		body {
			background-color: #fcfbfb;
		}
	</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body'); ?>
<div class="uk-container uk-margin-large uk-flex uk-flex-center">
	<div class="uk-card uk-box-shadow-small uk-card-default uk-width-1-2@s">
		<div class="uk-card-header">
			<h3 class="uk-card-title uk-margin-remove uk-text-bold">Autenticar</h3>
			<small class="uk-text-muted">Acesso restrito à <strong>Administração da UP Maxixe</strong> –– <a href="<?php echo e(route('home')); ?>">Voltar</a></small>

		</div>
		<form class="uk-form-stacked" method="POST" action="<?php echo e(route('login')); ?>" novalidate>
			<?php echo e(csrf_field()); ?>

			<div class="uk-card-body">
				<div class="uk-margin">
					<small class="uk-form-label">
						Email
					</small>
					<div class="uk-width-1-1 uk-inline">
						<span class="uk-form-icon" uk-icon="icon: user">
						</span>
						<input id="email" type="email" class="uk-input"
						 name="email" value="<?php echo e(old('email')); ?>" required autofocus>
					</div>
					<?php if($errors->has('email')): ?>
					<small class="uk-text-warning"><?php echo e($errors->first('email')); ?></small>
					<?php endif; ?>
				</div>
				<div class="uk-margin">
					<label class="uk-form-label">
						Password
					</label>
					<div class="uk-width-1-1 uk-inline">
						<span class="uk-form-icon"
						 uk-icon="icon: lock">
						</span>
						<input id="password" type="password" class="uk-input"
						 name="password" required>
					</div>
					<?php if($errors->has('password')): ?>
					<small class="uk-text-warning"><?php echo e($errors->first('password')); ?></small>
					<?php endif; ?>
				</div>

				<div class="uk-inline uk-float-left">
					<label>
						<input type="checkbox" name="remember" <?php echo e(old( 'remember') ? 'checked' : ''); ?> class="uk-checkbox"> Manter-me autenticado
						<br>
						<small class="uk-text-muted">(Evite selecionar esta opção se estiver em um dispositivo público para melhor segurança)</small>

					</label>
				</div>

			</div>
			<div class="uk-card-footer uk-clearfix">
				<button type="submit" class="uk-button uk-button-default uk-box-shadow-small mdl-js-button uk-float-right">
					<strong>Continuar <i uk-icon='chevron-right'></i></strong>
				</button>
			</div>
		</form>
	</div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>