<?php
  /*
    Must init this variable..
    this serves to know on which page the user to highlight
    the correspondent MENU (NAVBAR) item down bellow. in @section('menus')
  */

  $isActive = 'noticias';
?>

<?php $__env->startSection('menus'); ?>
  <?php echo $__env->make('inc.menus', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php echo $__env->make('inc.mobilemenus', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>



<?php $__env->startSection('navbar-right-content'); ?>
  <?php if(auth()->guard()->check()): ?>
    <?php echo $__env->make('inc.navbar-user', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php endif; ?>
<?php $__env->stopSection(); ?>



<?php $__env->startSection('body'); ?>


    <!--Message-->
      <?php echo $__env->make('inc.messages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!--/Message-->

    <!--POSTS-->
    <section class="uk-section uk-section-muted">
        <div class="uk-container uk-container-small">
            <h2 class="uk-text-bold uk-h1 uk-margin-remove-adjacent uk-margin-remove-top">Notícias <a href="<?php echo e(route('noticias.create')); ?>" class="hvr-grow-shadow uk-dark uk-button uk-button-default uk-box-shadow-small uk-float-right" style="position: relative; top: 6.4px; border-color: orange" uk-icon='plus' uk-tooltip="title: Adicionar Novo; delay: 300"></a></h2>
        </div>

        <!-- posts -->
        <div class="uk-container uk-container-small uk-margin-medium">
            <div class="uk-grid uk-grid-medium uk-child-width-1-1 uk-child-width-1-3@s" uk-grid>
              <?php if(count($posts) > 0): ?>
                <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                  <?php
                    # localize month name to pt.. not the best way, i know... but whatever!
                      // retrieve raw post month
                      $monthRaw = $post->updated_at->month;
                      // finally translate that shit
                      $ptmonth = $months[$monthRaw];
                    ?>

                  <div>
                    <div class="uk-card uk-card-default uk-card-hover">
                        <?php // check for the first image
                          $first_image = $post->firstimage;
                        ?>

                        <?php if(gettype($first_image) == 'object'): ?>
                          <div style="background-image: url(<?php echo e(asset('/storage/noticias_image')); ?>/<?php echo e($post->id); ?>/<?php echo e($first_image->url); ?>); background-size: cover; background-position: center center;" class="uk-card-media-top">
                              <img uk-img style='height: 150px' alt="">
                          </div>
                        <?php endif; ?>

                        <?php if(auth()->guard()->check()): ?> 
                          <div class="uk-inline uk-float-right uk-padding-small">
                              <span style="cursor: pointer" class="uk-icon-button" uk-icon="more-vertical"></span>
                              <div class="uk-background-muted" uk-dropdown="mode: click">
                                <small class="uk-text-bold uk-text-muted">Mais Opções: </small><br>
                                <div class="uk-grid uk-grid-small" uk-grid>
                                  <div>
                                    <a href="<?php echo e(route('noticias.index')); ?>/<?php echo e($post->id); ?>/edit" class="uk-icon-button uk-text-bold" uk-icon="pencil"></a>
                                  </div>

                                  <div>
                                    <?php echo Form::open(['action' => ['NoticiasController@destroy', $post->id], 'method'=>'POST']); ?>

                                      <?php echo e(Form::hidden('_method', 'DELETE')); ?>

                                      <button type="submit" class="uk-icon-button uk-text-danger" uk-icon="trash"></button>
                                      
                                    <?php echo Form::close(); ?>

                                  </div>
                                </div>

                              </div>
                          </div>
                        <?php endif; ?>

                        <div class="uk-card-body uk-transition-toggle" >
                            <h3 class="uk-card-title uk-margin-remove-bottom uk-margin-remove-top"><?php echo e($post->title); ?></h3>
                            <p class="uk-text-meta uk-margin-remove-top"><small><?php echo e($post->created_at->format('H:i A')); ?></small><br><?php echo e($post->created_at->day); ?> de <?php echo e($ptmonth); ?>, <?php echo e($post->created_at->year); ?></p>
                            <?php if($post->created_at->diffInDays(Carbon\Carbon::now()) < 2): ?>
                              <span class="uk-label">Recente</span>
                            <?php endif; ?>
                            <hr>
                            <a href="<?php echo e(route('noticias.index')); ?>/<?php echo e($post->id); ?>" class="uk-button uk-button-text uk-text-muted">Ver</a>
                        </div>
                    </div>
                  </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              <?php else: ?>
                <div class="uk-margin">
                  <div class="uk-card uk-card-small uk-card-default uk-card-body">
                    <h7 class="uk-h7 uk-text-muted">
                      Nenhuma notícia postada até então.
                  </div>
                </div>
              <?php endif; ?>
            </div>
        </div>
        <!-- /events -->
    </section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>