<?php $__env->startSection('title'); ?>
    UP Maxixe –> Projectos | Pesquisa
<?php $__env->stopSection(); ?>

<?php
  /*
    Must init this variable..
    highlight @section('[mobile]menus')
  */
  $isActive = 'pesquisaeextensao';

  /*
    Must init this variable.
    Previous and Next Page.
  */
  $previousPage = "Centros e Núcleos";
  $nextPage = "Revistas e Artigos";
?>

<?php $__env->startSection('menus'); ?>
  <?php echo $__env->make('inc.menus', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php echo $__env->make('inc.mobilemenus', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>



<?php $__env->startSection('navbar-right-content'); ?>
  <?php if(auth()->guard()->check()): ?>
    <?php echo $__env->make('inc.navbar-user', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php endif; ?>
<?php $__env->stopSection(); ?>



<?php $__env->startSection('body'); ?>


    <!--Message-->
    <?php echo $__env->make('inc.messages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!--/Message-->


    <section class="uk-section uk-section-small">
        <div class="uk-container uk-container-small">
            <h2 class="uk-margin-remove-bottom uk-text-bold uk-h1 uk-margin-remove-adjacent uk-margin-remove-top">Projectos de Pesquisa</h2>
            
            
        </div>
    </section>


    
    <?php echo $__env->make('inc.prev_next', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>