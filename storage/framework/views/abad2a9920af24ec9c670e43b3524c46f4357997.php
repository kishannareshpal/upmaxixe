<?php $__env->startSection('title'); ?>
    UP Maxixe –> O Director
<?php $__env->stopSection(); ?>

<?php
  /*
    Must init this variable..
    highlight @section('[mobile]menus')
  */
  $isActive = 'direccao';
  $isActiveSub = 'direccao.odirector';


  /*
    Must init this variable.
    Previous and Next Page.
  */
  // $previousPage = ""; -> NO PREVIOUS PAGE!!
  $nextPage = "Gabinete do Director";
?>

<?php $__env->startSection('menus'); ?>
  <?php echo $__env->make('inc.menus', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php echo $__env->make('inc.mobilemenus', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>



<?php $__env->startSection('navbar-right-content'); ?>
  <?php if(auth()->guard()->check()): ?>
    <?php echo $__env->make('inc.navbar-user', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php endif; ?>
<?php $__env->stopSection(); ?>



<?php $__env->startSection('body'); ?>


    <!--Message-->
    <?php echo $__env->make('inc.messages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!--/Message-->

    <!--POSTS-->
    <section class="uk-section uk-section-muted">
        <div class="uk-container uk-container-small">
          <h5 class="uk-margin-remove-bottom uk-text-bold uk-h5 uk-margin-remove-adjacent uk-text-muted uk-margin-remove-top">Direcção</h5>
          <h1 class="uk-text-bold uk-h1 uk-margin-remove-top">O Director</h1>
        </div>



        <!-- mensagem -->
        <div class="uk-container uk-container-small uk-margin-medium">
          <div class="uk-card uk-border-rounded uk-box-shadow-medium uk-background-default uk-dark uk-padding uk-grid-collapse uk-child-width-1-4@s uk-margin" uk-grid>
            <div class="uk-text-center">
                <img uk-img class="uk-border-circle" width="200" data-src="<?php echo e(asset('images/pages/direccao/odirector/director.jpg')); ?>" alt="">
            </div>
            <div class="uk-width-expand">
              <div class="uk-card-body">
                  <h2 class="uk-muted uk-margin-remove"><a href="<?php echo e(route('docentes')); ?>/6" class="uk-link-reset uk-text-bold" style="color: black">PhD. P. Ezio Lorenzo Bono</a></h2>
                  <p class="uk-margin-remove uk-text-bold">Director da Universidade Pedagógica da Maxixe - UniSaF</p>
                  <small>– Professor de Filosofia</small>
                  <br>
                  <img draggable="false" style="pointer-events: none; position: relative; top: 20px; padding: 2px" src="<?php echo e(asset('images/pages/direccao/odirector/signature.png')); ?>" alt="">
              </div>
            </div>
          </div>


          <div class="uk-grid" uk-grid>
            
            <div class="uk-width-expand">
              <div class="uk-text-justify uk-card uk-border-rounded uk-box-shadow-medium uk-background-default uk-dark">
                  <p class="uk-padding-small" style="background-color: #fa6d6c; border-top-left-radius: 5px; border-top-right-radius: 5px;"><img src="<?php echo e(asset('images/icons/conversation.svg')); ?>" uk-svg width="24" height="24"/>&nbsp; <span style="color: white">MENSAGEM DO DIRECTOR</span></p>
                  <div class="uk-card-body uk-padding-medium uk-padding-remove-top">
                    <h2 class="uk-margin-remove-bottom">Bem Vindos</h2><br>
                    <p class="uk-margin-remove-top uk-margin-remove-bottom uk-text-bold uk-text-article">
                      <i>Carríssimos</i>,
                    </p>
                    <p class="uk-margin-remove-top uk-text-article uk-dropcap">
                      O grande drama da vida é a ignorância, pois, infelizmente, sempre haverá mais ignorantes que sabedores, enquanto a ignorância for gratuita e a ciência dispendiosa (Marques Maçarica).
                      Entretanto, uma sociedade nutrida de ignorância não tem esperança. Ora, o futuro da vida habita numa sociedade culta, sensata e humilde guiada pela sabedoria na busca de soluções para os problemas.
                      <br>
                      Exímia nestes preceitos, a nossa instituição pauta por uma educação integral como forma de enaltecer não só, a condição humana do individuo, mas também, a sua dimensão socio-cultural, dando-o lentes para olhar a vida com mais lucidez e esperança. Como pode imaginar, a Delegação da UP Maxixe distingue-se pelo seu plano curricular muito articulado e flexível, exactamente para permitir mais liberdade ao estudante de enriquecer a sua formação com conhecimentos de outras áreas do seu interesse.
                      <div style="text-indent: 30px">De forma exclusiva, a nossa instituição oferece uma planilha de disciplinas de teor filosófico e teológico obrigatórias para todos os estudantes em todos os cursos, para evitar produzir cientístas “cegos” em valores éticos e morais.</div>
                      <div style="text-indent: 30px">A inovação e criatividade na maneira de pensar os nossos cursos é nosso lema. Por esta razão, esta universidade tem, até finais de 2015, todo o seu staff docente com o grau mínimo de mestrado, exactamente, para responder de forma pontual e eloquente às exigências de formação hoje. Os nossos cursos de licenciatura e mestrado graduam estudantes que são uma prova viva de sucesso em várias universidades internacionais. Estes standards prestegiam-nos bastante como também dasafiam-nos a investir e inovar sempre.</div> <br>
                      <div style="text-indent: 30px">Portanto, estudar na UP-Maxixe não é apenas fazer uma experiência académica, mas submergir-se a uma prova única e singular de intercultura, dentro da “Terra de boa gente” que, com o seu grande “parque” cultural, turistico e artistico invejável fascina todos os seus visitantes.</div>
                    </p>
                  </div>
                </div>
            </div>

            
            <div class="uk-width-1-3@m">
              <div class="uk-card uk-border-rounded uk-box-shadow-medium uk-background-default uk-dark">
                <p class="uk-padding-small" style="border-top-left-radius: 5px; border-top-right-radius: 5px;"><img src="<?php echo e(asset('images/icons/contact.svg')); ?>" uk-svg width="24" height="24"/>&nbsp; CONTACTOS</p>
                <div class="uk-card-body uk-padding-small uk-padding-remove-top">
                  <p class="uk-margin-remove-bottom">Email: <a href="mailto:ebono@up.ac.mz">ebono@up.ac.mz</a></p>
                  <hr class="uk-margin-small" />
                  <p class="uk-margin-remove-top uk-margin-remove-bottom">Celular: <a href="telf:00258828384160">+258 82-8384-160</a></p>
                  <hr class="uk-margin-small" />
                  <p class="uk-margin-remove-top"><a href="https://www.calendly.com/eziobono"><img style="color: black !important;" src="<?php echo e(asset('images/icons/deadline.svg')); ?>" uk-svg width="14" height="14" alt="Marcar Audiência Logo">  Marcar Audiência</a></p>

                </hr>
              </div>
            </div>
          </div>
        </div>
        <!-- /events -->
    </section>


    
    <?php echo $__env->make('inc.prev_next', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>