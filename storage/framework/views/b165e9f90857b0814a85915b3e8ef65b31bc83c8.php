<!--NAVBAR / MENUS-->


<?php if(empty($isActiveSub)): ?>
    <?php
        # Dont make isActiveSub important
        $isActiveSub = "";
    ?>
<?php endif; ?>

<?php if(empty($isActiveSubSub)): ?>
    <?php
        # Dont make isActiveSubSub important
        $isActiveSubSub = "";
    ?>
<?php endif; ?>

<style>
    li a {
      font-size: 14px !important;
      text-transform: none !important;
    }

    .uk-navbar-nav > li.uk-active > a {
      color: rgb(21, 136, 233) !important;
    }

    .uk-navbar-dropdown-nav > li.uk-active > a {
      color: rgb(21, 136, 233) !important;
      font-weight: bold;
    }
</style>


<section uk-parallax="bgy: -50" class="uk-section uk-section-small uk-visible@m uk-background-cover" style="background-image: url(<?php echo e(asset('images/cover.jpg')); ?>);">
    
    <div class="uk-container uk-container-large">
        <div class="uk-grid uk-grid-large" uk-grid>
            <div class="uk-width-expand">
                <div class="uk-visible@m" style="border-radius: 5px;" uk-navbar="mode: click; dropbar-mode: push;">
                    <div class="uk-border-rounded uk-box-shadow-small uk-align-center uk-navbar-center uk-background-default">
                        <ul class="uk-navbar-nav uk-flex-wrap">
                            
                            <li class="<?php if($isActive == "inicio"): ?> uk-active <?php endif; ?> uk-button uk-button-text"><a href="<?php echo e(route('home')); ?>">Início</a></li>

                            <li class="<?php if($isActive == "universidade"): ?> uk-active <?php endif; ?> uk-button uk-button-text">
                                <a href="#">Universidade <i uk-icon="icon: chevron-down"> </i></a>
                                <div class="uk-navbar-dropdown">
                                    <ul style="text-align: left" class="uk-nav uk-navbar-dropdown-nav">
                                        <li class="<?php if($isActiveSub == "universidade.quemsomos"): ?> uk-active <?php endif; ?>"><a href="<?php echo e(route('quemsomos')); ?>">Quem Somos</a></li>
                                        <li class="uk-nav-divider"></li>
                                        <li class="<?php if($isActiveSub == "universidade.missaoevalores"): ?> uk-active <?php endif; ?>"><a href="<?php echo e(route('missaoevalores')); ?>">Missão, Visão e Valores</a></li>
                                        <li class="uk-nav-divider"></li>
                                        <li class="<?php if($isActiveSub == "universidade.organigrama"): ?> uk-active <?php endif; ?>"><a href="<?php echo e(route('organigrama')); ?>">Organigrama</a></li>
                                        <li class="uk-nav-divider"></li>
                                        <li class="<?php if($isActiveSub == "universidade.orgaos"): ?> uk-active <?php endif; ?>"><a href="<?php echo e(route('orgaossuperiores')); ?>">Órgãos Superiores</a></li>
                                        <li class="uk-nav-divider"></li>
                                        <li class="<?php if($isActiveSub == "universidade.conselho"): ?> uk-active <?php endif; ?>"><a href="<?php echo e(route('conselhodirectivo')); ?>">Conselho Directivo</a></li>
                                        <li class="uk-nav-divider"></li>
                                        <li class="<?php if($isActiveSub == "universidade.docentes"): ?> uk-active <?php endif; ?>"><a href="<?php echo e(route('docentes')); ?>">Docentes</a></li>
                                    </ul>
                                </div>
                            </li>

                            <li class="<?php if($isActive == "direccao"): ?> uk-active <?php endif; ?> uk-button uk-button-text">
                                <a href="#">Direcção <i uk-icon="icon: chevron-down"> </i></a>
                                <div class="uk-navbar-dropdown">
                                    <ul style="text-align: left" class="uk-nav uk-navbar-dropdown-nav">
                                        <li class="<?php if($isActiveSub == "direccao.odirector"): ?> uk-active <?php endif; ?>"><a href="<?php echo e(route('odirector')); ?>">O Director</a></li>
                                        <li class="uk-nav-divider"></li>
                                        <li class="<?php if($isActiveSub == "direccao.gabinete"): ?> uk-active <?php endif; ?>"><a href="<?php echo e(route('gabinetedodirector')); ?>">Gabinete do Director</a></li>
                                        <li class="uk-nav-divider"></li>
                                        <li class="<?php if($isActiveSub == "direccao.secretaria"): ?> uk-active <?php endif; ?>"><a href="<?php echo e(route('secretariageral')); ?>">Secretaria</a></li>
                                        <li class="uk-nav-divider"></li>
                                        <li class="<?php if($isActiveSub == "direccao.registo"): ?> uk-active <?php endif; ?>"><a href="<?php echo e(route('registoacademico')); ?>">Registo Académico</a></li>
                                    </ul>
                                </div>
                            </li>

                            

                            <li class="<?php if($isActive == "cei"): ?> uk-active <?php endif; ?> uk-button uk-button-text" >
                                <a href="#">Cooperação e<br>Internacionalização <i uk-icon="icon: chevron-down"> </i></a>
                                <div class="uk-navbar-dropdown" style="width: 250px">
                                    <ul style="text-align: left" class="uk-nav uk-navbar-dropdown-nav">
                                        <li class="<?php if($isActiveSub == "cei.apresentacao"): ?> uk-active <?php endif; ?>"><a href="<?php echo e(route('cei.apresentacao')); ?>">Apresentação</a></li>                                
                                        <li class="uk-nav-divider"></li>
                                        <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
                                            <li class="<?php if($isActiveSub == "cei.coop"): ?> uk-active <?php endif; ?> uk-parent">
                                                <a href="#">Cooperação</a>
                                                <ul class="uk-nav-sub">
                                                    <li class="<?php if($isActiveSubSub == "cei.coop.nacional"): ?> uk-active <?php endif; ?>"><a href="<?php echo e(route('cei.cooperacao.nacional')); ?>">– Nacional</a></li>
                                                    <li class="<?php if($isActiveSubSub == "cei.coop.internacional"): ?> uk-active <?php endif; ?>"><a href="<?php echo e(route('cei.cooperacao.internacional')); ?>">– Internacional</a></li>                                                    
                                                </ul>
                                            </li>
                                        </ul>
                                        <li class="uk-nav-divider"></li>
                                        <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
                                            <li class="<?php if($isActiveSub == "cei.int"): ?> uk-active <?php endif; ?> uk-parent">
                                                <a href="#">Internacionalização</a>
                                                <ul class="uk-nav-sub">
                                                    <li class="<?php if($isActiveSubSub == "cei.int.visiting"): ?> uk-active <?php endif; ?>"><a href="<?php echo e(route('cei.internacionalizacao.visiting')); ?>">– Visiting Professors and Researchers</a></li>
                                                    <li class="<?php if($isActiveSubSub == "cei.int.profconv"): ?> uk-active <?php endif; ?>"><a href="<?php echo e(route('cei.internacionalizacao.professoresconvidados')); ?>">– Professores Convidados</a></li>
                                                    <li class="<?php if($isActiveSubSub == "cei.int.editais"): ?> uk-active <?php endif; ?>"><a href="<?php echo e(route('editais.index')); ?>">– Editais</a></li>                                                    
                                                </ul>
                                            </li>
                                        </ul>
                                        <li class="uk-nav-divider"></li>
                                        <li class="<?php if($isActiveSub == "cei.informacao"): ?> uk-active <?php endif; ?>"><a href="<?php echo e(route('cei.informacao')); ?>">Informação</a></li>
                                        <li class="uk-nav-divider"></li>
                                        <li class="<?php if($isActiveSub == "cei.newsletter"): ?> uk-active <?php endif; ?>"><a href="<?php echo e(route('cei.newsletter')); ?>">Newsletter</a></li>
                                    </ul>
                                </div>
                            </li>
                            

                            <li class="<?php if($isActive == "cursos"): ?> uk-active <?php endif; ?> uk-button uk-button-text">
                              <a href="#">Cursos <i uk-icon="icon: chevron-down"> </i></a>
                              <div class="uk-navbar-dropdown">
                                  <ul style="text-align: left" class="uk-nav uk-navbar-dropdown-nav">
                                      <li><a href="#">Cursos de Licenciatura</a></li>
                                      <li class="uk-nav-divider"></li>
                                      <li><a href="#">Cursos de Mestrado</a></li>
                                  </ul>
                              </div>
                            </li>

                            

                            <li class="<?php if($isActive == "departamentos"): ?> uk-active <?php endif; ?> uk-button uk-button-text">
                                <a href="#">Departamentos <i uk-icon="icon: chevron-down"> </i></a>
                                <div class="uk-navbar-dropdown">
                                    <ul style="text-align: left" class="uk-nav uk-navbar-dropdown-nav">
                                        <li><a href="#">Ensino a Distância</a></li>
                                        <li class="uk-nav-divider"></li>
                                        <li><a href="#">Recursos Humanos</a></li>
                                        <li class="uk-nav-divider"></li>
                                        <li><a href="#">Adm. e Finanças</a></li>
                                        <li class="uk-nav-divider"></li>
                                        <li><a href="#">Património</a></li>
                                        <li class="uk-nav-divider"></li>
                                        <li><a href="#">UGEA</a></li>
                                        <li class="uk-nav-divider"></li>
                                        <li><a href="#">Serviços Sociais</a></li>
                                    </ul>
                                </div>
                            </li>


                            <li class="<?php if($isActive == "dppe"): ?> uk-active <?php endif; ?> uk-button uk-button-text">
                                <a href="#">Pós-Graduação,<br>Pesquisa e Extensão <i uk-icon="icon: chevron-down"> </i></a>
                                <div class="uk-navbar-dropdown uk-navbar-dropdown-width-2">
                                    <div class="uk-navbar-dropdown-grid uk-child-width-1-2" uk-grid>
                                        <div>
                                            <ul style="text-align: left" class="uk-nav uk-navbar-dropdown-nav">
                                                <li class="<?php if($isActiveSub == "dppe.apresentacao"): ?> uk-active <?php endif; ?>"><a href="<?php echo e(route('dppe.apresentacao')); ?>">Apresentação</a></li>
                                                <li class="uk-nav-divider"></li>
                                                <li class="<?php if($isActiveSub == "dppe.organigrama"): ?> uk-active <?php endif; ?>"><a href="<?php echo e(route('dppe.organigrama')); ?>">Organigrama</a></li>
                                                <li class="uk-nav-divider"></li>  
                                                <li class="<?php if($isActiveSub == "dppe.planoestrategico"): ?> uk-active <?php endif; ?>"><a href="<?php echo e(route('dppe.planoestrategico')); ?>">Plano Estratégico</a></li>
                                                <li class="uk-nav-divider"></li>
                                                <li class="<?php if($isActiveSub == "dppe.regulamento"): ?> uk-active <?php endif; ?>"><a href="<?php echo e(route('dppe.regulamento')); ?>">Regulamento</a></li>
                                                <li class="uk-nav-divider"></li>
                                                <li class="<?php if($isActiveSub == "dppe.boletimdodppe"): ?> uk-active <?php endif; ?>"><a href="<?php echo e(route('dppe.boletimdodppe')); ?>">Boletim da DPPE</a></li>
                                                <li class="uk-nav-divider"></li>
                                                <li class="<?php if($isActiveSub == "dppe.centrosenucleos"): ?> uk-active <?php endif; ?>"><a href="<?php echo e(route('centrosenucleos.index')); ?>">Centros e Núcleos</a></li>
                                                <li class="uk-nav-divider"></li>
                                            </ul>
                                        </div>

                                        <div>
                                            <ul style="text-align: left" class="uk-nav uk-navbar-dropdown-nav">
                                                <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
                                                    <li class="<?php if($isActiveSub == "dppe.dpp"): ?> uk-active <?php endif; ?> uk-parent">
                                                        <a href="#">Dep. de Pesquisa e Publicação</a>
                                                        <ul class="uk-nav-sub">
                                                            <li class="<?php if($isActiveSubSub == "dppe.dpp.apresentacao"): ?> uk-active <?php endif; ?>"><a href="<?php echo e(route('dppe.dpp.apresentacao')); ?>">– Apresentação</a></li>
                                                            <li class="<?php if($isActiveSubSub == "dppe.dpp.projectos"): ?> uk-active <?php endif; ?>"><a href="<?php echo e(route('dppe.dpp.projectos')); ?>">– Projectos</a></li>
                                                            <li class="<?php if($isActiveSubSub == "dppe.dpp.linhasdepesquisa"): ?> uk-active <?php endif; ?>"><a href="<?php echo e(route('dppe.dpp.linhasdepesquisa')); ?>">– Linhas de Pesquisa</a></li>                                                    
                                                        </ul>
                                                    </li>
                                                </ul>
                                                <li class="uk-nav-divider"></li>
                                                <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
                                                    <li class="<?php if($isActiveSub == "dppe.dpp"): ?> uk-active <?php endif; ?> uk-parent">
                                                        <a href="#">Dep. de Extensão e Inovação</a>
                                                        <ul class="uk-nav-sub">
                                                            <li class="<?php if($isActiveSubSub == "dppe.dpi.apresentacao"): ?> uk-active <?php endif; ?>"><a href="<?php echo e(route('dppe.dpi.apresentacao')); ?>">– Apresentação</a></li>
                                                            <li class="<?php if($isActiveSubSub == "dppe.dpi.projectos"): ?> uk-active <?php endif; ?>"><a href="<?php echo e(route('dppe.dpi.projectos')); ?>">– Projectos</a></li>
                                                            <li class="<?php if($isActiveSubSub == "dppe.dpi.escolinhas"): ?> uk-active <?php endif; ?>"><a href="<?php echo e(route('dppe.dpi.escolinhas')); ?>">– Escolinhas</a></li>                                                    
                                                        </ul>
                                                    </li>
                                                </ul>
                                                <li class="uk-nav-divider"></li>
                                                <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
                                                    <li class="<?php if($isActiveSub == "dppe.dpp"): ?> uk-active <?php endif; ?> uk-parent">
                                                        <a href="#">Dep. de Formação e Pós-Graduação</a>
                                                        <ul class="uk-nav-sub">
                                                            <li class="<?php if($isActiveSubSub == "dppe.dfp.apresentacao"): ?> uk-active <?php endif; ?>"><a href="<?php echo e(route('dppe.dfp.apresentacao')); ?>">– Apresentação</a></li>
                                                            <li class="<?php if($isActiveSubSub == "dppe.dfp.efectivodedocente"): ?> uk-active <?php endif; ?>"><a href="<?php echo e(route('dppe.dfp.efectivodedocente')); ?>">– Efectivos de Docentes</a></li>
                                                            <li class="<?php if($isActiveSubSub == "dppe.dfp.docentesporformacao"): ?> uk-active <?php endif; ?>"><a href="<?php echo e(route('dppe.dfp.docentesporformacao')); ?>">– Docentes por Formação</a></li>                                                    
                                                        </ul>
                                                    </li>
                                                </ul>
                                                <li class="uk-nav-divider"></li>
                                                <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
                                                    <li class="<?php if($isActiveSub == "dppe.dpp"): ?> uk-active <?php endif; ?> uk-parent">
                                                        <a href="#">Publicações</a>
                                                        <ul class="uk-nav-sub">
                                                            <li class="<?php if($isActiveSubSub == "dppe.publ.revistaseartigos"): ?> uk-active <?php endif; ?>"><a href="<?php echo e(route('dppe.publ.revistaseartigos')); ?>">– Revistas e Artigos</a></li>
                                                            <li class="<?php if($isActiveSubSub == "dppe.publ.actasdeconferencia"): ?> uk-active <?php endif; ?>"><a href="<?php echo e(route('dppe.publ.actasdeconferencia')); ?>">– Actas de Conferência</a></li>
                                                            <li class="<?php if($isActiveSubSub == "dppe.publ.livros"): ?> uk-active <?php endif; ?>"><a href="<?php echo e(route('dppe.publ.livros')); ?>">– Livros</a></li>  
                                                            <li class="<?php if($isActiveSubSub == "dppe.publ.revistasdeposgraduacao"): ?> uk-active <?php endif; ?>"><a href="<?php echo e(route('dppe.publ.revistasdeposgraduacao')); ?>">– Revista de Pós-Graduação</a></li>                                                    
                                                        </ul>
                                                    </li>
                                                </ul>
                                                <li class="uk-nav-divider"></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </li>



                            <li class="<?php if($isActive == "serviços"): ?> uk-active <?php endif; ?> uk-button uk-button-text">
                                <a href="#">Serviços <i uk-icon="icon: chevron-down"> </i></a>
                                <div class="uk-navbar-dropdown">
                                    <ul style="text-align: left" class="uk-nav uk-navbar-dropdown-nav">
                                        <li><a href="#">Pastorial Universitária</a></li>
                                        <li class="uk-nav-divider"></li>
                                        <li><a href="#">Associação dos Estudantes</a></li>
                                        <li class="uk-nav-divider"></li>
                                        <li><a href="#">Intercâmbios - Internacionalização</a></li>
                                        <li class="uk-nav-divider"></li>
                                        <li><a href="https://mail.up.ac.mz" target="_blank"><i uk-icon="link"></i> Email Corporativo</i></a></li>
                                        <li class="uk-nav-divider"></li>
                                        <li><a href="https://cead.up.ac.mz" target="_blank"><i uk-icon="link"></i> CEAD</a></li>
                                        <li class="uk-nav-divider"></li>
                                        <li><a href="https://sigeup.up.ac.mz" target="_blank"><i uk-icon="link"></i> SIGEUP</a></li>
                                        <li class="uk-nav-divider"></li>
                                        <li><a href="<?php echo e(route('upengenheiros')); ?>" target="_blank"><i uk-icon="link"></i> UP Engenheiros</a></li>
                                        
                                        
                                    </ul>
                                </div>
                            </li>
                            
                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/NAVBAR - MENUS-->
