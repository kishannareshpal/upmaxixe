<?php $__env->startSection('title'); ?>
    UP Maxixe –> Eventos -> ...
<?php $__env->stopSection(); ?>

<?php
  /*
    Must init this variable..
    this serves to know on which page the user to highlight
    the correspondent MENU (NAVBAR) item down bellow. in @section('menus')
  */

  $isActive = 'eventos';
?>

<?php $__env->startSection('menus'); ?>
  <?php echo $__env->make('inc.menus', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php echo $__env->make('inc.mobilemenus', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('navbar-right-content'); ?>
  <?php if(auth()->guard()->check()): ?>
    <?php echo $__env->make('inc.navbar-user', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php endif; ?>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('body'); ?>


    <section class="uk-section uk-section-small uk-section-muted uk-margin-remove">
      <div class="uk-container uk-container-small uk-margin-medium">
        <div class="uk-float-left uk-text-right">
          <a href="<?php echo e(route('eventos.index')); ?>" class="uk-button uk-button-default uk-float-right uk-button-muted uk-border-rounded"> <i uk-icon="chevron-left" style="position: relative; bottom: 1px"></i> Voltar àos Eventos</a>
        </div>
      </div>

      <div class="uk-container uk-container-small">
        <!--Message-->
        <?php echo $__env->make('inc.messages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!--/Message-->

        <div class="uk-container uk-container-small uk-margin-small-bottom">
          <h2 class="uk-text-bold uk-margin-remove-bottom uk-h2"><?php echo e($evento['name']); ?></h2>
          <div class="uk-divider-small"></div>
        </div>

        
        <div class="uk-grid-small" uk-grid>
          <div class="uk-width-1-3@s uk-text-center">
            <div class="uk-card uk-card-default uk-card-body uk-background-default uk-border-rounded">
              <div>
                <h3 class="uk-text-warning uk-margin-bottom-remove uk-text-bold">Data do Evento</h3>
                <span uk-icon="icon: calendar; ratio: 1"></span>
                <small class="uk-margin-remove-bottom">
                  <?php if(isset($evento['end_time'])): ?>
                    <?php if($evento['start_time']->format('d') == $evento['end_time']->format('d')): ?>
                      <?php echo e($weeks[$evento['start_time']->format('w')]); ?>

                    <?php else: ?>
                      <?php echo e($weeks[$evento['start_time']->format('w')]); ?> até <?php echo e($weeks[$evento['end_time']->format('w')]); ?>

                    <?php endif; ?>

                  <?php else: ?>
                    <?php echo e($evento['start_time']->format('d')); ?>

                  <?php endif; ?>
                </small>
                
                <h1 style="line-height: 1.4" class="uk-h1 uk-text-bold uk-margin-remove uk-padding-remove">
                  <?php if(isset($evento['end_time'])): ?>
                    <?php if($evento['start_time']->format('d') == $evento['end_time']->format('d')): ?>
                      <?php echo e($evento['start_time']->format('d')); ?>

                    <?php else: ?>
                      <?php echo e($evento['start_time']->format('d')); ?> até <?php echo e($evento['end_time']->format('d')); ?>

                    <?php endif; ?>

                  <?php else: ?>
                    <?php echo e($evento['start_time']->format('d')); ?>

                  <?php endif; ?>
                </h1>

                <small class="uk-margin-remove uk-text-bold uk-text-meta uk-text-muted">
                  <?php if(isset($evento['end_time'])): ?>
                    
                    <?php if( $evento['start_time']->format('n') == $evento['end_time']->format('n') ): ?>
                      <?php echo e($months[$evento['start_time']->format('n')]); ?>

                    <?php else: ?>
                      <?php echo e($months[$evento['start_time']->format('n')]); ?> até <?php echo e($months[$evento['end_time']->format('n')]); ?>

                    <?php endif; ?>

                    
                    <?php if( $evento['start_time']->format('Y') == $evento['end_time']->format('Y') ): ?>
                      <?php echo e($evento['start_time']->format('Y')); ?>

                    <?php else: ?>
                      <?php echo e($evento['start_time']->format('Y')); ?> até <?php echo e($evento['end_time']->format('Y')); ?>

                    <?php endif; ?>

                  <?php else: ?>
                    
                    <?php echo e($months[$evento['start_time']->format('n')]); ?>

                    <?php echo e($evento['start_time']->format('Y')); ?>

                  <?php endif; ?>


                </small>
                <p class="uk-margin-remove uk-text-bold">
                  <?php if(isset($evento['end_time'])): ?>
                    das <?php echo e($evento['start_time']->format('H:i')); ?> até <?php echo e($evento['end_time']->format('H:i')); ?> horas.
                  <?php else: ?>
                    pelas <?php echo e($evento['start_time']->format('H:i')); ?> horas.
                  <?php endif; ?>
                </p>
              </div>
            </div>
          </div>
          


          
          <div class="uk-width-expand@s">
            <div class="uk-card uk-card-default uk-card-body uk-border-rounded">
              <?php if(isset($evento['cover'])): ?>
                <div class="uk-position-relative uk-visible-toggle uk-light uk-box-shadow-medium" uk-slideshow="ratio: 7:3; animation: fade; min-height: 190;">
                    <ul uk-lightbox class="uk-slideshow-items">
                      <li>
                          <img uk-img data-src="<?php echo e($evento['cover']['source']); ?>" uk-cover>
                          <div class="uk-position-bottom-out uk-position-medium uk-text-center">
                            <a style="border: 3px solid #333;" href="<?php echo e($evento['cover']['source']); ?>" class="uk-box uk-button uk-button-small uk-button-secondary uk-border-rounded uk-box-shadow-large" uk-icon="expand">Ver imagem </a>
                          </div>
                      </li>
                    </ul>
                    
                    <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" data-uk-slidenav-previous="ratio: 1.5" data-uk-slideshow-item="previous"></a>
                    <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" data-uk-slidenav-next="ratio: 1.5" data-uk-slideshow-item="next"></a>
                </div>
              <?php endif; ?>

              <?php if(isset($evento['description'])): ?>
                <h3 class="uk-text-bold">Descrição do evento</h3>
                <div class="uk-divider-small"></div>
                <p class="uk-text-break uk-dropcap"><?php echo $evento['description']; ?></p>
                <br><hr>
              <?php endif; ?>

              <ul class="uk-list uk-list-bullet">
                <?php if(isset($evento['place']['name'])): ?>
                  <li class="uk-text-justify"><strong class="uk-text-warning">Local: </strong><?php echo e($evento['place']['name']); ?></li>                    
                <?php endif; ?>
                <?php if(isset($evento['category'])): ?>
                  <li class="uk-text-justify"><strong class="uk-text-warning">Categoria: </strong><?php echo e($evento['category']); ?></li>                    
                <?php endif; ?>
              </ul>

            </div>
          </div>
          

        </div>

      </div>
    </section>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>