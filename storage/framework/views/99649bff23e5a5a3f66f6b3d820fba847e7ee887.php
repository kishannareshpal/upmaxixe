<?php $__env->startSection('title'); ?>
    UP Maxixe –> CEI -> Apresentação
<?php $__env->stopSection(); ?>

<?php
  /*
    Must init this variable..
    highlight @section('[mobile]menus')
  */
  $isActive = 'cei';
  $isActiveSub = 'cei.apresentacao';

  /*
    Must init this variable.
    Previous and Next Page.
  */
  // $previousPage = "" -> NO PREVIOUS PAGE!!
  $nextPage = "Informação";

?>

<?php $__env->startSection('menus'); ?>
  <?php echo $__env->make('inc.menus', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php echo $__env->make('inc.mobilemenus', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>



<?php $__env->startSection('navbar-right-content'); ?>
  <?php if(auth()->guard()->check()): ?>
    <?php echo $__env->make('inc.navbar-user', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php endif; ?>
<?php $__env->stopSection(); ?>



<?php $__env->startSection('body'); ?>

  
  <section class="uk-section uk-section-small">
    <div class="uk-container uk-container-small">
      <h5 class="uk-margin-remove-bottom uk-text-bold uk-h5 uk-margin-remove-adjacent uk-text-muted uk-margin-remove-top">Cooperação e Internacionalização</h5>
      <h1 class="uk-margin-small-bottom uk-text-bold uk-h1 uk-margin-remove-adjacent uk-margin-remove-top">
        Apresentação
      </h1>
      <span class="uk-text-muted">Índices: </span>
      <a href="#funcoes"><span class="uk-badge">Funções da Repartição</span></a>
      <span class="uk-badge uk-hidden@s">Contactos</span>

    </div>
  </section>

  
  <section class="uk-section uk-section-muted uk-padding-remove-bottom">
    <div class="uk-container uk-container-small">
      <div uk-grid>
        <div class="uk-width-expand">
          <p>
            A Repartição de Cooperação faz parte do Gabinete de Cooperação, Comunicação e Imagem e foi criada com a finalidade de desenvolver parcerias com Universidades e outras instituições nacionais e estrangeiras, governamentais e não-governamentais, visando dar resposta às necessidades de desenvolvimento da UP-Maxixe nos aspectos que potenciem a prossecução da sua missão no que diz respeito ao Ensino, Pesquisa e Extensão.
          </p>
          <p>
            Para além da representar a UP Maxixe nas suas relações de parceria e de cooperação local, regional e internacional, este sector tem competência de:
          </p>
          
          <ul class="uk-list uk-list-bullet" style="padding-left: 20px">
            <li>Assessorar directamente ao Director em todos os assuntos relacionados com a cooperação entre a UP e seus parceiros.</li>
            <li>Prestar informação útil sobre a Universidade aos parceiros internos e externos.</li>
            <li>Trocar correspondência nacional e internacional entre a UP-Maxixe e seus parceiros.</li>
            <li>Gerir, monitorar e avaliar a implementação dos acordos nacionais e internacionais que vinculem a UP-Maxixe.</li>
          </ul>
        </div>
        
        <div class="uk-width-1-3@s uk-visible@s">
            <div style="border: 1px solid #2979ff" class="uk-card uk-border-rounded uk-padding-small uk-card-body">
              <h3>Contactos</h3>
              <div class="uk-divider-small"></div>
              <ul class="uk-list">
                <li class="uk-text-bold">Gabinete de Cooperação, Comunicação e Imagem</li>
                <li class="uk-link-reset"><img src="<?php echo e(asset('images/icons/gps.svg')); ?>" uk-svg width="20" height="20"> <a href="https://goo.gl/maps/jWDP6msHPoo">Av. Américo Boavida, s/n, CP. 12, Cidade da Maxixe</a></li>        
                <li class="uk-link-reset"><img src="<?php echo e(asset('images/icons/phone.svg')); ?>" uk-svg width="20" height="20"> <a href="tel:+25829330356" class="uk-button-text uk-button">+258 293-30356</a></li>
              </ul>
            </div>
        </div>
      </div>
    </div>
  </section>


  
  <section id="funcoes" class="uk-section uk-section-small uk-section-muted">
    <div class="uk-container uk-container-small">
      <h3>Funções da Repartição</h3>
      <div class="uk-divider-small"></div>
      <p>
        A Repartição de Cooperação tem as seguintes funções:
      </p>
      <ul class="uk-list" style="padding-left: 30px">
        <li><span style="margin-right: 1rem"><strong>a)</strong></span>Apoiar o Director e outros responsáveis da UP-Maxixe na definição e execução de políticas de cooperação institucional;</li>
        <li><span style="margin-right: 1rem"><strong>b)</strong></span>Elaborar estudos no âmbito da política de relações internacionais para melhoria da cooperação institucional;</li>
        <li><span style="margin-right: 1rem"><strong>c)</strong></span>Preparar Protocolos de Cooperação/ Acordos/Memorandos/Adendas;</li>
        <li><span style="margin-right: 1rem"><strong>d)</strong></span>Colaborar na promoção, dinamização e concretização dos acordos, bem como apoiar no desenvolvimento de programas específicos de cooperação com Universidades dos países de língua portuguesa, da SADC, da Francofonia, Commonwealth, entre outras;</li>
        <li><span style="margin-right: 1rem"><strong>e)</strong></span>Dinamizar novas acções de cooperação e monitorá-las;</li>
        <li><span style="margin-right: 1rem"><strong>f)</strong></span> Prestar informações às instituições parceiras em matérias de políticas de relações internacionais da UP-Maxixe;</li>
        <li><span style="margin-right: 1rem"><strong>g)</strong></span>Organizar, participar e colaborar em realizações internacionais que envolvam, directa ou indirectamente a UP-Maxixe;</li>
        <li><span style="margin-right: 1rem"><strong>h)</strong></span>Assessorar os Departamentos e outras unidades orgânicas em matérias relacionadas com a cooperação nacional e internacional;</li>
        <li><span style="margin-right: 1rem"><strong>i)</strong></span> Monitorar os projectos de cooperação;</li>
        <li><span style="margin-right: 1rem"><strong>j)</strong></span> Organizar os processos administrativos de candidatura, de mobilidade de docentes e estudantes;</li>
        <li><span style="margin-right: 1rem"><strong>k)</strong></span>Acompanhar e apoiar todas iniciativas de internacionalização da UP-Maxixe.</li>
      </ul>
      <br>

      <p>
        Para cumprir com as suas funções, a Repartição de Cooperação subdivide os termos de referência das suas actividades em duas grandes categorias. A primeira diz respeito à Cooperação Nacional, através da qual cria uma rede sólida entre a UP-Maxixe e Instituições locais, provinciais e nacionais. A segunda categoria diz respeito à Cooperação Internacional, por meio da qual procura criar uma rede que leve à internacionalização das actividades desenvolvidas na UP-Maxixe, no que diz respeito ao Ensino, à Pesquisa e à Extensão. Nestes âmbitos, a Repartição de Cooperação desenvolve as seguintes tarefas:
      </p>
      <ul class="uk-list" style="padding-left: 30px">
        <li><span style="margin-right: 1rem"><strong>a)</strong></span>Promover a mobilidade de docentes, investigadores e o desenvolvimento de projectos de formação e outras acções conjuntas de índole académico e científico;</li>
        <li><span style="margin-right: 1rem"><strong>b)</strong></span>Mobilizar acções para o estabelecimento de parcerias, regionais e internacionais;</li>
        <li><span style="margin-right: 1rem"><strong>c)</strong></span>Coordenar o processo de recolha de dados sobre a cooperação existente em Moçambique e que seja de interesse da UP-Maxixe;</li>
        <li><span style="margin-right: 1rem"><strong>d)</strong></span>Colectar informação necessária referente aos grandes desafios da Cooperação Internacional;</li>
        <li><span style="margin-right: 1rem"><strong>e)</strong></span>Informar-se e divulgar novos processos de integração regional e internacional;</li>
        <li><span style="margin-right: 1rem"><strong>f)</strong></span>Preparar propostas de colaboração regional e internacional;</li>
        <li><span style="margin-right: 1rem"><strong>g)</strong></span>Dinamizar, acompanhar e organizar todos processos relacionados com a mobilidade de estudantes e de docentes;</li>
        <li><span style="margin-right: 1rem"><strong>h)</strong></span>Em coordenação com as unidades orgânicas e serviços administrativos da instituição, incitar, promover e assegurar o acompanhamento de diversas acções de cooperação internacional, no âmbito dos acordos de cooperação existente;</li>
        <li><span style="margin-right: 1rem"><strong>i)</strong></span> Procurar parcerias viáveis para o desenvolvimento da cooperação institucional;</li>
        <li><span style="margin-right: 1rem"><strong>j)</strong></span> Recolher e divulgar os resultados da cooperação a nível institucional assim como a nível dos parceiros interessados;</li>
        <li><span style="margin-right: 1rem"><strong>k)</strong></span>Organizar e divulgar um banco de dados de todos os Acordos de Cooperação internacional e nacional;</li>
        <li><span style="margin-right: 1rem"><strong>l)</strong></span>Organizar cerimónias de assinatura de Acordos de Cooperação entre a UP-Maxixe e organismos internacionais;</li>
        <li><span style="margin-right: 1rem"><strong>m)</strong></span>Disponibilizar informação útil, relevante e actualizada através das diversas plataformas existentes e, sobretudo, na página web da UP-Maxixe;</li>
        <li><span style="margin-right: 1rem"><strong>n)</strong></span>Garantir, em cada ano, a disponibilidade da lista diplomática.</li>
      </ul>
    </div>
  </section>

  
  <section class="uk-section uk-section-muted uk-hidden@s"> 
    <div class="uk-container uk-container-small">
      <h3>Contactos da Repartição</h3>
      <div class="uk-divider-small"></div>
      <ul class="uk-list" style="padding-left: 20px">
        <li>Universidade Pedagógica da Maxixe / UniSaF</li>
        <li class="uk-text-bold">Gabinete de Cooperação, Comunicação e Imagem</li>
        <li><span uk-icon="location"></span>Av. Américo Boavida, s/n, CP. 12, Cidade da Maxixe</li>        
        <li class="uk-link-reset"><span uk-icon="receiver"></span><a href="tel:+25829330356" class="uk-button-text uk-button">+258 293-30356</a></li>
      </ul>
    </div>

    

  </section>

  
  <?php echo $__env->make('inc.prev_next', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>