<?php $__env->startSection('title'); ?>
    Dashboard –> Notícias -> Edit
<?php $__env->stopSection(); ?>

<?php
  /*
    Must init this variable..
    this serves to know on which page the user to highlight
    the correspondent MENU (NAVBAR) item.
  */

  $isActive = 'noticias';
?>

<?php $__env->startSection('navbar-left-content'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('navbar-right-content'); ?>
  <?php if(auth()->guard()->check()): ?>
    <?php echo $__env->make('inc.navbar-user', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php endif; ?>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('headcsslink'); ?>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.4.1/jquery.fancybox.min.css" />
  <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>

  <script src="../vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('jscript'); ?>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.4.1/jquery.fancybox.min.js"></script>
  
  <script>
      $(document).ready(function (){
        CKEDITOR.replace('article-ckeditor', {width: '100%'});
        var url = $('#img_urls').val();

        if (url != 0) {
          try {
            url = JSON.parse(url.toString());
          
            url.forEach(url => {
              $('#img_ul').append(image_element(url));            
            });
          } catch (error) {
            $('#img_ul').append(image_element(url));            
          }
        }

      });

      var element;
      var images_array = [];


      $('.iframe-btn').fancybox({	
        'width'		  : 900,
        'height'	  : 600,
        'type'		  : 'iframe',
        'autoScale' : false
      });


      function gettogether_thoseimages(){
        images_array = [];
        $('#img_ul').find('.imagess').each(function(element_index){
          var image_url = $('#img_ul').find('.imagess');
          images_array.push(image_url[element_index].src);
        });

        // Set the current list of images to the hidden input that will be pushed to database containing the updated list of images.
        $('#img_urls').val(JSON.stringify(images_array));
        $('#numberofimages').html(images_array.length + (images_array.length == 1 ? ' imagem selecionada' : ' imagens selecionadas'));
      }


      function image_element(url){
        var path_to_icon = "<?php echo e(asset('images/icons/delete.svg')); ?>";
        return '<li class="uk-transition-toggle removeli">'+
                  '<div>'+
                    '<img class="imagess" uk-img data-src="' + url + '" width="100">' + 
                    '<div class="uk-transition-fade uk-position-center">' + 
                      '<button class="uk-preserve uk-button uk-button-danger uk-button-small uk-box-shadow-large">Apagar</button>' + 
                    '</div>' +
                  '</div>' +
                '</li>';
      }

      function responsive_filemanager_callback(field_id){
        var url = $('#'+field_id).val();
      
        try {
          url = JSON.parse(url.toString());
          // $('#'+field_id).val(url.length + " Imagens Selecionadas");
          url.forEach(url => {
            $('#img_ul').append(image_element(url));            
          });
        } catch (error) {
          // $('#'+field_id).val("1 Imagem Selecionada");
          $('#img_ul').append(image_element(url));            
        }
        // gettogether_thoseimages();
      }

      $('#img_ul').on('DOMSubtreeModified', function(event) {
        gettogether_thoseimages();
        console.log('Image url list refreshed.');
      });
      

      $('#img_ul').on('click', '.removeli', function() {
        element = this;
        let conf = confirm("Tem a certeza que deseja remover a imagem?");
        
        if(conf) {
          element.remove();
        }
      });

  </script>
<?php $__env->stopSection(); ?>



<?php $__env->startSection('body'); ?>
  <?php echo Form::open(['action' => 'NoticiasController@store', 'method' => 'POST', 'id'=>'form']); ?>

    <section class="uk-section uk-section-small uk-visible@m">
        <div class="uk-container uk-container-small">

          <div class="uk-alert-warning" uk-alert>
            <h4><span style="position: relative; bottom: 2px" uk-icon='plus-circle'> </span>&nbsp Adicionando nova notícia.</h4>
          </div>

          <?php echo $__env->make('inc.messages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

          <div class="uk-box-shadow-small uk-padding-small uk-align-right uk-navbar-right ">
            <ul class="uk-navbar-nav">
              <?php echo e(Form::submit('Publicar', ['class'=>'uk-button uk-button-primary uk-margin-right uk-text-bold'])); ?>

              <a uk-tooltip="Cancelar e..." href="<?php echo e(route('noticias.index')); ?>" class="uk-button uk-button-default"> <i style="position: relative; bottom: 1px" uk-icon='arrow-up'></i> Voltar às Notícias</a>
            </ul>
          </div>
        </div>
    </section>

    <!--ARTICLE-->
    <section class="uk-section uk-article uk-section-muted">
        
        <div class="uk-container uk-container-small">
          
          <small class="uk-text-muted uk-h7 uk-margin-remove-adjacent uk-margin-remove-top uk-margin-remove-bottom"><span uk-icon='arrow-down'></span><i>Título</i></small>
          <?php if($errors->has('title')): ?>
            <small class="uk-text-warning"> –– <?php echo e($errors->first('title')); ?></small>
          <?php endif; ?>
          <?php echo e(Form::text('title', '', ['class' => 'uk-input uk-form-large uk-text-bold uk-heading', 'style' => 'border-style: dashed', 'id' => 'preventDefaultEnterAction'])); ?>


          
          
          <small class="uk-text-muted uk-h7"><span uk-icon='arrow-down'></span><i>Subtítulo (opcional)</i></small>
          <?php echo e(Form::textarea('subtitle', '', ['class' => 'uk-input uk-text-lead uk-height-1-1 uk-padding-small', 'style' => 'border-style: dashed;', 'rows' => '2', 'draggable' => 'false'])); ?>

        </div>


        <!-- Upload photos area -->
        <div class="uk-container uk-container-small uk-margin-medium">
            <?php echo e(Form::hidden('image_urls', '', ['class' => 'uk-input uk-disabled uk-width-1-4@s', 'placeholder'=>'Nenhuma imagem selecionada.', 'id'=>'img_urls'])); ?>            
            <div class="uk-card uk-padding-small uk-card-default uk-card-body uk-margin-small">                
              <small class="uk-text-muted uk-h7 uk-margin-remove-adjacent uk-margin-remove-top uk-margin-remove-bottom"><span uk-icon='arrow-down'></span><i>Selecione Imagens (opcional)</i></small><br>
              <ul class="uk-thumbnav uk-margin-remove" uk-margin id="img_ul">
                
              </ul><br>
              <a href="<?php echo e(route('home')); ?>/filemanager/dialog.php?type=1&field_id=img_urls" class="iframe-btn uk-button uk-button-small uk-button-primary" type="button">Adicionar Imagem</a>
              <br><small id="numberofimages" class="uk-margin-remove uk-text-muted">0 imagens selecionadas.</small>
            </div>

        </div>




        <!-- body -->
        
        <div class="uk-container uk-container-small">
            <small class="uk-text-muted uk-h7"><span uk-icon='arrow-down'></span><i>Corpo do Texto</i></small>
            <?php if($errors->has('body')): ?>
            <small class="uk-text-warning"> –– <?php echo e($errors->first('body')); ?></small>
            <?php endif; ?>
            <div class="uk-grid uk-grid-medium" data-uk-grid>
              
              
              <div style="width: 100%">
                <?php echo e(Form::textarea('body', '', ['id' => 'article-ckeditor'])); ?>

              </div>

            </div>
        </div>



        <!-- /body -->
    </section>
  <?php echo Form::close(); ?>


    <!-- /BOTTOM BAR -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>