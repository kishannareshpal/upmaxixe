<?php $__env->startSection('title'); ?>
    UP Maxixe –> Docentes
<?php $__env->stopSection(); ?>

<?php
  /*
    Must init this variable..
    highlight @section('[mobile]menus')
  */
  $isActive = 'universidade';
  $isActiveSub = 'universidade.docentes';


  /*
    Must init this variable.
    Previous and Next Page.
  */
  $previousPage = "Conselho Directivo";
  // $nextPage = ""; -> NO NEXT PAGE!!
?>

<?php $__env->startSection('menus'); ?>
  <?php echo $__env->make('inc.menus', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php echo $__env->make('inc.mobilemenus', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>



<?php $__env->startSection('navbar-right-content'); ?>
  <?php if(auth()->guard()->check()): ?>
    <?php echo $__env->make('inc.navbar-user', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php endif; ?>
<?php $__env->stopSection(); ?>



<?php $__env->startSection('body'); ?>
    <section class="uk-section uk-section-small">
        <div class="uk-container uk-container-small">
          <!--Message-->
          <?php echo $__env->make('inc.messages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
          <!--/Message-->

          <h2 class="uk-text-bold uk-h1">
            <?php if(auth()->guard()->check()): ?>
              <h2 class="uk-text-bold uk-h1 uk-margin-remove-adjacent uk-margin-remove-top">Docentes <a href="<?php echo e(route('docentes.manage')); ?>" class="hvr-grow-shadow uk-dark uk-button uk-button-secondary uk-border-rounded uk-box-shadow-small uk-float-right" style="position: relative; top: 6.4px;" uk-icon='plus' uk-tooltip="title: Adicionar Novo; delay: 250"></a></h2>
            <?php else: ?>
              Docentes
            <?php endif; ?>
          </h2>



          
          <span style="position: relative; bottom: 2px" class="uk-margin-top uk-label uk-label-success uk-text-bold"><span uk-icon='arrow-down'></span>PROFESSORES DOUTORES</span>
          <table class="uk-table uk-table-hover uk-table-divider uk-placeholder uk-table-middle">
              <thead>
                  <tr>
                      <th class="uk-text-bold uk-width-1-1">Nome Completo</th>
                      <th class="uk-text-bold">Perfil</th>
                  </tr>
              </thead>
              <tbody>
                <?php $__currentLoopData = $doutores; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $doutor): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <tr>
                      <td><?php echo e($doutor->name); ?></td>
                      <td><a href="<?php echo e(route('docentes')); ?>/<?php echo e($doutor->id); ?>" class="uk-button uk-button-small uk-button-primary">Ver</a></td>
                  </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </tbody>
          </table>



          
          <span style="position: relative; bottom: 2px" class="uk-margin-top uk-label uk-text-bold uk-label-success"><span uk-icon='arrow-down'></span>MESTRES</span>
          <table class="uk-table uk-table-hover uk-table-divider uk-placeholder uk-table-middle">
              <thead>
                  <tr>
                      <th class="uk-text-bold uk-width-1-1">Nome Completo</th>
                      <th class="uk-text-bold">Perfil</th>
                  </tr>
              </thead>
              <tbody>
                  <?php $__currentLoopData = $mestres; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $mestre): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td><?php echo e($mestre->name); ?></td>
                        <td><a href="<?php echo e(route('docentes')); ?>/<?php echo e($mestre->id); ?>" class="uk-button uk-button-small uk-button-primary">Ver</a></td>
                    </tr>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </tbody>
          </table>


          
          <span style="position: relative; bottom: 2px" class="uk-margin-top uk-label uk-text-bold uk-label-success"><span uk-icon='arrow-down'></span>Licenciados</span>
          <table class="uk-table uk-table-hover uk-table-divider uk-placeholder uk-table-middle">
              <thead>
                  <tr>
                      <th class="uk-text-bold uk-width-1-1">Nome Completo</th>
                      <th class="uk-text-bold">Perfil</th>
                  </tr>
              </thead>
                <tbody>
                  <?php $__currentLoopData = $licenciados; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $licenciado): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td><?php echo e($licenciado->name); ?></td>
                        <td><a href="<?php echo e(route('docentes')); ?>/<?php echo e($licenciado->id); ?>" class="uk-button uk-button-small uk-button-primary">Ver</a></td>
                    </tr>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </tbody>
          </table>
        </div>
    </section>


    
    <?php echo $__env->make('inc.prev_next', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>