<?php $__env->startSection('title'); ?>
    UP Maxixe –> Centros e Núcleos
<?php $__env->stopSection(); ?>

<?php
  /*
    Must init this variable..
    highlight @section('[mobile]menus')
  */
  $isActive = 'pesquisaeextensao';


  /*
    Must init this variable.
    Previous and Next Page.
  */
  // $previousPage = ""; -> NO PREVIOUS PAGE!!
  $nextPage = "Projectos de Pesquisa";
?>

<?php $__env->startSection('menus'); ?>
  <?php echo $__env->make('inc.menus', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php echo $__env->make('inc.mobilemenus', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>



<?php $__env->startSection('navbar-right-content'); ?>
  <?php if(auth()->guard()->check()): ?>
    <?php echo $__env->make('inc.navbar-user', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('headcsslink'); ?>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('jscript'); ?>

  <script type="text/javascript">
    function showmethemodal(element, id){
      document.getElementById('modal_list_second').innerHTML = "";
      var elm = element.innerHTML;
      element.innerHTML = "<div style='position: relative; bottom: 2px' uk-spinner='ratio: .5'></div>";

      $.ajax({
        url: "<?php echo e(route('centrosenucleos.index')); ?>/showmodal/"+id,
        type: "GET",
        success: function(response) {
          // IF THE IMAGE EXIST SHOW A MODAL WITH IMAGE
            UIkit.modal('#showwithoutimage').show();
            element.innerHTML = elm;

            document.getElementById('modal_title_second').innerHTML = response.title;
            document.getElementById('modal_description_second').innerHTML = response.body;
            document.getElementById('modal_list_second').innerHTML += "<li>" + "<span uk-icon='calendar' style='position: relative; bottom: 1px'></span><i>Publicado em: " + "<strong>" + response.created_at + "</strong></i></li>";
        }
      });
    }
  </script>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('body'); ?>

    <section class="uk-section uk-margin-remove-bottom uk-section-small">
        <div class="uk-container uk-container-small">

            <h1 class="uk-margin-remove-bottom uk-h1">
              <a href="<?php echo e(route('noticias.index')); ?>" class="uk-text-bold uk-link-reset">Centros e Núcleos</a>
              <?php if(auth()->guard()->check()): ?>
                <a href="<?php echo e(route('centrosenucleos.create')); ?>" class="hvr-grow-shadow uk-dark uk-button uk-button-primary uk-border-rounded uk-box-shadow-small uk-float-right" style="position: relative; top: 6.4px;" uk-icon='plus' uk-tooltip="title: Adicionar Novo Núcleo; delay: 300"></a>
              <?php endif; ?>
            </h1>
            <!--Message-->
              <?php echo $__env->make('inc.messages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <!--/Message-->

        </div>
    </section>

    <section class="uk-section uk-section-small uk-section-muted">
        <div class="uk-container uk-container-small">
            <?php $__currentLoopData = $nucleos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $nucleo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <!-- card -->
                <div class="nature-card uk-box-shadow-medium uk-background-default uk-border-rounded uk-hvr-dark">
                  <div class="uk-card uk-dark">

                    <div class="uk-card-body">
                      <h3 class="uk-h2 uk-margin-remove-adjacent uk-text-bold"><?php echo e($nucleo->title); ?></h3>
                      <p><?php echo str_limit($nucleo->body, $limit = 300, $end = ' [...] '); ?></p>

                      <div class="uk-grid uk-grid-small uk-grid-divider uk-flex uk-flex-middle" data-uk-grid>
                        <div class="uk-width-expand uk-text-small">
                          <button type="button" onclick='showmethemodal(this, <?php echo e($nucleo->id); ?>)' class="uk-button uk-button-default" name="info"><span uk-icon="icon:expand; ratio: .8" style="position: relative; bottom: 1px;"></span> Info</button>
                        </div>
                        <div class="uk-width-auto">
                          <?php if(auth()->guard()->check()): ?> 
                            <div class="uk-inline uk-padding-small">
                              <span style="cursor: pointer" class="uk-icon-button uk-background-default" uk-icon="more-vertical"></span>
                              <div class="uk-background-default uk-border-rounded" uk-dropdown="mode: click; animation: uk-animation-slide-bottom-small; duration: 150" >
                                <small class="uk-text-bold uk-text-muted">Mais Opções: </small><br>
                                <div class="uk-grid uk-grid-small uk-light" uk-grid>
                                  <div>
                                    <a href="<?php echo e(route('centrosenucleos.index')); ?>/<?php echo e($nucleo->id); ?>/edit" class="uk-icon-button uk-background-secondary uk-button-secondary" uk-icon="pencil"></a>
                                  </div>

                                  <div>
                                    <?php echo Form::open(['action' => ['Centros_NucleosController@destroy', $nucleo->id], 'method'=>'POST']); ?>

                                      <?php echo e(Form::hidden('_method', 'DELETE')); ?>

                                      <button type="submit" class="uk-icon-button uk-text-danger uk-button-secondary" uk-icon="trash"></button>
                                      
                                    <?php echo Form::close(); ?>

                                  </div>
                                </div>
                              </div>
                            </div>
                          <?php endif; ?>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>
                <br>
                <!-- /card -->
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php echo e($nucleos->links()); ?>

        </div>

        
        <div id="showwithoutimage" class="uk-flex-top" uk-modal>
            <div class="uk-modal-dialog uk-padding-large uk-modal-body uk-margin-auto-vertical">
                <button class="uk-modal-close-default" type="button" uk-close></button>
                <h2 id="modal_title_second" class="uk-h2 uk-text-bold"></h2>
                <ul id="modal_list_second" class="uk-background-muted uk-text-center uk-dark uk-padding-small uk-list uk-list-divide"></ul>
                <p id="modal_description_second" class="uk-text-muted uk-text-justify" ></p>
            </div>
        </div>
    </section>


    
    <?php echo $__env->make('inc.prev_next', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>