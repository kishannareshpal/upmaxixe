
<?php if(count($errors) > 0): ?>
  <?php
    $i = 1;
  ?>
  <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <div id="alert<?php echo e($i); ?>" class="uk-alert-danger" uk-alert>
        <a class="uk-alert-close" uk-close>
          <span class="uk-padding-remove uk-margin-remove-top" uk-countdown="date: <?php echo e(Carbon\Carbon::now()->addSeconds(10)->toIso8601String()); ?>">
            <small class="uk-text-muted uk-float-right uk-countdown-seconds uk-margin-remove uk-padding-remove"></small>
          </span>
        </a>
        <p><?php echo e($error); ?></p>
    </div>

    <script type="text/javascript">
      var closetimeout = 10000;
      setTimeout(function () {
        UIkit.alert('#alert<?php echo e($i); ?>').close();
      }, closetimeout);
    </script>

    <?php
      $i = $i+1;
    ?>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endif; ?>



<?php if(session('success')): ?>
  <div class="uk-alert-success" uk-alert>
      <a class="uk-alert-close" uk-close>
        <span class="uk-padding-remove uk-margin-remove-top" uk-countdown="date: <?php echo e(Carbon\Carbon::now()->addSeconds(10)->toIso8601String()); ?>">
          <small class="uk-text-muted uk-float-right uk-countdown-seconds uk-margin-remove uk-padding-remove"></small>
        </span>
      </a>
      <p class="uk-padding-remove uk-margin-remove-bottom"><?php echo e(session('success')); ?></p>
  </div>

  
  <script type="text/javascript">
    var closetimeout = 10000;
    setTimeout(function () {
      UIkit.alert('.uk-alert-success').close();
    }, closetimeout);
  </script>
<?php endif; ?>


<?php if(session('failed')): ?>
  <div class="uk-alert-danger" uk-alert>
      <a class="uk-alert-close" uk-close>
        <span class="uk-padding-remove uk-margin-remove-top" uk-countdown="date: <?php echo e(Carbon\Carbon::now()->addSeconds(10)->toIso8601String()); ?>">
          <small class="uk-text-muted uk-float-right uk-countdown-seconds uk-margin-remove uk-padding-remove"></small>
        </span>
      </a>
      <p class="uk-padding-remove uk-margin-remove-bottom"><?php echo e(session('failed')); ?></p>

  </div>

  
  <script type="text/javascript">
    var closetimeout = 10000;
    setTimeout(function () {
      UIkit.alert('.uk-alert-danger').close();
    }, closetimeout);
  </script>
<?php endif; ?>
