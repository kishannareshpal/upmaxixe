<?php $__env->startSection('title'); ?>
    UP Maxixe –> CEI -> Apresentação
<?php $__env->stopSection(); ?>

<?php
  /*
    Must init this variable..
    highlight @section('[mobile]menus')
  */
  $isActive = 'cei';
  $isActiveSub = 'cei.informacao';

  /*
    Must init this variable.
    Previous and Next Page.
  */
  // $previousPage = "Professores Convidados";
  $nextPage = "Newsletter";

?>

<?php $__env->startSection('menus'); ?>
  <?php echo $__env->make('inc.menus', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php echo $__env->make('inc.mobilemenus', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>



<?php $__env->startSection('navbar-right-content'); ?>
  <?php if(auth()->guard()->check()): ?>
    <?php echo $__env->make('inc.navbar-user', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('headcsslink'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body'); ?>

  <section class="uk-section uk-section-small">
    <div class="uk-container uk-container-small">
      <h5 class="uk-margin-remove-bottom uk-text-bold uk-h5 uk-margin-remove-adjacent uk-text-muted uk-margin-remove-top">Cooperação e Internacionalização</h5>
      <h1 class="uk-margin-small-bottom uk-text-bold uk-h1 uk-margin-remove-adjacent uk-margin-remove-top">
        Informação sobre a Repartição
      </h1>
    </div>
  </section>

  
  <section class="uk-section uk-section-muted">
    <div class="uk-container uk-container-small">
      
      
      <div uk-grid>
        <div class="uk-width-expand">
          <p>
            A Repartição de Cooperação é a estrutura administrativa ligada ao gabinete do Director da UP-Maxixe e, no exercício das suas funções, periodicamente publica informações úteis para os estudantes, pesquisadores e professores internacionais e para todos os interessados. Outrossim, no âmbito da internacionalização, publica, sempre que houver, editais de bolsas de estudo e de pesquisa bem como outros anúncios de concursos que visem melhorar o envolvimento, a proactividade e o desempenho da Universidade no que diz respeito ao ensino, à pesquisa e à extensão.
          </p>
          <p>
            Nesta ordem de ideia, colocamos abaixo as informações úteis referentes à vida universitária no Campus da UP-Maxixe e na Cidade da Maxixe:
          </p>
          
          <table class="uk-table uk-table-hover" style="border: 2px solid #eee">
              <thead>
                  <tr>
                    <th class="uk-box-shadow-small">
                      <img src="<?php echo e(asset('images/icons/questions (1).svg')); ?>" style="position: relative; bottom: 2px" uk-svg width="30" height="30"/> Informações Úteis
                    </th>
                  </tr>
              </thead>
              <tbody>
                  <tr><td> <a href="#"><span uk-icon="arrow-right"></span> Custo de vida na Cidade de Maxixe.                 </a> </td></tr>
                  <tr><td> <a href="#"><span uk-icon="arrow-right"></span> Alojamento.                                        </a> </td></tr>
                  <tr><td> <a href="#"><span uk-icon="arrow-right"></span> Restauração e bolsas de estudo.                    </a> </td></tr>
                  <tr><td> <a href="#"><span uk-icon="arrow-right"></span> Serviços médicos.                                  </a> </td></tr>
                  <tr><td> <a href="#"><span uk-icon="arrow-right"></span> Serviços para estudantes.                          </a> </td></tr>
                  <tr><td> <a href="#"><span uk-icon="arrow-right"></span> Estudo e pesquisa.                                 </a> </td></tr>
                  <tr><td> <a href="#"><span uk-icon="arrow-right"></span> Práticas pedagógicas e profissionalizantes.        </a> </td></tr>
                  <tr><td> <a href="#"><span uk-icon="arrow-right"></span> Actividades desportivas.                           </a> </td></tr>
                  <tr><td> <a href="#"><span uk-icon="arrow-right"></span> Associações e Grupos de Interesse para Estudantes. </a> </td></tr>
              </tbody>
          </table>
        </div>
        
        <div class="uk-width-1-3@s uk-margin-small">
            <div style="border: 1px solid #2979ff" class="uk-card uk-border-rounded uk-padding-small uk-card-body">
              <h3>Contactos da Repartição</h3>
              <div class="uk-divider-small"></div>
              <ul class="uk-list">
                <li>Universidade Pedagógica da Maxixe / UniSaF</li>
                <li class="uk-text-bold">Gabinete de Cooperação, Comunicação e Imagem</li>
                <li class="uk-link-reset"><img src="<?php echo e(asset('images/icons/gps.svg')); ?>" uk-svg width="20" height="20"> <a href="https://goo.gl/maps/jWDP6msHPoo">Av. Américo Boavida, s/n, CP. 12, Cidade da Maxixe</a></li>        
                <li class="uk-link-reset"><img src="<?php echo e(asset('images/icons/phone.svg')); ?>" uk-svg width="20" height="20"> <a href="tel:+25829330356" class="uk-button-text uk-button">+258 293-30356</a></li>
              </ul>
            </div>
        </div>
      </div>
    </div>
  </section>


  
  <?php echo $__env->make('inc.prev_next', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>