<?php
  /*
    Must init this variable..
    this serves to know on which page the user to highlight
    the correspondent MENU (NAVBAR) item down bellow. in @section('menus')
  */

  $isActive = 'eventos';
?>

<?php $__env->startSection('menus'); ?>
  <?php echo $__env->make('inc.menus', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php echo $__env->make('inc.mobilemenus', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>



<?php $__env->startSection('navbar-right-content'); ?>
  <?php if(auth()->guard()->check()): ?>
    <?php echo $__env->make('inc.navbar-user', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php endif; ?>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('headcsslink'); ?>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
  <link href="<?php echo e(asset('css/cards.css')); ?>" rel="stylesheet">
<?php $__env->stopSection(); ?>



<?php $__env->startSection('jscript'); ?>
  <script type="text/javascript">
    function showmethemodal(element, id){
      document.getElementById('modal_list').innerHTML = "";
      document.getElementById('modal_list_second').innerHTML = "";
      var elm = element.innerHTML;
      element.innerHTML = "<div style='position: relative; bottom: 2px' uk-spinner='ratio: .5'></div>";

      $.ajax({
        url: "<?php echo e(route('eventos.index')); ?>/showmodal/"+id,
        type: "GET",
        success: function(response){
          // IF THE IMAGE EXIST SHOW A MODAL WITH IMAGE
          if (response.imageurl !== null) {
            UIkit.modal('#showwithimage').show();
            element.innerHTML = elm;

            document.getElementById('modal_image').setAttribute('style', 'background-image: url("http://localhost/upmaxixe/public/storage/eventos_image/' + response.id + '/' + response.imageurl + '")');
            document.getElementById('modal_image_a').setAttribute('href', 'http://localhost/upmaxixe/public/storage/eventos_image/' + response.id + '/' + response.imageurl);
            document.getElementById('modal_image_b').setAttribute('href', 'http://localhost/upmaxixe/public/storage/eventos_image/' + response.id + '/' + response.imageurl);
            document.getElementById('modal_title').innerHTML = response.name;

            if (response.description !== null) {
              document.getElementById('modal_description').innerHTML = response.description + "<hr>";
            }
            document.getElementById('modal_list').innerHTML += "<li>" + "Local: " + "<strong>" + response.local + "</strong></li>";
            document.getElementById('modal_list').innerHTML += "<li>" + "Data: " + "<strong>" + response.horario_do_inicio + "</strong</li>";

            if (response.horario_do_fim !== undefined) {
              document.getElementById('modal_list').innerHTML += "<li>" + "Data do Fim do Evento: " + "<strong>" + response.horario_do_fim + "</strong></li>";
            }

            if (response.capacidade == 'unlimited') {
              var capacidade = "ILIMITADA";
              document.getElementById('modal_list').innerHTML += "<li>" + "Capacidade: " + "<strong>" + capacidade + "</strong>" + "</li>";
            } else {
              var capacidade = response.capacidade;
              document.getElementById('modal_list').innerHTML += "<li>" + "Capacidade: " + "<strong>" + capacidade + "</strong> Pessoas." + "</li>";
            }

            if (response.precoindividual == 'free') {
              var precoindividual = "GRATUITO";
              document.getElementById('modal_list').innerHTML += "<li>" + "Preço Individual: " + "<strong>" + precoindividual + "</strong>" + "</li>";
            } else {
              var precoindividual = response.capacidade;
              document.getElementById('modal_list').innerHTML += "<li>" + "Preço Individual: " + "<strong>" + precoindividual + " MT </strong>" + "</li>";
            }

          // IF THE IMAGE DOESNT EXIST SHOW A SIMPLER MODAL
          } else {
            UIkit.modal('#showwithoutimage').show();
            element.innerHTML = elm;

            document.getElementById('modal_title_second').innerHTML = response.name;
            if (response.description !== null) {
              document.getElementById('modal_description_second').innerHTML = response.description;
            }

            document.getElementById('modal_list_second').innerHTML += "<li>" + "Local: " + "<strong>" + response.local + "</strong></li>";
            document.getElementById('modal_list_second').innerHTML += "<li>" + "Data: " + "<strong>" + response.horario_do_inicio + "</strong</li>";

            if (response.horario_do_fim !== null) {
              document.getElementById('modal_list_second').innerHTML += "<li>" + "Data do Fim do Evento: " + "<strong>" + response.horario_do_fim + "</strong></li>";
            }

            if (response.capacidade == 'unlimited') {
              var capacidade = "ILIMITADA";
              document.getElementById('modal_list_second').innerHTML += "<li>" + "Capacidade: " + "<strong>" + capacidade + "</strong>" + "</li>";
            } else {
              var capacidade = response.capacidade;
              document.getElementById('modal_list_second').innerHTML += "<li>" + "Capacidade: " + "<strong>" + capacidade + "</strong> Pessoas." + "</li>";
            }

            if (response.precoindividual == 'free') {
              var precoindividual = "GRATUITO";
              document.getElementById('modal_list_second').innerHTML += "<li>" + "Preço Individual: " + "<strong>" + precoindividual + "</strong>" + "</li>";
            } else {
              var precoindividual = response.capacidade;
              document.getElementById('modal_list_second').innerHTML += "<li>" + "Preço Individual: " + "<strong>" + precoindividual + " MT </strong>" + "</li>";
            }
          } // end -> else if(imageDoesNotExist)


        }
      });
    }
  </script>
<?php $__env->stopSection(); ?>



<?php $__env->startSection('body'); ?>

    <!--ARTICLE-->
    <section class="uk-section uk-article uk-background-secondary">
        <div class="uk-container uk-container-small">
            <?php echo $__env->make('inc.messages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <a style="position: relative; bottom: 2px;" href="<?php echo e(route('eventos.index')); ?>" class="uk-button uk-button-default uk-button-primary uk-text-bold uk-border-rounded uk-dark"> <span style="position: relative; bottom: 1px" uk-icon='arrow-left'></span> Voltar</a>
            <h2 class="uk-text-bold uk-h2 uk-margin-remove-adjacent uk-text-muted ">Arquivo de Todos os Eventos<span uk-icon="icon: arrow-down; ratio: 1.4"></span></h2>
        </div>

        <div class="uk-container uk-container-small uk-margin-medium">
          
          
          <div class="uk-grid uk-child-width-1-2@s uk-child-width-1-3@m" uk-grid>
            <?php $__currentLoopData = $eventos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $evento): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <?php
                $horario_do_inicio = Carbon\Carbon::parse($evento->horario_do_inicio);
                $horario_do_fim = Carbon\Carbon::parse($evento->horario_do_fim);
              ?>
    						<!-- card -->
    						<div class="nature-card">
    							<div class="uk-card uk-background-default">
    								
                    <div style="border-radius: 3px" class="uk-text-center uk-padding uk-light uk-background-primary">
                      <div class="uk-width-auto uk-text-bold">
                        <span uk-icon="icon: calendar; ratio: 2"></span>
                        <br>
                        <small class="uk-margin-remove-bottom"><?php echo e($weeks[$horario_do_inicio->dayOfWeek]); ?></small>
                        <h1 style="line-height: .8" class="uk-h1 uk-text-bold uk-margin-remove uk-padding-remove">
                          <?php if($horario_do_inicio->day == $horario_do_fim->day): ?>
                            <?php echo e($horario_do_inicio->day); ?>

                          <?php else: ?>
                            <?php echo e($horario_do_inicio->day); ?> –– <?php echo e($horario_do_fim->day); ?>

                          <?php endif; ?>
                        </h1>
                        <small class="uk-margin-remove uk-text-bold uk-text-meta uk-text-muted">
                          <?php if($horario_do_inicio->month == $horario_do_fim->month): ?>
                            <?php echo e($months[$horario_do_inicio->month]); ?>

                          <?php else: ?>
                            <?php echo e($months[$horario_do_inicio->month]); ?> –– <?php echo e($months[$horario_do_fim->month]); ?>

                          <?php endif; ?>
                        </small>
                      </div>
                    </div>

                    <div class="uk-card-body">
    									<h4 class="uk-h4 uk-margin-remove-adjacent uk-text-bold"><?php echo e($evento->name); ?></h4>
    								</div>

                    <div style="border-radius: 3px" class="uk-card-footer uk-background-muted">
                      <div class="uk-grid uk-grid-small uk-grid-divider uk-flex uk-flex-middle" data-uk-grid>
                        <div class="uk-width-expand uk-text-small">
                          <button type="button" onclick='showmethemodal(this, <?php echo e($evento->id); ?>)' class="uk-button uk-button-default uk-button-small" name="info"><span uk-icon="icon:expand; ratio: .8" style="position: relative; bottom: 1px;"></span> Info</button>
                        </div>
                        <div class="uk-width-auto">
                          <?php if(auth()->guard()->check()): ?> 
                            <span style="cursor: pointer" class="uk-icon-button uk-alert-muted" uk-icon="more-vertical"></span>
                            <div class="uk-background-default uk-border-rounded" uk-dropdown="mode: click; animation: uk-animation-slide-bottom-small; duration: 150" >
                              <small class="uk-text-bold ">Mais Opções: </small><br>
                              <div class="uk-grid uk-grid-small uk-light" uk-grid>
                                <div>
                                  <a href="<?php echo e(route('eventos.index')); ?>/<?php echo e($evento->id); ?>/edit" class="uk-icon-button uk-background-secondary uk-button-secondary" uk-icon="pencil"></a>
                                </div>
                                <div>
                                  <?php echo Form::open(['action' => ['EventosController@destroy', $evento->id], 'method'=>'POST']); ?>

                                    <?php echo e(Form::hidden('_method', 'DELETE')); ?>

                                    <button type="submit" class="uk-icon-button uk-text-danger uk-button-secondary" uk-icon="trash"></button>
                                    
                                  <?php echo Form::close(); ?>

                                </div>
                              </div>

                            </div>
                          <?php endif; ?>
                        </div>
                      </div>
                    </div>
    							</div>
    						</div>
    						<!-- /card -->
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </div>


          
          
          <div id="showwithimage" class="uk-modal-full" uk-modal>
              <div class="uk-modal-dialog">
                  <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>
                  <div uk-lightbox class="uk-grid-collapse uk-child-width-1-2@s uk-flex-middle " uk-grid> 
                      <a href="#" id="modal_image_a" class="uk-inline-clip uk-transition-toggle">
                        <div id="modal_image" class="uk-background-cover uk-placeholder" uk-height-viewport></div>
                        <div class="uk-transition-fade uk-position-cover uk-position-small uk-overlay-primary uk-flex uk-flex-middle uk-flex-center uk-text-bold">
                          <p class="uk-h4 uk-margin-remove">Ver a Imagem</p>
                        </div>
                      </a>
                      <div class="uk-padding-large uk-text-center" uk-height-viewport>
                          <h1 id="modal_title"></h1>
                          <a href="#" id="modal_image_b" class="uk-button uk-button-primary uk-float-bottom" name="button"><i style="position: relative; bottom: 1px" uk-icon="image"></i>&nbsp Ver a Imagem</a>

                          <p id="modal_description"></p>
                          <ul id="modal_list" class="uk-alert-primary uk-dark uk-padding-small uk-list"></ul>
                      </div>
                  </div>
              </div>
          </div>

          
          <div id="showwithoutimage" class="uk-flex-top" uk-modal>
              <div class="uk-modal-dialog uk-padding-large uk-modal-body uk-margin-auto-vertical">
                  <button class="uk-modal-close-default" type="button" uk-close></button>

                  <h2 id="modal_title_second" class="uk-h2 uk-text-bold"></h2>
                  <p id="modal_description_second" class="uk-text-muted uk-text-justify" ></p>
                  <ul id="modal_list_second" class="uk-background-muted uk-dark uk-padding-small uk-list uk-list-divide"></ul>
              </div>
          </div>

          <div class="uk-container uk-container-small uk-margin-medium">
            <?php echo e($eventos->links()); ?>

          </div>
        </div>

    </section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>