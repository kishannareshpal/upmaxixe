<?php $__env->startSection('title'); ?>
    UP Maxixe –> Notícias -> ...
<?php $__env->stopSection(); ?>

<?php
  /*
    Must init this variable..
    this serves to know on which page the user to highlight
    the correspondent MENU (NAVBAR) item down bellow. in @section('menus')
  */

  $isActive = 'noticias';
?>

<?php $__env->startSection('menus'); ?>
  <?php echo $__env->make('inc.menus', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php echo $__env->make('inc.mobilemenus', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('navbar-right-content'); ?>
  <?php if(auth()->guard()->check()): ?>
    <?php echo $__env->make('inc.navbar-user', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('headcsslink'); ?>
  <style>
    /* Makes a hover effect to make a clickable card  */
    .card-k {
      transition: 0.4s;
      cursor: pointer;
    }
    .card-k:hover{
      background-color: #f3f3f3
    }
  </style>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('body'); ?>


    
    <section class="uk-section uk-margin-remove-bottom uk-section-small">
        <div class="uk-container uk-container-small">
            <!--Message-->
              <?php echo $__env->make('inc.messages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <!--/Message-->
            
            <h1 class="uk-margin-remove-bottom uk-margin-remove-top uk-h1">
              <a href="<?php echo e(route('noticias.index')); ?>" class="uk-text-bold uk-link-reset"><img src="<?php echo e(asset('images/icons/newspaper.svg')); ?>" style="position: relative; bottom: 3.1px" width="30" height="30" uk-svg> Notícias</a>
              
            </h1>
            <p style="position: relative; bottom: 10px" class="uk-margin-remove-top uk-text-bold"><small>da</small> UP Maxixe</p>

        </div>
    </section>

    <!--ARTICLE-->
    <section class="uk-section uk-article uk-section-muted">
        <div class="uk-container uk-container-small">
          <a style="position: relative; bottom: 2px;" href="<?php echo e(route('noticias.index')); ?>" class="uk-button uk-button-default uk-button-small uk-text-bold uk-border-rounded uk-dark"> <span style="position: relative; bottom: 1px" uk-icon='arrow-left'></span> Voltar</a>
          <h2 class="uk-text-bold uk-margin-remove-bottom uk-h2"><?php echo e($post->title); ?></h2>
          <small class="uk-text-muted uk-h7 uk-margin-remove-adjacent uk-margin-remove-top uk-margin-remove-bottom">
             <a class="uk-button-text uk-link-reset" href="<?php echo e(route('noticias.index')); ?>">Notícias</a>
              / <i>
                    <span style="position: relative; bottom: 1px" uk-icon="calendar"></span>
                    Publicado as <?php echo e($post->created_at->format('H:i\h')); ?>, no dia <?php echo e($post->created_at->day); ?> de <?php echo e($ptmonth); ?> de <?php echo e($post->created_at->year); ?>

                    <?php if(auth()->guard()->check()): ?> –– <a href="<?php echo e($post->id); ?>/edit" class="uk-button uk-button-text uk-text-muted uk-text-bold" style="text-transform: none; position: relative; bottom: 1px">Editar</a> <?php endif; ?>
                </i>
          </small>
          <?php if(!blank($post->subtitle)): ?>
            <p class="uk-text-lead"><?php echo e($post->subtitle); ?></p>
          <?php endif; ?>
        </div>

        <?php
          # Extract imageurl from the string to an array of urls = $image_urls.
          preg_match_all('#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#', $post->image_urls, $image_urls);
          $image_urls = $image_urls[0]; // TRICK: i did this because it for some reason returns two arrays: [the first one, containing the actual image urls that i need] and [the second one, literaly an array of "g"'s]
        ?>

        <?php if(count($image_urls) > 0): ?>

          <div class="uk-container uk-container-small uk-section uk-padding-small">
            <div class="uk-position-relative uk-visible-toggle uk-light uk-box-shadow-medium" uk-slideshow="ratio: 7:3; animation: fade; min-height: 190;">
                <ul uk-lightbox class="uk-slideshow-items">
                  <?php $__currentLoopData = $image_urls; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $url): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li>
                      <img uk-img data-src="<?php echo e($url); ?>" uk-cover>
                      <div class="uk-position-bottom uk-position-medium uk-text-center uk-inverse">
                        <a style="border: 1px solid #333; opacity: .8" href="<?php echo e($url); ?>" class="uk-box uk-button uk-button-small uk-button-secondary uk-border-rounded uk-box-shadow-large" uk-icon="expand"></a>
                      </div>
                    </li>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
                
                <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" data-uk-slidenav-previous="ratio: 1.5" data-uk-slideshow-item="previous"></a>
                <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" data-uk-slidenav-next="ratio: 1.5" data-uk-slideshow-item="next"></a>
            </div>
          </div>
        <?php endif; ?>





        <!-- body -->
        <div class="uk-container uk-container-small">
            <div>
              <p><?php echo $post->body; ?></p>
            </div>
        </div>
        <!-- /body -->
    </section>


    <section class="uk-section uk-section-small uk-dark">
      <div class="uk-container uk-container-small uk-margin-medium">
        <div>
          <div class="uk-text-center">
            <small>Ír para a Notícia...</small>
          </div>
          <br>
          <div class="uk-flex-center" uk-grid>
            <?php if(!empty($newer_post->id)): ?>
              
              <div class="uk-margin-small-bottom uk-width-1-3@s">
                <div style="border: 1px solid #F90F47;" class="card-k uk-border-rounded uk-text-break uk-card uk-card-default uk-card-body uk-transition-toggle uk-padding-small">
                  <p class="uk-margin-remove"><?php echo e($newer_post->title); ?></p>
                  <span class="uk-comment-meta uk-margin-remove-top">
                    <small><?php echo e($newer_post->created_at->format('d M\, Y \– h:i')); ?></small><br>
                    <span style="text-transform: none; opacity: .5" class="uk-label uk-label-danger"><small>Seguinte</small></span>
                    <small class="uk-transition-fade uk-transition-slide-right-small uk-position-bottom-right" style="bottom: 10px; right: 10px"><span style="color: #F90F47 !important;" uk-icon="arrow-right">Ler </span></small>
                  </span>
                  <a href="<?php echo e(route('noticias.index')); ?>/<?php echo e($newer_post->id); ?>"><span style="position: absolute; width: 100%; height: 100%; top: 0; left: 0; z-index: 1;" class="linkSpanner"></span></a>
                </div>
              </div>
            <?php endif; ?>
            <?php if(!empty($older_post->id)): ?>
              
              <div class="uk-margin-small-bottom uk-width-1-3@s">
                <div style="border: 1px solid #F90F47;" class="card-k uk-border-rounded uk-text-break uk-card uk-card-default uk-card-body uk-transition-toggle uk-padding-small">
                  <p class="uk-margin-remove"><?php echo e($older_post->title); ?></p>
                  <span class="uk-comment-meta uk-margin-remove-top">
                    <small><?php echo e($older_post->created_at->format('d M\, Y \– h:i')); ?></small><br>
                    <span style="text-transform: none; opacity: .5" class="uk-label uk-label-danger"><small>Anterior</small></span>
                    <small class="uk-transition-fade uk-transition-slide-right-small uk-position-bottom-right" style="bottom: 10px; right: 10px"><span style="color: #F90F47 !important;" uk-icon="arrow-right">Ler </span></small>
                  </span>
                  <a href="<?php echo e(route('noticias.index')); ?>/<?php echo e($older_post->id); ?>"><span style="position: absolute; width: 100%; height: 100%; top: 0; left: 0; z-index: 1;" class="linkSpanner"></span></a>
                </div>
              </div>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </section>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>