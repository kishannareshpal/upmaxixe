<?php
  /*
    Must init this variable..
    this serves to know on which page the user to highlight
    the correspondent MENU (NAVBAR) item down bellow. in @section('menus')
  */

  $isActive = 'universidade';
?>

<?php $__env->startSection('menus'); ?>
  <?php echo $__env->make('inc.menus', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php echo $__env->make('inc.mobilemenus', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>



<?php $__env->startSection('navbar-right-content'); ?>
  <?php if(auth()->guard()->check()): ?>
    <?php echo $__env->make('inc.navbar-user', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php endif; ?>
<?php $__env->stopSection(); ?>



<?php $__env->startSection('body'); ?>
    <section class="uk-section uk-section-small uk-section-muted uk-margin-remove">
        <div class="uk-container uk-container-small uk-margin-medium">
          <div class="uk-float-left uk-text-right">
            <a href="<?php echo e(route('docentes')); ?>" class="uk-button uk-button-default uk-float-right uk-button-muted uk-border-rounded"> <i uk-icon="chevron-left" style="position: relative; bottom: 1px"></i> Voltar àos Docentes</a>
          </div>
        </div>

        <div class="uk-container uk-container-small">
          <!--Message-->
          <?php echo $__env->make('inc.messages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
          <!--/Message-->


          <div class="uk-grid-small" uk-grid>
            <div class="uk-width-1-3@s uk-text-center">
              <div class="uk-card uk-card-default uk-card-body uk-background-secondary uk-border-rounded" style="overflow-x: scroll">
                  <?php if(!empty($docente->photo)): ?>
                    <div class="uk-card-title">
                      <img uk-img class="uk-border-circle" data-src="<?php echo e(asset('/storage/docentes_image')); ?>/<?php echo e($docente->photo); ?>" alt="<?php echo e($docente->photo); ?>">
                    </div>
                  <?php endif; ?>
                  <h2 class="uk-text-bold mdl-color-text--white"><?php echo e($docente->name); ?></h2>
                  <?php $__currentLoopData = str_getcsv($docente->cadeiras); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cadeira): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <p style="font-size: 10px; overflow-wrap: break-word" class="uk-label"><?php echo e($cadeira); ?></p>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                  <br><br>

                  <?php if(!empty($docente->email)): ?>
                    <small class="uk-light">
                      <i style="position: relative; bottom: 1px" uk-icon="mail"></i>
                      <a class="uk-button-text" href="mailto:<?php echo e($docente->email); ?>">
                        <i><?php echo e($docente->email); ?></i>
                      </a>
                    </small>
                  <?php endif; ?>

                  <br>

                  <?php if($docente->celular !== '+258'): ?>
                    <small class="uk-light">
                      <i style="position: relative; bottom: 1px" uk-icon="phone"></i>
                      <a class="uk-button-text" href="tel:<?php echo e($docente->celular); ?>">
                        <?php echo e($docente->celular); ?>

                      </a>
                    </small>
                  <?php endif; ?>

                  <br>

                  <?php if($docente->id === 6): ?>
                    <small class="uk-light">
                      <i style="position: relative; bottom: 1px" uk-icon="calendar"></i>
                      <a class="uk-button-text" href="https://www.calendly.com/eziobono" target="_blank">
                        Marcar Audiência
                      </a>
                    </small>
                  <?php endif; ?>

              </div>

              <br>

              <?php if(auth()->guard()->check()): ?>
                <a href="<?php echo e(route('docentes.manage')); ?>/edit/<?php echo e($docente->id); ?>" class="uk-button uk-button-default uk-text-bold uk-border-rounded"><i uk-icon="pencil" style="position: relative; bottom: 1px"></i> Editar</a>
                <br>
                <?php echo Form::open(['action' => ['PagesController@deleteDocente', $docente->id], 'method'=>'POST']); ?>

                  <?php echo e(Form::hidden('_method', 'DELETE')); ?>

                  
                  <button type="submit" class="uk-margin-top uk-button uk-button-default uk-button-danger uk-text-bold uk-border-rounded"><i uk-icon="trash" style="position: relative; bottom: 1px"></i> Remover</buttton>
                <?php echo Form::close(); ?>

              <?php endif; ?>

            </div>

            <div class="uk-width-expand@s">
              <div class="uk-card uk-card-default uk-card-body uk-border-rounded">
                  <h3 class="uk-text-bold">Biografia</h3>
                  <div class="uk-divider-small"></div>
                  <p class="uk-text-justify uk-dropcap"><?php echo e($docente->biography); ?></p>
              </div>

              <br>

              <?php if(count($docente->obras) > 0): ?>
                <div class="uk-card uk-card-default uk-card-body">
                    <h3 class="uk-text-bold">Obras Literárias</h3>
                    <div class="uk-divider-small"></div>
                    <p class="uk-text-justify">Das suas obras literárias, destacam-se:</p>

                    <ul class="uk-list uk-list-divider uk-text-muted uk-hover">
                      <?php $__currentLoopData = $docente->obras; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $obra): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li class="uk-text-justify"><?php echo e($obra->referencia); ?></li>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </div>
              <?php endif; ?>


            </div>
          </div>

        </div>
    </section>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>