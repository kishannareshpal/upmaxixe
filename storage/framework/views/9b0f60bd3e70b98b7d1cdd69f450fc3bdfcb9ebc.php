<?php $__env->startSection('title'); ?>
    UP Maxixe –> Conselho Directivo
<?php $__env->stopSection(); ?>

<?php
  /*
    Must init this variable..
    highlight @section('[mobile]menus')
  */
  $isActive = 'universidade';
  $isActiveSub = 'universidade.conselho';


  /*
    Must init this variable.
    Previous and Next Page.
  */
  $previousPage = "Órgãos Superiores";
  $nextPage = "Docentes";
?>

<?php $__env->startSection('menus'); ?>
  <?php echo $__env->make('inc.menus', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php echo $__env->make('inc.mobilemenus', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>



<?php $__env->startSection('navbar-right-content'); ?>
  <?php if(auth()->guard()->check()): ?>
    <?php echo $__env->make('inc.navbar-user', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php endif; ?>
<?php $__env->stopSection(); ?>



<?php $__env->startSection('body'); ?>


    <!--Message-->
    <?php echo $__env->make('inc.messages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!--/Message-->


    <section class="uk-section uk-section-small">
        <div class="uk-container uk-container-small">
            <h2 class="uk-margin-remove-bottom uk-text-bold uk-h1 uk-margin-remove-adjacent uk-margin-remove-top">Conselho Directivo</h2>
            
            
        </div>
    </section>

    <section class="uk-section uk-section-muted uk-section-small">
      <div class="uk-container">
        <div class="uk-child-width-1-3@s uk-flex-center uk-grid-small uk-grid-match" uk-grid>
            <div class="">
              <div class="uk-text-center">
                <img uk-img uk-img class="uk-border-circle uk-margin-remove" data-src="<?php echo e(asset('images/conselhodirectivo/1.jpg')); ?>" width="200" height="200">
                <h5 class="uk-margin-remove uk-text-bold uk-text-primary">PhD. Ezio Lorenzo Bono</h5>
                <div style="line-height: 1">
                  <small class="uk-margin-remove uk-text-bold">O Director</small>
                </div>
              </div>
            </div>
        </div>


        <div class="uk-child-width-1-4@s uk-flex-center uk-grid-small uk-grid-match" uk-grid>
            <div class="">
                <div class="uk-text-center">
                    <img uk-img class="uk-border-circle uk-margin-remove" data-src="<?php echo e(asset('images/conselhodirectivo/2.jpg')); ?>" width="200" height="200">
                    <h5 class="uk-margin-remove uk-text-bold uk-text-primary">PhD. Crisalita Djeco Funes</h5>
                    <div style="line-height: 1">
                      <small class="uk-margin-remove uk-text-bold">Directora Adjunta Pedagógica</small>
                    </div>

                </div>
            </div>
            <div class="">
                <div class="uk-text-center">
                    <img uk-img class="uk-border-circle uk-margin-remove" data-src="<?php echo e(asset('images/conselhodirectivo/3.jpg')); ?>" width="200" height="200">
                    <h5 class="uk-margin-remove uk-text-bold uk-text-primary">PhD. Isabel Vilanculos</h5>
                    <div style="line-height: 1">
                      <small class="uk-margin-remove uk-text-bold">Directora Adjunta de Pesquisa e Pós Graduação</small>
                    </div>
                </div>
            </div>
            <div class="">
                <div class="uk-text-center">
                    <img uk-img class="uk-border-circle uk-margin-remove" data-src="<?php echo e(asset('images/conselhodirectivo/4.jpg')); ?>" width="200" height="200">
                    <h5 class="uk-margin-remove uk-text-bold uk-text-primary">Padre Michelli Angelo</h5>
                    <div style="line-height: 1">
                      <small class="uk-margin-remove uk-text-bold">Director Adjunto Administrativo</small>
                    </div>
                </div>
            </div>
            <div class="">
                <div class="uk-text-center">
                    <img uk-img class="uk-border-circle uk-margin-remove" data-src="<?php echo e(asset('images/conselhodirectivo/5.jpg')); ?>" width="200" height="200">
                    <h5 class="uk-margin-remove uk-text-bold uk-text-primary">dr. Benedito Cristina Cumbe</h5>
                    <div style="line-height: 1">
                      <small class="uk-margin-remove uk-text-bold">Director Adjunto Administrativo</small>
                    </div>
                </div>
            </div>
        </div>



        <div class="uk-child-width-1-6@s uk-flex-center uk-grid-small uk-grid-match" uk-grid>
            <div class="">
                <div class="uk-text-center">
                    <img uk-img class="uk-border-circle uk-margin-remove" data-src="<?php echo e(asset('images/conselhodirectivo/6.jpg')); ?>" width="200" height="200">
                    <h5 class="uk-margin-remove uk-text-bold uk-text-primary">MA. Daniel Muando</h5>
                    <div style="line-height: 1">
                      <small class="uk-margin-remove uk-text-bold">Ensino à Distância</small>
                    </div>

                </div>
            </div>
            <div class="">
                <div class="uk-text-center">
                    <img uk-img class="uk-border-circle uk-margin-remove" data-src="<?php echo e(asset('images/conselhodirectivo/7.jpg')); ?>" width="200" height="200">
                    <h5 class="uk-margin-remove uk-text-bold uk-text-primary">MA. Crimildo F. Muhache</h5>
                    <div style="line-height: 1">
                      <small class="uk-margin-remove uk-text-bold">Ciências Sociais</small>
                    </div>

                </div>
            </div>
            <div class="">
                <div class="uk-text-center">
                    <img uk-img class="uk-border-circle uk-margin-remove" data-src="<?php echo e(asset('images/conselhodirectivo/8.jpg')); ?>" width="200" height="200">
                    <h5 class="uk-margin-remove uk-text-bold uk-text-primary">MA. Isabel Hoguane</h5>
                    <div style="line-height: 1">
                      <small class="uk-margin-remove uk-text-bold">Ciências de Educação e Psicologia</small>
                    </div>
                </div>
            </div>
            <div class="">
                <div class="uk-text-center">
                    <img uk-img class="uk-border-circle uk-margin-remove" data-src="<?php echo e(asset('images/conselhodirectivo/9.jpg')); ?>" width="200" height="200">
                    <h5 class="uk-margin-remove uk-text-bold uk-text-primary">dr. Horâcio Mucivame</h5>
                    <div style="line-height: 1">
                      <small class="uk-margin-remove uk-text-bold">CLCA</small>
                    </div>
                </div>
            </div>
            <div class="">
              <div class="uk-text-center">
                  <img uk-img class="uk-border-circle uk-margin-remove" data-src="<?php echo e(asset('images/conselhodirectivo/10.jpg')); ?>" width="200" height="200">
                  <h5 class="uk-margin-remove uk-text-bold uk-text-primary">MA. Bruno Comini</h5>
                  <div style="line-height: 1">
                    <small class="uk-margin-remove uk-text-bold">ESCOGE & ESTEC</small>
                  </div>
              </div>
            </div>
          </div>


        </div>
      </div>
    </section>



    
    <section class="uk-section uk-section-muted uk-section-small">
      <div class="uk-container uk-container-small uk-margin-medium uk-placeholder">
        <h3 class="uk-text-center">Chefes de Departamentos</h3>
        <div class="uk-child-width-expand@s uk-flex-center uk-grid-small uk-grid-match" uk-grid>
            <div class="">
                <div class="uk-text-center">
                    <img uk-img class="uk-border-circle uk-margin-remove" data-src="<?php echo e(asset('images/conselhodirectivo/11.jpg')); ?>" width="200" height="200">
                    <h5 class="uk-margin-remove uk-text-bold uk-text-primary">MA. Bento Ombe</h5>
                    <div style="line-height: 1">
                      <small class="uk-margin-remove uk-text-bold">Gestão de Recursos Humanos</small>
                    </div>

                </div>
            </div>
            <div class="">
                <div class="uk-text-center">
                    <img uk-img class="uk-border-circle uk-margin-remove" data-src="<?php echo e(asset('images/conselhodirectivo/12.jpg')); ?>" width="200" height="200">
                    <h5 class="uk-margin-remove uk-text-bold uk-text-primary">drª. Tânia Mondlane</h5>
                    <div style="line-height: 1">
                      <small class="uk-margin-remove uk-text-bold">HIPOGEP</small>
                    </div>

                </div>
            </div>
            <div class="">
                <div class="uk-text-center">
                    <img uk-img class="uk-border-circle uk-margin-remove" data-src="<?php echo e(asset('images/conselhodirectivo/13.jpg')); ?>" width="200" height="200">
                    <h5 class="uk-margin-remove uk-text-bold uk-text-primary">MA. Mateus Chabai</h5>
                    <div style="line-height: 1">
                      <small class="uk-margin-remove uk-text-bold">Ensino de Filosofia</small>
                    </div>
                </div>
            </div>
            <div class="">
                <div class="uk-text-center">
                    <img uk-img class="uk-border-circle uk-margin-remove" data-src="<?php echo e(asset('images/conselhodirectivo/14.jpg')); ?>" width="200" height="200">
                    <h5 class="uk-margin-remove uk-text-bold uk-text-primary">MA. Rafael Zunguze</h5>
                    <div style="line-height: 1">
                      <small class="uk-margin-remove uk-text-bold">Psicologia Escolar</small>
                    </div>
                </div>
            </div>
            <div class="">
              <div class="uk-text-center">
                  <img uk-img class="uk-border-circle uk-margin-remove" data-src="<?php echo e(asset('images/conselhodirectivo/15.jpg')); ?>" width="200" height="200">
                  <h5 class="uk-margin-remove uk-text-bold uk-text-primary">MA. Jofredino L. Faife</h5>
                  <div style="line-height: 1">
                    <small class="uk-margin-remove uk-text-bold">Ciências de Educação</small>
                  </div>
              </div>
            </div>
            <div class="">
              <div class="uk-text-center">
                  <img uk-img class="uk-border-circle uk-margin-remove" data-src="<?php echo e(asset('images/conselhodirectivo/16.jpg')); ?>" width="200" height="200">
                  <h5 class="uk-margin-remove uk-text-bold uk-text-primary">MA. Dionísio Mavume</h5>
                  <div style="line-height: 1">
                    <small class="uk-margin-remove uk-text-bold">Ensino de Inglês</small>
                  </div>
              </div>
            </div>

            <div class="">
              <div class="uk-text-center">
                  <img uk-img class="uk-border-circle uk-margin-remove" data-src="<?php echo e(asset('images/conselhodirectivo/17.jpg')); ?>" width="200" height="200">
                  <h5 class="uk-margin-remove uk-text-bold uk-text-primary">MA. Bruno Comini</h5>
                  <div style="line-height: 1">
                    <small class="uk-margin-remove uk-text-bold">Engenharia Civil</small>
                  </div>
              </div>
            </div>
          </div>
      </div>
    </section>

    
    <?php echo $__env->make('inc.prev_next', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>