<?php $__env->startSection('title'); ?>
    UP Maxixe –> CEI -> Apresentação
<?php $__env->stopSection(); ?>

<?php
  /*
    Must init this variable..
    highlight @section('[mobile]menus')
  */
  $isActive = 'cei';
  $isActiveSub = 'cei.newsletter';

  /*
    Must init this variable.
    Previous and Next Page.
  */
  $previousPage = "Informação";
  // $nextPage = "" -> NO NEXT PAGE!!
?>

<?php $__env->startSection('menus'); ?>
  <?php echo $__env->make('inc.menus', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php echo $__env->make('inc.mobilemenus', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>



<?php $__env->startSection('navbar-right-content'); ?>
  <?php if(auth()->guard()->check()): ?>
    <?php echo $__env->make('inc.navbar-user', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php endif; ?>
<?php $__env->stopSection(); ?>



<?php $__env->startSection('body'); ?>



    
  <?php echo $__env->make('inc.prev_next', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>