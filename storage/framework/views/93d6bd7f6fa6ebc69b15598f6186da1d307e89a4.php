<div style="position: relative; bottom: 1.2px" class="uk-inline">
  <a uk-icon="user" class="uk-padding-remove uk-padding-remove uk-button uk-link-heading"></a>
  <div class="drop-nav uk-dropdown uk-border-rounded uk-background-default" uk-dropdown="mode: click; animation: uk-animation-slide-bottom-small; duration: 150">
      <span uk-icon="arrow-right" style="position: relative; top: 2px"></span> <a class="uk-text-bold uk-button uk-button-text" href="<?php echo e(route('dashboard.index')); ?>">Administração</a>
      <hr>
      <a class="uk-button uk-button-danger" href="<?php echo e(route('logout')); ?>" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Saír <i uk-icon='sign-out'></i></a>
  </div>
  <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
    <?php echo e(csrf_field()); ?>

  </form>
</div>
