<script>
      var manualUploader = new qq.FineUploader({
          element: document.getElementById('fine-uploader-manual-trigger'),
          template: 'qq-template-manual-trigger',
          <?php if($use=='edit'): ?>
            request: {
                customHeaders: {
                    'X-CSRF-TOKEN': document.querySelector('head > meta:nth-child(4)').content
                },
                method: 'post',
                endpoint: '<?php echo e(route('noticias.index')); ?>/ui/<?php echo e($post->id); ?>'
            },
          <?php endif; ?>

          <?php if($use=='create'): ?>

            <?php if(empty($last_post->id)): ?>
              <?php
                $lastPost = 0;
              ?>
            <?php else: ?>
              <?php
                $lastPost = $last_post->id;
              ?>
            <?php endif; ?>

            request: {
                customHeaders: {
                    'X-CSRF-TOKEN': document.querySelector('head > meta:nth-child(4)').content
                },
                method: 'post',
                endpoint: '<?php echo e(route('noticias.index')); ?>/ui/<?php echo e($lastPost+1); ?>'
            },
          <?php endif; ?>

          <?php if($use=='destaques'): ?>
            request: {
                customHeaders: {
                    'X-CSRF-TOKEN': document.querySelector('head > meta:nth-child(4)').content
                },
                method: 'post',
                endpoint: '<?php echo e(route('noticias.index')); ?>/ui/0'
            },
          <?php endif; ?>

          autoUpload: false,
          // debug: true,
          messages: {
            noFilesError: 'Nenhuma imagem foi selecionada.'
          },
          callbacks: {
              onComplete: function(id, name, responseJSON){
                if (responseJSON.success) {
                  // document.getElementById('makemegreen').classList.remove('mdl-color--yellow-50');
                  // document.getElementById('makemegreen').classList.add('mdl-color--green-50');
                  var x = document.getElementsByName('makemegreen').length;

                  for (var i = 0; i < x; i++) {
                    document.getElementsByName('makemegreen')[i].classList.remove('mdl-color--yellow-50');
                    document.getElementsByName('makemegreen')[i].classList.add('mdl-color--green-50')
                  }
                }
              }
          }
      });

      qq(document.getElementById("trigger-upload")).attach("click", function() {
          manualUploader.uploadStoredFiles();
      });
</script>
