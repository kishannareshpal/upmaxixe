<?php $__env->startSection('title'); ?>
    UP Maxixe –> Quem Somos
<?php $__env->stopSection(); ?>

<?php
  /*
    Must init this variable..
    highlight @section('[mobile]menus')
  */
  $isActive = 'universidade';
  $isActiveSub = 'universidade.quemsomos';

  /*
    Must init this variable.
    Previous and Next Page.
  */
  // $previousPage = ""; -> NO PREVIOUS PAGE!!
  $nextPage = "Missão, Visão e Valores";
?>

<?php $__env->startSection('menus'); ?>
  <?php echo $__env->make('inc.menus', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php echo $__env->make('inc.mobilemenus', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>



<?php $__env->startSection('navbar-right-content'); ?>
  <?php if(auth()->guard()->check()): ?>
    <?php echo $__env->make('inc.navbar-user', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php endif; ?>
<?php $__env->stopSection(); ?>



<?php $__env->startSection('body'); ?>


    <!--Message-->
    <?php echo $__env->make('inc.messages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!--/Message-->

    <!--POSTS-->
    <section class="uk-section uk-section-muted">
        <div class="uk-container uk-container-small">
            <h2 class="uk-margin-remove-bottom uk-text-bold uk-h1 uk-margin-remove-adjacent uk-margin-remove-top">Quem Somos?</h2>
            <h4 class="uk-margin-remove-top">Historial da UP – UniSaF Maxixe</h4>
            <div class="uk-divider-small"></div>
        </div>

        <!-- posts -->
        <div class="uk-container uk-container-small uk-margin-medium">

          <img uk-img draggable="false" width="200" class="uk-align-center" data-src="<?php echo e(asset('images/logoup.png')); ?>">

          <p style="text-align: justify" class="uk-text-article uk-dropcap">
            A universidade pedagógica-Maxixe é fruto de um acordo de cooperação assinado em 28 de Dezembro de 2015 entre a Universidade Pedagógica, representada pelo Magnífico Reitor Prof. Doutor Carlos Machili e a congregação Sagrada Família, representada pelo Delegado Pe. Ezio Lorenzo Bono.
            Esta delegação conta com 11 cursos de licenciatura, nomeadamente: licenciatura em Ciências da Educação, Ensino de Português, Ensino de História e Geografia, Cont. Admin. E Finanças, Ensino Básico, Direito, Ensino de Filosofia, Psicologia Educacional e HIPOGEP.<br>

            <img uk-img class="uk-padding-small uk-margin-small-bottom" data-src="<?php echo e(asset('images/cover/6.jpg')); ?>" alt="">

            

            No seu primeiro ano de actividade contou com 175 estudantes inscritos e um corpo docente formando por 25 Profissionais do Ensino; Assistentes residentes na Maxixe, e os Docentes Regentes que se deslocavam periodicamente, de Maputo e Xai-Xai a Maxixe e vice-versa.<br>
            <br>
            No ano 2007, com a introdução dos novos cursos, tivemos 186 estudantes do primeiro ano, mas 175 do segundo ano, totalizando de 361 estudantes e uma equipa composta por 30 docentesa atendendo as turmas de 1º e 2º anos, manhã e tarde, respectivamente.<br>
            <br>
            No dia 14 de Dezembro de 2007, foi assinado uma adenda ao acordo de cooperação entre a UP e a UnisaF, representadas respectivamente, pelo Magífico Reitor Prof. Doutor José Rogério Uthui e o Director P.EzioLorenzo Bono. Esta adenda prolong ou cooperação por mais de 5 anos, até que a UniSaf pudesse Alcançar a sua idepedência.<br>
            <br>

            <img uk-img class="uk-padding-small uk-margin-small-bottom" data-src="<?php echo e(asset('images/cover/1.jpg')); ?>" alt="">

            No dia 6 de março de 2009 foi celebrada a primeira Cerimónia de Graduação, na qual 122 estudantes receberam o diploma de Bacharel em Ciências da Educação, Ensino de Português e Ensino de Inglês.<br>
            No mesmo ano académico foram introduzidos na UniSaf os primeiros 3 cursos Pós-laboral de Licenciatura em História Política e Gestão Pública, Ensino de Administraçáo Comércio e Finanças e Psicologia Escolar.<br>
            <br>
            Em 2010 Indroduzimos o Curso de Licenciatura em Ensino de Filosofia.<br>
            No Ano de 2011 foram criados 3 Centros de Recursos para Ensino a Distância (Mxixe, Inharrime, Quissico). Finalmente no ano académico de 2012 foi introduzido o 1º curso de Mestrado em Administração e Gestão dea Educação.
          </p>
        </div>
        <!-- /events -->
    </section>

    
    <?php echo $__env->make('inc.prev_next', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>