<?php
  $isActive = 'destaques';

?>

<?php $__env->startSection('title'); ?>
    Dashboard–>Destaques – UniSaF
<?php $__env->stopSection(); ?>

<?php $__env->startSection('navbar-right-content'); ?>
  <?php if(auth()->guard()->check()): ?>
    <?php echo $__env->make('inc.navbar-user', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php endif; ?>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('headcsslink'); ?>
  <script src="//code.jquery.com/jquery-latest.min.js"></script>

  <script type="text/javascript">
    var id;
  </script>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('jscript'); ?>

  <script type="text/javascript">
    // function launch_delete_sequence(){
      // location.href='{-{ route('eventos.index')}}/di/{-{$post->id}}/'+image_to_delete_modal;
    // }

    function deletemodal(currentID){
      UIkit.modal('#delete-modal').show();
      document.getElementById('deletemodalbody').innerHTML = '<div id="deletespinner" uk-spinner></div>';


      $.ajax({
          url: "<?php echo e(route('dashboard.destaques')); ?>/show/"+currentID,
          type: "GET",

          success: function(response){
              document.getElementById('deletespinner').classList.add('hidden');
              document.getElementById('deletemodalbody').innerHTML = <?php echo $__env->make('pages.dashboard.includes.deletemodal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>;
              // image found.
              var filename = response.url;
          }
      });
    }


    function editmodal(currentID){

      UIkit.modal('#edit-modal').show();
      document.getElementById('editmodalbody').innerHTML = '<div id="editspinner" uk-spinner></div>';

      $.ajax({
  				url: "<?php echo e(route('dashboard.destaques')); ?>/show/"+currentID,
  				type: "GET",
          // data: {
          //   id: currentID
          // },
          // dataType: 'json',
  				success: function(response){
            document.getElementById('editspinner').classList.add('hidden');
            if (response === "") {
              // oops no image found.
              document.getElementById('editmodalbody').innerHTML = <?php echo $__env->make('pages.dashboard.includes.editmodal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>;

            } else {
              // image found.
              var filename = response.url;
              var description;
              if (response.description !== null) {
                description = response.description;
              } else {
                description = "";
              }

              document.getElementById('editmodalbody').innerHTML = <?php echo $__env->make('pages.dashboard.includes.editmodal-image', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>;
            }


            var btn = document.getElementById('imgBtn');
            var bar = document.getElementById('progress');


            UIkit.upload('.js-upload', {
                url: "<?php echo e(route('dashboard.destaques')); ?>/ui/"+currentID,
                name: 'file',

                beforeSend: function (env) {
                    // console.log('beforeSend', arguments);
                    // console.log(env);
                    env.headers = {
                      'X-CSRF-TOKEN': document.querySelector('head > meta:nth-child(4)').content
                    }

                    // The environment object can still be modified here.
                    // var {data, method, headers, xhr, responseType} = environment;

                },
                beforeAll: function () {

                  bar.max = 0;
                  bar.value = 0;
                },

                load: function () {
                    bar.max = 0;
                    bar.value = 0;
                },

                error: function () {
                    console.log('error', arguments);
                },

                complete: function (e) {
                    if (e.responseText == 'file_unsupported') {
                      document.getElementById('msg').innerHTML = 'Tipo de ficheiro selecionado não suportado. Selecione uma imagem.';
                    } else if (e.responseText == 'request_failed') {
                      document.getElementById('msg').innerHTML = 'Ocorreu um erro. Tente novamente';
                    } else{
                      document.getElementById('editcancelbtn').setAttribute('disabled', '');
                      document.getElementById('msg').classList.remove('uk-text-warning');
                      document.getElementById('msg').classList.add('uk-text-success');
                      document.getElementById('msg').innerHTML = 'Imagem enviada com sucesso.';



                      if (document.getElementById('imagemactual')) {
                        document.getElementById('imagemactual').classList.add('uk-animation-fade');
                        document.getElementById('imagemactual').classList.add('uk-animation-reverse');
                        document.getElementById('imagemactual').classList.add('hidden');
                      }

                    }
                },
                //
                loadStart: function (e) {
                    // console.log('loadStart', arguments);
                    bar.max = e.total;
                    bar.value = e.loaded;
                },
                //
                progress: function (e) {
                    // console.log('progress', arguments);

                    bar.max = e.total;
                    bar.value = e.loaded;
                },
                //
                loadEnd: function (e) {
                    // console.log('loadEnd', arguments);
                    // btn.innerHTML = 'Max: ' + e.total + ' –– ' + e.loaded;
                    // console.log('loadEnd', e.total);

                    bar.max = e.total;
                    bar.value = e.loaded;
                },

                completeAll: function (e) {
                    if (e.responseText !== 'file_unsupported') {
                      btn.innerHTML = '<i uk-icon="check" class="uk-text-success" style="position: relative; bottom: 1px"></i>';
                      btn.classList.remove('uk-text-muted');
                      btn.classList.add('uk-text-success');

                    } else {
                      btn.innerHTML = '<i uk-icon="image" class="uk-text-muted" style="position: relative; bottom: 1px"></i>&nbsp Selecionar Imagem';
                      btn.classList.add('uk-text-muted');
                      btn.classList.remove('uk-text-success');
                      document.getElementById('msg').removeAttribute('hidden', '');
                    }
                }

            });
  				}
  		  });

    }

</script>
<?php $__env->stopSection(); ?>



<?php $__env->startSection('body'); ?>
<div class="uk-container uk-margin">
  <div class="uk-card uk-card-body uk-width-1-1">
    
    


    
    <a href="<?php echo e(route('dashboard.index')); ?>" class="uk-button uk-button-default uk-border-rounded uk-margin-bottom"> <i uk-icon="arrow-left" style="position: relative; bottom: 1px"></i> Voltar à Administração</a>

    


    <div>
      <div class="uk-card uk-card-small uk-card-default uk-border-rounded uk-card-body">
        <h3 class="uk-card-title uk-text-bold uk-margin-remove-bottom">Destaques</h3>
        <span class="uk-text-muted">Os destaques são imagens (com descrições) mostradas na página inicial ao visitante para que possa saber das últimas novidades na <i>UniSaF</i> (ou os momentos marcantes).</span>
        <hr>

        

        
        <small class="uk-text-muted uk-h7 uk-margin-remove-adjacent uk-margin-remove-top uk-margin-remove-bottom"><span uk-icon='arrow-down'></span><i>Todos os Destaques (Máx: 10 destaques)</i></small>
        <div class="uk-placeholder uk-padding-small">
          <?php echo $__env->make('inc.messages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

          <div class="uk-child-width-1-5@m" uk-grid>

            <?php $__currentLoopData = $images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <div>
                <div class="uk-card uk-card-default">
                    

                    <div style="background-image: url(<?php echo e(asset('/storage/destaques_image')); ?>/<?php echo e($image->id); ?>/<?php echo e($image->url); ?>); background-size: cover; background-position: center center;" class="uk-card-media-top">
                        <img style='height: 90px' alt="">
                    </div>

                    <div class="uk-card-body" style="overflow: scroll;">
                        <?php if(empty($image->description)): ?>
                          <small class="uk-text-muted">[Descrição não definida]</small>
                        <?php else: ?>
                          <p><?php echo str_limit($image->description, $limit = 100, $end = ' [...]'); ?></p>
                        <?php endif; ?>
                        
                        <ul class="uk-iconnav">
                          <li><button type="button" onclick="editmodal(<?php echo e($image->id); ?>)" uk-toggle="target: #edit-modal" id="edit-<?php echo e($image->id); ?>" uk-tooltip='title: Editar; pos: bottom' uk-icon="icon: pencil"></button></li>
                          <li><button type="button" onclick="deletemodal(<?php echo e($image->id); ?>)" id="remove-<?php echo e($image->id); ?>"  uk-toggle="target: #delete-modal" class="uk-text-danger" uk-tooltip='title: Remover destaque; pos:bottom' uk-icon="icon: trash"></button></li>
                        </ul>
                    </div>
                </div>
              </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            
            <?php if(count($images) < 10): ?>
              <?php
                if (!empty($last_image)) {
                  $last_image_id = $last_image->id+1;
                } else {
                  $last_image_id = 1;
                }
              ?>
              <div>
                <div class="uk-card uk-alert-primary hvr-grow-shadow uk-box-shadow-normal" style="cursor: pointer" onclick="editmodal(<?php echo e($last_image_id); ?>)" uk-toggle="target: #edit-modal" id="edit-<?php echo e($last_image_id); ?>">
                    <div class="uk-card-body">
                      <div class="uk-text-center uk-vertical-auto">
                        <span class="uk-text-bold" uk-icon="icon: plus"></span>
                        <small class="uk-text-bold">Adicionar Novo</small>
                      </div>
                    </div>
                </div>
              </div>

            <?php endif; ?>



          </div>

          <!-- Edit Modal -->
          <div id="edit-modal" uk-modal="bg-close: false">
              <div id="editmodalbody" class="uk-modal-dialog uk-modal-body">
                  <div id="editspinner" uk-spinner></div>
                  
                  

              </div>
          </div>

          <!-- Delete Modal -->
          <div id="delete-modal" uk-modal="bg-close: false">
              <div id="deletemodalbody" class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical">
                  
              </div>
          </div>
        </div>

      </div>
    </div>

	</div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>