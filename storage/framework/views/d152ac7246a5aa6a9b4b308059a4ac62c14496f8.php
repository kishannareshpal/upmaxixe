<?php $__env->startSection('title'); ?>
    UP Maxixe –> Revistas | Artigos
<?php $__env->stopSection(); ?>

<?php
  /*
    Must init this variable..
    highlight @section('[mobile]menus')
  */
  $isActive = 'pesquisaeextensao';
  $isActiveSub = 'pee.revistaseartigos';
?>

<?php $__env->startSection('menus'); ?>
  <?php echo $__env->make('inc.menus', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php echo $__env->make('inc.mobilemenus', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>



<?php $__env->startSection('navbar-right-content'); ?>
  <?php if(auth()->guard()->check()): ?>
    <?php echo $__env->make('inc.navbar-user', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php endif; ?>
<?php $__env->stopSection(); ?>



<?php $__env->startSection('body'); ?>

    <!--Message-->
    <?php echo $__env->make('inc.messages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!--/Message-->


    <section class="uk-section uk-section-muted uk-section-small">
        <div class="uk-container uk-container-small">
            <h2 class="uk-text-bold uk-margin-remove-bottom uk-h1 uk-margin-remove-adjacent uk-margin-remove-top">Revistas e Artigos</h2>
            <p class="uk-text-small uk-margin-remove-top">da UP Maxixe</>
            

            <section id="guti" class="uk-section uk-section-small">
                <div>
                  <div class="uk-card uk-card-default uk-grid-collapse uk-margin" uk-grid>
                    <div class="uk-card-media-left uk-cover-container uk-width-1-3@s">
                      <img uk-img data-src="<?php echo e(asset('images/pages/pesquisaeextensao/revistaseartigos/revistas/guti/guti_front.png')); ?>" alt="" uk-cover>
                      <canvas width="600" height="400"></canvas>
                    </div>
                    <div class="uk-background-default uk-width-expand@s">
                      <div class="uk-card-body">
                        <h2 class="uk-margin-remove-top">GUTI – Letras e Humanidade</h2>
                        <hr class="uk-divider-small">
                        <i>Volume 1 – 2018</i>
                        <p class="uk-text-justify">
                          Ao iniciar mais um ano académico, a inédita revista científica da UP-Maxixe sai a público, como um pretexto para aglutinar pesquisadores que se ocupam da questão humana nas suas variadas declinações. A revista <i><b>Guti</b></i>, que em Gitonga significa <i>‘sabedoria’</i>, procura não só contribuir na divulgação de estudos sobre linguística, literatura e filosofia, bem como propiciar a divulgação de trabalhos inéditos de análise da história e cultura moçambicana a partir de diferentes disciplinas e o debate acerca da educação, tendo como eixo a problemática identidade/universalidade.
                        </p>  
                        <div>
                          <a href="https://www.upmaxixe.ac.mz/storage/files/1/PDFs/Guti - Letras e Humanidade.pdf" target="_blank" style="margin-bottom: 5px" class="uk-button uk-button-primary uk-button-small uk-border-rounded"> <img src="<?php echo e(asset('images/icons/pdf.svg')); ?>" width="18" height="18" uk-svg alt="Download PDF">&nbsp Baixar a última edição <span style="text-transform: none;">(1a)</span></a><br>
                          <a href="#" class="">Call for papers</a>  |  <a href="#" class="">Template</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                
            </section>
           
        </div>
    </section>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>