<?php $__env->startSection('title'); ?>
    UP Maxixe –> Faculdades
<?php $__env->stopSection(); ?>

<?php
  /*
    Must init this variable..
    this serves to know on which page the user to highlight
    the correspondent MENU (NAVBAR) item down bellow. in @section('menus')
  */

  $isActive = 'faculdades';
?>

<?php $__env->startSection('menus'); ?>
  <?php echo $__env->make('inc.menus', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php echo $__env->make('inc.mobilemenus', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>



<?php $__env->startSection('navbar-right-content'); ?>
  <?php if(auth()->guard()->check()): ?>
    <?php echo $__env->make('inc.navbar-user', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php endif; ?>
<?php $__env->stopSection(); ?>



<?php $__env->startSection('body'); ?>


    <!--Message-->
    <?php echo $__env->make('inc.messages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!--/Message-->
    <section class="uk-section uk-section-small">
        <div class="uk-container uk-container-small">
          <h1 class="uk-text-bold uk-h1 uk-margin-remove-adjacent uk-margin-remove-top">Faculdades</h1>

          <div class="uk-background-secondary uk-border-rounded uk-box-shadow-small uk-margin" uk-navbar="mode: click; dropbar: true; dropbar-mode: push">
              <div class="uk-navbar-center">

                  <ul class="uk-navbar-nav">
                      <li>
                          <a href="#" uk-icon="arrow-down">de Ciências</a>
                          <div class="uk-navbar-dropdown">
                              <ul class="uk-nav uk-navbar-dropdown-nav">
                                  <li><a href="#fcs" uk-scroll>Ciências Sociais</a></li>
                                  <hr class="uk-margin-remove">
                                  <li><a href="#fcep" uk-scroll>Ciências de Educação</a></li>
                                  <hr class="uk-margin-remove">
                                  <li><a href="#fclca" uk-scroll>Ciências de Linguagem, Comunicação e Arte</a></li>
                              </ul>
                          </div>
                      </li>

                      <li>
                          <a href="#" uk-icon="arrow-down">Escola Superior</a>
                          <div class="uk-navbar-dropdown">
                              <ul class="uk-nav uk-navbar-dropdown-nav">
                                  <li><a href="#escog" uk-scroll>Contabilidade e Gestão</a></li>
                                  <hr class="uk-margin-remove">
                                  <li><a href="#estec" uk-scroll>Técnica</a></li>
                                  <hr class="uk-margin-remove">
                                  <li><a href="#cead" uk-scroll>Ensino à Distância</a></li>
                              </ul>
                          </div>
                      </li>

                      <li><a href="#mestrado" uk-scroll>Mestrado em Educação</a></li>
                  </ul>

              </div>
          </div>

        </div>
    </section>

    
    <section class="uk-section uk-section-small uk-box-shadow-medium uk-section-muted">
        <div class="uk-container uk-container-small">
            <h3 class="uk-text-bold uk-margin-remove-bottom uk-margin-remove-top">Faculdades de Ciências</h3>
            <div class="uk-placeholder">
              <h3 id="fcs" class="uk-margin-remove-top uk-text-muted uk-text-bold">Ciências Sociais (FCS)</h3>

              <img uk-img class="uk-border-rounded" data-src="<?php echo e(asset('images/pages/faculdades/cienciassociais.jpg')); ?>" alt="">

              <p class="uk-dropcap uk-text-justify">
                Sem as Ciências Sociais e Humanas, não temos os ingredientes necessários para criar valor e gerar decisões razoáveis para responder às necessidades da sociedade. Nesta área do saber, encontramos o conhecimento que nos ajuda a colocar questões, a pensar criativamente, a raciocinar de forma adequada e inovadora. As Ciências Sociais estudam os processos envolvidos na definição de indivíduos enquanto agentes guiados por valores: como agem, como interagem, como criam. Nestas áreas, entendemos o passado e as culturas diferentes para interpretar o presente e planear o futuro com informação.
                No Departamento de Ciências Sociais comprometemo-nos com a produção de conhecimento, com a qualificação dos estudantes e com a transferência de conhecimento nos diferentes domínios desta área do saber. Nela estudam e investigam docentes e estudantes, integrados ou colaborando com os diversos centros de pesquisa da Universidade Pedagógica no desenvolvimento de projectos de investigação.
              </p>

              <h5 class="uk-text-bold uk-margin-remove-bottom">Plano de Estudo: </h5>
              <ul class="uk-list uk-list-bullet uk-margin-remove-top">
                  <li><span class="uk-label uk-label-warning">História</span></li>
                  <li><span class="uk-label uk-label-warning">História Política e Gestão Pública</span></li>
                  <li><span class="uk-label uk-label-warning">Filosofia</span></li>
                  <li><span class="uk-label uk-label-warning">Direito</span></li>
              </ul>
            </div>


            <div class="uk-placeholder">
              <h3 id="fcep" class="uk-margin-remove-top uk-text-muted uk-text-bold">Ciências de Educação e Psicologia (FCEP)</h3>

              <img uk-img class="uk-border-rounded" data-src="<?php echo e(asset('images/pages/faculdades/cienciasdeeducacao.jpg')); ?>" alt="">

              <p class="uk-dropcap uk-text-justify">
                A educação constitui uma das tarefas fundamentais de todas as sociedades e temm diferentes formas de realização, incluindo a educação formal, não – formal e informal.
                Neste sentido, o departamento de Ciências de Educação e Psicologia tem a missão de formar professores qualificados para docência das diversas matérias leccionadas desde o ensino primário bem como a formação de outros profissionais da Educação que possam questionar as práticas educativas, orientá-las com referência a abordagens científicas e teorias educacionais diversas para melhor entendimento da educação e, desta forma, elevar os níveis de cidadania moçambicana, a partir da provisão de serviços de educação e formação de qualidade e extensivos a todos os que deles tem direito.
                Deste modo, para responder a esta missão, o Departamento de Ciências de Educação e Psicologia funciona com quatro cursos nomeadamente, licenciatura em Ciências da Educação (ministrado no regime laboral), licenciatura em Psicologia Educacional (ministrado no regime pós-laboral), licenciatura em Ensino Básico (ministrado à distância) e licenciatura em Administração e Gestão Escolar (ministrado à distância).
              </p>

              <h5 class="uk-text-bold uk-margin-remove-bottom">Plano de Estudo: </h5>
              <ul class="uk-list uk-list-bullet uk-margin-remove-top">
                  <li><span class="uk-label uk-label-warning">Psicologia Educacional</span></li>
                  <li><span class="uk-label uk-label-warning">Ensino Básico</span></li>
                  <li><span class="uk-label uk-label-warning">Admin. e Gestão Escolar</span></li>
              </ul>
            </div>


            <div id="fclca" class="uk-placeholder">
              <h3 class="uk-margin-remove-top uk-text-muted uk-text-bold">Ciências de Linguagem, Comunicação e Arte (FCLCA)</h3>
              <img uk-img width="100%" class="uk-border-rounded" data-src="<?php echo e(asset('images/pages/faculdades/cienciasdecomunicacao.jpg')); ?>" alt="">
              <p class="uk-dropcap uk-text-justify">
                A Faculdade de Ciências de Linguagem Comunicação e Artes (FCLCA) disponibiliza, nesta Delegação da UP - Maxixe, pelo menos dois cursos nomeadamente, o de Português e o de Inglês. É do Curso de Ensino de Língua Portuguesa que queremos apresentar.
                O curso de Licenciatura em Ensino de Português já é ministrado, nesta delegação, há nove anos, idade que coincide com a criação da Universidade Pedagógica Sagrada Família (UniSaF).
                Com a introdução do novo currículo universitário, este curso tem saídas (Minors) em Inglês e Francês, o que habilita os formandos a uma certa polivalência no campo de emprego. Os efectivos quer docentes quer discentes, esses, não param de crescer ano após ano e, consequentemente, as graduações caracterizam a conclusão do curso. Só para ver, em 2013 foram graduados 75 finalistas (22 homens e 53 mulheres) dos quais 70 optaram por via de monografia científica, cuja qualidade ostentada não é de minimizar.
              </p>

              <h5 class="uk-text-bold uk-margin-remove-bottom">Plano de Estudo: </h5>
              <ul class="uk-list uk-list-bullet uk-margin-remove-top">
                  <li><span class="uk-label uk-label-warning">Ensino de Inglês</span></li>
                  <li><span class="uk-label uk-label-warning">Ensino de Português</span></li>
              </ul>
            </div>


        </div>
    </section>


    
    <section class="uk-section uk-box-shadow-medium uk-section-small">
        <div class="uk-container uk-container-small">
            <h3 class="uk-text-bold uk-margin-remove-bottom uk-margin-remove-top">Escola Superior</h3>
            <div id="escog" class="uk-placeholder">
              <h3 class="uk-margin-remove-top uk-text-muted uk-text-bold">Contabilidade e Gestão (ESCOG)</h3>
              <img uk-img width="100%" class="uk-border-rounded" data-src="<?php echo e(asset('images/pages/faculdades/contabilidadeegestao.jpg')); ?>" alt="">
              <p class="uk-dropcap uk-text-justify">
                A Escola Superior de Contabilidade e Gestão (ESCOG), pertencente à Universidade Pedagógica Moçambique (UP), disponibiliza na Delegação da UP – Maxixe através do DEPARTAMENTO DE CONTABILIDADE E GESTÃO, os cursos de Gestão de Comércio e, a partir deste ano, de Gestão dos Recursos Humanos.
                Por inerência da formação do Chefe de Departamento foi incluído neste Departamento também o curso de Engenharia de Construção Civil, pertencente à Escola Superior Técnica (ESTEC), curso iniciado também em 2015.
                O Curso de licenciatura em Gestão de Comércio prevê actualmente duas saídas profissionais: Contabilidade e Marketing. Com a liberalização dos MINORS se espera ampliar as especializações disponíveis para os estudantes.
                O curso registou um crescimento ao longo dos anos, podendo contar agora num universo de 23 docentes, dos quais cerca de 40% já possui o grau de Mestre e cerca do 40% está no programa de Mestrado. No que diz respeito ao corpo estudantil no ano 2015 conta com 200 estudantes.
                No ano 2015 houve a primeira “colheita”, fruto do trabalho feito nos anos antecedentes, tendo os primeiros estudantes defendido com sucesso a própria monografia científica, tendo adquirido o grau tanto desejado de Licenciado.
                O Curso de licenciatura em Gestão de Recursos Humanos iniciou este ano 2015, com uma satisfatória procura pelo mercado estudantil, contando actualmente com uma turma de 42 estudantes e um universo de 7 docentes, dos quais 3 mestres e os restantes 4 no programa de mestrado.
                O maior desafio deste ano foi a introdução do curso de Engenharia de Construção Civil, curso muito exigente mas no qual a Universidade decidiu apostar tecendo relações com as Universidades estrangeiras, tendo actualmente em conversações com a Universidade Politécnica de Catalunha (Espanha), a Universidade dos Estudos de Brescia e o Politécnico de Milão (Itália).
                Actualmente contamos com um universo estudantil de 58 estudantes, e está a decorrer o processo de efectivação de mais 3 docentes para o curso, além do apoio que o curso vai ter a partir do 2º semestre com a presença de um Mestre em Engenharia Civil formado na Itália e no Portugal.
              </p>

              <h5 class="uk-text-bold uk-margin-remove-bottom">Plano de Estudo: </h5>
              <ul class="uk-list uk-list-bullet uk-margin-remove-top">
                  <li><span class="uk-label uk-label-warning">Gestão de Comércio</span></li>
                  <li><span class="uk-label uk-label-warning">Gestão de Recursos Humanos</span></li>
              </ul>
            </div>


            <div id="estec" class="uk-placeholder">
              <h3 class="uk-margin-remove-top uk-text-muted uk-text-bold">Técnica (ESTEC)</h3>
              <p class="uk-dropcap uk-text-justify">

              </p>

              <h5 class="uk-text-bold uk-margin-remove-bottom">Plano de Estudo: </h5>
              <ul class="uk-list uk-list-bullet uk-margin-remove-top">
                  <li><span class="uk-label uk-label-warning">Engenharia Civil</span></li>
              </ul>
            </div>


            <div id="cead" class="uk-placeholder">
              <h3 class="uk-margin-remove-top uk-text-muted uk-text-bold">Ensino à Distância (CEAD)</h3>
              <img uk-img width="100%" class="uk-border-rounded" data-src="<?php echo e(asset('images/pages/faculdades/ensinoadistancia.jpg')); ?>" alt="">
              <p class="uk-dropcap uk-text-justify">
                Os principais documentos programáticos do governo, com destaque para o Plano de Acção para a Redução da Pobreza Absoluta (PARPA), o plano quinquenal do governo (2005 - 2009) e o Plano Estratégico da Educação, reconhecem que a Educação à Distância é crucial para assegurar a formação contínua e em serviço de professores e outros técnicos para o desenvolvimento do pais.
              </p>
              <br>
              <h5 class="uk-text-bold uk-margin-remove">Objectivos da Educação à Distância na UP: </h5>
              <p class="uk-margin-remove-top">
                Proporcionar maior equidade na área de formação de professores em todo o território nacional; Promover cursos de formação inicial e em serviço aos professores do Ensino Secundário Geral e outros quadros da Educação; Contribuir com a melhoria da qualidade de ensino através da formação inicial e em serviço de professores sem retirá-los das zonas de origem ou de trabalho; Reduzir progressivamente os custos de formação de quadros da educação; Responder à crescente demanda de acesso ao ensino superior no país; Aumentar o número de graduados nos cursos oferecidos pela instituição.
              </p>


              <h5 class="uk-text-bold uk-margin-remove-bottom">Plano de Estudo: </h5>
              <ul class="uk-list uk-list-bullet uk-margin-remove-top">
                  <li><span class="uk-label uk-label-warning">Administração e Gestão Escolar</span></li>
                  <li><span class="uk-label uk-label-warning">Ensino de Inglês</span></li>
                  <li><span class="uk-label uk-label-warning">Ensino Básico</span></li>
              </ul>
            </div>

        </div>
    </section>



    
    <section class="uk-section uk-box-shadow-small uk-section-muted uk-section-small">
        <div class="uk-container uk-container-small">
            <h3 class="uk-text-bold uk-margin-remove-bottom uk-margin-remove-top">Mestrado em Educação</h3>
            <div id="mestrado" class="uk-placeholder">
              <p class="uk-dropcap uk-text-justify">
                A UP-Maxixe (UniSaF), face ao decreto ministerial que exige até o 2015 o nível de mestrado para os docentes universitários, pretende lançar para os anos académicos 2014-2015 Mestrados em “Educação com especialização” conforme o modelo apresentado no “livro azul” da UP: “Cursos de Mestrado em Educação” de 2008 e “Cursos de Pós-graduação/Mestrado e Doutoramento” de 2013.
                A Iª Sessão de 2014 do Conselho Académico da UP aprovou o lançamento deste mestrados com múltiplas saídas, para a UP-Maxixe.
                Este mestrado está reservado principalmente para: - os docentes efectivos da UP-Maxixe - os docentes não efectivos da UP-Maxixe - outros licenciados externos (se sobrarem vagas).
              </p>

              <img uk-img width="100%" class="uk-border-rounded" data-src="<?php echo e(asset('images/pages/faculdades/mestradoemeducacao.jpg')); ?>" alt="">

              <h5 class="uk-text-bold uk-margin-remove-bottom">Plano de Estudo: </h5>
              <ul class="uk-list uk-list-bullet uk-margin-remove-top">
                  <li><span class="uk-label uk-label-warning">Mestrado em Educação (com especialização)</span></li>
              </ul>
            </div>

        </div>
    </section>





    
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>