<?php $__env->startSection('title'); ?>
    UP Maxixe –> CEI -> Editais
<?php $__env->stopSection(); ?>

<?php
  /*
    Must init this variable..
    highlight @section('[mobile]menus')
  */
  $isActive = 'cei';
  $isActiveSub = 'cei.int';
  $isActiveSubSub = 'cei.int.editais';

?>

<?php $__env->startSection('menus'); ?>
  <?php echo $__env->make('inc.menus', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php echo $__env->make('inc.mobilemenus', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>



<?php $__env->startSection('navbar-right-content'); ?>
  <?php if(auth()->guard()->check()): ?>
    <?php echo $__env->make('inc.navbar-user', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php endif; ?>
<?php $__env->stopSection(); ?>



<?php $__env->startSection('body'); ?>
  <section class="uk-section uk-section-small">
    <div class="uk-container uk-container-small">
        <h5 class="uk-margin-remove-bottom uk-text-bold uk-h5 uk-margin-remove-adjacent uk-text-muted uk-margin-remove-top">Cooperação e Internacionalização</h5>
        <h1 class="uk-margin-small-bottom uk-text-bold uk-h1 uk-margin-remove-adjacent uk-margin-remove-top">
          Editais
          <?php if(auth()->guard()->check()): ?>
            <a href="<?php echo e(route('editais.create')); ?>" class="hvr-grow-shadow uk-dark uk-button uk-button-primary uk-border-rounded uk-box-shadow-small uk-float-right" style="position: relative; top: 6.4px;" uk-icon='plus' uk-tooltip="title: Publicar Novo Edital; delay: 300"></a>
          <?php endif; ?>
        </h1>
        <?php echo $__env->make('inc.messages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    </div>
  </section>

  
  <section class="uk-section uk-section-muted">
    <div class="uk-container uk-container-small">

      <?php if(count($editais) > 0): ?>
        <?php $__currentLoopData = $editais; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $edital): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

          <?php
            # localize month name to pt.. not the best way, i know... but whatever!
              // retrieve raw edital month
              $monthRaw = $edital->created_at->month;
              // finally translate that shit
              $ptmonth = $months[$monthRaw];
          ?>


          <article class="uk-section uk-section-small uk-padding-remove-top">
            <header>
              <h2 class="uk-margin-remove-adjacent uk-text-bold uk-margin-small-bottom"><a title="Continuar a ler: '<?php echo e($edital->title); ?>'" class="uk-link-heading" href="<?php echo e(route('editais.index')); ?>/<?php echo e($edital->id); ?>"><?php echo e($edital->title); ?></a></h2>
              <p class="uk-article-meta">
                <span style="position: relative; bottom: 1px" uk-icon="calendar"></span>
                Postado as <?php echo e($edital->created_at->format('H:i A')); ?>, no dia <?php echo e($edital->created_at->day); ?> de <?php echo e($ptmonth); ?> de <?php echo e($edital->created_at->year); ?>

                <?php if($edital->created_at->diffInDays(Carbon\Carbon::now()) < 90): ?>
                  <span title="Postado a menos de 3 meses!" style="position: relative; bottom: 1px" class="uk-label uk-label-success uk-border-rounded"><img src="<?php echo e(asset('images/icons/blue_dot.svg')); ?>" style="position: relative; bottom: 1px" uk-svg width="15" height="15"> Recente</span>
                  
                <?php endif; ?>
              </p>
            </header>

            <p style="overflow-wrap: break-word"><?php echo str_limit(strip_tags($edital->body), $limit = 300, $end = ' [...] '); ?></p>
            <a class="uk-button uk-button-default uk-button-small hvr-fade" style="border: 1px solid" href="<?php echo e(route('editais.index')); ?>/<?php echo e($edital->id); ?>" title="Saber Mais...">Saber mais...</a>
            <?php if(auth()->guard()->check()): ?> 
              <div class="uk-inline uk-padding-small">
                  <span style="cursor: pointer" class="uk-icon-button uk-background-default" uk-icon="more-vertical"></span>
                  <div class="uk-background-default uk-border-rounded" uk-dropdown="mode: click; animation: uk-animation-slide-bottom-small; duration: 150" >
                    <small class="uk-text-bold uk-text-muted">Mais Opções: </small><br>
                    <div class="uk-grid uk-grid-small uk-light" uk-grid>
                      <div>
                        <a href="<?php echo e(route('editais.index')); ?>/<?php echo e($edital->id); ?>/edit" class="uk-icon-button uk-background-secondary uk-button-secondary" uk-icon="pencil"></a>
                      </div>

                      <div>
                        <?php echo Form::open(['action' => ['IntEditaisController@destroy', $edital->id], 'method'=>'POST']); ?>

                          <?php echo e(Form::hidden('_method', 'DELETE')); ?>

                          <button type="submit" class="uk-icon-button uk-text-danger uk-button-secondary" uk-icon="trash"></button>
                          
                        <?php echo Form::close(); ?>

                      </div>
                    </div>

                  </div>
              </div>
            <?php endif; ?>
            <hr>
          </article>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        
      <?php else: ?>
        <div class="uk-margin">
          <div class="uk-card uk-card-small uk-card-default uk-card-body">
            <h6 class="uk-h6 uk-text-muted">
              Nenhum edital postado até então.
            </h6>
          </div>
        </div>
      <?php endif; ?>
    </div>
  </section>
<?php $__env->stopSection(); ?>




<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>