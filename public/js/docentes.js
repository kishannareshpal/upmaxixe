// the input to type the obra's refference
var obraInput = document.getElementById('obraInput');
// the added obra's list
var obras_ul = document.getElementById('obras_ul');

var ob;

/*
  function to add obras
*/
function addObra(){
  // get the text from the input
  obras = obraInput.value;

  // create an 'li'
  var li = document.createElement("li");
  var button = document.createElement("button");
  button.setAttribute('class', 'uk-invisible-hover uk-margin-small-right uk-text-danger uk-icon-button');
  button.setAttribute('onclick', 'removeObra(this)');
  button.setAttribute('uk-icon', 'trash');
  button.setAttribute('type', 'button');

  var hiddenInput = document.createElement("input");
  hiddenInput.setAttribute('type', 'hidden');
  hiddenInput.setAttribute('name', 'obras[]');
  hiddenInput.setAttribute('value', obras);
  // <input type='hidden' name='fruits[]' value='Apple'/>

  // var deleteButton = "<button type='button' onclick='removeObra(this)' class='uk-invisible-hover uk-margin-small-right uk-text-danger uk-icon-button' uk-icon='trash'></button>"
  li.appendChild(hiddenInput);
  li.appendChild(document.createTextNode(obras + " "));
  li.appendChild(button);
  // var final_li = li.innerHtml = obras + deleteButton;

  // add the obra ('li') to the list
  obras_ul.appendChild(li);
  obraInput.value = '';
};


/*
  function to add obras
  -> obra is the "li" that will be removed
*/
function removeObra(obra){
  obra.parentNode.classList = "uk-animation-fade uk-animation-fast uk-animation-reverse"
  setTimeout(function () {
    obra.parentNode.parentNode.removeChild(obra.parentElement);
  }, 105);
}
