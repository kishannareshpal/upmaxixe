// /**
//  * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
//  * For licensing, see LICENSE.md or http://ckeditor.com/license
//  */

// CKEDITOR.editorConfig = function( config ) {
// 	// Define changes to default configuration here.
// 	// For complete reference see:
// 	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

// 	// The toolbar groups arrangement, optimized for two toolbar rows.
// 	config.toolbarGroups = [
// 		{ name: 'basicstyles'},
// 		{ name: 'cleanup' },
// 		// { name: 'links' },
// 		{ name: 'styles' },
// 		{ name: 'paragraph',   groups: [ 'list', 'align', 'bidi' ] },
// 		{ name: 'tools' },
// 		{ name: 'undo' },
// 		// { name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
// 		// { name: 'insert' },
// 		// { name: 'forms' },
// 		// { name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
// 		// { name: 'others' },
// 		// '/', // this adds a <new line> on the toolbar items
// 		// { name: 'colors' },
// 		// { name: 'about' }
// 	];

// 	// Remove some buttons provided by the standard plugins, which are
// 	// not needed in the Standard(s) toolbar.
// 	config.removeButtons = 'Anchor,Marker,Format';

// 	// Set the most common block elements.
// 	config.format_tags = 'p';

// 	// Simplify the dialog windows.
// 	config.removeDialogTabs = 'image:advanced;link:advanced;link:target';

// 	// Simplify more by removing some plugins
// 	config.removePlugins = 'elementspath';

// 	// localize the editor's ui
// 		// fallback locale
// 		config.defaultLanguage = 'en';

// 	// change to Portuguese
// 	config.language = 'pt';

// 	// automatically resize the window on overflow-y
// 	config.extraPlugins = 'autogrow';
// 	config.autoGrow_minHeight = 250;
// 	// config.autoGrow_maxHeight = 250;

// 	config.toolbarCanCollapse = true;
// 	// config.skin = 'flat';
// };

// // Simplify the links dialog menu even more, by removing some extra features
// CKEDITOR.on( 'dialogDefinition', function(ev)
// {
//     // Take the dialog name and its definition from the event
//     // data.
//     var dialogName = ev.data.name;
//     var dialogDefinition = ev.data.definition;
//     // Check if the definition is from the dialog we're
//     // interested on (the "Link" dialog).
//     if (dialogName == 'link'){
//         // Get a reference to the "Link Info" tab.
//         var infoTab = dialogDefinition.getContents( 'info' );
//         infoTab.remove( 'linkType' );
// 		infoTab.remove( 'protocol' );
// 		infoTab._.dialog.resize(245, 150);
//     }
// });


// // CKEDITOR.stylesSet.add( 'my_styles', [
// //     // Block-level styles
// //     { name: 'Blue Title', element: 'h2', styles: { 'color': 'Blue' } },
// //     { name: 'Red Title' , element: 'h3', styles: { 'color': 'Red' } },
// //
// //     // Inline styles
// //     { name: 'CSS Style', element: 'span', attributes: { 'class': 'my_style' } },
// //     { name: 'Marker: Yellow', element: 'span', styles: { 'background-color': 'Yellow' } }
// // ] );


/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For complete reference see:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

	// The toolbar groups arrangement, optimized for two toolbar rows.
	config.toolbarGroups = [
		// { name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },	
		{ name: 'paragraph',   groups: [ 'list', 'blocks', 'align', 'bidi' ] },
		{ name: 'editing',     groups: [ 'find', 'selection'] },
		{ name: 'insert' },		
		{ name: 'links' },
		{ name: 'forms' },
		{ name: 'tools' },
		{ name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'others' },
	];

	// Remove some buttons provided by the standard plugins, which are
	// not needed in the Standard(s) toolbar.
	config.removeButtons = 'Anchor, Marker';



	// localize the editor's ui
	config.language = 'pt';
	// fallback locale
	config.defaultLanguage = 'en';
	
	// Set the most common block elements.
	config.format_tags = 'p;h2;pre';
	
	// automatically resize the window on overflow-y
	config.extraPlugins = 'copyformatting,autogrow,textindent,ckawesome,autolink,image2,justify,preview,table';

	config.autoGrow_minHeight = 250;
	// config.autoGrow_maxHeight = 250;

	config.removePlugins = 'elementspath';


	// Simplify the dialog windows.
	config.removeDialogTabs = 'link:advanced';

	config.skin = 'office2013';
};
