@if ($paginator->hasPages())
  <div style="border-radius: 10px" class="uk-text-center">
    <small class="uk-dark uk-text-muted uk-text-bold uk-margin-remove">Mostrando {{$paginator->perPage()}} resultados por página</small>
    <ul class="uk-margin-small-top uk-pagination uk-flex-center">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
          <li class="uk-margin-left uk-disabled" ><span class="uk-text-center" uk-icon="chevron-left" style="position: relative; top: 2px"></span></li>

        @else
            <li class="uk-margin-left"><a class="uk-text-center" uk-icon="chevron-left" style="position: relative; top: 2px" href="{{ $paginator->previousPageUrl() }}"></a></li>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li style="margin: 3px" class="uk-disabled"><span>{{$element}}</span></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li style="margin: 3px" class="uk-margin-remove uk-disabled"><span class="uk-text-center"><h4 class="uk-text-primary">{{ $page }}</h4></span></li>
                    @else
                        <li style="margin: 3px" class="uk-margin-remove"><a class="uk-text-center" href="{{ $url }}"><h4 class="uk-light">{{ $page }}</h4></a></li>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            {{-- <li class="page-item"><a class="page-link" href="{{ $paginator->nextPageUrl() }}" rel="next">&rsaquo;</a></li> --}}
            <li class="uk-margin-right"><a class="uk-text-center" uk-icon="chevron-right" style="position: relative; top: 2px" href="{{ $paginator->nextPageUrl() }}"></a></li>
        @else
            <li class="uk-margin-right uk-disabled"><span class="uk-text-center" style="position: relative; top: 2px" uk-icon="chevron-right" style="position: relative; top: 2px"></a></li>
            {{-- <li class="page-item disabled"><span class="page-link">&rsaquo;</span></li> --}}
            {{-- <li class="uk-disabled"><span uk-icon="chevron-right" class="uk-button uk-button-default"></span></li> --}}
        @endif
    </ul>
  </div>
@endif
