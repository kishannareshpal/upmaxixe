@extends('layouts.app')

@php
  /*
    Must init this variable..
    this serves to know on which page the user to highlight
    the correspondent MENU (NAVBAR) item down bellow. in @section('menus')
  */

  $isActive = 'register';
@endphp

@section('title')
	UP Maxixe -> Registar
@endsection

@section('body')
<div class="uk-container uk-margin uk-flex uk-flex-center">
	<div class="uk-card uk-card-secondary uk-width-1-2@s uk-box-shadow-large">
		<div class="uk-card-header">
			<h3 class="uk-card-title uk-margin-remove uk-text-bold">Registar</h3>
			<small class="uk-text-muted">Acesso restrito à <strong>Administração da UP Maxixe</strong> –– <a href="{{route('home')}}">Voltar</a></small>
			<hr>
		</div>
		<form class="uk-form-stacked" method="POST" action="{{ route('register') }}" novalidate>
			{{ csrf_field() }}
			<div class="uk-card-body">
				<div class="uk-margin">
					<label class="uk-form-label {{ $errors->has('name') ? ' uk-text-danger' : '' }}">
						Nome
					</label>
					<div class="uk-width-1-1 uk-inline">
						<span class="uk-form-icon {{ $errors->has('name') ? ' uk-text-danger' : '' }}" uk-icon="icon: user">
						</span>
						<input id="name" type="name" class="uk-input {{ $errors->has('name') ? ' uk-form-danger' : '' }}"
						 name="name" value="{{ old('name') }}" required autofocus>
					</div>
					@if ($errors->has('email'))
					<span class="uk-text-small uk-text-danger">{{ $errors->first('email') }}</span>
					@endif
				</div>
				<div class="uk-margin">
					<label class="uk-form-label {{ $errors->has('email') ? ' uk-text-danger' : '' }}">
						Email
					</label>
					<div class="uk-width-1-1 uk-inline">
						<span class="uk-form-icon {{ $errors->has('email') ? ' uk-text-danger' : '' }}" uk-icon="icon: user">
						</span>
						<input id="email" type="email" class="uk-input {{ $errors->has('email') ? ' uk-form-danger' : '' }}"
						 name="email" value="{{ old('email') }}" required>
					</div>
					@if ($errors->has('email'))
					<span class="uk-text-small uk-text-danger">{{ $errors->first('email') }}</span>
					@endif
				</div>
				<div class="uk-margin">
					<label class="uk-form-label {{ $errors->has('password') ? ' uk-text-danger' : '' }}">
						Password
					</label>
					<div class="uk-width-1-1 uk-inline">
						<span class="uk-form-icon {{ $errors->has('password') ? ' uk-text-danger' : '' }}"
						 uk-icon="icon: lock">
						</span>
						<input id="password" type="password" class="uk-input {{ $errors->has('password') ? ' uk-form-danger' : '' }}"
						 name="password" required>
					</div>
					@if ($errors->has('password'))
					<span class="uk-text-small uk-text-danger">{{ $errors->first('password') }}</span>
					@endif
				</div>
				<div class="uk-margin">
					<label class="uk-form-label">
						Confirmar Password
					</label>
					<div class="uk-width-1-1 uk-inline">
						<span class="uk-form-icon" uk-icon="icon: lock">
						</span>
						<input id="password" type="password" class="uk-input" name="password_confirmation"
						 required>
					</div>
				</div>
			</div>
			<div class="uk-card-footer uk-clearfix">
				<button type="submit" class="uk-button uk-background-primary uk-box-shadow-medium">
					Registar
				</button>
			</div>
		</form>
	</div>
</div>
@endsection
