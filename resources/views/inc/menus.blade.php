<!--NAVBAR / MENUS-->


@empty($isActiveSub)
    @php
        # Dont make isActiveSub important
        $isActiveSub = "";
    @endphp
@endempty

@empty($isActiveSubSub)
    @php
        # Dont make isActiveSubSub important
        $isActiveSubSub = "";
    @endphp
@endempty

<style>
    li a {
      font-size: 14px !important;
      text-transform: none !important;
    }

    .uk-navbar-nav > li.uk-active > a {
      color: rgb(21, 136, 233) !important;
    }

    .uk-navbar-dropdown-nav > li.uk-active > a {
      color: rgb(21, 136, 233) !important;
      font-weight: bold;
    }
</style>


<section uk-parallax="bgy: -50" class="uk-section uk-section-small uk-visible@m uk-background-cover" style="background-image: url({{asset('images/cover.jpg')}});">
    {{-- <img src="{{asset('images/cover.jpg')}}" alt="" uk-cover> --}}
    <div class="uk-container uk-container-large">
        <div class="uk-grid uk-grid-large" uk-grid>
            <div class="uk-width-expand">
                <div class="uk-visible@m" style="border-radius: 5px;" uk-navbar="mode: click; dropbar-mode: push;">
                    <div class="uk-border-rounded uk-box-shadow-small uk-align-center uk-navbar-center uk-background-default">
                        <ul class="uk-navbar-nav uk-flex-wrap">
                            {{-- uk-active / --}}
                            <li class="@if ($isActive == "inicio") uk-active @endif uk-button uk-button-text"><a href="{{route('home')}}">Início</a></li>

                            <li class="@if ($isActive == "universidade") uk-active @endif uk-button uk-button-text">
                                <a href="#">Universidade <i uk-icon="icon: chevron-down"> </i></a>
                                <div class="uk-navbar-dropdown">
                                    <ul style="text-align: left" class="uk-nav uk-navbar-dropdown-nav">
                                        <li class="@if ($isActiveSub == "universidade.quemsomos") uk-active @endif"><a href="{{route('quemsomos')}}">Quem Somos</a></li>
                                        <li class="uk-nav-divider"></li>
                                        <li class="@if ($isActiveSub == "universidade.missaoevalores") uk-active @endif"><a href="{{route('missaoevalores')}}">Missão, Visão e Valores</a></li>
                                        <li class="uk-nav-divider"></li>
                                        <li class="@if ($isActiveSub == "universidade.organigrama") uk-active @endif"><a href="{{route('organigrama')}}">Organigrama</a></li>
                                        <li class="uk-nav-divider"></li>
                                        <li class="@if ($isActiveSub == "universidade.orgaos") uk-active @endif"><a href="{{route('orgaossuperiores')}}">Órgãos Superiores</a></li>
                                        <li class="uk-nav-divider"></li>
                                        <li class="@if ($isActiveSub == "universidade.conselho") uk-active @endif"><a href="{{route('conselhodirectivo')}}">Conselho Directivo</a></li>
                                        <li class="uk-nav-divider"></li>
                                        <li class="@if ($isActiveSub == "universidade.docentes") uk-active @endif"><a href="{{route('docentes')}}">Docentes</a></li>
                                    </ul>
                                </div>
                            </li>

                            <li class="@if ($isActive == "direccao") uk-active @endif uk-button uk-button-text">
                                <a href="#">Direcção <i uk-icon="icon: chevron-down"> </i></a>
                                <div class="uk-navbar-dropdown">
                                    <ul style="text-align: left" class="uk-nav uk-navbar-dropdown-nav">
                                        <li class="@if ($isActiveSub == "direccao.odirector") uk-active @endif"><a href="{{route('odirector')}}">O Director</a></li>
                                        <li class="uk-nav-divider"></li>
                                        <li class="@if ($isActiveSub == "direccao.gabinete") uk-active @endif"><a href="{{route('gabinetedodirector')}}">Gabinete do Director</a></li>
                                        <li class="uk-nav-divider"></li>
                                        <li class="@if ($isActiveSub == "direccao.secretaria") uk-active @endif"><a href="{{route('secretariageral')}}">Secretaria</a></li>
                                        <li class="uk-nav-divider"></li>
                                        <li class="@if ($isActiveSub == "direccao.registo") uk-active @endif"><a href="{{route('registoacademico')}}">Registo Académico</a></li>
                                    </ul>
                                </div>
                            </li>

                            {{-- <li class="@if ($isActive == "noticias") uk-active @endif uk-button uk-button-text"><a href=" {{route('noticias.index')}} ">Notícias</a></li> --}}

                            <li class="@if ($isActive == "cei") uk-active @endif uk-button uk-button-text" >
                                <a href="#">Cooperação e<br>Internacionalização <i uk-icon="icon: chevron-down"> </i></a>
                                <div class="uk-navbar-dropdown" style="width: 250px">
                                    <ul style="text-align: left" class="uk-nav uk-navbar-dropdown-nav">
                                        <li class="@if ($isActiveSub == "cei.apresentacao") uk-active @endif"><a href="{{route('cei.apresentacao')}}">Apresentação</a></li>                                
                                        <li class="uk-nav-divider"></li>
                                        <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
                                            <li class="@if ($isActiveSub == "cei.coop") uk-active @endif uk-parent">
                                                <a href="#">Cooperação</a>
                                                <ul class="uk-nav-sub">
                                                    <li class="@if ($isActiveSubSub == "cei.coop.nacional") uk-active @endif"><a href="{{route('cei.cooperacao.nacional')}}">– Nacional</a></li>
                                                    <li class="@if ($isActiveSubSub == "cei.coop.internacional") uk-active @endif"><a href="{{route('cei.cooperacao.internacional')}}">– Internacional</a></li>                                                    
                                                </ul>
                                            </li>
                                        </ul>
                                        <li class="uk-nav-divider"></li>
                                        <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
                                            <li class="@if ($isActiveSub == "cei.int") uk-active @endif uk-parent">
                                                <a href="#">Internacionalização</a>
                                                <ul class="uk-nav-sub">
                                                    <li class="@if ($isActiveSubSub == "cei.int.visiting") uk-active @endif"><a href="{{route('cei.internacionalizacao.visiting')}}">– Visiting Professors and Researchers</a></li>
                                                    <li class="@if ($isActiveSubSub == "cei.int.profconv") uk-active @endif"><a href="{{route('cei.internacionalizacao.professoresconvidados')}}">– Professores Convidados</a></li>
                                                    <li class="@if ($isActiveSubSub == "cei.int.editais") uk-active @endif"><a href="{{route('editais.index')}}">– Editais</a></li>                                                    
                                                </ul>
                                            </li>
                                        </ul>
                                        <li class="uk-nav-divider"></li>
                                        <li class="@if ($isActiveSub == "cei.informacao") uk-active @endif"><a href="{{route('cei.informacao')}}">Informação</a></li>
                                        <li class="uk-nav-divider"></li>
                                        <li class="@if ($isActiveSub == "cei.newsletter") uk-active @endif"><a href="{{route('cei.newsletter')}}">Newsletter</a></li>
                                    </ul>
                                </div>
                            </li>
                            {{-- <li class="@if ($isActive == "eventos") uk-active @endif uk-button uk-button-text"><a href=" {{route('eventos.index')}} ">Eventos</a></li> --}}

                            <li class="@if ($isActive == "cursos") uk-active @endif uk-button uk-button-text">
                              <a href="#">Cursos <i uk-icon="icon: chevron-down"> </i></a>
                              <div class="uk-navbar-dropdown">
                                  <ul style="text-align: left" class="uk-nav uk-navbar-dropdown-nav">
                                      <li><a href="#">Cursos de Licenciatura</a></li>
                                      <li class="uk-nav-divider"></li>
                                      <li><a href="#">Cursos de Mestrado</a></li>
                                  </ul>
                              </div>
                            </li>

                            {{-- <li class="@if ($isActive == "faculdades") uk-active @endif uk-button uk-button-text"><a href="{{route('faculdades')}}">Faculdades</a></li> --}}

                            <li class="@if ($isActive == "departamentos") uk-active @endif uk-button uk-button-text">
                                <a href="#">Departamentos <i uk-icon="icon: chevron-down"> </i></a>
                                <div class="uk-navbar-dropdown">
                                    <ul style="text-align: left" class="uk-nav uk-navbar-dropdown-nav">
                                        <li><a href="#">Ensino a Distância</a></li>
                                        <li class="uk-nav-divider"></li>
                                        <li><a href="#">Recursos Humanos</a></li>
                                        <li class="uk-nav-divider"></li>
                                        <li><a href="#">Adm. e Finanças</a></li>
                                        <li class="uk-nav-divider"></li>
                                        <li><a href="#">Património</a></li>
                                        <li class="uk-nav-divider"></li>
                                        <li><a href="#">UGEA</a></li>
                                        <li class="uk-nav-divider"></li>
                                        <li><a href="#">Serviços Sociais</a></li>
                                    </ul>
                                </div>
                            </li>


                            <li class="@if ($isActive == "dppe") uk-active @endif uk-button uk-button-text">
                                <a href="#">Pós-Graduação,<br>Pesquisa e Extensão <i uk-icon="icon: chevron-down"> </i></a>
                                <div class="uk-navbar-dropdown uk-navbar-dropdown-width-2">
                                    <div class="uk-navbar-dropdown-grid uk-child-width-1-2" uk-grid>
                                        <div>
                                            <ul style="text-align: left" class="uk-nav uk-navbar-dropdown-nav">
                                                <li class="@if ($isActiveSub == "dppe.apresentacao") uk-active @endif"><a href="{{route('dppe.apresentacao')}}">Apresentação</a></li>
                                                <li class="uk-nav-divider"></li>
                                                <li class="@if ($isActiveSub == "dppe.organigrama") uk-active @endif"><a href="{{route('dppe.organigrama')}}">Organigrama</a></li>
                                                <li class="uk-nav-divider"></li>  
                                                <li class="@if ($isActiveSub == "dppe.planoestrategico") uk-active @endif"><a href="{{route('dppe.planoestrategico')}}">Plano Estratégico</a></li>
                                                <li class="uk-nav-divider"></li>
                                                <li class="@if ($isActiveSub == "dppe.regulamento") uk-active @endif"><a href="{{route('dppe.regulamento')}}">Regulamento</a></li>
                                                <li class="uk-nav-divider"></li>
                                                <li class="@if ($isActiveSub == "dppe.boletimdodppe") uk-active @endif"><a href="{{route('dppe.boletimdodppe')}}">Boletim da DPPE</a></li>
                                                <li class="uk-nav-divider"></li>
                                                <li class="@if ($isActiveSub == "dppe.centrosenucleos") uk-active @endif"><a href="{{route('centrosenucleos.index')}}">Centros e Núcleos</a></li>
                                                <li class="uk-nav-divider"></li>
                                            </ul>
                                        </div>

                                        <div>
                                            <ul style="text-align: left" class="uk-nav uk-navbar-dropdown-nav">
                                                <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
                                                    <li class="@if ($isActiveSub == "dppe.dpp") uk-active @endif uk-parent">
                                                        <a href="#">Dep. de Pesquisa e Publicação</a>
                                                        <ul class="uk-nav-sub">
                                                            <li class="@if ($isActiveSubSub == "dppe.dpp.apresentacao") uk-active @endif"><a href="{{route('dppe.dpp.apresentacao')}}">– Apresentação</a></li>
                                                            <li class="@if ($isActiveSubSub == "dppe.dpp.projectos") uk-active @endif"><a href="{{route('dppe.dpp.projectos')}}">– Projectos</a></li>
                                                            <li class="@if ($isActiveSubSub == "dppe.dpp.linhasdepesquisa") uk-active @endif"><a href="{{route('dppe.dpp.linhasdepesquisa')}}">– Linhas de Pesquisa</a></li>                                                    
                                                        </ul>
                                                    </li>
                                                </ul>
                                                <li class="uk-nav-divider"></li>
                                                <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
                                                    <li class="@if ($isActiveSub == "dppe.dpp") uk-active @endif uk-parent">
                                                        <a href="#">Dep. de Extensão e Inovação</a>
                                                        <ul class="uk-nav-sub">
                                                            <li class="@if ($isActiveSubSub == "dppe.dpi.apresentacao") uk-active @endif"><a href="{{route('dppe.dpi.apresentacao')}}">– Apresentação</a></li>
                                                            <li class="@if ($isActiveSubSub == "dppe.dpi.projectos") uk-active @endif"><a href="{{route('dppe.dpi.projectos')}}">– Projectos</a></li>
                                                            <li class="@if ($isActiveSubSub == "dppe.dpi.escolinhas") uk-active @endif"><a href="{{route('dppe.dpi.escolinhas')}}">– Escolinhas</a></li>                                                    
                                                        </ul>
                                                    </li>
                                                </ul>
                                                <li class="uk-nav-divider"></li>
                                                <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
                                                    <li class="@if ($isActiveSub == "dppe.dpp") uk-active @endif uk-parent">
                                                        <a href="#">Dep. de Formação e Pós-Graduação</a>
                                                        <ul class="uk-nav-sub">
                                                            <li class="@if ($isActiveSubSub == "dppe.dfp.apresentacao") uk-active @endif"><a href="{{route('dppe.dfp.apresentacao')}}">– Apresentação</a></li>
                                                            <li class="@if ($isActiveSubSub == "dppe.dfp.efectivodedocente") uk-active @endif"><a href="{{route('dppe.dfp.efectivodedocente')}}">– Efectivos de Docentes</a></li>
                                                            <li class="@if ($isActiveSubSub == "dppe.dfp.docentesporformacao") uk-active @endif"><a href="{{route('dppe.dfp.docentesporformacao')}}">– Docentes por Formação</a></li>                                                    
                                                        </ul>
                                                    </li>
                                                </ul>
                                                <li class="uk-nav-divider"></li>
                                                <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
                                                    <li class="@if ($isActiveSub == "dppe.dpp") uk-active @endif uk-parent">
                                                        <a href="#">Publicações</a>
                                                        <ul class="uk-nav-sub">
                                                            <li class="@if ($isActiveSubSub == "dppe.publ.revistaseartigos") uk-active @endif"><a href="{{route('dppe.publ.revistaseartigos')}}">– Revistas e Artigos</a></li>
                                                            <li class="@if ($isActiveSubSub == "dppe.publ.actasdeconferencia") uk-active @endif"><a href="{{route('dppe.publ.actasdeconferencia')}}">– Actas de Conferência</a></li>
                                                            <li class="@if ($isActiveSubSub == "dppe.publ.livros") uk-active @endif"><a href="{{route('dppe.publ.livros')}}">– Livros</a></li>  
                                                            <li class="@if ($isActiveSubSub == "dppe.publ.revistasdeposgraduacao") uk-active @endif"><a href="{{route('dppe.publ.revistasdeposgraduacao')}}">– Revista de Pós-Graduação</a></li>                                                    
                                                        </ul>
                                                    </li>
                                                </ul>
                                                <li class="uk-nav-divider"></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </li>



                            <li class="@if ($isActive == "serviços") uk-active @endif uk-button uk-button-text">
                                <a href="#">Serviços <i uk-icon="icon: chevron-down"> </i></a>
                                <div class="uk-navbar-dropdown">
                                    <ul style="text-align: left" class="uk-nav uk-navbar-dropdown-nav">
                                        <li><a href="#">Pastorial Universitária</a></li>
                                        <li class="uk-nav-divider"></li>
                                        <li><a href="#">Associação dos Estudantes</a></li>
                                        <li class="uk-nav-divider"></li>
                                        <li><a href="#">Intercâmbios - Internacionalização</a></li>
                                        <li class="uk-nav-divider"></li>
                                        <li><a href="https://mail.up.ac.mz" target="_blank"><i uk-icon="link"></i> Email Corporativo</i></a></li>
                                        <li class="uk-nav-divider"></li>
                                        <li><a href="https://cead.up.ac.mz" target="_blank"><i uk-icon="link"></i> CEAD</a></li>
                                        <li class="uk-nav-divider"></li>
                                        <li><a href="https://sigeup.up.ac.mz" target="_blank"><i uk-icon="link"></i> SIGEUP</a></li>
                                        <li class="uk-nav-divider"></li>
                                        <li><a href="{{route('upengenheiros')}}" target="_blank"><i uk-icon="link"></i> UP Engenheiros</a></li>
                                        {{-- <li class="uk-nav-divider"></li> --}}
                                        {{-- <li><a href="#" target="_blank"><i uk-icon="link"></i> CEAD</a></li> --}}
                                    </ul>
                                </div>
                            </li>
                            {{-- <li class="uk-button uk-button-text">
                                <a href="#">Serviços Online <i uk-icon="icon: chevron-down"> </i></a>
                                <div class="uk-navbar-dropdown mdl-color--grey-100">
                                    <ul style="text-align: left" class="uk-nav uk-navbar-dropdown-nav">

                                    </ul>
                                </div>
                            </li> --}}
                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/NAVBAR - MENUS-->
