@php
  function getPrevNextRoute($NextOrPrev){
      switch ($NextOrPrev) {
        # on Universidade tab...
        case 'Quem Somos':
          return 'quemsomos';
          break;

        case 'Missão, Visão e Valores':
          return 'missaoevalores';
          break;

        case 'Organigrama':
          return 'organigrama';
          break;

        case 'Órgãos Superiores':
          return 'orgaossuperiores';
          break;

        case 'Conselho Directivo':
          return 'conselhodirectivo';
          break;

        case 'Docentes':
          return 'docentes';
          break;



        # on Direcção tab...
        case 'O Director':
          return 'odirector';
          break;


        case 'Gabinete do Director':
          return 'gabinetedodirector';
          break;

        case 'Secretaria':
          return 'secretariageral';
          break;

        case 'Registo Académico':
          return 'registoacademico';
          break;



        # on Pesquisa e Extensão tab...
        // case 'Apresentação':
        //   return 'centrosenucleos.index';
        //   break;

        // case 'Centros e Núcleos':
        //   return 'centrosenucleos.index';
        //   break;
        
        // case 'Centros e Núcleos':
        //   return 'centrosenucleos.index';
        //   break;

        // case 'Centros e Núcleos':
        //   return 'centrosenucleos.index';
        //   break;

        // case 'Centros e Núcleos':
        //   return 'centrosenucleos.index';
        //   break;
          
        // case 'Centros e Núcleos':
        //   return 'centrosenucleos.index';
        //   break;


        // case 'Escolinhas':
        //   return 'escolinhas';
        //   break;

        // case 'Revistas e Artigos':
        //   return 'revistaseartigos';
        //   break;

        // case 'Projectos de Pesquisa':
        //   return 'projectosdepesquisa';
        //   break;

        


        # on Cooperação, Internacionalização e Imagens tab...
        case 'Professores Convidados':
          return 'ceo.professoresconvidados';
          break;


        case 'Newsletter':
          return 'cei.newsletter';
          break;

        case 'Informação':
          return 'cei.informacao';
          break;

        case 'Apresentação':
          return 'cei.apresentacao';
          break;
      }
  }
@endphp


<section class="uk-section uk-section-small uk-background-default">
  <div class="uk-container uk-container-small uk-margin-medium">
    @isset($nextPage)
      <div class="uk-float-right">
        <a href="{{route(getPrevNextRoute($nextPage))}}" class="uk-link-heading uk-padding-small">
          {{$nextPage}} <i uk-icon="chevron-right" style="position: relative; bottom: 1px"></i>
        </a>
      </div>
    @endisset

    @isset($previousPage)
      <div class="uk-float-left uk-text-right">
        <a href="{{route(getPrevNextRoute($previousPage))}}" class="uk-link-heading uk-padding-small">
          <i uk-icon="chevron-left" style="position: relative; bottom: 1px"></i> {{$previousPage}}
        </a>
      </div>
    @endisset

    <!-- OLD VERSION OF THE "PREVIOUS PAGE" BUTTON -->
    {{-- <div class="uk-float-left uk-text-right">
      <small class="">Órgãos Superiores</small><br>
      <a href="{{route('orgaossuperiores')}}" class="uk-button uk-button-default uk-float-right uk-button-secondary"><i uk-icon="chevron-left" style="position: relative; bottom: 1px"></i></a>
    </div> --}}

  </div>
</section>
