{{-- MOBILE MENUS --}}

<style>
    li a {
      font-size: 14px !important;
      text-transform: none !important;
    }

    .uk-nav-sub > li.uk-active > a {
      color: rgb(21, 136, 233) !important;
    }

    /* .uk-navbar-dropdown-nav > li.uk-active > a {
      color: rgb(21, 136, 233) !important;
      font-weight: bold;
    } */
</style>


@empty($isActiveSub)
    @php
        # Dont make isActiveSub important
        $isActiveSub = "";
    @endphp
@endempty

@empty($isActiveSubSub)
    @php
        # Dont make isActiveSubSub important
        $isActiveSubSub = "";
    @endphp
@endempty

<div id="offcanvas-nav" uk-offcanvas="overlay: true">
  <div class="uk-offcanvas-bar uk-offcanvas-bar-animation uk-offcanvas-slide">
    <a class="uk-navbar-item uk-logo uk-border-rounded uk-padding-small" href="{{ route('home') }}">
        {{-- <img uk-img class="uk-background-default uk-padding-xsmall" style="margin-right: 10px" data-src="{{ asset('images/logo.png') }}" width="50px"> --}}
        <img uk-img style="padding: 2px" class="uk-border-circle uk-background-default" width="40" data-src="{{ asset('images/logoup.png') }}" alt="">
        <div class="uk-column uk-text-bold uk-margin-left">
          <span style="position: relative; top: 7px;">UP Maxixe</span>
          {{-- position: relative; top: 13px; right: 76.4px; --}}
          <div class="uk-column-span">
            <span style="font-size: 10px;">UniSaF</span>
          </div>
        </div>
    </a>

    <button class="uk-offcanvas-close uk-close uk-icon" type="button" data-uk-close></button>

    <ul class="uk-nav uk-nav-default uk-dark uk-margin-small-bottom" uk-nav>
      <li class="@if ($isActive == "inicio") uk-active @endif"><a href="{{route('home')}}">Início</a></li>

      <li class="@if ($isActive == "universidade") uk-active @endif uk-parent">
        <a href="#">Universidade <i uk-icon="chevron-down"></i></a>
        <ul class="uk-nav-sub">
            <li class="@if ($isActiveSub == "universidade.quemsomos") uk-active @endif"><a href="{{route('quemsomos')}}"><span uk-icon="arrow-right"></span> Quem Somos</a></li>
            <li class="@if ($isActiveSub == "universidade.missaoevalores") uk-active @endif"><a href="{{route('missaoevalores')}}"><span uk-icon="arrow-right"></span> Missão, Visão e Valores</a></li>
            <li class="@if ($isActiveSub == "universidade.organigrama") uk-active @endif"><a href="{{route('organigrama')}}"><span uk-icon="arrow-right"></span> Organigrama</a></li>
            <li class="@if ($isActiveSub == "universidade.orgaos") uk-active @endif"><a href="{{route('orgaossuperiores')}}"><span uk-icon="arrow-right"></span> Órgãos Superiores</a></li>
            <li class="@if ($isActiveSub == "universidade.conselho") uk-active @endif"><a href="{{route('conselhodirectivo')}}"><span uk-icon="arrow-right"></span> Conselho Directivo</a></li>
            <li class="@if ($isActiveSub == "universidade.docentes") uk-active @endif"><a href="{{route('docentes')}}"><span uk-icon="arrow-right"></span> Docentes</a></li>
        </ul>
      </li>

      <li class="@if ($isActive == "direccao") uk-active @endif uk-parent">
          <a href="#">Direcção <i uk-icon="icon: chevron-down"> </i></a>
          <ul class="uk-nav-sub uk-dark ">
              <li class="@if ($isActiveSub == "direccao.odirector") uk-active @endif"><a href="{{route('odirector')}}"><span uk-icon="arrow-right"></span> O Director</a></li>
              <li class="@if ($isActiveSub == "direccao.gabinete") uk-active @endif"><a href="{{route('gabinetedodirector')}}"><span uk-icon="arrow-right"></span> Gabinete do Director</a></li>
              <li class="@if ($isActiveSub == "direccao.secretaria") uk-active @endif"><a href="{{route('secretariageral')}}"><span uk-icon="arrow-right"></span> Secretaria</a></li>
              <li class="@if ($isActiveSub == "direccao.registo") uk-active @endif"><a href="{{route('registoacademico')}}"><span uk-icon="arrow-right"></span> Registo Académico</a></li>
          </ul>
      </li>

      {{-- <li class="@if ($isActive == "noticias") uk-active @endif uk-button uk-button-text"><a href=" {{route('noticias.index')}} ">Notícias</a></li> --}}

      <li class="@if ($isActive == "cei") uk-active @endif uk-parent" >
        <a href="#">Cooperação e<br>Internacionalização <i uk-icon="icon: chevron-down"> </i></a>
        <ul class="uk-nav-sub uk-dark">
            <li class="@if ($isActiveSub == "cei.apresentacao") uk-active @endif"><a href="{{route('cei.apresentacao')}}"><span uk-icon="arrow-right"></span> Apresentação</a></li>                                
            <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
                <li class="@if ($isActiveSub == "cei.coop") uk-active @endif uk-parent">
                    <a href="#">Cooperação</a>
                    <ul class="uk-nav-sub">
                        <li class="@if ($isActiveSubSub == "cei.coop.nacional") uk-active @endif"><a href="{{route('cei.cooperacao.nacional')}}"><span uk-icon="arrow-right"></span> Nacional</a></li>
                        <li class="@if ($isActiveSubSub == "cei.coop.internacional") uk-active @endif"><a href="{{route('cei.cooperacao.internacional')}}"><span uk-icon="arrow-right"></span> Internacional</a></li>                                                    
                    </ul>
                </li>
            </ul>
            <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
                <li class="@if ($isActiveSub == "cei.int") uk-active @endif uk-parent">
                    <a href="#">Internacionalização</a>
                    <ul class="uk-nav-sub">
                        <li class="@if ($isActiveSubSub == "cei.int.visiting") uk-active @endif"><a href="{{route('cei.internacionalizacao.visiting')}}"><span uk-icon="arrow-right"></span> Visiting Professors and Researchers</a></li>
                        <li class="@if ($isActiveSubSub == "cei.int.profconv") uk-active @endif"><a href="{{route('cei.internacionalizacao.professoresconvidados')}}"><span uk-icon="arrow-right"></span> Professores Convidados</a></li>
                        <li class="@if ($isActiveSubSub == "cei.int.editais") uk-active @endif"><a href="{{route('editais.index')}}"><span uk-icon="arrow-right"></span> Editais</a></li>                                                    
                    </ul>
                </li>
            </ul>
            <li class="@if ($isActiveSub == "cei.informacao") uk-active @endif"><a href="{{route('cei.informacao')}}"><span uk-icon="arrow-right"></span> Informação</a></li>
            <li class="@if ($isActiveSub == "cei.newsletter") uk-active @endif"><a href="{{route('cei.newsletter')}}"><span uk-icon="arrow-right"></span> Newsletter</a></li>
        </ul>
      </li>
      
      {{-- <li class="@if ($isActive == "eventos") uk-active @endif uk-button uk-button-text"><a href=" {{route('eventos.index')}} ">Eventos</a></li> --}}

      <li class="@if ($isActive == "cursos") uk-active @endif uk-parent">
        <a href="#">Cursos <i uk-icon="icon: chevron-down"> </i></a>
        <ul class="uk-nav-sub uk-dark ">
            <li class="@if ($isActiveSub == "cursos.licenciatura") uk-active @endif"><a href="#"><span uk-icon="arrow-right"></span> Cursos de Licenciatura</a></li>
            <li class="@if ($isActiveSub == "cursos.mestrado") uk-active @endif"><a href="#"><span uk-icon="arrow-right"></span> Cursos de Mestrado</a></li>
        </ul>
      </li>

      {{-- <li class="@if ($isActive == "faculdades") uk-active @endif"><a href="{{route('faculdades')}}">Faculdades</a></li> --}}


      <li class="@if ($isActive == "departamentos") uk-active @endif uk-parent">
          <a href="#">Departamentos <i uk-icon="icon: chevron-down"> </i></a>
          <ul class="uk-nav-sub uk-dark ">
              <li><a href="#"><span uk-icon="arrow-right"></span> Ensino a Distância</a></li>
              <li><a href="#"><span uk-icon="arrow-right"></span> Recursos Humanos</a></li>
              <li><a href="#"><span uk-icon="arrow-right"></span> Adm. e Finanças</a></li>
              <li><a href="#"><span uk-icon="arrow-right"></span> Património</a></li>
              <li><a href="#"><span uk-icon="arrow-right"></span> UGEA</a></li>
              <li><a href="#"><span uk-icon="arrow-right"></span> Serviços Sociais</a></li>
          </ul>
      </li>

      {{-- <li class="uk-parent">
        <a href="#">Serviços Online <i uk-icon="chevron-down"></i></a>
        <ul class="uk-nav-sub ">
          <li><a href="#"><span uk-icon="arrow-right"></span> Email Corporativo</a></li>
          <li><a href="#"><span uk-icon="arrow-right"></span> SIGEUP</a></li>
          <li><a href="#"><span uk-icon="arrow-right"></span> Plataforma de Engenheiros</a></li>
          <li><a href="#"><span uk-icon="arrow-right"></span> CEAD</a></li>
        </ul>
      </li> --}}

      <li class="@if ($isActive == "pesquisaeextensao") uk-active @endif uk-parent">
          <a href="#">Pós-Graduação,<br>Pesquisa e Extensão <i uk-icon="icon: chevron-down"> </i></a>
          <ul class="uk-nav-sub uk-dark">
              <li class="@if ($isActiveSub == "dppe.apresentacao") uk-active @endif"><a href="{{route('dppe.apresentacao')}}"><span uk-icon="arrow-right"></span> Apresentação</a></li>   
              <li class="@if ($isActiveSub == "dppe.organigrama") uk-active @endif"><a href="{{route('dppe.organigrama')}}"><span uk-icon="arrow-right"></span> Organigrama</a></li>
              <li class="@if ($isActiveSub == "dppe.planoestrategico") uk-active @endif"><a href="{{route('dppe.planoestrategico')}}"><span uk-icon="arrow-right"></span> Plano Estratégico</a></li>
              <li class="@if ($isActiveSub == "dppe.regulamento") uk-active @endif"><a href="{{route('dppe.regulamento')}}"><span uk-icon="arrow-right"></span> Regulamento</a></li>
              <li class="@if ($isActiveSub == "dppe.boletimdodppe") uk-active @endif"><a href="{{route('dppe.boletimdodppe')}}"><span uk-icon="arrow-right"></span> Boletim da DPPE</a></li>
              
              <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
                <li class="@if ($isActiveSub == "dppe.dpp") uk-active @endif uk-parent">
                    <a href="#">Dep. de Pesquisa e<br>Publicação</a>
                    <ul class="uk-nav-sub">
                        <li class="@if ($isActiveSubSub == "dppe.dpp.apresentacao") uk-active @endif"><a href="{{route('dppe.dpp.apresentacao')}}"><span uk-icon="arrow-right"></span> Apresentação</a></li>
                        <li class="@if ($isActiveSubSub == "dppe.dpp.projectos") uk-active @endif"><a href="{{route('dppe.dpp.projectos')}}"><span uk-icon="arrow-right"></span> Projectos</a></li>
                        <li class="@if ($isActiveSubSub == "dppe.dpp.linhasdepesquisa") uk-active @endif"><a href="{{route('dppe.dpp.linhasdepesquisa')}}"><span uk-icon="arrow-right"></span> Linhas de Pesquisa</a></li>                                                    
                    </ul>
                </li>
              </ul>

              <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
                <li class="@if ($isActiveSub == "dppe.dpp") uk-active @endif uk-parent">
                    <a href="#">Dep. de Extensão e<br>Inovação</a>
                    <ul class="uk-nav-sub">
                        <li class="@if ($isActiveSubSub == "dppe.dpi.apresentacao") uk-active @endif"><a href="{{route('dppe.dpi.apresentacao')}}"><span uk-icon="arrow-right"></span> Apresentação</a></li>
                        <li class="@if ($isActiveSubSub == "dppe.dpi.projectos") uk-active @endif"><a href="{{route('dppe.dpi.projectos')}}"><span uk-icon="arrow-right"></span> Projectos</a></li>
                        <li class="@if ($isActiveSubSub == "dppe.dpi.escolinhas") uk-active @endif"><a href="{{route('dppe.dpi.escolinhas')}}"><span uk-icon="arrow-right"></span> Escolinhas</a></li>                                                        
                    </ul>
                </li>
              </ul>
              <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
                <li class="@if ($isActiveSub == "dppe.dpp") uk-active @endif uk-parent">
                    <a href="#">Publicações</a>
                    <ul class="uk-nav-sub">
                        <li class="@if ($isActiveSubSub == "dppe.publ.revistaseartigos") uk-active @endif"><a href="{{route('dppe.publ.revistaseartigos')}}"><span uk-icon="arrow-right"></span> Revistas e Artigos</a></li>
                        <li class="@if ($isActiveSubSub == "dppe.publ.actasdeconferencia") uk-active @endif"><a href="{{route('dppe.publ.actasdeconferencia')}}"><span uk-icon="arrow-right"></span> Actas de Conferência</a></li>
                        <li class="@if ($isActiveSubSub == "dppe.publ.livros") uk-active @endif"><a href="{{route('dppe.publ.livros')}}"><span uk-icon="arrow-right"></span> Livros</a></li>  
                        <li class="@if ($isActiveSubSub == "dppe.publ.revistasdeposgraduacao") uk-active @endif"><a href="{{route('dppe.publ.revistasdeposgraduacao')}}"><span uk-icon="arrow-right"></span> Revista de Pós-Graduação</a></li>                                                    
                    </ul>
                </li>
              </ul>
              
              <li class="@if ($isActiveSub == "dppe.centrosenucleos") uk-active @endif"><a href="{{route('centrosenucleos.index')}}"><span uk-icon="arrow-right"></span> Centros e Núcleos</a></li>
          </ul>
      </li>

      <li class="@if ($isActive == "serviços") uk-active @endif uk-parent">
          <a href="#">Serviços <i uk-icon="icon: chevron-down"> </i></a>
          <ul class="uk-nav-sub uk-dark ">
              <li><a href="#"><span uk-icon="arrow-right"></span> Pastorial Universitária</a></li>
              <li><a href="#"><span uk-icon="arrow-right"></span> Associação dos Estudantes</a></li>
              <li><a href="#"><span uk-icon="arrow-right"></span> Intercâmbios - Internacionalização</a></li>
              <li><a href="https://mail.up.ac.mz" target="_blank"><span uk-icon="link"></span> Email Corporativo</i></a></li>
              <li><a href="https://sigeup.up.ac.mz" target="_blank"><span uk-icon="link"></span> SIGEUP</a></li>
              <li><a href="{{route('upengenheiros')}}" target="_blank"><span uk-icon="link"></span> Plataforma de Engenheiros</a></li>
              {{-- <li><a href="#" target="_blank"><span uk-icon="link"></span> CEAD</a></li> --}}
          </ul>
      </li>
    </ul>

    <div style="font-family: raleway !important">
        <a target="_blank" style="background-color: white; color: black" href="https://www.sigeup.up.ac.mz/" uk-tooltip="title: Sistema de Gestão da Universidade Pedagógica; pos: bottom; delay: 400" class="uk-margin-small-bottom uk-button uk-button-default uk-button-small uk-border-rounded uk-margin-small-right"> <img src="{{ asset('images/icons/foreign.svg') }}" uk-svg width="10" height="10" alt=""> SIGEUP</a>
        <a target="_blank" style="background-color: white" href="https://cead.up.ac.mz/" uk-tooltip="title: Centro de Educação Aberta e à Distância; pos: bottom; delay: 400" class="uk-margin-small-bottom uk-button uk-button-default uk-button-small uk-border-rounded uk-margin-small-right"><img width="90" src="{{asset('images/cead.png')}}" alt=""></a>
        <a target="_blank" style="background-color: white; color: black" href="https://cead.up.ac.mz/elearning" class="uk-button uk-button-default uk-button-small uk-button-muted uk-border-rounded uk-margin-small-bottom"> <img src="{{ asset('images/icons/foreign.svg') }}" uk-svg width="10" height="10" alt=""> MOODLE</a>
        <a target="_blank" style="background-color: white; color: black" href="http://www.mongue.org" class="uk-button uk-button-default uk-button-small uk-button-muted uk-border-rounded uk-margin-small-bottom"> <img src="{{ asset('images/icons/foreign.svg') }}" uk-svg width="10" height="10" alt=""> Mo.N.G.U.E</a>    
    </div>
       

  </div>
</div>
{{-- ./MOBILE MENUS --}}
