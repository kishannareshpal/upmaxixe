<!--HEADER.NAV-->
    <nav class="uk-background-primary uk-light uk-padding-small">
      <small class="uk-text-bold">
        <span class="uk-visible@s">
          <div class="uk-text-center">
            <img src="{{ asset('images/icons/fax.svg') }}" uk-svg width="20" height="20" alt=""> <a class="uk-button-text" href="tel:+25829330359">+258 293-30359</a> ––– FAX: <a class="uk-button-text" href="#">+258 293-30354</a>
            &nbsp <img src="{{ asset('images/icons/gps.svg') }}" uk-svg width="20" height="20" alt=""> <a class="uk-button-text" target="_blank" href="https://goo.gl/maps/jWDP6msHPoo">Av. Américo Boavida. CP 12, Cidade de Maxixe</a>
            &nbsp <img src="{{ asset('images/icons/email.svg') }}" uk-svg width="20" height="20" alt=""> <a class="uk-button-text" target="_blank" href="mailto:info@upmaxixe.ac.mz"> info@upmaxixe.ac.mz</a>
          </div>

        </span>

        <span class="uk-hidden@s">
          <div class="uk-text-center">
            <img src="{{ asset('images/icons/fax.svg') }}" uk-svg width="15" height="15" alt=""><a class="uk-button-text" href="tel:+25829330359">+258 293-30359</a> ––– FAX: <a class="uk-button-text"  href="#">+258 293-30354</a>
            <br> <img src="{{ asset('images/icons/gps.svg') }}" uk-svg width="15" height="15" alt=""><a class="uk-button-text" target="_blank" href="https://goo.gl/maps/jWDP6msHPoo">Av. Américo Boavida. CP 12, Cidade de Maxixe</a>
            <br> <img src="{{ asset('images/icons/email.svg') }}" uk-svg width="15" height="15" alt=""><a class="uk-button-text" target="_blank" href="mailto:info@upmaxixe.ac.mz"> info@upmaxixe.ac.mz</a>
          </div>
        </span>
      </small>
    </nav>

    <header class="mdl-color--grey-50" style="border-bottom: 1px solid #f2f2f2" uk-sticky="show-on-up: true; animation: uk-animation-slide-top">
        <div class="uk-container">
            <nav id="navbar" uk-navbar="mode: click;">
                <div class="uk-navbar-left">
                    @yield('navbar-left-content')
                    <a class="uk-navbar-item uk-logo" href="{{ route('home') }}">
                        <img uk-img style="margin-right: 10px" class="uk-visible@s" data-src="{{ asset('images/logoup.png') }}" width="80px">
                        <img uk-img style="margin-right: 10px" class="uk-hidden@s" data-src="{{ asset('images/logoup.png') }}" width="50px">

                        <div class="uk-column">
                          <span style="position: relative; top: 2px;" class="uk-visible@s mdl-color-text--black">Universidade Pedagógica</span>
                          <span style="position: relative; bottom: 2px; font-size: 10px; line-height: .5;" class="uk-hidden@s uk-text-bold mdl-color-text--black">Universidade Pedagógica</span>
                          {{-- position: relative; top: 13px; right: 76.4px; --}}
                          <div class="uk-column-span">
                            <span style="font-size: 18px;" class="uk-visible@s">Delegação da Maxixe</span>
                            <span style="position: relative; bottom: 5px; font-size: 12px; line-height: .5" class="uk-hidden@s">Delegação da Maxixe</span>
                          </div>
                        </div>
                    </a>
                </div>
                <div class="uk-navbar-right">
                    <div class="uk-navbar-item uk-padding-remove-left" style="font-family: 'raleway' !important;">
                        {{-- style="border: .5px solid #eee; padding: 5px; border-radius: 10px" --}}
                        <div class="uk-text-center uk-text-middle">
                            {{-- <small>Siga-nos nas Redes sociais!</small><br> --}}
                            <a href="https://www.youtube.com/channel/UCzNAxCM1a5VAQiwDNOEyqig" title="Assista os videos da UP Maxixe no nosso canal de YouTube!" target="_blank"><img src="{{ asset('images/icons/youtube.svg') }}" uk-svg width="30" height="30"></a>
                            <a href="https://www.facebook.com/upmaxixe" title="Siga-nos no Facebook!" target="_blank"><img src="{{ asset('images/icons/facebook.svg') }}" uk-svg width="25" height="25"></a>
                        </div>
                        <a target="_blank" href="https://www.sigeup.up.ac.mz/" title="Sistema de Gestão da Universidade Pedagógica" class="uk-margin-left uk-visible@m uk-button uk-button-default uk-button-small uk-button-muted uk-border-rounded uk-margin-small-right"> <img src="{{ asset('images/icons/foreign.svg') }}" uk-svg width="10" height="10" alt=""> SIGEUP</a>
                        <a target="_blank" href="https://cead.up.ac.mz/" title="Centro de Educação Aberta e à Distância" class="uk-visible@m uk-button uk-button-default uk-button-small uk-button-muted uk-border-rounded uk-margin-small-right"><img width="90" src="{{asset('images/cead.png')}}" alt=""></a>
                        <a target="_blank" href="https://cead.up.ac.mz/elearning" class="uk-visible@m uk-button uk-button-default uk-button-small uk-button-muted uk-border-rounded uk-margin-small-right"> <img src="{{ asset('images/icons/foreign.svg') }}" uk-svg width="10" height="10" alt=""> MOODLE</a>
                        <a target="_blank" href="http://www.mongue.org" title="Mozambique.Nature.Growth.University.Education." class="uk-visible@m uk-button uk-button-default uk-button-small uk-button-muted uk-border-rounded uk-margin-right"><span style="text-transform: none;"> <img src="{{ asset('images/icons/foreign.svg') }}" uk-svg width="10" height="10" alt=""> Mo.N.G.U.E.</span></a>
                        

                        <a class="uk-navbar-toggle uk-hidden@m" data-uk-toggle data-uk-navbar-toggle-icon href="#offcanvas-nav"></a>
                        @yield('navbar-right-content')
                    </div>
                </div>
            </nav>
        </div>
    </header>
<!--/HEADER.NAV-->
