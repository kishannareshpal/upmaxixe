<!--FOOTER-->


@empty($isActiveSub)
    @php
        # Dont make isActiveSub important
        $isActiveSub = "";
    @endphp
@endempty

@empty($isActiveSubSub)
    @php
        # Dont make isActiveSubSub important
        $isActiveSubSub = "";
    @endphp
@endempty


<style>
  .uk-nav-default > li.uk-active > a {
    color: rgb(21, 136, 233) !important;
  }
</style>

<div class="uk-overlay-default uk-section uk-section-xsmall" style="border-top: 1px solid #f2f2f2; background-color: #050627">
    <div class="uk-container uk-container-small uk-text-small uk-padding">
        <h3 class="uk-text-bold uk-light">MENU</h3>
        <hr>
        <div class="uk-grid uk-child-width-1-3@s uk-child-width-1-3@m" uk-grid>
            <div class="">
              <ul class="uk-nav uk-nav-default uk-dark" uk-nav>
                <li class="@if ($isActive == "inicio") uk-active @endif"><a href="{{route('home')}}">Início</a></li>

                <li class="@if ($isActive == "universidade") uk-active @endif uk-parent">
                  <a href="#">Universidade <i uk-icon="chevron-down"></i></a>
                  <ul class="uk-nav-sub uk-text-left uk-background-default uk-border-rounded ">
                      <li class="@if ($isActiveSub == "universidade.quemsomos") uk-active @endif"><a href="{{route('quemsomos')}}"><span uk-icon="arrow-right"></span> Quem Somos</a></li>
                      <li class="@if ($isActiveSub == "universidade.missaoevalores") uk-active @endif"><a href="{{route('missaoevalores')}}"><span uk-icon="arrow-right"></span> Missão e Valores</a></li>
                      <li class="@if ($isActiveSub == "universidade.organigrama") uk-active @endif"><a href="{{route('organigrama')}}"><span uk-icon="arrow-right"></span> Organigrama</a></li>
                      <li class="@if ($isActiveSub == "universidade.orgaos") uk-active @endif"><a href="{{route('orgaossuperiores')}}"><span uk-icon="arrow-right"></span> Órgãos Superiores</a></li>
                      <li class="@if ($isActiveSub == "universidade.conselho") uk-active @endif"><a href="{{route('conselhodirectivo')}}"><span uk-icon="arrow-right"></span> Conselho Directivo</a></li>
                      <li class="@if ($isActiveSub == "universidade.docentes") uk-active @endif"><a href="{{route('docentes')}}"><span uk-icon="arrow-right"></span> Docentes</a></li>
                  </ul>
                </li>

                <li class="@if ($isActive == "direccao") uk-active @endif uk-parent">
                    <a href="#">Direcção <i uk-icon="icon: chevron-down"> </i></a>
                    <ul class="uk-nav-sub uk-text-left uk-background-default uk-border-rounded ">
                        <li class="@if ($isActiveSub == "direccao.odirector") uk-active @endif"><a href="{{route('odirector')}}"><span uk-icon="arrow-right"></span> O Director</a></li>
                        <li class="@if ($isActiveSub == "direccao.gabinete") uk-active @endif"><a href="{{route('gabinetedodirector')}}"><span uk-icon="arrow-right"></span> Gabinete do Director</a></li>
                        <li class="@if ($isActiveSub == "direccao.secretaria") uk-active @endif"><a href="{{route('secretariageral')}}"><span uk-icon="arrow-right"></span> Secretaria</a></li>
                        <li class="@if ($isActiveSub == "direccao.registo") uk-active @endif"><a href="{{route('registoacademico')}}"><span uk-icon="arrow-right"></span> Registo Académico</a></li>
                    </ul>
                </li>

                <li class="@if ($isActive == "cei") uk-active @endif uk-parent">
                    <a href="#">Cooperação e<br>Internacionalização <i uk-icon="icon: chevron-down"> </i></a>
                    <ul class="uk-nav-sub uk-text-left uk-background-default uk-border-rounded">
                        <li class="@if ($isActiveSub == "cei.apresentacao") uk-active @endif"><a href="{{route('cei.apresentacao')}}"><span uk-icon="arrow-right"></span> Apresentação</a></li>                                
                        <li class="@if ($isActiveSub == "cei.informacao") uk-active @endif"><a href="{{route('cei.informacao')}}"><span uk-icon="arrow-right"></span> Informação</a></li>
                        <li class="@if ($isActiveSub == "cei.newsletter") uk-active @endif"><a href="{{route('cei.newsletter')}}"><span uk-icon="arrow-right"></span> Newsletter</a></li>

                        <ul class="uk-nav-default" uk-nav>
                            <li class="@if ($isActiveSub == "cei.coop") uk-active @endif uk-parent">
                                <a href="#">Cooperação <span uk-icon="chevron-down"></span></a>
                                <ul class="uk-nav-sub">
                                    <li class="@if ($isActiveSubSub == "cei.coop.nacional") uk-active @endif"><a href="{{route('cei.cooperacao.nacional')}}"><span uk-icon="arrow-right"></span> Nacional</a></li>
                                    <li class="@if ($isActiveSubSub == "cei.coop.internacional") uk-active @endif"><a href="{{route('cei.cooperacao.internacional')}}"><span uk-icon="arrow-right"></span> Internacional</a></li>
                                </ul>
                            </li>
                        </ul>

                        <ul class="uk-nav-default" uk-nav>
                            <li class="@if ($isActiveSub == "cei.int") uk-active @endif uk-parent">
                                <a href="#">Internacionalização <span uk-icon="chevron-down"></span></a>
                                <ul class="uk-nav-sub">
                                    <li class="@if ($isActiveSubSub == "cei.int.visiting") uk-active @endif"><a href="{{route('cei.internacionalizacao.visiting')}}"><span uk-icon="arrow-right"></span> Visiting Professors and Researchers</a></li>
                                    <li class="@if ($isActiveSubSub == "cei.int.profconv") uk-active @endif"><a href="{{route('cei.internacionalizacao.professoresconvidados')}}"><span uk-icon="arrow-right"></span> Professores Convidados</a></li>
                                    <li class="@if ($isActiveSubSub == "cei.int.editais") uk-active @endif"><a href="{{route('editais.index')}}"><span uk-icon="arrow-right"></span> Editais</a></li>
                                </ul>
                            </li>
                        </ul>
                    </ul>
                </li>

              </ul>
            </div>

            <div class="">
              <ul class="uk-nav uk-nav-default uk-dark" uk-nav>
                {{-- <li class="@if ($isActive == "eventos") uk-active @endif"><a href=" {{route('eventos.index')}} ">Eventos</a></li> --}}

                <li class="@if ($isActive == "noticias") uk-active @endif"><a href=" {{route('noticias.index')}} ">Notícias</a></li>

                {{-- <li class="@if ($isActive == "faculdades") uk-active @endif"><a href="{{route('faculdades')}}">Faculdades</a></li> --}}

                <li class="@if ($isActive == "cursos") uk-active @endif uk-parent">
                    <a href="#">Cursos <i uk-icon="icon: chevron-down"> </i></a>
                    <ul class="uk-nav-sub uk-text-left uk-background-default uk-border-rounded">
                        <li class="@if ($isActiveSub == "cursos.licenciatura") uk-active @endif"><a href="#"><span uk-icon="arrow-right"></span> Cursos de Licenciatura</a></li>
                        <li class="@if ($isActiveSub == "cursos.mestrado") uk-active @endif"><a href="#"><span uk-icon="arrow-right"></span> Cursos de Mestrado</a></li>
                    </ul>
                </li>


                <li class="@if ($isActive == "departamentos") uk-active @endif uk-parent">
                    <a href="#">Departamentos <i uk-icon="icon: chevron-down"> </i></a>
                    <ul class="uk-nav-sub uk-text-left uk-background-default uk-border-rounded">
                        <li><a href="#"><span uk-icon="arrow-right"></span> Ensino a Distância</a></li>
                        <li><a href="#"><span uk-icon="arrow-right"></span> Recursos Humanos</a></li>
                        <li><a href="#"><span uk-icon="arrow-right"></span> Adm. e Finanças</a></li>
                        <li><a href="#"><span uk-icon="arrow-right"></span> Património</a></li>
                        <li><a href="#"><span uk-icon="arrow-right"></span> UGEA</a></li>
                        <li><a href="#"><span uk-icon="arrow-right"></span> Serviços Sociais</a></li>
                    </ul>
                </li>

                <li class="@if ($isActive == "dppe") uk-active @endif uk-parent">
                    <a href="#">Pós-Graduação,<br>Pesquisa e Extensão <i uk-icon="icon: chevron-down"> </i></a>
                    <ul class="uk-nav-sub uk-text-left uk-background-default uk-border-rounded ">
                        <li class="@if ($isActiveSub == "dppe.apresentacao") uk-active @endif"><a href="{{route('dppe.apresentacao')}}"><span uk-icon="arrow-right"></span> Apresentação</a></li>   
                        <li class="@if ($isActiveSub == "dppe.organigrama") uk-active @endif"><a href="{{route('dppe.organigrama')}}"><span uk-icon="arrow-right"></span> Organigrama</a></li>
                        <li class="@if ($isActiveSub == "dppe.planoestrategico") uk-active @endif"><a href="{{route('dppe.planoestrategico')}}"><span uk-icon="arrow-right"></span> Plano Estratégico</a></li>
                        <li class="@if ($isActiveSub == "dppe.regulamento") uk-active @endif"><a href="{{route('dppe.regulamento')}}"><span uk-icon="arrow-right"></span> Regulamento</a></li>
                        <li class="@if ($isActiveSub == "dppe.boletimdodppe") uk-active @endif"><a href="{{route('dppe.boletimdodppe')}}"><span uk-icon="arrow-right"></span> Boletim da DPPE</a></li>
                        <li class="@if ($isActiveSub == "dppe.centrosenucleos") uk-active @endif"><a href="{{route('centrosenucleos.index')}}"><span uk-icon="arrow-right"></span> Centros e Núcleos</a></li>
                        
                        <ul class="uk-nav-default" uk-nav>
                          <li class="@if ($isActiveSub == "dppe.dpp") uk-active @endif uk-parent">
                              <a href="#">Dep. de Pesquisa e Publicação <span uk-icon="chevron-down"></span></a>
                              <ul class="uk-nav-sub">
                                  <li class="@if ($isActiveSubSub == "dppe.dpp.apresentacao") uk-active @endif"><a href="{{route('dppe.dpp.apresentacao')}}"><span uk-icon="arrow-right"></span> Apresentação</a></li>
                                  <li class="@if ($isActiveSubSub == "dppe.dpp.projectos") uk-active @endif"><a href="{{route('dppe.dpp.projectos')}}"><span uk-icon="arrow-right"></span> Projectos</a></li>
                                  <li class="@if ($isActiveSubSub == "dppe.dpp.linhasdepesquisa") uk-active @endif"><a href="{{route('dppe.dpp.linhasdepesquisa')}}"><span uk-icon="arrow-right"></span> Linhas de Pesquisa</a></li>                                                    
                              </ul>
                          </li>
                        </ul>

                        <ul class="uk-nav-default" uk-nav>
                          <li class="@if ($isActiveSub == "dppe.dpp") uk-active @endif uk-parent">
                              <a href="#">Dep. de Extensão e Inovação <span uk-icon="chevron-down"></span></a>
                              <ul class="uk-nav-sub">
                                  <li class="@if ($isActiveSubSub == "dppe.dpi.apresentacao") uk-active @endif"><a href="{{route('dppe.dpi.apresentacao')}}"><span uk-icon="arrow-right"></span> Apresentação</a></li>
                                  <li class="@if ($isActiveSubSub == "dppe.dpi.projectos") uk-active @endif"><a href="{{route('dppe.dpi.projectos')}}"><span uk-icon="arrow-right"></span> Projectos</a></li>
                                  <li class="@if ($isActiveSubSub == "dppe.dpi.escolinhas") uk-active @endif"><a href="{{route('dppe.dpi.escolinhas')}}"><span uk-icon="arrow-right"></span> Escolinhas</a></li>                                                        
                              </ul>
                          </li>
                        </ul>

                        <ul class="uk-nav-default" uk-nav>
                          <li class="@if ($isActiveSub == "dppe.dpp") uk-active @endif uk-parent">
                              <a href="#">Publicações <span uk-icon="chevron-down"></span></a>
                              <ul class="uk-nav-sub">
                                  <li class="@if ($isActiveSubSub == "dppe.publ.revistaseartigos") uk-active @endif"><a href="{{route('dppe.publ.revistaseartigos')}}"><span uk-icon="arrow-right"></span> Revistas e Artigos</a></li>
                                  <li class="@if ($isActiveSubSub == "dppe.publ.actasdeconferencia") uk-active @endif"><a href="{{route('dppe.publ.actasdeconferencia')}}"><span uk-icon="arrow-right"></span> Actas de Conferência</a></li>
                                  <li class="@if ($isActiveSubSub == "dppe.publ.livros") uk-active @endif"><a href="{{route('dppe.publ.livros')}}"><span uk-icon="arrow-right"></span> Livros</a></li>  
                                  <li class="@if ($isActiveSubSub == "dppe.publ.revistasdeposgraduacao") uk-active @endif"><a href="{{route('dppe.publ.revistasdeposgraduacao')}}"><span uk-icon="arrow-right"></span> Revista de Pós-Graduação</a></li>                                                    
                              </ul>
                          </li>
                        </ul>
                    </ul>
                </li>
              </ul>
            </div>
            <div class="">
              <ul class="uk-nav uk-nav-default uk-dark" uk-nav>

                <li class="@if ($isActive == "serviços") uk-active @endif uk-parent">
                    <a href="#">Serviços <i uk-icon="icon: chevron-down"> </i></a>
                    <ul class="uk-nav-sub uk-text-left uk-background-default uk-border-rounded ">
                        <li><a href="#"><span uk-icon="arrow-right"></span> Pastorial Universitária</a></li>
                        <li><a href="#"><span uk-icon="arrow-right"></span> Associação dos Estudantes</a></li>
                        <li><a href="#"><span uk-icon="arrow-right"></span> Intercâmbios - Internacionalização</a></li>
                        <li><a href="https://mail.up.ac.mz" target="_blank"><span uk-icon="link"></span> Email Corporativo</i></a></li>
                        <li><a href="https://sigeup.up.ac.mz" target="_blank"><span uk-icon="link"></span> SIGEUP</a></li>
                        <li><a href="{{route('upengenheiros')}}" target="_blank"><span uk-icon="link"></span> Plataforma de Engenheiros</a></li>
                        {{-- <li><a href="#" target="_blank"><span uk-icon="link"></span> CEAD</a></li> --}}
                    </ul>
                </li>

                @auth
                  {{-- <a class="uk-button uk-button-danger"  ></i></a> --}}
                  <li class="uk-text-danger uk-link-heading"><a class="" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Saír da conta<i uk-icon='sign-out'></i></a></li>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                  </form>
                @else
                  <li class="@if ($isActive == "login") uk-active @endif"><a href=" {{route('login')}} ">Login</a></li>
                @endauth
              </ul>
            </div>
        </div>


        <h3 class="uk-text-bold uk-light uk-text-right">CONTACTOS</h3>
        <hr>
        <div class="uk-grid uk-child-width-1-1 uk-text-right uk-light" uk-grid>
          <div class="uk-text-right">
            <div class="uk-margin-small">
              <img src="{{ asset('images/icons/phone.svg') }}" uk-svg width="20" height="20" alt=""> <a class="uk-button-text uk-button" style="text-transform: none;"  href="tel:+25829330359"> +258 293-30359</a>
            </div>
            <div class="uk-margin-small">
              <span style="color: #005fe4 !important">FAX</span>: <a class="uk-button-text uk-button" style="text-transform: none;" href="#">+258 293-30354</a>
            </div>
            <div class="uk-margin-small">
              <img src="{{ asset('images/icons/gps.svg') }}" uk-svg width="20" height="20" alt=""> <a class="uk-button-text uk-button" style="text-transform: none;" target="_blank" href="https://maps.google.com/"> Av. Américo Boavida. CP 12, Cidade de Maxixe</a>
            </div>
            <div class="uk-margin-small">
              <img src="{{ asset('images/icons/email.svg') }}" uk-svg width="20" height="20" alt=""></i> <a class="uk-button-text uk-button" style="text-transform: none;" target="_blank" href="mailto:info@upmaxixe.ac.mz"> info@upmaxixe.ac.mz</a>
            </div>
          </div>
        </div>
    </div>
</div>

<div class="uk-section uk-section-xsmall uk-section-default" style="border-top: 1px solid #f2f2f2">
    <div class="uk-container uk-container-small uk-text-small">
        <div class="uk-grid" uk-grid>
            <div class="uk-width-expand">
                <span style="font-size: 12px" class="uk-link-reset">
                    <strong>Copyright (c) <script>document.write(new Date().getFullYear())</script></strong>
                    <span class="uk-visible@m">– Universidade Pedagógica da Maxixe (UniSaF)</i></span>
                    {{-- (DEMO SITE FOR UP) --}}
                    <span class="uk-hidden@m">
                        <br>
                        Universidade Pedagógica da Maxixe (UniSaF)
                    </span>
                </span>
                <br>
                <small style="color: #bbb">
                  Feito com <span uk-icon='heart' class="uk-text-danger" ratio='.5'></span> por <a href="https://kishannareshpal.github.io" class="uk-link-reset uk-text-bold">Kishan Nareshpal Jadav.</a>
                </small>
            </div>
        </div>
    </div>
</div>
<!--/FOOTER-->
