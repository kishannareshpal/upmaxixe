{{-- Check for errors --}}
@if (count($errors) > 0)
  @php
    $i = 1;
  @endphp
  @foreach ($errors->all() as $error)
    <div id="alert{{$i}}" class="uk-alert-danger" uk-alert>
        <a class="uk-alert-close" uk-close>
          <span class="uk-padding-remove uk-margin-remove-top" uk-countdown="date: {{Carbon\Carbon::now()->addSeconds(10)->toIso8601String()}}">
            <small class="uk-text-muted uk-float-right uk-countdown-seconds uk-margin-remove uk-padding-remove"></small>
          </span>
        </a>
        <p>{{$error}}</p>
    </div>

    <script type="text/javascript">
      var closetimeout = 10000;
      setTimeout(function () {
        UIkit.alert('#alert{{$i}}').close();
      }, closetimeout);
    </script>

    @php
      $i = $i+1;
    @endphp
  @endforeach
@endif


{{-- Check for success --}}
@if (session('success'))
  <div class="uk-alert-success" uk-alert>
      <a class="uk-alert-close" uk-close>
        <span class="uk-padding-remove uk-margin-remove-top" uk-countdown="date: {{Carbon\Carbon::now()->addSeconds(10)->toIso8601String()}}">
          <small class="uk-text-muted uk-float-right uk-countdown-seconds uk-margin-remove uk-padding-remove"></small>
        </span>
      </a>
      <p class="uk-padding-remove uk-margin-remove-bottom">{{session('success')}}</p>
  </div>

  {{-- to autom close the alert --}}
  <script type="text/javascript">
    var closetimeout = 10000;
    setTimeout(function () {
      UIkit.alert('.uk-alert-success').close();
    }, closetimeout);
  </script>
@endif

{{-- Check for erros --}}
@if (session('failed'))
  <div class="uk-alert-danger" uk-alert>
      <a class="uk-alert-close" uk-close>
        <span class="uk-padding-remove uk-margin-remove-top" uk-countdown="date: {{Carbon\Carbon::now()->addSeconds(10)->toIso8601String()}}">
          <small class="uk-text-muted uk-float-right uk-countdown-seconds uk-margin-remove uk-padding-remove"></small>
        </span>
      </a>
      <p class="uk-padding-remove uk-margin-remove-bottom">{{session('failed')}}</p>

  </div>

  {{-- to autom close the alert --}}
  <script type="text/javascript">
    var closetimeout = 10000;
    setTimeout(function () {
      UIkit.alert('.uk-alert-danger').close();
    }, closetimeout);
  </script>
@endif
