<script type="text/template" id="qq-template-manual-trigger">
  <div class="js-upload qq-uploader">
     <div class="qq-uploader-selector uk-placeholder">
       <div>
         <div class="qq-total-progress-bar-container-selector qq-total-progress-bar-container">
             <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar"></div>
         </div>
         <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
             <span class="qq-upload-drop-area-text-selector"></span>
         </div>

         <div>
           <span uk-icon="icon: cloud-upload"></span>
           <span class="uk-text-middle">Anexe as fotos arrastando-as até aqui ou</span>
           <div uk-form-custom>
               <input type="file" multiple>
               <span class="qq-upload-button-selector uk-button-text uk-button-default uk-text-primary" style="padding: 2px">selecione-os</span>.
           </div>
         </div>


         <span class="qq-drop-processing-selector" uk-spinner>
             <div uk-spinner ratio='.7'></div>
             <!-- <span class="qq-drop-processing-spinner-selector" uk-spinner></span> -->
         </span>

         <ul style="margin-top: 10px; max-height: 360px; overflow-y: scroll" class="qq-upload-list-selector uk-list uk-list-striped" aria-live="polite" aria-relevant="additions removals">
             <li name='makemegreen' class="mdl-color--yellow-50">

                 <div class="qq-progress-bar-container-selector">
                     <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>
                 </div>

                 <div class="uk-comment">
                    <header class="uk-comment-header uk-grid-medium uk-flex-middle" uk-grid>
                        <div class="uk-width-auto">
                            <img class="qq-thumbnail-selector uk-comment-avatar" qq-max-size="100" qq-server-scale>
                        </div>
                        <div class="uk-width-expand">
                            <input class="qq-edit-filename uk-margin-remove-right" tabindex="0" type="text">
                            <span class="qq-upload-file-selector qq-upload-file uk-margin-remove"></span>
                            <!-- <span class="qq-edit-filename-icon-selector uk-margin-remove uk-link" style="cursor: pointer" uk-icon='pencil' aria-label="Editar nome do ficheiro."></span> -->
                            <ul class="uk-comment-meta uk-margin-remove uk-padding-remove">
                                <li>
                                  <button type="button" class="qq-btn qq-upload-cancel-selector uk-button uk-button-text uk-text-muted">Remover</button>
                                </li>
                                <li>
                                  <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
                                </li>
                                <li>
                                  <button type="button" class="qq-btn qq-upload-retry-selector uk-button uk-button-text">Tentar novamente</button>
                                </li>
                                <li>
                                  <span class="qq-upload-size-selector qq-upload-size"></span>
                                </li>
                            </ul>
                        </div>
                    </header>
                 </div>

                 <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
             </li>
         </ul>

         <div style="margin-top: 2px">
             <button type="button" id="trigger-upload" class="uk-button  uk-box-shadow-medium uk-button-primary">
                 <i uk-icon='upload' class="uk-position-relative" style="bottom: 1px"></i> Anexar
             </button>
         </div>

         <dialog class="qq-alert-dialog-selector">
             <div class="qq-dialog-message-selector"></div>
             <div class="qq-dialog-buttons">
                 <button type="button" class="qq-cancel-button-selector">Close</button>
             </div>
         </dialog>

         <dialog class="qq-confirm-dialog-selector">
             <div class="qq-dialog-message-selector"></div>
             <div class="qq-dialog-buttons">
                 <button type="button" class="qq-cancel-button-selector">No</button>
                 <button type="button" class="qq-ok-button-selector">Yes</button>
             </div>
         </dialog>

         <dialog class="qq-prompt-dialog-selector">
             <div class="qq-dialog-message-selector"></div>
             <input type="text">
             <div class="qq-dialog-buttons">
                 <button type="button" class="qq-cancel-button-selector">Cancel</button>
                 <button type="button" class="qq-ok-button-selector">Ok</button>
             </div>
         </dialog>
       </div>
     </div>
   </div>
 </script>


 {{-- Fine uploader styles --}}
 @include('inc.fineuploader.fineuploader-style')
