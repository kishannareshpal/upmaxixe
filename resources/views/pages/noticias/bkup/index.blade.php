@extends('layouts.app')

@section('title')
    UP Maxixe –> Notícias
@endsection

@php
  /*
    Must init this variable..
    this serves to know on which page the user to highlight
    the correspondent MENU (NAVBAR) item down bellow. in @section('menus')
  */

  $isActive = 'noticias';
@endphp



@section('menus')
  @include('inc.menus')
  @include('inc.mobilemenus')
@endsection


{{--  The right side of the navbar  --}}
@section('navbar-right-content')
  @auth
    @include('inc.navbar-user')
  @endauth
@endsection

@section('headcsslink')
  <style>
    /* Fade */
    .hvr-fade {
      display: inline-block;
      vertical-align: middle;
      -webkit-transform: perspective(1px) translateZ(0);
      transform: perspective(1px) translateZ(0);
      box-shadow: 0 0 1px rgba(0, 0, 0, 0);
      overflow: hidden;
      -webkit-transition-duration: 0.3s;
      transition-duration: 0.3s;
      -webkit-transition-property: color, background-color;
      transition-property: color, background-color;
    }
    .hvr-fade:hover, .hvr-fade:focus, .hvr-fade:active {
      background-color: #1589e9;
      color: white;
    }
  </style>
@endsection

{{--  Body  --}}
@section('body')


    <section class="uk-section uk-margin-remove-bottom uk-section-small">
        <div class="uk-container uk-container-small">

            <h1 class="uk-margin-remove-bottom uk-h1">
              <a href="{{route('noticias.index')}}" class="uk-text-bold uk-link-reset"><img src="{{asset('images/icons/newspaper.svg')}}" style="position: relative; bottom: 3.1px" width="30" height="30" uk-svg> Notícias</a>
              @auth
                <a href="{{route('noticias.create')}}" class="hvr-grow-shadow uk-dark uk-button uk-button-primary uk-border-rounded uk-box-shadow-small uk-float-right" style="position: relative; top: 6.4px;" uk-icon='plus' uk-tooltip="title: Publicar Nova Notícia; delay: 300"></a>
              @endauth
            </h1>
            <p style="position: relative; bottom: 10px" class="uk-margin-remove-top uk-text-bold"><small>da</small> UP Maxixe</p>
            <!--Message-->
              @include('inc.messages')
            <!--/Message-->

            {{-- <nav class="uk-box-shadow-small uk-background-secondary uk-border-rounded" uk-navbar="mode: click; dropbar: true; dropbar-mode: push; boundary-align: true; align: center;">
              <div class="uk-navbar-center">
                <ul class="uk-navbar-nav">
                    <li>
                        <a href="#" uk-icon="arrow-down">Arquivos</a>
                        <div class="uk-navbar-dropdown uk-navbar-dropdown-width-2">
                          <div class="uk-navbar-dropdown-grid uk-child-width-1-3" uk-grid>

                            <div>
                              <ul class="uk-nav uk-navbar-dropdown-nav uk-text-center">
                                  <li class="uk-nav-header">2018</li>
                                  <li class="uk-nav-divider"></li>
                                  <li><a href="#">Janeiro</a></li>
                                  <li><a href="#">Fevereiro</a></li>
                                  <li><a href="#">Março</a></li>
                                  <li><a href="#">Abril</a></li>
                                  <li><a href="#">Maio</a></li>
                                  <li><a href="#">Junho</a></li>
                                  <li><a href="#">Julho</a></li>
                                  <li><a href="#">Agosto</a></li>
                                  <li><a href="#">Setembro</a></li>
                                  <li><a href="#">Outubro</a></li>
                                  <li><a href="#">Novembro</a></li>
                                  <li><a href="#">Dezembro</a></li>
                              </ul>
                            </div>

                            <div>
                              <ul class="uk-nav uk-navbar-dropdown-nav uk-text-center">
                                  <li class="uk-nav-header">2017</li>
                                  <li class="uk-nav-divider"></li>
                                  <li><a href="#">Janeiro</a></li>
                                  <li><a href="#">Fevereiro</a></li>
                                  <li><a href="#">Março</a></li>
                                  <li><a href="#">Abril</a></li>
                                  <li><a href="#">Maio</a></li>
                                  <li><a href="#">Junho</a></li>
                                  <li><a href="#">Julho</a></li>
                                  <li><a href="#">Agosto</a></li>
                                  <li><a href="#">Setembro</a></li>
                                  <li><a href="#">Outubro</a></li>
                                  <li><a href="#">Novembro</a></li>
                                  <li><a href="#">Dezembro</a></li>
                              </ul>
                            </div>

                            <div>
                              <ul class="uk-nav uk-navbar-dropdown-nav uk-text-center">
                                  <li class="uk-nav-header">2016</li>
                                  <li class="uk-nav-divider"></li>
                                  <li><a href="#">Janeiro</a></li>
                                  <li><a href="#">Fevereiro</a></li>
                                  <li><a href="#">Março</a></li>
                                  <li><a href="#">Abril</a></li>
                                  <li><a href="#">Maio</a></li>
                                  <li><a href="#">Junho</a></li>
                                  <li><a href="#">Julho</a></li>
                                  <li><a href="#">Agosto</a></li>
                                  <li><a href="#">Setembro</a></li>
                                  <li><a href="#">Outubro</a></li>
                                  <li><a href="#">Novembro</a></li>
                                  <li><a href="#">Dezembro</a></li>
                              </ul>
                            </div>

                          </div>
                        </div>
                    </li>



                </ul>
              </div>
          </nav> --}}

        </div>
    </section>

    <section class="uk-section-muted uk-section-small">
      <div class="uk-container uk-container-small">
        <div class="uk-grid" uk-grid>
          <div class="uk-width-1-1@m">
            @if ($posts->firstItem() == $posts->currentPage())
              <h4 class="uk-heading-line uk-text-bold uk-text-success"><span>Últimas Notícias<small></small></span></h4>
            @else
              <h4 class="uk-heading-line uk-text-bold uk-text-success"><i><span>Página {{$posts->currentPage()}} / {{$posts->lastPage()}}</i></span></h4>
            @endif

            @if (count($posts) > 0)
              @foreach ($posts as $post)

                @php
                  # localize month name to pt.. not the best way, i know... but whatever!
                    // retrieve raw post month
                    $monthRaw = $post->created_at->month;
                    // finally translate that shit
                    $ptmonth = $months[$monthRaw];
                @endphp


                <article class="uk-section uk-section-small uk-padding-remove-top">
                  <header>
                    <h2 class="uk-margin-remove-adjacent uk-text-bold uk-margin-small-bottom"><a title="Continuar a ler: '{{$post->title}}'" class="uk-link-heading" href="{{route('noticias.index')}}/{{$post->id}}">{{$post->title}}</a></h2>
                    <p class="uk-article-meta">
                      <span style="position: relative; bottom: 1px" uk-icon="calendar"></span>
                      Postado as {{$post->created_at->format('H:i A')}}, no dia {{$post->created_at->day}} de {{$ptmonth}} de {{$post->created_at->year}}
                      @if ($post->created_at->diffInDays(Carbon\Carbon::now()) < 2)
                         <span style="position: relative; bottom: 1px" class="uk-label">Recente</span>
                      @endif
                    </p>


                  </header>

                  @php // check for the first image
                    $first_image = $post->firstimage;
                  @endphp

                  @if (gettype($first_image) == 'object')
                    <figure>
                      <div class="uk-height-medium uk-background-cover uk-blend-darken" data-src="{{ asset('/storage/noticias_image') }}/{{$post->id}}/{{$first_image->url}}" uk-img>
                      </div>
                      {{-- <div uk-img data-src=""></div> --}}
                      @isset($post->subtitle)
                        <figcaption class="uk-padding-small uk-text-center uk-text-muted">{{$post->subtitle}}</figcaption>
                      @endisset
                    </figure>
                  @endif



                  <div style="overflow-y: scroll">
                    <p>{!! str_limit($post->body, $limit = 100, $end = ' [...] ') !!}</p>
                  </div>
                  <a class="uk-button uk-button-default uk-button-small hvr-fade" style="border: 1px solid" href="{{route('noticias.index')}}/{{$post->id}}" title="Continuar a Ler">Continuar a Ler...</a>
                  @auth {{-- Show more options to the event (such as edit and delete) --}}
                    <div class="uk-inline uk-padding-small">
                        <span style="cursor: pointer" class="uk-icon-button uk-background-default" uk-icon="more-vertical"></span>
                        <div class="uk-background-default uk-border-rounded" uk-dropdown="mode: click; animation: uk-animation-slide-bottom-small; duration: 150" >
                          <small class="uk-text-bold uk-text-muted">Mais Opções: </small><br>
                          <div class="uk-grid uk-grid-small uk-light" uk-grid>
                            <div>
                              <a href="{{route('noticias.index')}}/{{$post->id}}/edit" class="uk-icon-button uk-background-secondary uk-button-secondary" uk-icon="pencil"></a>
                            </div>

                            <div>
                              {!!Form::open(['action' => ['NoticiasController@destroy', $post->id], 'method'=>'POST'])!!}
                                {{Form::hidden('_method', 'DELETE')}}
                                <button type="submit" class="uk-icon-button uk-text-danger uk-button-secondary" uk-icon="trash"></button>
                                {{-- {{Form::submit('Apagar', ['class'=> 'uk-button uk-margin-top uk-button-danger'])}} --}}
                              {!!Form::close()!!}
                            </div>
                          </div>

                        </div>
                    </div>
                  @endauth
                  <hr>
                </article>
              @endforeach
            @else
              <div class="uk-margin">
                <div class="uk-card uk-card-small uk-card-default uk-card-body">
                  <h7 class="uk-h7 uk-text-muted">
                    Nenhuma notícia postada até então.
                </div>
              </div>
            @endif


            {{$posts->links()}}

          </div>
        </div>
      </div>
      <!--/CONTENT-->
    </section>
@endsection
