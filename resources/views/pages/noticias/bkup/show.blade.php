@extends('layouts.app')

@section('title')
    UP Maxixe –> Notícias -> ...
@endsection

@php
  /*
    Must init this variable..
    this serves to know on which page the user to highlight
    the correspondent MENU (NAVBAR) item down bellow. in @section('menus')
  */

  $isActive = 'noticias';
@endphp

@section('menus')
  @include('inc.menus')
  @include('inc.mobilemenus')
@endsection

@section('navbar-right-content')
  @auth
    @include('inc.navbar-user')
  @endauth
@endsection

{{--  Body  --}}
@section('body')


    {{-- <div class="uk-container uk-visible@m">
        <hr class="uk-margin-remove">
    </div> --}}
    <section class="uk-section uk-margin-remove-bottom uk-section-small uk-text-center">
        <div class="uk-container uk-container-small">
            <!--Message-->
              @include('inc.messages')
            <!--/Message-->
            <a style="position: relative; bottom: 2px;" href="{{route('noticias.index')}}" class="uk-button uk-button-default uk-button-primary uk-text-bold uk-border-rounded uk-dark"> <span style="position: relative; bottom: 1px" uk-icon='arrow-left'></span> Voltar às Notícias</a>
        </div>
    </section>

    <!--ARTICLE-->
    <section class="uk-section uk-article uk-section-muted">
        <div class="uk-container uk-container-small">
          <h2 class="uk-text-bold uk-margin-remove-bottom uk-h2">{{$post->title}}</h2>
          <small class="uk-text-muted uk-h7 uk-margin-remove-adjacent uk-margin-remove-top uk-margin-remove-bottom">
             <a class="uk-button-text uk-link-reset" href="{{route('noticias.index')}}">Notícias</a>
              / <i>
                    <span style="position: relative; bottom: 1px" uk-icon="calendar"></span>
                    Publicado as {{$post->created_at->format('H:i\h')}}, no dia {{$post->created_at->day}} de {{$ptmonth}} de {{$post->created_at->year}}
                    @auth –– <a href="{{$post->id}}/edit" class="uk-button uk-button-text uk-text-muted uk-text-bold" style="text-transform: none; position: relative; bottom: 1px">Editar</a> @endauth
                </i>
          </small>
          @if (!blank($post->subtitle))
            <p class="uk-text-lead">{{$post->subtitle}}</p>
          @endif
        </div>

        @if (!blank($post->images))
          <div class="uk-container uk-container-small uk-section uk-padding-small">
            <div class="uk-position-relative uk-visible-toggle uk-light uk-box-shadow-medium" uk-slideshow="ratio: 7:3; animation: fade; min-height: 190;">
                <ul uk-lightbox class="uk-slideshow-items">
                  @foreach ($post->images as $image)
                    <li>
                        <img uk-img data-src="{{ asset('/storage/noticias_image') }}/{{$post->id}}/{{$image->url}}" uk-cover>
                        <div class="uk-position-bottom uk-position-medium uk-text-center uk-inverse">
                          <a style="border: 1px solid #333; opacity: .8" href="{{ asset('/storage/noticias_image')}}/{{$post->id}}/{{$image->url}}" class="uk-box uk-button uk-button-small uk-button-secondary uk-border-rounded uk-box-shadow-large" uk-icon="expand"></a>
                        </div>
                    </li>
                  @endforeach
                </ul>
                {{-- <ul class="uk-slideshow-nav uk-dotnav uk-position-relative" style="left: 10px;bottom: 20px"></ul> --}}
                <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" data-uk-slidenav-previous="ratio: 1.5" data-uk-slideshow-item="previous"></a>
                <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" data-uk-slidenav-next="ratio: 1.5" data-uk-slideshow-item="next"></a>
            </div>
          </div>
        @endif





        <!-- body -->
        <div class="uk-container uk-container-small">
            <div>
              <p>{!!$post->body!!}</p>
            </div>
        </div>
        <!-- /body -->
    </section>


    <section class="uk-section uk-section-small uk-background-secondary">
      <div class="uk-container uk-container-small uk-margin-medium">
        <div class="uk-text-center">
          <small>Ír para a Notícia...</small>
          <br>
          @if (!empty($newer_post->id))
            <a uk-tooltip="{{$newer_post->title}}" href="{{route('noticias.index')}}/{{$newer_post->id}}" class="uk-button uk-button-default uk-button-secondary"><i uk-icon="chevron-left" style="position: relative; bottom: 1px"></i> Anterior</a>
          @endif
          @if (!empty($older_post->id))
            <a uk-tooltip="{{$older_post->title}}" href="{{route('noticias.index')}}/{{$older_post->id}}" class="uk-button uk-button-default uk-button-secondary">Seguinte <i uk-icon="chevron-right" style="position: relative; bottom: 1px"></i> </a>
          @endif
        </div>
      </div>
    </section>


@endsection
