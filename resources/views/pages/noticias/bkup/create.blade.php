@extends('layouts.app')

@section('title')
    Dashboard –> Notícias -> Edit
@endsection

@php
  /*
    Must init this variable..
    this serves to know on which page the user to highlight
    the correspondent MENU (NAVBAR) item.
  */

  $isActive = 'noticias';
@endphp

@section('navbar-left-content')
@endsection

@section('navbar-right-content')
  @auth
    @include('inc.navbar-user')
  @endauth
@endsection


@section('headcsslink')
  <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
@endsection


@section('jscript')
    <script>
      var editor_config = {
        content_css: "{{ asset('css/app.css') }}",
        branding: false,
        autoresize_min_height: 500,
        language_url: "{{ asset('vendor/tinyMCE_notcomposer/langs/pt_PT.js') }}",
        menubar: false,
        path_absolute : "{{ route('home') }}",
        selector: "textarea.my-editor",
        external_plugins: {
          'responsivefilemanager': "{{ asset('vendor/tinyMCE_notcomposer/plugins/responsivefilemanager/plugin.min.js') }}",
        },
        plugins: [
          "autoresize advlist textcolor imagetools charmap autolink lists link image preview hr responsivefilemanager",
          "wordcount visualchars code fullscreen",
          "media table contextmenu directionality",
          "emoticons paste colorpicker"
        ],
        toolbar: "insertfile undo redo | forecolor backcolor | styleselect | bold italic | alignleft aligncenter alignright alignjustify | charmap bullist numlist outdent indent | link image table hr | fullscreen preview",
        relative_urls: false,

        filemanager_title:"UP Maxixe – Ficheiros",
        external_filemanager_path:"/filemanager/",
        file_picker_types: 'file image media',
        file_picker_callback: function(cb, value, meta) {
            var width = window.innerWidth-30;
            var height = window.innerHeight-60;
            if(width > 1800) width=1800;
            if(height > 1200) height=1200;
            if(width>600){
              var width_reduce = (width - 20) % 138;
              width = width - width_reduce + 10;
            }
              var urltype=2;
            if (meta.filetype == 'image') { urltype=1; }
            if (meta.filetype == 'media') { urltype=3; }
            var title = "UP Maxixe – Ficheiros";
            if (typeof this.settings.filemanager_title !== "undefined" && this.settings.filemanager_title) {
              title=this.settings.filemanager_title;
            }
            var akey="key";
            if (typeof this.settings.filemanager_access_key !== "undefined" && this.settings.filemanager_access_key) {
              akey=this.settings.filemanager_access_key;
            }
            var sort_by="";
            if (typeof this.settings.filemanager_sort_by !== "undefined" && this.settings.filemanager_sort_by) {
              sort_by="&sort_by="+this.settings.filemanager_sort_by;
            }
            var descending="false";
            if (typeof this.settings.filemanager_descending !== "undefined" && this.settings.filemanager_descending) {
              descending=this.settings.filemanager_descending;
            }
            var fldr="";
            if (typeof this.settings.filemanager_subfolder !== "undefined" && this.settings.filemanager_subfolder) {
              fldr="&fldr="+this.settings.filemanager_subfolder;
            }
            var crossdomain="";
            if (typeof this.settings.filemanager_crossdomain !== "undefined" && this.settings.filemanager_crossdomain) {
              crossdomain="&crossdomain=1";
              // Add handler for a message from ResponsiveFilemanager
              if(window.addEventListener){
                window.addEventListener('message', filemanager_onMessage, false);
              } else {
                window.attachEvent('onmessage', filemanager_onMessage);
              }
            }

            tinymce.activeEditor.windowManager.open({
              title: title,
              file: this.settings.external_filemanager_path+'dialog.php?type='+urltype+'&descending='+descending+sort_by+fldr+crossdomain+'&lang='+this.settings.language+'&akey='+akey,
              width: width,
              height: height,
              resizable: true,
              maximizable: true,
              inline: 1
            }, {
              setUrl: function (url) {
                cb(url);
              }
            });
        },


      };

      tinymce.init(editor_config);
    </script>

    {{-- tinyMCE k-stylings --}}
     <style>
      .mce-top-part {
        position: relative;
        background-color: white;
      }

      div#mceu_22-body {
        background-color: #eee;
      }
  
      .mce-edit-area {
        background-color: #FFF;
        box-shadow: 0 1px 10px 0px rgba(0, 0, 0, 0.2);
        padding: 1.5cm;
        margin: 30px;
        border-width: 0px 0px 0px !important;
      }
    </style>

@endsection


{{--  Body  --}}
@section('body')
  {!! Form::open(['action' => 'NoticiasController@store', 'method' => 'POST', 'id'=>'form']) !!}
    <section class="uk-section uk-section-small uk-visible@m">
        <div class="uk-container uk-container-small">

          <div class="uk-alert-warning" uk-alert>
              <h4><span style="position: relative; bottom: 2px" uk-icon='plus-circle'> </span>&nbsp Adicionando nova notícia.</h4>
          </div>

          @include('inc.messages')

          <div class="uk-box-shadow-small uk-padding-small uk-align-right uk-navbar-right ">
              <ul class="uk-navbar-nav">
                  {{Form::submit('Publicar', ['class'=>'uk-button uk-button-primary uk-margin-right uk-text-bold'])}}
                  <a uk-tooltip="Cancelar e..." href="{{route('noticias.index')}}" class="uk-button uk-button-default"> <i style="position: relative; bottom: 1px" uk-icon='arrow-up'></i> Voltar às Notícias</a>
              </ul>
          </div>
        </div>
    </section>

    <!--ARTICLE-->
    <section class="uk-section uk-article uk-section-muted">
        {{-- Header area --}}

        <div class="uk-container uk-container-small uk-margin-small-bottom">
            {{-- <h2 class="uk-text-bold uk-margin-remove-bottom uk-h2">{{$post->title}}</h2> --}}
            <small class="uk-text-muted uk-h7 uk-margin-remove-adjacent uk-margin-remove-top uk-margin-remove-bottom"><span uk-icon='arrow-down'></span><i>Título</i></small>
            @if ($errors->has('title'))
  					 <small class="uk-text-warning"> –– {{ $errors->first('title') }}</small>
  					@endif
            {{Form::text('title', '', ['class' => 'uk-input uk-form-large uk-text-bold uk-heading', 'style' => 'border-style: dashed', 'id' => 'preventDefaultEnterAction'])}}

            {{-- <p class="uk-text-lead">Bacon ipsum dolor amet shankle jowl meatloaf, drumstick porchetta boudin chuck buffalo bacon pork belly pastrami leberkas.</p> --}}
            {{-- {!! $post->subtitle = 'Bacon ipsum dolor amet shankle jowl meatloaf, drumstick porchetta boudin chuck buffalo bacon pork belly pastrami leberkas.' !!} --}}
            <small class="uk-text-muted uk-h7"><span uk-icon='arrow-down'></span><i>Subtítulo (opcional)</i></small>
            {{Form::textarea('subtitle', '', ['class' => 'uk-input uk-text-lead uk-height-1-1 uk-padding-small', 'style' => 'border-style: dashed;', 'rows' => '2', 'draggable' => 'false'])}}
        </div>


        <!-- Upload photos area -->
        {{-- <div class="uk-container uk-container-small uk-margin-medium">
            <div id="fine-uploader-manual-trigger"></div>
        </div> --}}


        <!-- body -->
        {{-- Corpo de Texto --}}
        <div class="uk-container uk-container-small">
          <small class="uk-text-muted uk-h7"><span uk-icon='arrow-down'></span><i>Corpo do Texto</i></small>
          @if ($errors->has('body'))
          <small class="uk-text-warning"> –– {{ $errors->first('body') }}</small>
          @endif  

          <div class="uk-container uk-container-large" style="background-color: #eee">
            <div id="toolbar-container"></div>
            <div>
              {{ Form::hidden('body', '') }} 
              <textarea name="body" class="form-control my-editor"></textarea>
            </div>
          </div>
        </div>

        



        <!-- /body -->
    </section>
  {!! Form::close() !!}

    <!-- /BOTTOM BAR -->
@endsection
