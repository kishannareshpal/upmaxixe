@extends('layouts.app')

@section('title')
    UP Maxixe –> Notícias
@endsection

@php
  /*
    Must init this variable..
    this serves to know on which page the user to highlight
    the correspondent MENU (NAVBAR) item down bellow. in @section('menus')
  */

  $isActive = 'noticias';
@endphp



@section('menus')
  @include('inc.menus')
  @include('inc.mobilemenus')
@endsection


{{--  The right side of the navbar  --}}
@section('navbar-right-content')
  @auth
    @include('inc.navbar-user')
  @endauth
@endsection

@section('headcsslink')
  <style>
    /* Fade */
    .hvr-fade {
      display: inline-block;
      vertical-align: middle;
      -webkit-transform: perspective(1px) translateZ(0);
      transform: perspective(1px) translateZ(0);
      box-shadow: 0 0 1px rgba(0, 0, 0, 0);
      overflow: hidden;
      -webkit-transition-duration: 0.3s;
      transition-duration: 0.3s;
      -webkit-transition-property: color, background-color;
      transition-property: color, background-color;
    }
    .hvr-fade:hover, .hvr-fade:focus, .hvr-fade:active {
      background-color: #1589e9;
      color: white;
    }

    /* Makes a hover effect to make a clickable card  */
    .card-k {
      transition: 0.4s;
      cursor: pointer;
    }
    .card-k:hover{
      background-color: #f3f3f3
    }
  </style>
@endsection

{{--  Body  --}}
@section('body')


    <section class="uk-section uk-margin-remove-bottom uk-section-small">
        <div class="uk-container uk-container-small">

            <h1 class="uk-margin-remove-bottom uk-h1">
              <a href="{{route('noticias.index')}}" class="uk-text-bold uk-link-reset"><img src="{{asset('images/icons/newspaper.svg')}}" style="position: relative; bottom: 3.1px" width="30" height="30" uk-svg> Notícias</a>
              @auth
                <a href="{{route('noticias.create')}}" class="hvr-grow-shadow uk-dark uk-button uk-button-danger uk-border-rounded uk-box-shadow-small uk-float-right" style="position: relative; top: 6.4px;" uk-icon='plus' uk-tooltip="title: Publicar Nova Notícia; delay: 300"></a>
              @endauth
            </h1>
            <p style="position: relative; bottom: 10px" class="uk-margin-remove-top uk-text-bold"><small>da</small> UP Maxixe</p>
            <!--Message-->
              @include('inc.messages')
            <!--/Message-->

            {{-- <nav class="uk-box-shadow-small uk-background-secondary uk-border-rounded" uk-navbar="mode: click; dropbar: true; dropbar-mode: push; boundary-align: true; align: center;">
              <div class="uk-navbar-center">
                <ul class="uk-navbar-nav">
                    <li>
                        <a href="#" uk-icon="arrow-down">Arquivos</a>
                        <div class="uk-navbar-dropdown uk-navbar-dropdown-width-2">
                          <div class="uk-navbar-dropdown-grid uk-child-width-1-3" uk-grid>

                            <div>
                              <ul class="uk-nav uk-navbar-dropdown-nav uk-text-center">
                                  <li class="uk-nav-header">2018</li>
                                  <li class="uk-nav-divider"></li>
                                  <li><a href="#">Janeiro</a></li>
                                  <li><a href="#">Fevereiro</a></li>
                                  <li><a href="#">Março</a></li>
                                  <li><a href="#">Abril</a></li>
                                  <li><a href="#">Maio</a></li>
                                  <li><a href="#">Junho</a></li>
                                  <li><a href="#">Julho</a></li>
                                  <li><a href="#">Agosto</a></li>
                                  <li><a href="#">Setembro</a></li>
                                  <li><a href="#">Outubro</a></li>
                                  <li><a href="#">Novembro</a></li>
                                  <li><a href="#">Dezembro</a></li>
                              </ul>
                            </div>

                            <div>
                              <ul class="uk-nav uk-navbar-dropdown-nav uk-text-center">
                                  <li class="uk-nav-header">2017</li>
                                  <li class="uk-nav-divider"></li>
                                  <li><a href="#">Janeiro</a></li>
                                  <li><a href="#">Fevereiro</a></li>
                                  <li><a href="#">Março</a></li>
                                  <li><a href="#">Abril</a></li>
                                  <li><a href="#">Maio</a></li>
                                  <li><a href="#">Junho</a></li>
                                  <li><a href="#">Julho</a></li>
                                  <li><a href="#">Agosto</a></li>
                                  <li><a href="#">Setembro</a></li>
                                  <li><a href="#">Outubro</a></li>
                                  <li><a href="#">Novembro</a></li>
                                  <li><a href="#">Dezembro</a></li>
                              </ul>
                            </div>

                            <div>
                              <ul class="uk-nav uk-navbar-dropdown-nav uk-text-center">
                                  <li class="uk-nav-header">2016</li>
                                  <li class="uk-nav-divider"></li>
                                  <li><a href="#">Janeiro</a></li>
                                  <li><a href="#">Fevereiro</a></li>
                                  <li><a href="#">Março</a></li>
                                  <li><a href="#">Abril</a></li>
                                  <li><a href="#">Maio</a></li>
                                  <li><a href="#">Junho</a></li>
                                  <li><a href="#">Julho</a></li>
                                  <li><a href="#">Agosto</a></li>
                                  <li><a href="#">Setembro</a></li>
                                  <li><a href="#">Outubro</a></li>
                                  <li><a href="#">Novembro</a></li>
                                  <li><a href="#">Dezembro</a></li>
                              </ul>
                            </div>

                          </div>
                        </div>
                    </li>



                </ul>
              </div>
          </nav> --}}

        </div>
    </section>

    <section class="uk-section-muted uk-section-small">
      <div class="uk-container uk-container-small">
        <div class="uk-grid" uk-grid>
          <div class="uk-width-1-1@m">
            @if ($posts->firstItem() == $posts->currentPage())
              <h4 class="uk-heading-line uk-text-bold uk-text-danger"><span>Últimas Notícias<small></small></span></h4>
            @else
              <h4 class="uk-heading-line uk-text-bold uk-text-success"><i><span>Página {{$posts->currentPage()}} / {{$posts->lastPage()}}</i></span></h4>
            @endif

            @if (count($posts) > 0)
              <div uk-grid="masonry: true" class="uk-child-width-1-2@m uk-grid-small uk-margin-bottom">
                @foreach ($posts as $post)
                  @php
                    # localize month name to pt.. not the best way, i know... but whatever!
                      // retrieve raw post month
                      $monthRaw = $post->created_at->month;
                      // finally translate that shit
                      $ptmonth = $months[$monthRaw];
                  @endphp

                    <div class="uk-margin-small-bottom">
                      <div class="card-k uk-border-rounded uk-text-break uk-card uk-card-default uk-card-body uk-transition-toggle">
                        <h3 class="uk-margin-remove uk-text-bold"><a title="Continuar a ler: '{{$post->title}}'" class="uk-link-heading" href="{{route('noticias.index')}}/{{$post->id}}">{{$post->title}}</a></h3>
                        @isset($post->subtitle)
                          <h5 class="uk-margin-remove uk-margin-small-bottom">{{$post->subtitle}}</h5>
                        @endisset
    
                        <span class="uk-comment-meta uk-margin-remove-top">
                          <small>{{$post->created_at->day}} de {{$ptmonth}}, {{$post->created_at->year}} – {{$post->created_at->format('H:i A')}}</small>
                          @if ($post->created_at->diffInDays(Carbon\Carbon::now()) < 10)
                            <span style="position: relative; bottom: 1px" class="uk-label uk-label-danger">
                              <small>
                                @switch($post->created_at->diffInDays(Carbon\Carbon::now()))
                                  @case(0)
                                    A algumas horas
                                    @break
                                  @case(1)
                                    Ontem
                                    @break
                                  @default
                                    {{ $post->created_at->diffInDays(Carbon\Carbon::now()) }} dias atras
                                @endswitch
                              </small>
                            </span>
                          @endif
                          <p class="uk-margin-remove uk-transition-fade uk-transition-slide-right-small uk-position-bottom-right" style="top: 5px; right: 10px">
                            {{-- <a class="uk-link-muted uk-text-bold" style="color: #000 !important; opacity: .4; z-index:auto" href="https://www.google.com" target="_blank">Editar</a> --}}
                            <a class="uk-link-muted uk-text-bold" style="color: #F90F47 !important; opacity: .4" href="#">Ler a notícia <span uk-icon="arrow-right"></span></a>
                          </p>
                        </span>

                        <a href="{{route('noticias.index')}}/{{$post->id}}"><span style="position: absolute; width: 100%; height: 100%; top: 0; left: 0; z-index: 1;" class="linkSpanner"></span></a>
    
                        @php
                          # Extract imageurl from the string to an array of urls = $image_urls.
                          preg_match_all('#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#', $post->image_urls, $image_urls);
                          $image_urls = $image_urls[0]; // TRICK: i did this because it for some reason returns two arrays: [the first one, containing the actual image urls that i need] and [the second one, literaly an array of "g"'s]
                        @endphp
    
                        @if (count($image_urls) > 0)
                          <figure>
                            <div class="uk-height-small uk-background-cover uk-blend-darken" data-src="{{ $image_urls[0] /* the first image */ }}" uk-img>
                            </div>
                          </figure>
                        @endif
    
                        <div style="overflow-y: scroll">
                          <p>{!! str_limit($post->body, $limit = 100, $end = ' [...] ') !!}</p>
                        </div>
                      </div>
                    </div>
                @endforeach
              </div>
            @else
              <div class="uk-margin">
                <div class="uk-card uk-card-small uk-card-default uk-card-body">
                  <h6 class="uk-h7 uk-text-muted">
                    Nenhuma notícia postada até então.
                  </h6>
                </div>
              </div>
            @endif

            {{$posts->links()}}

          </div>
        </div>
      </div>
      <!--/CONTENT-->
    </section>
@endsection
