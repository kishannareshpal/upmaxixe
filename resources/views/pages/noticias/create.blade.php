@extends('layouts.app')

@section('title')
    Notícias -> Create
@endsection

@php
  /*
    Must init this variable..
    this serves to know on which page the user to highlight
    the correspondent MENU (NAVBAR) item.
  */

  $isActive = 'noticias';
@endphp

@section('navbar-left-content')
@endsection

@section('navbar-right-content')
  @auth
    @include('inc.navbar-user')
  @endauth
@endsection


@section('headcsslink')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.4.1/jquery.fancybox.min.css" />
  <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>

  <script src="../vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
@endsection


@section('jscript')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.4.1/jquery.fancybox.min.js"></script>
  {{-- CK Editor logic --}}
  <script>
      $(document).ready(function (){
        CKEDITOR.replace('article-ckeditor', {width: '100%'});
        var url = $('#img_urls').val();

        if (url != 0) {
          try {
            url = JSON.parse(url.toString());
          
            url.forEach(url => {
              $('#img_ul').append(image_element(url));            
            });
          } catch (error) {
            $('#img_ul').append(image_element(url));            
          }
        }

      });

      var element;
      var images_array = [];


      $('.iframe-btn').fancybox({	
        'width'		  : 900,
        'height'	  : 600,
        'type'		  : 'iframe',
        'autoScale' : false
      });


      function gettogether_thoseimages(){
        images_array = [];
        $('#img_ul').find('.imagess').each(function(element_index){
          var image_url = $('#img_ul').find('.imagess');
          images_array.push(image_url[element_index].src);
        });

        // Set the current list of images to the hidden input that will be pushed to database containing the updated list of images.
        $('#img_urls').val(JSON.stringify(images_array));
        $('#numberofimages').html(images_array.length + (images_array.length == 1 ? ' imagem selecionada' : ' imagens selecionadas'));
      }


      function image_element(url){
        var path_to_icon = "{{asset('images/icons/delete.svg')}}";
        return '<li class="uk-transition-toggle removeli">'+
                  '<div>'+
                    '<img class="imagess" uk-img data-src="' + url + '" width="100">' + 
                    '<div class="uk-transition-fade uk-position-center">' + 
                      '<button class="uk-preserve uk-button uk-button-danger uk-button-small uk-box-shadow-large">Apagar</button>' + 
                    '</div>' +
                  '</div>' +
                '</li>';
      }

      function responsive_filemanager_callback(field_id){
        var url = $('#'+field_id).val();
      
        try {
          url = JSON.parse(url.toString());
          // $('#'+field_id).val(url.length + " Imagens Selecionadas");
          url.forEach(url => {
            $('#img_ul').append(image_element(url));            
          });
        } catch (error) {
          // $('#'+field_id).val("1 Imagem Selecionada");
          $('#img_ul').append(image_element(url));            
        }
        // gettogether_thoseimages();
      }

      $('#img_ul').on('DOMSubtreeModified', function(event) {
        gettogether_thoseimages();
        console.log('Image url list refreshed.');
      });
      

      $('#img_ul').on('click', '.removeli', function() {
        element = this;
        let conf = confirm("Tem a certeza que deseja remover a imagem?");
        
        if(conf) {
          element.remove();
        }
      });

  </script>
@endsection


{{--  Body  --}}
@section('body')
  {!! Form::open(['action' => 'NoticiasController@store', 'method' => 'POST', 'id'=>'form']) !!}
    <section class="uk-section uk-section-small uk-visible@m">
        <div class="uk-container uk-container-small">

          <div class="uk-alert-warning" uk-alert>
            <h4><span style="position: relative; bottom: 2px" uk-icon='plus-circle'> </span>&nbsp Adicionando nova notícia.</h4>
          </div>

          @include('inc.messages')

          <div class="uk-box-shadow-small uk-padding-small uk-align-right uk-navbar-right ">
            <ul class="uk-navbar-nav">
              {{Form::submit('Publicar', ['class'=>'uk-button uk-button-primary uk-margin-right uk-text-bold'])}}
              <a uk-tooltip="Cancelar e..." href="{{route('noticias.index')}}" class="uk-button uk-button-default"> <i style="position: relative; bottom: 1px" uk-icon='arrow-up'></i> Voltar às Notícias</a>
            </ul>
          </div>
        </div>
    </section>

    <!--ARTICLE-->
    <section class="uk-section uk-article uk-section-muted">
        {{-- Header area --}}
        <div class="uk-container uk-container-small">
          {{-- <h2 class="uk-text-bold uk-margin-remove-bottom uk-h2">{{$post->title}}</h2> --}}
          <small class="uk-text-muted uk-h7 uk-margin-remove-adjacent uk-margin-remove-top uk-margin-remove-bottom"><span uk-icon='arrow-down'></span><i>Título</i></small>
          @if ($errors->has('title'))
            <small class="uk-text-warning"> –– {{ $errors->first('title') }}</small>
          @endif
          {{Form::text('title', '', ['class' => 'uk-input uk-form-large uk-text-bold uk-heading', 'style' => 'border-style: dashed', 'id' => 'preventDefaultEnterAction'])}}

          {{-- <p class="uk-text-lead">Bacon ipsum dolor amet shankle jowl meatloaf, drumstick porchetta boudin chuck buffalo bacon pork belly pastrami leberkas.</p> --}}
          {{-- {!! $post->subtitle = 'Bacon ipsum dolor amet shankle jowl meatloaf, drumstick porchetta boudin chuck buffalo bacon pork belly pastrami leberkas.' !!} --}}
          <small class="uk-text-muted uk-h7"><span uk-icon='arrow-down'></span><i>Subtítulo (opcional)</i></small>
          {{Form::textarea('subtitle', '', ['class' => 'uk-input uk-text-lead uk-height-1-1 uk-padding-small', 'style' => 'border-style: dashed;', 'rows' => '2', 'draggable' => 'false'])}}
        </div>


        <!-- Upload photos area -->
        <div class="uk-container uk-container-small uk-margin-medium">
            {{Form::hidden('image_urls', '', ['class' => 'uk-input uk-disabled uk-width-1-4@s', 'placeholder'=>'Nenhuma imagem selecionada.', 'id'=>'img_urls'])}}            
            <div class="uk-card uk-padding-small uk-card-default uk-card-body uk-margin-small">                
              <small class="uk-text-muted uk-h7 uk-margin-remove-adjacent uk-margin-remove-top uk-margin-remove-bottom"><span uk-icon='arrow-down'></span><i>Selecione Imagens (opcional)</i></small><br>
              <ul class="uk-thumbnav uk-margin-remove" uk-margin id="img_ul">
                {{-- LI's Will be dynamically added by the callback of Responsive Filemanager--}}
              </ul><br>
              <a href="{{route('home')}}/filemanager/dialog.php?type=1&field_id=img_urls" class="iframe-btn uk-button uk-button-small uk-button-primary" type="button">Adicionar Imagem</a>
              <br><small id="numberofimages" class="uk-margin-remove uk-text-muted">0 imagens selecionadas.</small>
            </div>

        </div>




        <!-- body -->
        {{-- style="width: 100%" --}}
        <div class="uk-container uk-container-small">
            <small class="uk-text-muted uk-h7"><span uk-icon='arrow-down'></span><i>Corpo do Texto</i></small>
            @if ($errors->has('body'))
            <small class="uk-text-warning"> –– {{ $errors->first('body') }}</small>
            @endif
            <div class="uk-grid uk-grid-medium" data-uk-grid>
              {{-- <p class="uk-text-small">{{$post->body}}</p> --}}
              {{-- <div name='body' id="pell"></div> --}}
              <div style="width: 100%">
                {{Form::textarea('body', '', ['id' => 'article-ckeditor'])}}
              </div>

            </div>
        </div>



        <!-- /body -->
    </section>
  {!! Form::close() !!}

    <!-- /BOTTOM BAR -->
@endsection
