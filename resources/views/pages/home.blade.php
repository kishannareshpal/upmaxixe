@extends('layouts.app')


@php
  /*
    Must init this variable..
    this serves to know on which page the user to highlight
    the correspondent MENU (NAVBAR) item down bellow. in @section('menus')
  */

  $isActive = 'inicio';
@endphp

@section('menus')
  @include('inc.menus')
  @include('inc.mobilemenus')
@endsection


@section('title')
    Universidade Pedagógica –– Maxixe (UniSaF)
@endsection

@section('navbar-right-content')
  @auth
    @include('inc.navbar-user')
  @endauth
@endsection

@section('headcsslink')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
  <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>

  {{-- Facebook Social Plugin --}}
  <script>
    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = 'https://connect.facebook.net/pt_PT/sdk.js#xfbml=1&version=v3.0&appId=241251759809366&autoLogAppEvents=1';
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
  </script>

  <style>
    .uk-card-small.uk-card-body, .uk-card-small .uk-card-body {
      padding: 10px 10px;
    }

    /* Makes a hover effect to make a clickable card  */
    .card-k {
      transition: 0.4s;
      cursor: pointer;
    }
    .card-k:hover{
      background-color: #f3f3f3
    }
  </style>
@endsection

@section('jscript')
  <script type="text/javascript">

  function showmethemodal(element, id){
    document.getElementById('modal_list').innerHTML = "";
    document.getElementById('modal_list_second').innerHTML = "";
    document.getElementById('modal_description').innerHTML = "";
    document.getElementById('modal_description_second').innerHTML = "";

    var elm = element.innerHTML;
    element.innerHTML = "<div style='position: relative; bottom: 2px' uk-spinner='ratio: .5'></div>";

    $.ajax({
      url: "{{route('eventos.index')}}/showmodal/"+id,
      type: "GET",
      data: {
        uid: id,
        access_token: '{{ csrf_token() }}', // this is important for Laravel to receive the
      },
      success: function(response){
        // IF THE IMAGE EXIST SHOW A MODAL WITH IMAGE
        if (response.imageurl !== null) {
          UIkit.modal('#showwithimage').show();
          element.innerHTML = elm;

          document.getElementById('modal_image').setAttribute('style', 'background-image: url("http://localhost/upmaxixe/public/storage/eventos_image/' + response.id + '/' + response.imageurl + '")');
          document.getElementById('modal_image_a').setAttribute('href', 'http://localhost/upmaxixe/public/storage/eventos_image/' + response.id + '/' + response.imageurl);
          // document.getElementById('modal_image_b').setAttribute('href', 'http://localhost/upmaxixe/public/storage/eventos_image/' + response.id + '/' + response.imageurl);
          document.getElementById('modal_title').innerHTML = response.name;

          if (response.description !== (null || " " || "")) {
            document.getElementById('modal_description').innerHTML = response.description + "<hr>";
          }

          document.getElementById('modal_list').innerHTML += "<li>" + "Local: " + "<strong>" + response.local + "</strong></li>";
          document.getElementById('modal_list').innerHTML += "<li>" + "Data: " + "<strong>" + response.horario_do_inicio + "</strong</li>";

          if (response.horario_do_fim !== undefined) {
            document.getElementById('modal_list').innerHTML += "<li>" + "Data do Fim do Evento: " + "<strong>" + response.horario_do_fim + "</strong></li>";
          }

          if (response.capacidade == 'unlimited') {
            var capacidade = "ILIMITADA";
            document.getElementById('modal_list').innerHTML += "<li>" + "Capacidade: " + "<strong>" + capacidade + "</strong>" + "</li>";
          } else {
            var capacidade = response.capacidade;
            document.getElementById('modal_list').innerHTML += "<li>" + "Capacidade: " + "<strong>" + capacidade + "</strong> Pessoas." + "</li>";
          }

          if (response.precoindividual == 'free') {
            var precoindividual = "GRATUITO";
            document.getElementById('modal_list').innerHTML += "<li>" + "Preço Individual: " + "<strong>" + precoindividual + "</strong>" + "</li>";
          } else {
            var precoindividual = response.capacidade;
            document.getElementById('modal_list').innerHTML += "<li>" + "Preço Individual: " + "<strong>" + precoindividual + " MT </strong>" + "</li>";
          }

        // IF THE IMAGE DOESNT EXIST SHOW A SIMPLER MODAL
        } else {
          UIkit.modal('#showwithoutimage').show();
          element.innerHTML = elm;

          document.getElementById('modal_title_second').innerHTML = response.name;
          if (response.description !== null) {
            document.getElementById('modal_description_second').innerHTML = response.description;
          }

          document.getElementById('modal_list_second').innerHTML += "<li>" + "Local: " + "<strong>" + response.local + "</strong></li>";
          document.getElementById('modal_list_second').innerHTML += "<li>" + "Data: " + "<strong>" + response.horario_do_inicio + "</strong</li>";

          if (response.horario_do_fim !== null) {
            document.getElementById('modal_list_second').innerHTML += "<li>" + "Data do Fim do Evento: " + "<strong>" + response.horario_do_fim + "</strong></li>";
          }

          if (response.capacidade == 'unlimited') {
            var capacidade = "ILIMITADA";
            document.getElementById('modal_list_second').innerHTML += "<li>" + "Capacidade: " + "<strong>" + capacidade + "</strong>" + "</li>";
          } else {
            var capacidade = response.capacidade;
            document.getElementById('modal_list_second').innerHTML += "<li>" + "Capacidade: " + "<strong>" + capacidade + "</strong> Pessoas." + "</li>";
          }

          if (response.precoindividual == 'free') {
            var precoindividual = "GRATUITO";
            document.getElementById('modal_list_second').innerHTML += "<li>" + "Preço Individual: " + "<strong>" + precoindividual + "</strong>" + "</li>";
          } else {
            var precoindividual = response.capacidade;
            document.getElementById('modal_list_second').innerHTML += "<li>" + "Preço Individual: " + "<strong>" + precoindividual + " MT </strong>" + "</li>";
          }
        } // end -> else if(imageDoesNotExist)

      }
    });
  }
  </script>
@endsection



{{--> BODY <--}}
@section('body')

  {{-- Destaques then, Mensagem do Director and Events --}}
  <section class="uk-section uk-margin-medium-top uk-padding-remove-top uk-padding-remove-bottom uk-margin-medium-bottom">
    <div class="uk-container uk-container-large">
      <div class="uk-grid" uk-grid>
        {{-- destaques left side --}}
        <div class="uk-width-expand">
            @if (count($destaques) > 0)
              <div class="uk-container">
                <div class="uk-position-relative uk-visible-toggle uk-light uk-box-shadow-medium" uk-slideshow="ratio: 800:500; animation: slide; min-height: 190; autoplay: true;">
                    <ul uk-lightbox class="uk-slideshow-items">
                        @foreach ($destaques as $destaque)
                          <li>
                            <div>
                              <img uk-img data-type="image" data-src="{{ asset('/storage/destaques_image') }}/{{$destaque->id}}/{{$destaque->url}}" uk-cover>
                            </div>
                            @if (!empty($destaque->description))
                              <div class="uk-position-bottom uk-preserve-color uk-padding-small uk-overlay-primary">
                                <p style="color: white" class="uk-margin-remove">{!! $destaque->description !!}</p>
                              </div>
                            @endif
                            <div>
                                <div class="uk-position-top-left uk-position-medium uk-text-center uk-inverse">
                                  <a style="border: 1px solid #333; opacity: .8" href="{{ asset('/storage/destaques_image') }}/{{$destaque->id}}/{{$destaque->url}}" class="uk-box uk-button uk-button-small uk-button-secondary uk-border-rounded uk-box-shadow-large" uk-icon="expand"></a>
                                </div>
                            </div>
                          </li>
                        @endforeach
                    </ul>

                    <a style="color: black !important; background-color: white; opacity: .3" class="uk-position-center-left uk-position-small" href="#" uk-slidenav-previous="ratio: 1.5" uk-slideshow-item="previous"></a>
                    <a style="color: black !important; background-color: white; opacity: .3" class="uk-position-center-right uk-position-small" href="#" uk-slidenav-next="ratio: 1.5" uk-slideshow-item="next"></a>

                </div>
              </div>
          @endif
          
          
          <div class="uk-container uk-margin-top">
            <h4 class="uk-heading-line uk-text-bold"><span class="uk-label uk-label-default" style="font-size: 0.75rem"> #Emdestaque</span></h4>
            <div style="border: 2px solid #1e87f0;" class="uk-card uk-card-default uk-grid-collapse uk-padding-medium uk-margin-large-bottom@s" uk-grid>
              <div class="uk-card-media-left uk-cover-container uk-width-1-3@m">
                <img uk-img data-src="{{asset('images/nelsonmandela_prof.png')}}" uk-cover>
                <canvas width="400" height="200"></canvas>
              </div>
              <div class="uk-background-cover uk-width-expand@m" style="background-color: #2a2929; opacity: .9;">
                  <div class="uk-card-body">
                    <small class="uk-margin-remove-bottom uk-light uk-text-bold"><i><span class="uk-text-warning">Conferência Internacional</span> –– Reflectindo sobre o Legado de Nelson Mandela</i></small>
                    <h4 class="uk-margin-remove-top uk-margin-remove-bottom uk-text-bold mdl-color-text--white">"Nelson Mandela: Educação e Reconciliação no Centenário do seu Nascimento (1918-2018)"</h4>
                    <small class="uk-text-warning uk-margin-remove-top">Nos dias <strong>13 e 14 de Setembro</strong> na <strong>Sala Magna</strong> da <span alt="Av. Américo Boavida. CP 12, Cidade de Maxixe"><strong>UP Maxixe</strong></span>.</small>
                  
                    <div class="uk-overflow-auto uk-border-rounded uk-margin-medium-top">
                      <table class="uk-table uk-table-middle uk-table-divider uk-table-small uk-light">
                        <thead>
                          <tr>
                            <th class="uk-text-left"><small>Arquivo</small></th>
                            <th class="uk-width-small uk-text-right" style="position: relative; right: 6px"><small>Download</small></th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td class="uk-text-bold">Termos de Referência<br>Terms of Reference<br>Termes de Référence</td>
                            <td class="uk-text-right"><a target="_blank" href="https://www.upmaxixe.ac.mz/storage/rfm/source/PDFs/Eventos/Conferencia%20Internacional%20(Nelson%20Mandela)%20%E2%80%93%202018/Termos%20de%20Referencia.pdf" class="uk-border-rounded uk-button uk-button-small uk-button-primary"><img src="{{asset('images/icons/pdf.svg')}}" uk-svg width="14" height="14" alt=""> PDF</a></td>
                          </tr>
                          <tr>
                            <td class="uk-text-bold">Ficha de Inscrição<br>Registration Form<br>Fiche d' Inscription</td>
                            <td class="uk-text-right"><a target="_blank" href="https://www.upmaxixe.ac.mz/storage/rfm/source/PDFs/Eventos/Conferencia%20Internacional%20(Nelson%20Mandela)%20%E2%80%93%202018/Ficha%20de%20inscricao%20-%20Confere%CC%82ncia%20Internacional.pdf" class="uk-border-rounded uk-button-primary uk-button uk-button-small"><img src="{{asset('images/icons/pdf.svg')}}" uk-svg width="14" height="14" alt=""> PDF</a></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
        {{-- END OF LEFT SIDEBAR (destaques and mensagem do dire) --}}

        {{-- eventos e noticias por baixo - right sidebar --}}
        <div class="uk-width-1-4@m uk-margin-small-bottom">
          {{-- eventos por cima - right sidebar --}}
          <div class="uk-margin-large-bottom">
            <h4 class="uk-heading-line uk-text-bold uk-margin-small-bottom"><span><img src="{{ asset('images/icons/calendar.svg') }}" width="20" height="24" alt="" uk-svg> <span class="uk-label uk-label-warning" style="font-size: 0.75rem">Eventos</span></span></h4>
           
            <div class="uk-position-relative uk-visible-toggle uk-light uk-margin-top-remove" uk-slider>
              <ul uk-lightbox uk-grid class="uk-slider-items uk-child-width-1-2 uk-grid-match">
                @foreach ($posts as $post)
                  @if(isset($post['full_picture']) && !isset($post['message']))
                    <li>
                      <div class="uk-panel uk-transition-toggle" style="border: 1px solid #eee;">
                        <a href="{{$post['full_picture']}}">
                          <img uk-img data-type="image" data-src="{{$post['full_picture']}}">
                        </a>
                        <span class="uk-transition-fade uk-position-top-right uk-preserve" style="position: relative; right: 10px; top: 7px; pointer-events: none"><img class="uk-preserve" src="{{asset('images/icons/expand.svg')}}" uk-svg width="20" height="20"></span>
                      </div>
                    </li>
                  @endif
                @endforeach
              </ul>
          
              <a style="color: black; background-color: white; opacity: .5" class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
              <a style="color: black; background-color: white; opacity: .5" class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slider-item="next"></a>
            </div>

            <dl class="uk-description-list">
              @foreach ($eventos as $evento)
                @php
                  $endTime_to_add = clone $evento['end_time'];
                  $now = new DateTime();
                @endphp

                @if (new DateTime() < $evento['start_time'] && new DateTime() < $evento['end_time'] || new DateTime() < $endTime_to_add->add(new DateInterval('P5D')))
                  <div class="card-k uk-card uk-border-rounded uk-box-shadow-medium uk-box-shadow-hover-large uk-margin-small-bottom uk-card-default uk-card-small uk-grid-collapse" uk-grid>
                      <div class="uk-width-expand">
                        <div class="uk-card-body">
                          {{-- <a href="" class="uk-link-reset"> --}}
                          <p class="uk-text-warning uk-margin-remove-bottom uk-text-bold">{!! str_limit($evento['name'], $limit = 150, $end = ' [...]') !!}</p>
                          {{-- </a> --}}

                          @if (new DateTime() > $evento['start_time'] && new DateTime() < $evento['end_time'])
                            <span style="background: red; color: white; padding: 2px 3px; border-radius: 3px; opacity: .5">Live</span>
                             – 
                            <span style="border-radius: 3px; opacity: .5">
                              @if ($evento['start_time']->format('d-M-Y') == $evento['end_time']->format('d-M-Y'))
                                <small>até às {{$evento['end_time']->format('H:i')}}.</small>                                  
                              @else
                                <small>até dia {{$evento['end_time']->format('d M\, Y')}}, às {{$evento['end_time']->format('H:i')}}.</small>                                  
                              @endif
                            </span>
                            <br>                                 
                          @elseif(new DateTime() < $evento['start_time'])
                            <small style="background: #505050; color: white; padding: 1px 1px 1px 3px; border-radius: 3px;">
                              @if ($evento['start_time']->format('d-M-Y') == $now->format('d-M-Y'))
                                Hoje
                              @else
                                {{$evento['start_time']->format('d M\, Y')}}
                              @endif
                            </small>
                            
                            @if ($evento['start_time']->format('d-M-Y') == $evento['end_time']->format('d-M-Y'))
                              &nbsp;  
                              <small style="border: 1px solid #505050; color: black; padding: 1px 1px 1px 3px; border-radius: 3px;">
                                {{$evento['start_time']->format('H:i')}} até {{$evento['end_time']->format('H:i')}}
                              </small>                              
                            @else
                              &nbsp;<small>até</small>
                              <small style="border: 1px solid #505050; color: black; padding: 1px 1px 1px 3px; border-radius: 3px;">
                                {{$evento['end_time']->format('d M\, Y')}}      
                              </small>                              
                            @endif
                            
                            <br>
                          @elseif(new DateTime() > $evento['start_time'])
                            <span title="Evento Terminado!" style="text-align: center; background: #FAA05A; color: white; padding: 1px 3px; border-radius: 3px">
                              <span style="position: relative; bottom: 1.5px;">
                                <img title="Evento Terminado!" src="{{ asset('images/icons/completed.svg') }}" uk-svg width="13" height="13" alt="Done!">
                                @php
                                  $now = new DateTime($now->format('d-M-Y'));
                                  $comparedtonow = new DateTime($evento['end_time']->format('d-M-Y'));
                                @endphp

                                @switch($now->diff($comparedtonow)->format('%a'))
                                  @case(0)
                                      <small>Este evento terminou <strong>hoje</strong> às <strong>{{ $evento['end_time']->format('H:i') }}</strong>.</small>                                   
                                      @break
                                  @case(1)
                                      <small>Este evento aconteceu ontem.</small>                                                                           
                                      @break
                                  @default
                                      <small>Este evento aconteceu a {{$now->diff($comparedtonow)->format('%a')}} dias atrás.</small>                                                                           
                                      @break
                                @endswitch
                              </span>
                            </span><br>
                          @endif

                          <small>{!! str_limit($evento['description'], $limit = 150, $end = ' [...]') !!}</small>
                        </div>

                        <a target="_blank" href="{{route('eventos.index')}}/{{$evento['id']}}"><span style="position: absolute; width: 100%; height: 100%; top: 0; left: 0; z-index: 1;" class="linkSpanner"></span></a>                        
                      </div>
                    
                    @isset($evento['cover']['source'])
                      <div uk-lightbox class="uk-card-media-right uk-cover-container uk-width-1-5" style="z-index: 100">
                        <a href="{{$evento["cover"]['source']}}" uk-cover data-caption="<a class='uk-button uk-light uk-button-primary uk-border-rounded' href='{{route('eventos.index')}}/{{$evento['id']}}'>Ver detalhes sobre o evento...</a>">
                          <img src="{{$evento["cover"]['source']}}" uk-cover>
                          <canvas width="50" height="50"></canvas>
                        </a>
                      </div>
                    @endisset
                  </div>
                @endif
              @endforeach 

              @foreach ($posts as $post)
                {{-- isset($post['full_picture']) && isset($post['message'])) || isset($post['message'] --}}
                @if($post['type'] != 'event')
                  @if (isset($post['full_picture']) && isset($post['message']) || isset($post['message']))
                    @switch($post['type'])
                        @case('link')
                          {{--  show the alternate view for links --}}
                          <div title="Ír para: {{$post['link']}}" style="border: 1px solid #c3dbff" class="card-k uk-transition-toggle uk-card uk-border-rounded uk-box-shadow-small uk-box-shadow-hover-medium uk-margin-small-bottom uk-card-default uk-card-small uk-grid-collapse" uk-grid>
                            @isset($post['full_picture'])
                              <div style="border-radius: 4px 0px 0px 4px !important" class="uk-card-media-right uk-transition-toggle uk-cover-container uk-width-1-5">
                                <div uk-cover>
                                  <img uk-img data-type="image" data-src="{{$post['full_picture']}}" uk-cover>
                                  <canvas width="50" height="50"></canvas>
                                  <span uk-cover class="uk-overlay uk-overlay-default uk-position-center"></span>
                                </div>
                              </div>
                            @endisset
                            <div class="uk-width-expand">
                              <div uk-lightbox class="uk-card-body">
                                @isset($post['message'])
                                  <small class="uk-text-primary">{{$post['link']}}</small>
                                  <br>
                                  <span style="color: black"><small>{!! str_limit(stripURL($post['message']), $limit = 600, $end = ' [...]') !!}</small></span>                                    
                                @endisset
                              </div>
                            </div>
                            <small class="uk-transition-fade uk-transition-slide-right-small uk-position-top-right" style="top: 5px; right: 10px"><a class="uk-link-muted" style="color: #F90F47 !important;" href="#"><img src="{{asset('images/icons/foreign.svg')}}" width="14" height="14" uk-svg></a></small>
                            <a href="{{$post['link']}}"><span style="position: absolute; width: 100%; height: 100%; top: 0; left: 0; z-index: 1;" class="linkSpanner"></span></a>
                          </div>
                          @break                  
                        @default
                          {{-- otherwise show the default --}}
                          <div style="border: 1px solid #eee" class="card-k uk-transition-toggle uk-card uk-border-rounded uk-box-shadow-small uk-box-shadow-hover-medium uk-margin-small-bottom uk-card-default uk-card-small uk-grid-collapse" uk-grid>
                            @isset($post['full_picture'])
                              <div uk-lightbox style="border-radius: 4px 0px 0px 4px !important" class="uk-card-media-right uk-cover-container uk-width-1-5 uk-width-1-6@s">
                                <div uk-cover>
                                  <img uk-img data-type="image" data-src="{{$post['full_picture']}}" uk-cover>
                                  <canvas width="50" height="50"></canvas>
                                </div> 
                              </div>
                            @endisset
                                  
                            <div class="uk-width-expand">
                              <div class="uk-card-body">
                                <span style="color: black"><small>{!! str_limit($post['message'], $limit = 150, $end = ' [...]') !!}</small></span>
                              </div>
                            </div>

                            @isset($post['full_picture'])
                              <div uk-lightbox>
                                <a href="{{$post['full_picture']}}" data-caption="<div class='uk-padding-small uk-background-default uk-border-rounded' style='color: black'>{{ $post['message'] }}</div>"><span style="position: absolute; width: 100%; height: 100%; top: 0; left: 0; z-index: 1;" class="linkSpanner"></span></a>
                              </div>
                            @else
                              <a title="Ír para: {!! str_limit($post['permalink_url'], $limit = 50, $end = ' [...]') !!}" target="_blank" href="{{$post['permalink_url']}}"><span style="position: absolute; width: 100%; height: 100%; top: 0; left: 0; z-index: 1;" class="linkSpanner"></span></a>
                              <small class="uk-transition-fade uk-transition-slide-right-small uk-position-top-right" style="top: 5px; right: 10px"><a class="uk-link-muted" style="color: #F90F47 !important;" href="#"><img src="{{asset('images/icons/foreign.svg')}}" width="14" height="14" uk-svg></a></small>
                            @endisset

                          </div>
                          @break  
                    @endswitch
                  @endif
                @endif
              @endforeach
            </dl>
          </div>

          {{-- noticias por baixo de eventos - right sidebar --}}
          <div>
            <h4 class="uk-heading-line uk-text-bold"><span><img src="{{ asset('images/icons/newspaper.svg') }}" width="20" height="24" alt="" uk-svg> <span class="uk-label uk-label-danger" style="font-size: 0.75rem; background-color: #F90F47"><a href="{{route('noticias.index')}}" class="uk-link-reset">Notícias</a></span></span></h4>
            <dl class="uk-description-list uk-description-list-divider">
              @foreach ($noticias as $noticia)
                <div class="uk-margin-small-bottom">
                  <div style="border: 1px solid #F90F47;" class="card-k uk-border-rounded uk-text-break uk-card uk-card-default uk-card-body uk-transition-toggle uk-padding-small">
                    <p class="uk-margin-remove">{{$noticia->title}}</p>
                    <span class="uk-comment-meta uk-margin-remove-top">
                      <small>{{$noticia->created_at->format('d M\, Y \– h:i')}}</small>
                      <small class="uk-transition-fade uk-transition-slide-right-small uk-position-top-right" style="top: 5px; right: 10px"><a class="uk-link-muted" style="color: #F90F47 !important;" href="#">Ler <span uk-icon="arrow-right"></span></a></small>
                    </span>
                    <a href="{{route('noticias.index')}}/{{$noticia->id}}"><span style="position: absolute; width: 100%; height: 100%; top: 0; left: 0; z-index: 1;" class="linkSpanner"></span></a>
                  </div>
                </div>
              @endforeach
            </dl>

            <div>
              <a href="{{route('noticias.index')}}" class="uk-button uk-button-default uk-button-small uk-text-danger uk-border-rounded uk-text-bold"><span uk-icon="chevron-right" style="position: relative; bottom: 1px"></span>Ver Mais Notícias...</a>
            </div>
          </div>
        </div>

      </div>
    </div>
  </section>


  <section class="uk-section uk-padding-remove uk-box-shadow-small">
    <div class="uk-card uk-card-default uk-grid-collapse uk-child-width-1-2@s uk-margin" uk-grid>
        <div class="uk-card-media-left uk-cover-container">
          <img uk-img data-src="{{asset('images/pages/pesquisaeextensao/revistaseartigos/revistas/guti/guti_cover.png')}}" alt="" uk-cover>
          <canvas width="600" height="400"></canvas>
        </div>
        <div class="uk-background-cover" style="background-image: url({{asset('images/pages/pesquisaeextensao/revistaseartigos/revistas/guti/guti_bg.png')}});">
            <div class="uk-card-body">
                <h6 class="uk-margin-remove-bottom uk-light uk-text-bold"><i>Leia a nossa Revista Científica:</i></h6>
                {{-- <img src="{{ asset('/images/guti.png')}}" alt="" width="200"> --}}
                <h1 class="uk-margin-remove-top uk-text-bold mdl-color-text--white">GUTI – Letras e Humanidade</h1>
                <h6 class="uk-margin-remove-bottom uk-light uk-text-bold"><i>Volume 1 (2018)</i></h6>                
                <hr class="uk-divider-small">
                <p class="mdl-color-text--white">
                  Ao iniciar mais um ano académico, a inédita revista científica da UP Maxixe sai a público, como um pretexto para aglutinar pesquisadores que se ocupam da questão humana nas suas variadas declinações. A revista <i><b>Guti</b></i>, que em Gitonga significa <i>‘sabedoria’</i>, procura não só contribuir na divulgação de estudos sobre linguística, literatura e filosofia, bem como propiciar a divulgação de trabalhos inéditos de análise da história e cultura moçambicana a partir de diferentes disciplinas e o debate acerca da educação, tendo como eixo a problemática identidade/universalidade.
                </p>

                {{-- <div class="uk-card uk-grid-collapse uk-child-width-1-4@s uk-margin" uk-grid>
                  <div class="uk-width-expand uk-padding-small">
                  </div>
                </div> --}}

                <a href="{{route('dppe.publ.revistaseartigos')}}/#guti" class="uk-button uk-button-danger uk-border-rounded uk-text-bold"><span uk-icon="chevron-right" style="position: relative; bottom: 1px"></span> Mais sobre a Revista...</a>

            </div>
        </div>
    </div>
  </section>



  <section class="uk-section uk-article uk-background-cover" style="background-image: url({{asset('images/campus1_blured.png')}})">
      <div class="uk-container uk-container-small">
          <h2 class="uk-text-bold uk-h1 uk-margin-remove-adjacent uk-text-center">O Nosso Orgulho</h2>
          <div class="uk-divider-small uk-text-center"></div>
          <p>Com apenas @php echo date('Y')-2006 @endphp anos desde a sua criação, a nossa Delegação, deu um enorme contributo no campo de formação de profissionais com o título de ensino superior, tendo suprimido para o mercado a lacuna que existia de quadros altamente formados.</p>

          <div class="uk-grid-small uk-child-width-expand@s uk-text-center" uk-grid>
              <div>
                  <img src="{{ asset('images/icons/graduation-hat.svg') }}" width="60" height="60" uk-svg>
                  <h2 class="uk-margin-remove uk-text-bold">2400+</h2>
                  <small class="uk-margin-remove uk-text-muted">Licenciados desde 2009</small>
              </div>
              <div class="uk-divider-small uk-hidden@s"></div>
              <div>
                  <img src="{{ asset('images/icons/presentation.svg') }}" width="60" height="60" uk-svg>
                  <h2 class="uk-margin-remove uk-text-bold">125+</h2>
                  <small class="uk-margin-remove uk-text-muted">Docentes</small>
              </div>
              <div class="uk-divider-small uk-hidden@s"></div>
              <div>
                  <img src="{{ asset('images/icons/class.svg') }}" width="60" height="60" uk-svg>
                  <h2 class="uk-margin-remove uk-text-bold">4300+</h2>
                  <small class="uk-margin-remove uk-text-muted">Estudantes</small>
              </div>
              <div class="uk-divider-small uk-hidden@s"></div>
              <div>
                  <img src="{{ asset('images/icons/campus.svg') }}" width="60" height="60" uk-svg>
                  <h2 class="uk-margin-remove uk-text-bold">2</h2>
                  <small class="uk-margin-remove uk-text-muted">Campus Universitários</small>
              </div>
          </div>
      </div>
  </section>

  {{--> QUOTE <--}}
  <section class="uk-section uk-section-muted uk-alert-warning">
    <div class="uk-container uk-container-small">
        <div class="uk-width-auto uk-text-center">
            <img uk-img class="uk-border-circle" width="200" height="200" data-src="{{asset('images/santapaula.jpg')}}">
            <p class="uk-margin-small-top uk-text-bold uk-margin-remove-bottom">Santa Paula Isabel Cerioli</p>
            <p class="uk-text-muted uk-text-italic uk-margin-remove"><i>(Fundadora da Congregação da Sagrada Família, Italia(1816-1865))</i></p>
        </div>
        <h2 style="font-family: 'times new roman'" class="uk-margin-remove-top uk-text-bold uk-h1 uk-margin-remove-adjacent uk-text-center"><q cite="Santa Paula Isabel Cerioli">Educar é dar uma Segunda criação mais excelente do que a Primeira.</q></h2>
    </div>
  </section>


    {{-- FACEBOOK_FEED --}}
    {{-- <section class="uk-section uk-section-default">
      <div class="uk-container uk-container-large uk-text-center">
        <div class="fb-page" data-href="https://www.facebook.com/upmaxixe" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/upmaxixe" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/upmaxixe">UniSaF</a></blockquote></div>
      </div>
    </section> --}}






@endsection
