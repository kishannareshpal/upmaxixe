@extends('layouts.app')

@section('title')
    Dashboard - UniSaF
@endsection

@php
  /*
    Must init this variable..
    this serves to know on which page the user to highlight
    the correspondent MENU (NAVBAR) item down bellow. in @section('menus')
  */

  $isActive = 'dashboard';
@endphp


@section('navbar-right-content')
  @auth
    @include('inc.navbar-user')
  @endauth
@endsection

@section('body')
<div class="uk-container uk-margin">
	<div class="uk-card uk-card-body uk-width-1-1">
		<h3 class="uk-card-title uk-text-bold uk-text-warning"><i style="position: relative; bottom: 2px" uk-icon='album'></i> Administração</h3>
    <div class="uk-child-width-1-3@m" uk-grid="masonry: true">

        <div>
            <div class="uk-card uk-card-small uk-card-default uk-border-rounded uk-card-body uk-box-shadow-hover-large ">
                <h3 class="uk-card-title uk-text-bold uk-margin-remove-bottom">Destaques</h3>
                <h6 class="uk-text-muted uk-margin-remove-top uk-margin-bottom">Imagens com descrição que destaca momentos importantes do ano/mês.<br><br>É mostrada na página inicial do site.</h6>
                <a class="uk-text-primary" href=" {{ route('dashboard.destaques') }}">Ver os destaques.</a>
            </div>
        </div>

        <div>
            <div class="uk-card uk-card-small uk-card-default uk-border-rounded uk-card-body uk-box-shadow-hover-large ">
              <h3 class="uk-card-title uk-margin-bottom"><a class="uk-link-reset uk-text-bold" href="{{route('eventos.index')}}">Eventos</a></h3>
                <a class="uk-text-primary" href="{{route('eventos.create')}}">Adicionar novo evento.</a><br>
                <a class="uk-text-primary" href="{{route('eventos.index')}}">Ver Eventos.</a>
            </div>
        </div>

        <div>
            <div class="uk-card uk-card-small uk-card-default uk-border-rounded uk-card-body uk-box-shadow-hover-large ">
                <h3 class="uk-card-title uk-margin-bottom"><a class="uk-link-reset uk-text-bold" href="{{route('noticias.index')}}">Notícias</a></h3>

                <a class="uk-text-primary" href="{{route('noticias.create')}}">Adicionar nova notícia.</a><br>
                <a class="uk-text-primary" href="{{route('noticias.index')}}">Ver Notícias.</a>
            </div>
        </div>

        <div>
            <div class="uk-card uk-card-small uk-card-default uk-border-rounded uk-card-body uk-box-shadow-hover-large ">
                <h3 class="uk-card-title uk-margin-bottom"><a class="uk-link-reset uk-text-bold" href="{{route('docentes')}}">Docentes</a></h3>

                <a class="uk-text-primary" href="{{route('docentes.manage')}}">Adicionar novo docente.</a><br>
                <a class="uk-text-primary" href="{{route('docentes')}}">Ver Docentes.</a>
            </div>
        </div>

        <div>
            <div class="uk-card uk-card-small uk-card-default uk-border-rounded uk-card-body uk-box-shadow-hover-large ">
                <h3 class="uk-card-title uk-margin-remove-bottom"><a class="uk-link-reset uk-text-bold" href="{{route('docentes')}}">Centros e Núcleos</a></h3>
                <p class="uk-margin-bottom uk-margin-remove-top uk-text-muted">Pesquisa | Extensão</p>

                <a class="uk-text-primary" href="{{route('centrosenucleos.create')}}">Adicionar novo núcleo.</a><br>
                <a class="uk-text-primary" href="{{route('centrosenucleos.index')}}">Ver Centros e Núcleos.</a>
            </div>
        </div>

    </div>
	</div>
</div>
@endsection
