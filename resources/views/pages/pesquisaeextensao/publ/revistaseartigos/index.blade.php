@extends('layouts.app')

@section('title')
    UP Maxixe –> Revistas | Artigos
@endsection

@php
  /*
    Must init this variable..
    highlight @section('[mobile]menus')
  */
  $isActive = 'pesquisaeextensao';
  $isActiveSub = 'pee.revistaseartigos';
@endphp

@section('menus')
  @include('inc.menus')
  @include('inc.mobilemenus')
@endsection


{{--  The right side of the navbar  --}}
@section('navbar-right-content')
  @auth
    @include('inc.navbar-user')
  @endauth
@endsection


{{--  Body  --}}
@section('body')

    <!--Message-->
    @include('inc.messages')
    <!--/Message-->


    <section class="uk-section uk-section-muted uk-section-small">
        <div class="uk-container uk-container-small">
            <h2 class="uk-text-bold uk-margin-remove-bottom uk-h1 uk-margin-remove-adjacent uk-margin-remove-top">Revistas e Artigos</h2>
            <p class="uk-text-small uk-margin-remove-top">da UP Maxixe</>
            {{-- <div class="uk-divider-small"></div> --}}

            <section id="guti" class="uk-section uk-section-small">
                <div>
                  <div class="uk-card uk-card-default uk-grid-collapse uk-margin" uk-grid>
                    <div class="uk-card-media-left uk-cover-container uk-width-1-3@s">
                      <img uk-img data-src="{{asset('images/pages/pesquisaeextensao/revistaseartigos/revistas/guti/guti_front.png')}}" alt="" uk-cover>
                      <canvas width="600" height="400"></canvas>
                    </div>
                    <div class="uk-background-default uk-width-expand@s">
                      <div class="uk-card-body">
                        <h2 class="uk-margin-remove-top">GUTI – Letras e Humanidade</h2>
                        <hr class="uk-divider-small">
                        <i>Volume 1 – 2018</i>
                        <p class="uk-text-justify">
                          Ao iniciar mais um ano académico, a inédita revista científica da UP-Maxixe sai a público, como um pretexto para aglutinar pesquisadores que se ocupam da questão humana nas suas variadas declinações. A revista <i><b>Guti</b></i>, que em Gitonga significa <i>‘sabedoria’</i>, procura não só contribuir na divulgação de estudos sobre linguística, literatura e filosofia, bem como propiciar a divulgação de trabalhos inéditos de análise da história e cultura moçambicana a partir de diferentes disciplinas e o debate acerca da educação, tendo como eixo a problemática identidade/universalidade.
                        </p>  
                        <div>
                          <a href="https://www.upmaxixe.ac.mz/storage/rfm/source/PDFs/Revistas/GUTI/1a%20Edicao/Guti%20-%20Letras%20e%20Humanidade.pdf" target="_blank" style="margin-bottom: 5px" class="uk-button uk-button-primary uk-button-small uk-border-rounded"> <img src="{{asset('images/icons/pdf.svg')}}" width="18" height="18" uk-svg alt="Download PDF">&nbsp Baixar a última edição <span style="text-transform: none;">(1a)</span></a><br>
                          <a href="https://www.upmaxixe.ac.mz/storage/rfm/source/PDFs/Revistas/GUTI/Chamadas/Chamada%2520de%2520Artigos%2520%25E2%2580%2593%2520Edi%25C3%25A7%25C3%25A3o%2520II%2520(GUTI).pdf" target="_blank" class="">Call for papers</>  |  <a href="https://www.upmaxixe.ac.mz/storage/rfm/source/PDFs/Revistas/GUTI/1a%20Edicao/Guti%20-%20Letras%20e%20Humanidade.pdf" target="_blank" class="">Template</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                {{-- <hr>
                <div>
                  <div class="uk-card uk-card-default uk-card-hover uk-card-body">
                    <h3 class="uk-card-title">Outra Revista</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                  </div>
                </div> --}}
            </section>
           
        </div>
    </section>


@endsection
