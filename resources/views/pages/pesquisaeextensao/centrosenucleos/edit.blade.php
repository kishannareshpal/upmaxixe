@extends('layouts.app')

@php
  /*
    Must init this variable..
    this serves to know on which page the user to highlight
    the correspondent MENU (NAVBAR) item.
  */

  $isActive = 'centrosenucleos';
@endphp

@section('navbar-left-content')
@endsection

@section('navbar-right-content')
  @auth
    @include('inc.navbar-user')
  @endauth
@endsection


@section('headcsslink')
  <script src="../../vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
@endsection


@section('jscript')
  {{-- CK Editor logic --}}
  <script>
      CKEDITOR.replace('article-ckeditor', {width: '100%'});

      // Set editor width to 100% and height to 350px.
      // var editor = CKEDITOR.instances['article-ckeditor'];
      // editor.resize( '100%', '10px' );

  </script>
@endsection


{{--  Body  --}}
@section('body')
  {!! Form::open(['action' => ['Centros_NucleosController@update', $nucleo->id], 'method' => 'POST', 'files'=>'true']) !!}
    <section class="uk-section uk-section-small">
        <div class="uk-container uk-container-small">

          <div class="uk-alert-warning" uk-alert>
              <h4><span style="position: relative; bottom: 2px" uk-icon='future'> </span>&nbsp Editando o núcleo: <strong> {{$nucleo->title}} </strong></h4>
          </div>

          @include('inc.messages')

          <div class="uk-box-shadow-small uk-padding-small uk-align-right uk-navbar-right">
              <ul class="uk-navbar-nav uk-flex-center">
                  {{Form::hidden('_method', 'PUT')}}
                  {{Form::submit('Guardar', ['class'=>'uk-button uk-button-primary uk-margin-right uk-text-bold'])}}
                  <a href="{{route('centrosenucleos.index')}}" class="uk-button uk-button-default">Cancelar</a>
              </ul>
              <small class="uk-text-muted">*Última revisão foi feita no dia {{$nucleo->updated_at->day}} de {{$ptmonth}} de {{$nucleo->updated_at->year}}, às {{$nucleo->updated_at->format('H:i')}}.</small>
          </div>
        </div>
    </section>

    <!--ARTICLE-->
    <section class="uk-section uk-article uk-section-muted">
        {{-- Header area --}}

        <div class="uk-container uk-container-small">
            <small class="uk-text-muted uk-h7 uk-margin-remove-adjacent uk-margin-remove-top uk-margin-remove-bottom"><span uk-icon='arrow-down'></span><i>Título</i></small>
            @if ($errors->has('title'))
  					 <small class="uk-text-warning"> –– {{ $errors->first('title') }}</small>
  					@endif
            {{Form::text('title', $nucleo->title, ['class' => 'uk-input uk-form-large uk-text-bold uk-heading', 'style' => 'border-style: dashed', 'id' => 'preventDefaultEnterAction'])}}
        </div>


        <!-- body -->
        {{-- style="width: 100%" --}}
        <div class="uk-container uk-container-small">
            <small class="uk-text-muted uk-h7"><span uk-icon='arrow-down'></span><i>Corpo do Texto</i></small>
            @if ($errors->has('body'))
            <small class="uk-text-warning"> –– {{ $errors->first('body') }}</small>
            @endif
            <div class="uk-grid uk-grid-medium" data-uk-grid>
              <div style="width: 100%">
                {{Form::textarea('body', $nucleo->body, ['id' => 'article-ckeditor'])}}
              </div>

            </div>
        </div>



        <!-- /body -->
    </section>
  {!! Form::close() !!}

    <!-- /BOTTOM BAR -->
@endsection
