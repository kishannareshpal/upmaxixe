@extends('layouts.app')

@section('title')
    UP Maxixe –> Gabinete do Director
@endsection

@php
  /*
    Must init this variable..
    highlight @section('[mobile]menus')
  */
  $isActive = 'direccao';
  $isActiveSub = 'direccao.gabinete';


  /*
    Must init this variable.
    Previous and Next Page.
  */
  $previousPage = "O Director";
  $nextPage = "Secretaria";
@endphp

@section('menus')
  @include('inc.menus')
  @include('inc.mobilemenus')
@endsection


{{--  The right side of the navbar  --}}
@section('navbar-right-content')
  @auth
    @include('inc.navbar-user')
  @endauth
@endsection


{{--  Body  --}}
@section('body')


    <!--Message-->
    @include('inc.messages')
    <!--/Message-->


    <section class="uk-section uk-section-small">
        <div class="uk-container uk-container-small">
            <h2 class="uk-margin-remove-bottom uk-text-bold uk-h1 uk-margin-remove-adjacent uk-margin-remove-top">Gabinete do Director</h2>
            {{-- <h4 class="uk-margin-remove-top">Historial da UP / UniSaF</h4> --}}
            {{-- <div class="uk-divider-small"></div> --}}
        </div>
    </section>


    {{-- Next and Previous Buttons --}}
    @include('inc.prev_next')

@endsection
