@extends('layouts.app')

@section('title')
    UP Maxixe –> Missão e Valores
@endsection

@php
  /*
    Must init this variable..
    highlight @section('[mobile]menus')
  */
  $isActive = 'universidade';
  $isActiveSub = 'universidade.missaoevalores';


  /*
    Must init this variable.
    Previous and Next Page.
  */
  $previousPage = "Quem Somos";
  $nextPage = "Organigrama";
@endphp

@section('menus')
  @include('inc.menus')
  @include('inc.mobilemenus')
@endsection


{{--  The right side of the navbar  --}}
@section('navbar-right-content')
  @auth
    @include('inc.navbar-user')
  @endauth
@endsection


{{--  Body  --}}
@section('body')


    <!--Message-->
    @include('inc.messages')
    <!--/Message-->


    <section class="uk-section uk-section-small">
        <div class="uk-container uk-container-small">
            <h2 class="uk-margin-remove-bottom uk-text-bold uk-h1 uk-margin-remove-adjacent uk-margin-remove-top">Missão, Visão e Valores</h2>
            {{-- <h4 class="uk-margin-remove-top">Historial da UP / UniSaF</h4> --}}
            {{-- <div class="uk-divider-small"></div> --}}
        </div>
    </section>

    {{-- Missão --}}
    <section class="uk-section uk-section-muted">
        <div class="uk-container uk-container-small">
          <h3>A Nossa Missão</h3>
          <p>
            A UP é uma instituição de ensino vocacional cuja missão estatutária é a formação superior de professores para todos níveis de ensino e de outros profissionais para área de educação e afins, a investigação e a extensão.
            Neste contexto, a UP pugna pela universalização e regionalização, para além da sua função instrumental na produção e disseminação de conhecimento para a transformação da sociedade moçambicana rumo ao desenvolvimento social, cultural e tecnológico.
          </p>
        </div>
    </section>

    {{-- Visão --}}
    <section class="uk-section">
        <div class="uk-container uk-container-small">
          <h3>A Nossa Visão</h3>
          <p>
            A UP é uma instituição de ensino superior de referência em Moçambique, com processos de formação, pesquisa de extensão, de qualidade, enquadrados em currículos estruturados em padrões regionais e internacionais, com uma infra-estrutura física e laboratorial suficiente e moderna, a funcionar com padrões de gestão colegiais, transparentes e modernizados.
          </p>
        </div>
    </section>

    {{--  --}}
    <section class="uk-section uk-section-muted">
        <div class="uk-container uk-container-small">
          <h3>Os nossos valores</h3>
          <p>
            Constituem valores da UP os seguintes: Autonomia, Liberdade e Autonomia, Excelência, Confiança, Glocalidade, Responsabilidade Social, e Justiça e Equidade, e devem ser considerados de forma inter-relacionada.
            {{-- 1.	Autonomia:
            2.	Liberdade e Democracia:
            3.	Excelência:
            4.	Confiança:
            5.	Glocalidade:
            6.	Responsabilidade Social:
            7.	Justiça e Equidade:
            --}}
          </p>
        </div>

        <div class="uk-container uk-container-small">
            <div class="uk-grid-match uk-child-width-1-1@m uk-padding" uk-grid>
                <div>
                  <ul uk-accordion>
                      <li class="uk-card uk-card-body uk-open uk-placeholder">
                          <a class="uk-accordion-title uk-text-bold" href="#">Autonomia</a>
                          <div class="uk-accordion-content">
                            <p>
                              Este valor refere-se ao facto de a UP pautar pela acção autónoma nos domínios pedagógico científico, administrativo e financeiro, segundo está consagrado nos seus estatutos e enquanto uma instituição iniversitária.
                            </p>
                          </div>
                      </li>

                      <li class="uk-card uk-card-body uk-open uk-placeholder">
                          <a class="uk-accordion-title uk-text-bold" href="#">Liberdade e Democracia</a>
                          <div class="uk-accordion-content">
                            <p>
                              A liberdade é um dos direitos humanos fundamentais e que somente pode ser garantido na sua plenitude num contexto democrático; enquanto uma instituição universitária, a UP sublinha particularmente, entre as várias liberdades positivas, a do uso livre e público da razão e a liberdade académica. A democracia, no contexto da UP, pretende sublinhar a observação rigorosa dos métodos de colegialidade e de respeito pela maioria na tomada de decisões em todos os órgãos colegiais que estatutariamente compõem a universidade.
                            </p>
                          </div>
                      </li>

                      <li class="uk-card uk-card-body uk-open uk-placeholder">
                          <a class="uk-accordion-title uk-text-bold" href="#">Excelência</a>
                          <div class="uk-accordion-content">
                            <p>
                              Este Valor consagra o engajamento permanente da UP de, em todas as actividades da leccionação, pesquisa e extensão, procurar de forma constante e sistemática, superar-se a si mesma como indivíduos e como comunidade. Por força do seu estatuto, a UP pretende ser referência incontornável na área da educação.
                            </p>
                          </div>
                      </li>

                      <li class="uk-card uk-card-body uk-open uk-placeholder">
                          <a class="uk-accordion-title uk-text-bold" href="#">Confiança</a>
                          <div class="uk-accordion-content">
                            <p>
                              Este Valor pretende sublinhar o imperativo de a UP privilegiar, em todas as suas acções, a criação de laços muito fortes e estreitos com a sociedade moçambicana em geral, e com as comunidades circundantes de todas as suas delegações em particular. Tanto as comunidades particulares como a sociedade moçambicana no seu geral devem ganhar confiança na capacidade da UP em cumprir a missão social que lhe foi atribuída.
                            </p>
                          </div>
                      </li>

                      <li class="uk-card uk-card-body uk-open uk-placeholder">
                          <a class="uk-accordion-title uk-text-bold" href="#">Glocalidade</a>
                          <div class="uk-accordion-content">
                            <p>
                              Envolve a necessidade de a UP, no cumprimento da sua missão nas áreas do ensino, pesquisa e extensão, guiar-se pelos princípios universalistas de excelência na qualidade (global) e, ao mesmo tempo, ter a dupla responsabilidade de, em primeiro lugar, referenciar os princípios universais aos contextos culturais moçambicanos e, em segundo lugar, deixar-se inspirar nas propostas de soluções locais aos desafios de desenvolvimento (local).
                            </p>
                          </div>
                      </li>

                      <li class="uk-card uk-card-body uk-open uk-placeholder">
                          <a class="uk-accordion-title uk-text-bold" href="#">Responsabilidade Social</a>
                          <div class="uk-accordion-content">
                            <p>
                              Este Valor pretende sublinhar o facto de a UP assumir um compromisso de usar todas as suas potencialidades científico-pedagógicos para participar activamente na resolução dos obstáculos psicológicos, políticos, sociais e económicos aos processo de desenvolvimento de Moçambique.
                            </p>
                          </div>
                      </li>

                      <li class="uk-card uk-card-body uk-open uk-placeholder">
                          <a class="uk-accordion-title uk-text-bold" href="#">Justiça e Equidade</a>
                          <div class="uk-accordion-content">
                            <p>
                              Este Valor releva a necessidade de respeito permanente pelos direitos das diferentes grupos sociais existentes dentro e fora da universidade assim como promoção de igual acesso e condições de trabalho para todos.
                            </p>
                          </div>
                      </li>

                  </ul>
                </div>

            </div>
        </div>
    </section>


    {{-- Next and Previous Buttons --}}
    @include('inc.prev_next')

@endsection
