@extends('layouts.app')

@php
  /*
    Must init this variable..
    this serves to know on which page the user to highlight
    the correspondent MENU (NAVBAR) item.
  */

  $isActive = 'eventos';
@endphp

@section('navbar-left-content')
@endsection

@section('navbar-right-content')
  @auth
    @include('inc.navbar-user')
  @endauth
@endsection


@section('headcsslink')
  <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
  <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
  <script src="https://npmcdn.com/flatpickr/dist/l10n/pt.js"></script>

  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/js/standalone/selectize.js"></script>
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/css/selectize.min.css" />

@endsection


@section('jscript')

  <script>
    $(function() {
    	$('#selectize').selectize({
        delimiter: ',',
        openOnFocus: true,
        create: true
      });
    });


    $('.selectize-input').removeClass('selectize-input');
    $('#selectize-selectized').addClass('uk-input');
  </script>

  <style>
    .selectize-input{
      border: 1px dashed #e5e5e5;
      box-sizing: border-box;
      -webkit-box-shadow: none;
      box-shadow: none;
      -webkit-border-radius: 0px;
      -moz-border-radius: 0px;
      border-radius: 0px;
    }

    .selectize-input.focus{
      -webkit-box-shadow: none;
      box-shadow: none;
      border: 1px dashed #1589e9;
    }
  </style>

  <script src="{{asset('js/docentes.js')}}" charset="utf-8"></script>


    <script type="text/javascript">
      $('#form').on('keyup keypress', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
          e.preventDefault();
          return false;
        }
      });

      $("#adddoc").on('keyup', function(e){
        if (e.keyCode == 13) {
          addObra();
        }
      });
    </script>
@endsection


{{--  Body  --}}
@section('body')
    <section class="uk-section uk-section-small uk-visible@m">
        <div class="uk-container uk-container-small">

          <a href="{{route('docentes')}}" uk-tooltip="title: Cancelar e Voltar; animation: uk-animation-fade" class="uk-button uk-button-default uk-border-rounded"><span style="position: relative; bottom: 1px" uk-icon="chevron-left"></span> Cancelar e Voltar</a>
          <div class="uk-alert-primary" uk-alert>
              <h4><span style="position: relative; bottom: 2px" uk-icon='plus-circle'> </span>&nbsp Adicionando Docente</h4>
          </div>

          @include('inc.messages')
        </div>
    </section>

    <section class="uk-section uk-article uk-section-muted">
      <div class="uk-container uk-container-small">
        {{--  # Add Docentes Section --}}
          {!! Form::open(['action' => 'PagesController@addDocente', 'method'=>'POST', 'id'=>'form', 'files'=>'true']) !!}
          <div class="">
            <div class="uk-margin">
              <small class="uk-text-muted uk-h7 uk-margin-remove-top"><span uk-icon='arrow-down'></span><i>Foto do docente (opcional)</i></small>
              <br>
              <div uk-form-custom="target: true">
                <input name="foto" type="file">
                <input class="uk-alert-warning uk-input uk-form-width-large" type="text" placeholder="clique aqui ou arraste-a até aqui para selecionar...">
              </div>
            </div>

            {{-- Nomes --}}
            <div class="uk-margin-small uk-grid-small" uk-grid>
                <div class="uk-width-1-5@s">
                  <small class="uk-text-muted uk-h7 uk-margin-remove-top"><span uk-icon='arrow-down'></span><i>Título Académico</i></small>
                  @if ($errors->has('titulo'))
                   <small class="uk-text-warning"> –– {{ $errors->first('titulo') }}</small>
                  @endif
                  {!! Form::select('titulo', ['licenciado'=>'Licenciado', 'mestre'=>'Mestre', 'doutor'=>'Doutor'], null, ['class' => 'uk-select', 'id' => 'form-stacked-select']) !!}
                </div>


                <div class="uk-width-expand@s">
                  <small class="uk-text-muted uk-h7 uk-margin-remove-top"><span uk-icon='arrow-down'></span><i>Nome Completo</i></small>
                  @if ($errors->has('nome'))
        					 <small class="uk-text-warning"> –– {{ $errors->first('nome') }}</small>
        					@endif
                  {{Form::text('nome', '', ['class' => 'uk-input uk-form-medium uk-text-bold uk-heading', 'style' => 'border-style: dashed'])}}
                </div>
            </div>

            {{-- Contactos --}}
            <div class="uk-margin-small uk-grid-small" uk-grid>
                <div class="uk-width-1-2@s">
                  <small class="uk-text-muted uk-h7 uk-margin-remove-top"><span uk-icon='arrow-down'></span><i>Email (opcional)</i></small>
                  {{Form::email('email', '', ['class' => 'uk-input uk-form-medium uk-text-bold uk-heading', 'style' => 'border-style: dashed'])}}
                </div>

                <div class="uk-width-expand@s">
                  <small class="uk-text-muted uk-h7 uk-margin-remove-top"><span uk-icon='arrow-down'></span><i>Celular (opcional)</i></small>
                  {{Form::text('celular', '+258', ['class' => 'uk-input uk-form-medium uk-heading', 'style' => 'border-style: dashed'])}}
                </div>
            </div>

            {{-- Cadeira(s) que lecciona --}}
            <div class="uk-grid-small" uk-grid>
              {{-- Local --}}
              <div class="uk-width-1-1@s">
                <small class="uk-text-muted uk-h7 uk-margin-top uk-margin-remove-bottom"><span uk-icon='arrow-down'></span><i>Cadeira(s) que lecciona</i></small>
                @if ($errors->has('cadeiras'))
      					 <small class="uk-text-warning"> –– {{ $errors->first('cadeiras') }}</small>
      					@endif
                {{Form::text('cadeiras', '', ['style' => 'border-style: dashed', 'id'=>'selectize'])}}
              </div>
            </div>

            <div class="uk-margin-small">
              <button class="uk-button uk-button-default uk-button-small" type="button" uk-toggle="target: #toggle-animation; animation: uk-animation-slide-top-medium uk-animation-fade"><i uk-icon='chevron-down'></i> Suas Obras Literárias (opcional)</button>
              <div id="toggle-animation" hidden class="uk-card uk-card-default uk-card-body uk-margin-small">
                <small><span uk-icon='arrow-down'></span><i>Referência bibliográfica da sua obra destacante</i></small>
                <div>

                  <div class="uk-grid-small uk-text-center" uk-grid>
                    <div class="uk-width-expand@s">
                      <input type="text" id="obraInput" style="border-style: dashed;" class="uk-input uk-form-medium">
                    </div>
                    <div class="uk-width-1-5@s">
                      <button id="adddoc" class="uk-button uk-button-primary uk-button" type="button" onclick="addObra()">Adicionar</button>
                    </div>
                  </div>

                  <ul name="obras_ul" style="max-height: 550px; overflow-y: scroll" id="obras_ul" class="uk-list uk-padding-small uk-alert-warning uk-list-divider uk-list-large uk-box-shadow-small">
                  </ul>

                </div>
              </div>
            </div>


            <div class="uk-margin-small">
              {{-- Biografia --}}
              <small class="uk-text-muted uk-h7 uk-margin-top"><span uk-icon='arrow-down'></span><i>Pequena Biografia</i></small>
              @if ($errors->has('biografia'))
               <small class="uk-text-warning"> –– {{ $errors->first('biografia') }}</small>
              @endif
              {{Form::textarea('biografia', '', ['class' => 'uk-textarea uk-height-1-1 uk-padding-small', 'style' => 'border-style: dashed;', 'rows' => '4'])}}
            </div>


            <div class="uk-margin-small uk-text-right">
              {{-- (Add) Submit button --}}
              <button class="uk-border-rounded uk-button uk-button-primary" type="submit">Adicionar o Docente</button>
            </div>
          </div>
          {!! Form::close() !!}
      </div>
    </section>


    <!-- /BOTTOM BAR -->
@endsection
