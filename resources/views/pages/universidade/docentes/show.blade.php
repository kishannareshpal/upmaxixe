@extends('layouts.app')


@php
  /*
    Must init this variable..
    this serves to know on which page the user to highlight
    the correspondent MENU (NAVBAR) item down bellow. in @section('menus')
  */

  $isActive = 'universidade';
@endphp

@section('menus')
  @include('inc.menus')
  @include('inc.mobilemenus')
@endsection


{{--  The right side of the navbar  --}}
@section('navbar-right-content')
  @auth
    @include('inc.navbar-user')
  @endauth
@endsection


{{--  Body  --}}
@section('body')
    <section class="uk-section uk-section-small uk-section-muted uk-margin-remove">
        <div class="uk-container uk-container-small uk-margin-medium">
          <div class="uk-float-left uk-text-right">
            <a href="{{route('docentes')}}" class="uk-button uk-button-default uk-float-right uk-button-muted uk-border-rounded"> <i uk-icon="chevron-left" style="position: relative; bottom: 1px"></i> Voltar àos Docentes</a>
          </div>
        </div>

        <div class="uk-container uk-container-small">
          <!--Message-->
          @include('inc.messages')
          <!--/Message-->


          <div class="uk-grid-small" uk-grid>
            <div class="uk-width-1-3@s uk-text-center">
              <div class="uk-card uk-card-default uk-card-body uk-background-secondary uk-border-rounded" style="overflow-x: scroll">
                  @if (!empty($docente->photo))
                    <div class="uk-card-title">
                      <img uk-img class="uk-border-circle" data-src="{{asset('/storage/docentes_image')}}/{{$docente->photo}}" alt="{{$docente->photo}}">
                    </div>
                  @endif
                  <h2 class="uk-text-bold mdl-color-text--white">{{$docente->name}}</h2>
                  @foreach (str_getcsv($docente->cadeiras) as $cadeira)
                    <p style="font-size: 10px; overflow-wrap: break-word" class="uk-label">{{$cadeira}}</p>
                  @endforeach

                  <br><br>

                  @if (!empty($docente->email))
                    <small class="uk-light">
                      <i style="position: relative; bottom: 1px" uk-icon="mail"></i>
                      <a class="uk-button-text" href="mailto:{{$docente->email}}">
                        <i>{{$docente->email}}</i>
                      </a>
                    </small>
                  @endif

                  <br>

                  @if ($docente->celular !== '+258')
                    <small class="uk-light">
                      <i style="position: relative; bottom: 1px" uk-icon="phone"></i>
                      <a class="uk-button-text" href="tel:{{$docente->celular}}">
                        {{$docente->celular}}
                      </a>
                    </small>
                  @endif

                  <br>

                  @if ($docente->id === 6)
                    <small class="uk-light">
                      <i style="position: relative; bottom: 1px" uk-icon="calendar"></i>
                      <a class="uk-button-text" href="https://www.calendly.com/eziobono" target="_blank">
                        Marcar Audiência
                      </a>
                    </small>
                  @endif

              </div>

              <br>

              @auth
                <a href="{{route('docentes.manage')}}/edit/{{$docente->id}}" class="uk-button uk-button-default uk-text-bold uk-border-rounded"><i uk-icon="pencil" style="position: relative; bottom: 1px"></i> Editar</a>
                <br>
                {!! Form::open(['action' => ['PagesController@deleteDocente', $docente->id], 'method'=>'POST']) !!}
                  {{Form::hidden('_method', 'DELETE')}}
                  {{-- {{Form::submit('', ['class'=>'uk-margin-top uk-button uk-button-default uk-button-danger uk-text-bold uk-border-rounded'])}} --}}
                  <button type="submit" class="uk-margin-top uk-button uk-button-default uk-button-danger uk-text-bold uk-border-rounded"><i uk-icon="trash" style="position: relative; bottom: 1px"></i> Remover</buttton>
                {!! Form::close() !!}
              @endauth

            </div>

            <div class="uk-width-expand@s">
              <div class="uk-card uk-card-default uk-card-body uk-border-rounded">
                  <h3 class="uk-text-bold">Biografia</h3>
                  <div class="uk-divider-small"></div>
                  <p class="uk-text-justify uk-dropcap">{{$docente->biography}}</p>
              </div>

              <br>

              @if (count($docente->obras) > 0)
                <div class="uk-card uk-card-default uk-card-body">
                    <h3 class="uk-text-bold">Obras Literárias</h3>
                    <div class="uk-divider-small"></div>
                    <p class="uk-text-justify">Das suas obras literárias, destacam-se:</p>

                    <ul class="uk-list uk-list-divider uk-text-muted uk-hover">
                      @foreach ($docente->obras as $obra)
                        <li class="uk-text-justify">{{$obra->referencia}}</li>
                      @endforeach
                    </ul>
                </div>
              @endif


            </div>
          </div>

        </div>
    </section>

@endsection
