@extends('layouts.app')

@section('title')
    UP Maxixe –> Docentes
@endsection

@php
  /*
    Must init this variable..
    highlight @section('[mobile]menus')
  */
  $isActive = 'universidade';
  $isActiveSub = 'universidade.docentes';


  /*
    Must init this variable.
    Previous and Next Page.
  */
  $previousPage = "Conselho Directivo";
  // $nextPage = ""; -> NO NEXT PAGE!!
@endphp

@section('menus')
  @include('inc.menus')
  @include('inc.mobilemenus')
@endsection


{{--  The right side of the navbar  --}}
@section('navbar-right-content')
  @auth
    @include('inc.navbar-user')
  @endauth
@endsection


{{--  Body  --}}
@section('body')
    <section class="uk-section uk-section-small">
        <div class="uk-container uk-container-small">
          <!--Message-->
          @include('inc.messages')
          <!--/Message-->

          <h2 class="uk-text-bold uk-h1">
            @auth
              <h2 class="uk-text-bold uk-h1 uk-margin-remove-adjacent uk-margin-remove-top">Docentes <a href="{{route('docentes.manage')}}" class="hvr-grow-shadow uk-dark uk-button uk-button-secondary uk-border-rounded uk-box-shadow-small uk-float-right" style="position: relative; top: 6.4px;" uk-icon='plus' uk-tooltip="title: Adicionar Novo; delay: 250"></a></h2>
            @else
              Docentes
            @endauth
          </h2>



          {{-- Tabela de docentes `Doutores` --}}
          <span style="position: relative; bottom: 2px" class="uk-margin-top uk-label uk-label-success uk-text-bold"><span uk-icon='arrow-down'></span>PROFESSORES DOUTORES</span>
          <table class="uk-table uk-table-hover uk-table-divider uk-placeholder uk-table-middle">
              <thead>
                  <tr>
                      <th class="uk-text-bold uk-width-1-1">Nome Completo</th>
                      <th class="uk-text-bold">Perfil</th>
                  </tr>
              </thead>
              <tbody>
                @foreach ($doutores as $doutor)
                  <tr>
                      <td>{{$doutor->name}}</td>
                      <td><a href="{{route('docentes')}}/{{$doutor->id}}" class="uk-button uk-button-small uk-button-primary">Ver</a></td>
                  </tr>
                @endforeach
              </tbody>
          </table>



          {{-- Tabela de docentes `Mestres` --}}
          <span style="position: relative; bottom: 2px" class="uk-margin-top uk-label uk-text-bold uk-label-success"><span uk-icon='arrow-down'></span>MESTRES</span>
          <table class="uk-table uk-table-hover uk-table-divider uk-placeholder uk-table-middle">
              <thead>
                  <tr>
                      <th class="uk-text-bold uk-width-1-1">Nome Completo</th>
                      <th class="uk-text-bold">Perfil</th>
                  </tr>
              </thead>
              <tbody>
                  @foreach ($mestres as $mestre)
                    <tr>
                        <td>{{$mestre->name}}</td>
                        <td><a href="{{route('docentes')}}/{{$mestre->id}}" class="uk-button uk-button-small uk-button-primary">Ver</a></td>
                    </tr>
                  @endforeach
              </tbody>
          </table>


          {{-- Tabela de docentes `Licenciados` --}}
          <span style="position: relative; bottom: 2px" class="uk-margin-top uk-label uk-text-bold uk-label-success"><span uk-icon='arrow-down'></span>Licenciados</span>
          <table class="uk-table uk-table-hover uk-table-divider uk-placeholder uk-table-middle">
              <thead>
                  <tr>
                      <th class="uk-text-bold uk-width-1-1">Nome Completo</th>
                      <th class="uk-text-bold">Perfil</th>
                  </tr>
              </thead>
                <tbody>
                  @foreach ($licenciados as $licenciado)
                    <tr>
                        <td>{{$licenciado->name}}</td>
                        <td><a href="{{route('docentes')}}/{{$licenciado->id}}" class="uk-button uk-button-small uk-button-primary">Ver</a></td>
                    </tr>
                  @endforeach
              </tbody>
          </table>
        </div>
    </section>


    {{-- Next and Previous Buttons --}}
    @include('inc.prev_next')

@endsection
