@extends('layouts.app')

@section('title')
    UP Maxixe –> Organigrama
@endsection

@php
  /*
    Must init this variable..
    highlight @section('[mobile]menus')
  */
  $isActive = 'universidade';
  $isActiveSub = 'universidade.organigrama';


  /*
    Must init this variable.
    Previous and Next Page.
  */
  $previousPage = "Missão, Visão e Valores";
  $nextPage = "Órgãos Superiores";

@endphp

@section('menus')
  @include('inc.menus')
  @include('inc.mobilemenus')
@endsection


{{--  The right side of the navbar  --}}
@section('navbar-right-content')
  @auth
    @include('inc.navbar-user')
  @endauth
@endsection


{{--  Body  --}}
@section('body')

    <style media="screen">
      .uk-hov {
        position: relative;
        -webkit-transition: box-shadow 0.15s ease-out;
        transition: box-shadow 0.15s ease-out;
        border-radius: 2px;
      }
      .uk-hov:hover {
        box-shadow: 0 0 0 2px rgba(0, 0, 0, 0.25);
      }
    </style>
    <!--Message-->
    @include('inc.messages')
    <!--/Message-->


    <section class="uk-section uk-section-small">
        <div uk-lightbox  class="uk-container uk-container-small">
            <h2 class="uk-text-bold uk-h1 uk-margin-remove-adjacent">Organigrama</h2>
            <div class="uk-divider-small"></div>
            <a class="uk-inline uk-hov" href="{{asset('images/organigrama.jpg')}}">
              <img uk-img draggable="false" class="uk-padding" data-src="{{asset('images/organigrama.jpg')}}">
            </a>
        </div>
    </section>



    {{-- Next and Previous Buttons --}}
    @include('inc.prev_next')

@endsection
