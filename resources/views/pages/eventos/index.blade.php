@extends('layouts.app')

@section('title')
    UP Maxixe –> Eventos
@endsection

@php
  /*
    Must init this variable..
    this serves to know on which page the user to highlight
    the correspondent MENU (NAVBAR) item down bellow. in @section('menus')
  */

  $isActive = 'eventos';
@endphp

@section('menus')
  @include('inc.menus')
  @include('inc.mobilemenus')
@endsection

{{--  The right side of  848087716 the navbar  --}}

@section('navbar-right-content')
  @auth
    @include('inc.navbar-user')
  @endauth
@endsection


@section('headcsslink')
  <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
  {{-- <script type="text/javascript" src="https://addevent.com/libs/atc/1.6.1/atc.min.js"></script> --}}

  <style>
    /* Outline Out */
    .hvr-outline-out {
      display: inline-block;
      vertical-align: middle;
      -webkit-transform: perspective(1px) translateZ(0);
      transform: perspective(1px) translateZ(0);
      box-shadow: 0 0 1px rgba(0, 0, 0, 0);
      position: relative;
    }
    .hvr-outline-out:before {
      content: '';
      position: absolute;
      border: #ff8a00 solid 1px;
      top: 0;
      right: 0;
      bottom: 0;
      left: 0;
      -webkit-transition-duration: 0.1s;
      transition-duration: 0.1s;
      -webkit-transition-property: top, right, bottom, left;
      transition-property: top, right, bottom, left;
    }
    .hvr-outline-out:hover:before, .hvr-outline-out:focus:before, .hvr-outline-out:active:before {
      top: -3px;
      right: -3px;
      bottom: -3px;
      left: -3px;
    }




    /* Responsive H1-H7 */
    @media (min-width: @screen-xs-min) {
          .h1-xs,
          .h2-xs,
          .h3-xs {
              margin-top: @line-height-computed;
              margin-bottom: (@line-height-computed / 2);

              small,
              .small {
                  font-size: 65%;
              }
          }

          .h4-xs,
          .h5-xs,
          .h6-xs {
              margin-top: (@line-height-computed / 2);
              margin-bottom: (@line-height-computed / 2);

              small,
              .small {
                  font-size: 75%;
              }
          }

          .h1-xs { font-size: @font-size-h1; }
          .h2-xs { font-size: @font-size-h2; }
          .h3-xs { font-size: @font-size-h3; }
          .h4-xs { font-size: @font-size-h4; }
          .h5-xs { font-size: @font-size-h5; }
          .h6-xs { font-size: @font-size-h6; }
      }

      @media (min-width: @screen-sm-min) {

          .h1-sm,
          .h2-sm,
          .h3-sm {
              margin-top: @line-height-computed;
              margin-bottom: (@line-height-computed / 2);

              small,
              .small {
                  font-size: 65%;
              }
          }

          .h4-sm,
          .h5-sm,
          .h6-sm {
              margin-top: (@line-height-computed / 2);
              margin-bottom: (@line-height-computed / 2);

              small,
              .small {
                  font-size: 75%;
              }
          }

          .h1-sm { font-size: @font-size-h1; }
          .h2-sm { font-size: @font-size-h2; }
          .h3-sm { font-size: @font-size-h3; }
          .h4-sm { font-size: @font-size-h4; }
          .h5-sm { font-size: @font-size-h5; }
          .h6-sm { font-size: @font-size-h6; }
      }

      @media (min-width: @screen-md-min) {

          .h1-md,
          .h2-md,
          .h3-md {
              margin-top: @line-height-computed;
              margin-bottom: (@line-height-computed / 2);

              small,
              .small {
                  font-size: 65%;
              }
          }

          .h4-md,
          .h5-md,
          .h6-md {
              margin-top: (@line-height-computed / 2);
              margin-bottom: (@line-height-computed / 2);

              small,
              .small {
                  font-size: 75%;
              }
          }

          .h1-md { font-size: @font-size-h1; }
          .h2-md { font-size: @font-size-h2; }
          .h3-md { font-size: @font-size-h3; }
          .h4-md { font-size: @font-size-h4; }
          .h5-md { font-size: @font-size-h5; }
          .h6-md { font-size: @font-size-h6; }
      }

      @media (min-width: @screen-lg-min) {

          .h1-lg,
          .h2-lg,
          .h3-lg {
              margin-top: @line-height-computed;
              margin-bottom: (@line-height-computed / 2);

              small,
              .small {
                  font-size: 65%;
              }
          }

          .h4-lg,
          .h5-lg,
          .h6-lg {
              margin-top: (@line-height-computed / 2);
              margin-bottom: (@line-height-computed / 2);

              small,
              .small {
                  font-size: 75%;
              }
          }

          .h1-lg { font-size: @font-size-h1; }
          .h2-lg { font-size: @font-size-h2; }
          .h3-lg { font-size: @font-size-h3; }
          .h4-lg { font-size: @font-size-h4; }
          .h5-lg { font-size: @font-size-h5; }
          .h6-lg { font-size: @font-size-h6; }
      }
  </style>
@endsection


@section('jscript')
  {{-- <script type="text/javascript">
    // function showmethemodal(element, id){
    //   document.getElementById('modal_list').innerHTML = "";
    //   document.getElementById('modal_list_second').innerHTML = "";
    //   document.getElementById('modal_description').innerHTML = "";
    //   document.getElementById('modal_description_second').innerHTML = "";

    //   var elm = element.innerHTML;
    //   element.innerHTML = "<div style='position: relative; bottom: 2px' uk-spinner='ratio: .5'></div>";

    //   $.ajax({
    //     url: "{{route('eventos.index')}}/showmodal/"+id,
    //     type: "GET",
    //     success: function(response){
    //       // IF THE IMAGE EXIST SHOW A MODAL WITH IMAGE
    //       if (response.imageurl !== null) {
    //         UIkit.modal('#showwithimage').show();
    //         element.innerHTML = elm;

    //         document.getElementById('modal_image').setAttribute('style', 'background-image: url("http://localhost/upmaxixe/public/storage/eventos_image/' + response.id + '/' + response.imageurl + '")');
    //         document.getElementById('modal_image_a').setAttribute('href', 'http://localhost/upmaxixe/public/storage/eventos_image/' + response.id + '/' + response.imageurl);
    //         document.getElementById('modal_image_b').setAttribute('href', 'http://localhost/upmaxixe/public/storage/eventos_image/' + response.id + '/' + response.imageurl);
    //         document.getElementById('modal_title').innerHTML = response.name;

    //         if (response.description !== (null || " " || "")) {
    //           document.getElementById('modal_description').innerHTML = response.description + "<hr>";
    //         }

    //         document.getElementById('modal_list').innerHTML += "<li>" + "Local: " + "<strong>" + response.local + "</strong></li>";
    //         document.getElementById('modal_list').innerHTML += "<li>" + "Data: " + "<strong>" + response.horario_do_inicio + "</strong</li>";

    //         if (response.horario_do_fim !== undefined) {
    //           document.getElementById('modal_list').innerHTML += "<li>" + "Data do Fim do Evento: " + "<strong>" + response.horario_do_fim + "</strong></li>";
    //         }

    //         if (response.capacidade == 'unlimited') {
    //           var capacidade = "ILIMITADA";
    //           document.getElementById('modal_list').innerHTML += "<li>" + "Capacidade: " + "<strong>" + capacidade + "</strong>" + "</li>";
    //         } else {
    //           var capacidade = response.capacidade;
    //           document.getElementById('modal_list').innerHTML += "<li>" + "Capacidade: " + "<strong>" + capacidade + "</strong> Pessoas." + "</li>";
    //         }

    //         if (response.precoindividual == 'free') {
    //           var precoindividual = "GRATUITO";
    //           document.getElementById('modal_list').innerHTML += "<li>" + "Preço Individual: " + "<strong>" + precoindividual + "</strong>" + "</li>";
    //         } else {
    //           var precoindividual = response.capacidade;
    //           document.getElementById('modal_list').innerHTML += "<li>" + "Preço Individual: " + "<strong>" + precoindividual + " MT </strong>" + "</li>";
    //         }

    //       // IF THE IMAGE DOESNT EXIST SHOW A SIMPLER MODAL
    //       } else {
    //         UIkit.modal('#showwithoutimage').show();
    //         element.innerHTML = elm;

    //         document.getElementById('modal_title_second').innerHTML = response.name;
    //         if (response.description !== null) {
    //           document.getElementById('modal_description_second').innerHTML = response.description;
    //         }

    //         document.getElementById('modal_list_second').innerHTML += "<li>" + "Local: " + "<strong>" + response.local + "</strong></li>";
    //         document.getElementById('modal_list_second').innerHTML += "<li>" + "Data: " + "<strong>" + response.horario_do_inicio + "</strong</li>";

    //         if (response.horario_do_fim !== null) {
    //           document.getElementById('modal_list_second').innerHTML += "<li>" + "Data do Fim do Evento: " + "<strong>" + response.horario_do_fim + "</strong></li>";
    //         }

    //         if (response.capacidade == 'unlimited') {
    //           var capacidade = "ILIMITADA";
    //           document.getElementById('modal_list_second').innerHTML += "<li>" + "Capacidade: " + "<strong>" + capacidade + "</strong>" + "</li>";
    //         } else {
    //           var capacidade = response.capacidade;
    //           document.getElementById('modal_list_second').innerHTML += "<li>" + "Capacidade: " + "<strong>" + capacidade + "</strong> Pessoas." + "</li>";
    //         }

    //         if (response.precoindividual == 'free') {
    //           var precoindividual = "GRATUITO";
    //           document.getElementById('modal_list_second').innerHTML += "<li>" + "Preço Individual: " + "<strong>" + precoindividual + "</strong>" + "</li>";
    //         } else {
    //           var precoindividual = response.capacidade;
    //           document.getElementById('modal_list_second').innerHTML += "<li>" + "Preço Individual: " + "<strong>" + precoindividual + " MT </strong>" + "</li>";
    //         }
    //       } // end -> else if(imageDoesNotExist)


    //     }
    //   });
    // }
  </script> --}}
@endsection


{{--  Body  --}}
@section('body')

    <!--ARTICLE-->
    <section class="uk-section uk-article uk-section-muted">
        <div class="uk-container uk-container-small">
              <h2 class="uk-text-bold uk-h1 uk-margin-remove-adjacent uk-margin-remove-top">
                <span>
                    <img style="position:relative; bottom: 5px" src="{{ asset('images/icons/calendar.svg') }}" uk-svg width="40" height="40" alt="">
                    Eventos
                </span>
                @auth
                  <a href="{{route('eventos.create')}}" class="hvr-grow-shadow uk-dark uk-button uk-button-secondary uk-border-rounded uk-box-shadow-small uk-float-right" style="position: relative; top: 6.4px;" uk-icon='plus' uk-tooltip="title: Publicar Novo Evento; delay: 250"></a>
                @endauth
              </h2>
            @include('inc.messages')
        </div>

        


        {{-- OLD EVENTS --}}
        <div class="uk-container uk-container-small uk-margin-medium uk-placeholder">
          <p><span uk-icon='arrow-down'></span><i>Eventos passados</i></p>
          <hr class="uk-margin-remove-top">
          <div class="uk-grid uk-grid-small uk-child-width-1-2@m">
          @if (count($all) > 0)
            @foreach ($all as $evento)
              @php
                $horario_do_inicio = Carbon\Carbon::parse($evento['start_time']->format('Y-m-d H:i:s'));
                
                if(isset($evento['end_time'])) {
                  $horario_do_fim = Carbon\Carbon::parse($evento['end_time']->format('Y-m-d H:i:s'));                  
                };
                
              @endphp
              @if (!(Carbon\Carbon::now('Africa/Maputo')->between($horario_do_inicio, $horario_do_fim)))

                <div class="uk-margin-small-bottom">
                  <div class="uk-card uk-card-small uk-box-shadow-small uk-background-default uk-border-rounded">
                    <div class="uk-text-left uk-card-body">
                      <a href="{{route('eventos.index')}}/{{$evento['id']}}" class="uk-link-reset"><h4 class="uk-margin-small-bottom uk-margin-remove-top uk-text-bold">{{$evento['name']}}</h4></a>
                      @isset($evento['start_time'])                      
                        <div style="border: 1px #eee solid;" class="uk-text-middle uk-card uk-grid-collapse uk-child-width-1-4@m uk-child-width-1-3@s uk-width-1-2 uk-border-rounded uk-margin" uk-grid>
                            <div class="uk-text-middle uk-text-center uk-text-bold">
                              <p style="line-height: .7" class="uk-text-bold uk-text-middle uk-text-warning uk-margin-remove-bottom uk-margin-small-top">{{$shortmonths[$evento['start_time']->format('n')]}}</p>
                              <h4 class="uk-text-bold uk-text-middle uk-margin-remove">{{$evento['start_time']->format('d')}}</h4>
                              <p style="line-height: .7" class="uk-text-bold uk-text-middle uk-text-warning uk-margin-remove-top uk-margin-small-bottom">{{$evento['start_time']->format('Y')}}</p>
                            </div>

                            <div class="uk-width-expand uk-card-body">
                              <span class="uk-text-middle uk-text-bold">
                                {{$shortweeks[$evento['start_time']->format('w')]}}, {{$evento['start_time']->format('h:i')}}
                              </span>
                            </div>
                        </div>
                      @endisset

                      <div class="uk-text-right">
                        <a href="{{route('eventos.index')}}/{{$evento['id']}}">Mais Info</a>
                      </div>
                      {{-- @isset($evento['end_time'])
                        <p class="uk-text-meta uk-margin-remove">Data e hora do fim: <strong>{{$horario_do_fim->format('d-M-Y \p\e\l\a\s H:i \h\o\r\a\s')}}</strong>.</p>                          
                      @endempty

                      @isset($evento['place'])
                        <p class="uk-text-muted uk-margin-remove-top uk-margin-remove-bottom">Local: <strong>{{$evento['place']['name']}}</strong>.</p>
                      @endisset --}}
                    </div>
                  </div>
                </div>
              @endif
            @endforeach
          </div>
          <br>
          {{-- <div class="uk-text-center">
            <a href="{{route('eventos.todos')}}" class="uk-button uk-button-default uk-border-rounded"><span uk-icon="chevron-right" style="position: relative; bottom: 1px"></span> Ver arquivo de todos os eventos...</a>
          </div> --}}

          @else
            <div class="uk-grid uk-grid-medium uk-child-width-1-1 uk-child-width-1-3@s" uk-grid>
              <div class="uk-margin">
                <div class="uk-card uk-card-small uk-card-default uk-card-body">
                  <h7 class="uk-h7 uk-text-muted">
                    Vazio
                </div>
              </div>
            </div>
          @endif

        </div>


        

    </section>
@endsection
