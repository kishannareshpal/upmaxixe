@extends('layouts.app')

@php
  /*
    Must init this variable..
    this serves to know on which page the user to highlight
    the correspondent MENU (NAVBAR) item.
  */

  $isActive = 'eventos';
@endphp

@section('navbar-left-content')
@endsection

@section('navbar-right-content')
  @auth
    @include('inc.navbar-user')
  @endauth
@endsection


@section('headcsslink')
  <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
  <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
  <script src="https://npmcdn.com/flatpickr/dist/l10n/pt.js"></script>

  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/js/standalone/selectize.js"></script>
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/css/selectize.min.css" />

  <script type="text/javascript">
    var image_to_delete_modal;
  </script>

@endsection


@section('jscript')
  <script type="text/javascript">
    $(document).ready(function(){
      $('input[name="horario_do_inicio"], input[name="horario_do_fim"]').flatpickr({
        enableTime: true,
        dateFormat: "d-m-Y H:i",
        defaultHour: 9,
        defaultDate: 'Carbon\Carbon::parse($evento->horario_do_inicio)->format("d-m-Y H:i")',
        locale: "pt",
        time_24hr: true,
      });
    });
  </script>

  <script>
    $(function() {
    	$('#capacidadeSelectize').selectize({
          delimiter: ',',
          valueField: 'value',
          labelField: 'title',
          searchField: 'title',
          options: [
              {value: 'unlimited', title: 'CAPACIDADE ILIMITADA'},
              // {id: 2, title: 'value2'},
              // {id: 3, title: 'value3'}
          ],
          openOnFocus: true,
          create: true,
          maxItems: 1
        });

        $('#precoindividualSelectize').selectize({
            delimiter: ',',
            valueField: 'value',
            labelField: 'title',
            searchField: 'title',
            options: [
                {value: 'free', title: 'GRÁTIS'},
                // {id: 2, title: 'value2'},
                // {id: 3, title: 'value3'}
            ],
            openOnFocus: true,
            create: true,
            maxItems: 1
          });
    });


    $('.selectize-input').removeClass('selectize-input');
    $('#selectize-selectized').addClass('uk-input');
  </script>

  <style>
    .selectize-input{
      border: 1px dashed #e5e5e5;
      box-sizing: border-box;
      -webkit-box-shadow: none;
      box-shadow: none;
      -webkit-border-radius: 0px;
      -moz-border-radius: 0px;
      border-radius: 0px;
    }

    .selectize-input.focus{
      -webkit-box-shadow: none;
      box-shadow: none;
      border: 1px dashed #1589e9;
    }
  </style>

  {{-- Prevent accidental enter key press form submition --}}
  <script type="text/javascript">
    $('#form').on('keyup keypress', function(e) {
      if (e.target.type !== "textarea") {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
          e.preventDefault();
          return false;
        }
      }
    });
  </script>

  <script type="text/javascript">
    function launch_delete_sequence(){
      location.href='{{ route('eventos.index')}}/di/{{$evento->id}}/'+image_to_delete_modal;
    }
  </script>

@endsection


{{--  Body  --}}
@section('body')
  {!! Form::open(['action' => ['EventosController@update', $evento->id], 'method'=>'POST', 'files'=>'true', 'id' => 'form']) !!}
    <section class="uk-section uk-section-small uk-visible@m">
        <div class="uk-container uk-container-small">

          <div class="uk-alert-primary" uk-alert>
              <h4><span style="position: relative; bottom: 2px" uk-icon='pencil'> </span>&nbsp Editando o evento: <strong>{{$evento->name}}</strong></h4>
          </div>

          @include('inc.messages')

          <div class="uk-box-shadow-small uk-padding-small uk-align-right uk-navbar-right">
              <ul class="uk-navbar-nav uk-flex-center">
                {{Form::hidden('_method', 'PUT')}}
                {{Form::submit('Guardar', ['class'=>'uk-button uk-button-primary uk-margin-right uk-text-bold'])}}
                {{-- <a href="{{route('eventos.index')}}" class="uk-button uk-button-default"> <i style="position: relative; bottom: 1px" uk-icon='arrow-up'></i> Voltar àos eventos</a> --}}
                <a href="{{route('eventos.index')}}" class="uk-button uk-button-default">Cancelar</a>
              </ul>
              <small class="uk-text-muted">*Última revisão foi feita no dia {{$evento->updated_at->day}} de {{$ptmonth}} de {{$evento->updated_at->year}}, às {{$evento->updated_at->format('H:i')}}.</small>
          </div>
        </div>
    </section>

    <!--FORM-->
    <section class="uk-section uk-article uk-section-muted">
        <div class="uk-container uk-container-small">
            <div class="uk-margin uk-placeholder">
              <small class="uk-text-muted uk-h7 uk-margin-remove-top"><span uk-icon='arrow-down'></span><i>Foto do evento (opcional)</i></small>
              <br>
              <div uk-form-custom="target: true">
                <input name="image" type="file">
                <input class="uk-input uk-form-width-medium" type="text" placeholder="Selecione uma imagem" disabled>
              </div>

              {{-- Show already uploaded image --}}
              @if (!empty($evento->imageurl))
                <div class="uk-card uk-card-default uk-card-body uk-margin-small">
                  <small><span uk-icon='arrow-down'></span><i>Imagens já existentes:</i></small>
                  <ul uk-lightbox class="uk-thumbnav uk-margin-top" uk-margin>
                    <li>
                      <a data-caption='<button onclick=" image_to_delete_modal = {{"'".strval($evento->imageurl)."'"}} " uk-toggle="#confirm_delete_modal" class="uk-button uk-button-danger"><i uk-icon="trash" style="position: relative; bottom: 1px"></i> Apagar</button>' href="{{ asset('/storage/eventos_image') }}/{{$evento->id}}/{{$evento->imageurl}}">
                        <img uk-img data-src="{{ asset('/storage/eventos_image') }}/{{$evento->id}}/{{$evento->imageurl}}" width="100">
                      </a>
                    </li>
                  </ul>
                </div>

                <!-- This is the modal -->
                <div id="confirm_delete_modal" uk-modal>
                  <div class="uk-modal-dialog uk-modal-body">
                      <h2 class="uk-modal-title">Confirmar</h2>
                      <p>Tem a certeza que deseja <strong>apagar a imagem?</strong></p>
                      <p class="uk-text-right">
                          <button class="uk-button uk-button-default uk-modal-close" type="button">Não, cancelar.</button>
                          {{-- location.href='{{ route('noticias.index')}}/di/{{$post->id}}/{{$image->url}} --}}
                          <button onclick="launch_delete_sequence()" class="uk-button uk-button-danger" type="button">Sim, Apagar</button>
                      </p>
                  </div>
                </div>
              @endif
            </div>



            {{-- Nome do evento --}}
            <div class="uk-margin-small">
              <small class="uk-text-muted uk-h7 uk-margin-remove-top"><span uk-icon='arrow-down'></span><i>Nome do evento</i></small>
              @if ($errors->has('nome'))
    					 <small class="uk-text-warning"> –– {{ $errors->first('nome') }}</small>
    					@endif
              {{Form::text('nome', $evento->name, ['class' => 'uk-input uk-form-medium uk-text-bold uk-heading', 'style' => 'border-style: dashed'])}}
            </div>

            {{-- Local e Data/Hora --}}
            <div class="uk-grid-small" uk-grid>
              {{-- Local --}}
              <div class="uk-width-1-2@s">
                <small class="uk-text-muted uk-h7 uk-margin-top uk-margin-remove-bottom"><span uk-icon='arrow-down'></span><i>Local do evento</i></small>
                @if ($errors->has('local'))
      					 <small class="uk-text-warning"> –– {{ $errors->first('local') }}</small>
      					@endif
                {{Form::text('local', $evento->local, ['class' => 'uk-input uk-form-medium', 'style' => 'border-style: dashed'])}}
              </div>

              {{-- Start Date and Time --}}
              <div class="uk-width-1-4@s">
                <small class="uk-text-muted uk-h7 uk-margin-top uk-margin-remove-bottom"><span uk-icon='arrow-down'></span><i>Data e Hora do início</i></small>
                @if ($errors->has('horario_do_inicio'))
      					<small class="uk-text-warning"> –– {{ $errors->first('horario_do_inicio') }}</small>
      					@endif

                {{Form::date('horario_do_inicio', $evento->horario_do_inicio, ['class' => 'uk-input uk-form-medium', 'style' => 'border-style: dashed'])}}
              </div>


              {{-- End Date and Time --}}
              <div class="uk-width-1-4@s">
                <small class="uk-text-muted uk-h7 uk-margin-top uk-margin-remove-bottom"><span uk-icon='arrow-down'></span><i>Data e Hora do fim</i></small>
                @if ($errors->has('horario_do_fim'))
      					 <small class="uk-text-warning"> –– {{ $errors->first('horario_do_fim') }}</small>
      					@endif
                {{Form::date('horario_do_fim', $evento->horario_do_fim, ['class' => 'uk-input uk-form-medium', 'style' => 'border-style: dashed'])}}
              </div>
            </div>


            <div class="uk-grid-small" uk-grid>
              {{-- Capacidade --}}
              <div class="uk-width-1-4@s">
                <small class="uk-text-muted uk-h7 uk-margin-remove-top"><span uk-icon='arrow-down'></span><i>Capacidade (Pessoas)</i></small>
                @if ($errors->has('capacidade'))
      					  <small class="uk-text-warning"> –– {{ $errors->first('capacidade') }}</small>
      					@endif
                {{Form::text('capacidade', $evento->capacidade, ['style' => 'border-style: dashed', 'id'=>'capacidadeSelectize'])}}
                {{-- {{Form::text('capacidade', '', ['class' => 'uk-input uk-form-medium uk-text-bold uk-heading', 'style' => 'border-style: dashed'])}} --}}
              </div>

              <div class="uk-width-1-4@s">
                <small class="uk-text-muted uk-h7 uk-margin-remove-top"><span uk-icon='arrow-down'></span><i>Preço Individual (Meticais)</i></small>
                @if ($errors->has('precoindividual'))
      					<small class="uk-text-warning"> –– {{ $errors->first('precoindividual') }}</small>
      					@endif
                {{Form::text('precoindividual', $evento->precoindividual, ['style' => 'border-style: dashed', 'id'=>'precoindividualSelectize'])}}
              </div>

            </div>


            {{-- Descrição do Evento --}}
            <div class="uk-margin-small">
              <small class="uk-text-muted uk-h7 uk-margin-top"><span uk-icon='arrow-down'></span><i>Decrição do Evento (opcional)</i></small>
              {{Form::textarea('description', $evento->description, ['class' => 'uk-textarea uk-height-1-1 uk-padding-small', 'style' => 'border-style: dashed;', 'rows' => '4'])}}
            </div>

        </div>



        <!-- /body -->
    </section>
  {!! Form::close() !!}

    <!-- /BOTTOM BAR -->
@endsection
