@extends('layouts.app')

@section('title')
    UP Maxixe –> Eventos -> ...
@endsection

@php
  /*
    Must init this variable..
    this serves to know on which page the user to highlight
    the correspondent MENU (NAVBAR) item down bellow. in @section('menus')
  */

  $isActive = 'eventos';
@endphp

@section('menus')
  @include('inc.menus')
  @include('inc.mobilemenus')
@endsection

@section('navbar-right-content')
  @auth
    @include('inc.navbar-user')
  @endauth
@endsection

{{--  Body  --}}
@section('body')


    <section class="uk-section uk-section-small uk-section-muted uk-margin-remove">
      <div class="uk-container uk-container-small uk-margin-medium">
        <div class="uk-float-left uk-text-right">
          <a href="{{route('eventos.index')}}" class="uk-button uk-button-default uk-float-right uk-button-muted uk-border-rounded"> <i uk-icon="chevron-left" style="position: relative; bottom: 1px"></i> Voltar àos Eventos</a>
        </div>
      </div>

      <div class="uk-container uk-container-small">
        <!--Message-->
        @include('inc.messages')
        <!--/Message-->

        <div class="uk-container uk-container-small uk-margin-small-bottom">
          <h2 class="uk-text-bold uk-margin-remove-bottom uk-h2">{{$evento['name']}}</h2>
          <div class="uk-divider-small"></div>
        </div>

        {{-- Date Card --}}
        <div class="uk-grid-small" uk-grid>
          <div class="uk-width-1-3@s uk-text-center">
            <div class="uk-card uk-card-default uk-card-body uk-background-default uk-border-rounded">
              <div>
                <h3 class="uk-text-warning uk-margin-bottom-remove uk-text-bold">Data do Evento</h3>
                <span uk-icon="icon: calendar; ratio: 1"></span>
                <small class="uk-margin-remove-bottom">
                  @isset($evento['end_time'])
                    @if ($evento['start_time']->format('d') == $evento['end_time']->format('d'))
                      {{ $weeks[$evento['start_time']->format('w')] }}
                    @else
                      {{ $weeks[$evento['start_time']->format('w')] }} até {{ $weeks[$evento['end_time']->format('w')] }}
                    @endif

                  @else
                    {{ $evento['start_time']->format('d') }}
                  @endisset
                </small>
                
                <h1 style="line-height: 1.4" class="uk-h1 uk-text-bold uk-margin-remove uk-padding-remove">
                  @isset($evento['end_time'])
                    @if ($evento['start_time']->format('d') == $evento['end_time']->format('d'))
                      {{ $evento['start_time']->format('d') }}
                    @else
                      {{ $evento['start_time']->format('d') }} até {{ $evento['end_time']->format('d') }}
                    @endif

                  @else
                    {{ $evento['start_time']->format('d') }}
                  @endisset
                </h1>

                <small class="uk-margin-remove uk-text-bold uk-text-meta uk-text-muted">
                  @isset($evento['end_time'])
                    {{-- Month --}}
                    @if ( $evento['start_time']->format('n') == $evento['end_time']->format('n') )
                      {{ $months[$evento['start_time']->format('n')] }}
                    @else
                      {{ $months[$evento['start_time']->format('n')] }} até {{ $months[$evento['end_time']->format('n')] }}
                    @endif

                    {{-- Year --}}
                    @if ( $evento['start_time']->format('Y') == $evento['end_time']->format('Y') )
                      {{ $evento['start_time']->format('Y') }}
                    @else
                      {{ $evento['start_time']->format('Y') }} até {{ $evento['end_time']->format('Y') }}
                    @endif

                  @else
                    {{-- Year and Month Fallback --}}
                    {{ $months[$evento['start_time']->format('n')] }}
                    {{ $evento['start_time']->format('Y') }}
                  @endisset


                </small>
                <p class="uk-margin-remove uk-text-bold">
                  @isset($evento['end_time'])
                    das {{ $evento['start_time']->format('H:i') }} até {{ $evento['end_time']->format('H:i') }} horas.
                  @else
                    pelas {{ $evento['start_time']->format('H:i') }} horas.
                  @endisset
                </p>
              </div>
            </div>
          </div>
          {{-- ./End of Date Card --}}


          {{-- Content Card --}}
          <div class="uk-width-expand@s">
            <div class="uk-card uk-card-default uk-card-body uk-border-rounded">
              @isset($evento['cover'])
                <div class="uk-position-relative uk-visible-toggle uk-light uk-box-shadow-medium" uk-slideshow="ratio: 7:3; animation: fade; min-height: 190;">
                    <ul uk-lightbox class="uk-slideshow-items">
                      <li>
                          <img uk-img data-src="{{ $evento['cover']['source'] }}" uk-cover>
                          <div class="uk-position-bottom-out uk-position-medium uk-text-center">
                            <a style="border: 3px solid #333;" href="{{ $evento['cover']['source'] }}" class="uk-box uk-button uk-button-small uk-button-secondary uk-border-rounded uk-box-shadow-large" uk-icon="expand">Ver imagem </a>
                          </div>
                      </li>
                    </ul>
                    {{-- <ul class="uk-slideshow-nav uk-dotnav uk-position-relative" style="left: 10px;bottom: 20px"></ul> --}}
                    <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" data-uk-slidenav-previous="ratio: 1.5" data-uk-slideshow-item="previous"></a>
                    <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" data-uk-slidenav-next="ratio: 1.5" data-uk-slideshow-item="next"></a>
                </div>
              @endisset

              @isset($evento['description'])
                <h3 class="uk-text-bold">Descrição do evento</h3>
                <div class="uk-divider-small"></div>
                <p class="uk-text-break uk-dropcap">{!! $evento['description'] !!}</p>
                <br><hr>
              @endisset

              <ul class="uk-list uk-list-bullet">
                @isset($evento['place']['name'])
                  <li class="uk-text-justify"><strong class="uk-text-warning">Local: </strong>{{$evento['place']['name']}}</li>                    
                @endisset
                @isset($evento['category'])
                  <li class="uk-text-justify"><strong class="uk-text-warning">Categoria: </strong>{{$evento['category']}}</li>                    
                @endisset
              </ul>

            </div>
          </div>
          {{-- ./Content Card --}}

        </div>

      </div>
    </section>


@endsection
