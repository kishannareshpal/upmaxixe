@extends('layouts.app')

@php
  /*
    Must init this variable..
    this serves to know on which page the user to highlight
    the correspondent MENU (NAVBAR) item down bellow. in @section('menus')
  */

  $isActive = 'eventos';
@endphp

@section('menus')
  @include('inc.menus')
  @include('inc.mobilemenus')
@endsection

{{--  The right side of the navbar  --}}

@section('navbar-right-content')
  @auth
    @include('inc.navbar-user')
  @endauth
@endsection


@section('headcsslink')
  <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
  <link href="{{ asset('css/cards.css') }}" rel="stylesheet">
@endsection



@section('jscript')
  <script type="text/javascript">
    function showmethemodal(element, id){
      document.getElementById('modal_list').innerHTML = "";
      document.getElementById('modal_list_second').innerHTML = "";
      var elm = element.innerHTML;
      element.innerHTML = "<div style='position: relative; bottom: 2px' uk-spinner='ratio: .5'></div>";

      $.ajax({
        url: "{{route('eventos.index')}}/showmodal/"+id,
        type: "GET",
        success: function(response){
          // IF THE IMAGE EXIST SHOW A MODAL WITH IMAGE
          if (response.imageurl !== null) {
            UIkit.modal('#showwithimage').show();
            element.innerHTML = elm;

            document.getElementById('modal_image').setAttribute('style', 'background-image: url("http://localhost/upmaxixe/public/storage/eventos_image/' + response.id + '/' + response.imageurl + '")');
            document.getElementById('modal_image_a').setAttribute('href', 'http://localhost/upmaxixe/public/storage/eventos_image/' + response.id + '/' + response.imageurl);
            document.getElementById('modal_image_b').setAttribute('href', 'http://localhost/upmaxixe/public/storage/eventos_image/' + response.id + '/' + response.imageurl);
            document.getElementById('modal_title').innerHTML = response.name;

            if (response.description !== null) {
              document.getElementById('modal_description').innerHTML = response.description + "<hr>";
            }
            document.getElementById('modal_list').innerHTML += "<li>" + "Local: " + "<strong>" + response.local + "</strong></li>";
            document.getElementById('modal_list').innerHTML += "<li>" + "Data: " + "<strong>" + response.horario_do_inicio + "</strong</li>";

            if (response.horario_do_fim !== undefined) {
              document.getElementById('modal_list').innerHTML += "<li>" + "Data do Fim do Evento: " + "<strong>" + response.horario_do_fim + "</strong></li>";
            }

            if (response.capacidade == 'unlimited') {
              var capacidade = "ILIMITADA";
              document.getElementById('modal_list').innerHTML += "<li>" + "Capacidade: " + "<strong>" + capacidade + "</strong>" + "</li>";
            } else {
              var capacidade = response.capacidade;
              document.getElementById('modal_list').innerHTML += "<li>" + "Capacidade: " + "<strong>" + capacidade + "</strong> Pessoas." + "</li>";
            }

            if (response.precoindividual == 'free') {
              var precoindividual = "GRATUITO";
              document.getElementById('modal_list').innerHTML += "<li>" + "Preço Individual: " + "<strong>" + precoindividual + "</strong>" + "</li>";
            } else {
              var precoindividual = response.capacidade;
              document.getElementById('modal_list').innerHTML += "<li>" + "Preço Individual: " + "<strong>" + precoindividual + " MT </strong>" + "</li>";
            }

          // IF THE IMAGE DOESNT EXIST SHOW A SIMPLER MODAL
          } else {
            UIkit.modal('#showwithoutimage').show();
            element.innerHTML = elm;

            document.getElementById('modal_title_second').innerHTML = response.name;
            if (response.description !== null) {
              document.getElementById('modal_description_second').innerHTML = response.description;
            }

            document.getElementById('modal_list_second').innerHTML += "<li>" + "Local: " + "<strong>" + response.local + "</strong></li>";
            document.getElementById('modal_list_second').innerHTML += "<li>" + "Data: " + "<strong>" + response.horario_do_inicio + "</strong</li>";

            if (response.horario_do_fim !== null) {
              document.getElementById('modal_list_second').innerHTML += "<li>" + "Data do Fim do Evento: " + "<strong>" + response.horario_do_fim + "</strong></li>";
            }

            if (response.capacidade == 'unlimited') {
              var capacidade = "ILIMITADA";
              document.getElementById('modal_list_second').innerHTML += "<li>" + "Capacidade: " + "<strong>" + capacidade + "</strong>" + "</li>";
            } else {
              var capacidade = response.capacidade;
              document.getElementById('modal_list_second').innerHTML += "<li>" + "Capacidade: " + "<strong>" + capacidade + "</strong> Pessoas." + "</li>";
            }

            if (response.precoindividual == 'free') {
              var precoindividual = "GRATUITO";
              document.getElementById('modal_list_second').innerHTML += "<li>" + "Preço Individual: " + "<strong>" + precoindividual + "</strong>" + "</li>";
            } else {
              var precoindividual = response.capacidade;
              document.getElementById('modal_list_second').innerHTML += "<li>" + "Preço Individual: " + "<strong>" + precoindividual + " MT </strong>" + "</li>";
            }
          } // end -> else if(imageDoesNotExist)


        }
      });
    }
  </script>
@endsection


{{--  Body  --}}
@section('body')

    <!--ARTICLE-->
    <section class="uk-section uk-article uk-background-secondary">
        <div class="uk-container uk-container-small">
            @include('inc.messages')
            <a style="position: relative; bottom: 2px;" href="{{route('eventos.index')}}" class="uk-button uk-button-default uk-button-primary uk-text-bold uk-border-rounded uk-dark"> <span style="position: relative; bottom: 1px" uk-icon='arrow-left'></span> Voltar</a>
            <h2 class="uk-text-bold uk-h2 uk-margin-remove-adjacent uk-text-muted ">Arquivo de Todos os Eventos<span uk-icon="icon: arrow-down; ratio: 1.4"></span></h2>
        </div>

        <div class="uk-container uk-container-small uk-margin-medium">
          {{-- <small><span uk-icon='arrow-down'></span><i>Eventos passados</i></small> --}}
          {{-- <hr class="uk-margin-remove-top"> --}}
          <div class="uk-grid uk-child-width-1-2@s uk-child-width-1-3@m" uk-grid>
            @foreach ($eventos as $evento)
              @php
                $horario_do_inicio = Carbon\Carbon::parse($evento->horario_do_inicio);
                $horario_do_fim = Carbon\Carbon::parse($evento->horario_do_fim);
              @endphp
    						<!-- card -->
    						<div class="nature-card">
    							<div class="uk-card uk-background-default">
    								{{-- <div class="uk-card-header">
    									<div class="uk-grid uk-grid-small uk-text-small" data-uk-grid>
    										<div class="uk-width-expand">
    											<span class="cat-txt uk-text-muted"><span style="position: relative; bottom: 1px" uk-icon="calendar"></span> {{$horario_do_inicio->day}} de {{$months[$horario_do_inicio->month]}} de {{$horario_do_inicio->year}}</span>
                        </div>
    									</div>
    								</div> --}}
                    <div style="border-radius: 3px" class="uk-text-center uk-padding uk-light uk-background-primary">
                      <div class="uk-width-auto uk-text-bold">
                        <span uk-icon="icon: calendar; ratio: 2"></span>
                        <br>
                        <small class="uk-margin-remove-bottom">{{$weeks[$horario_do_inicio->dayOfWeek]}}</small>
                        <h1 style="line-height: .8" class="uk-h1 uk-text-bold uk-margin-remove uk-padding-remove">
                          @if ($horario_do_inicio->day == $horario_do_fim->day)
                            {{$horario_do_inicio->day}}
                          @else
                            {{$horario_do_inicio->day}} –– {{$horario_do_fim->day}}
                          @endif
                        </h1>
                        <small class="uk-margin-remove uk-text-bold uk-text-meta uk-text-muted">
                          @if ($horario_do_inicio->month == $horario_do_fim->month)
                            {{$months[$horario_do_inicio->month]}}
                          @else
                            {{$months[$horario_do_inicio->month]}} –– {{$months[$horario_do_fim->month]}}
                          @endif
                        </small>
                      </div>
                    </div>

                    <div class="uk-card-body">
    									<h4 class="uk-h4 uk-margin-remove-adjacent uk-text-bold">{{$evento->name}}</h4>
    								</div>

                    <div style="border-radius: 3px" class="uk-card-footer uk-background-muted">
                      <div class="uk-grid uk-grid-small uk-grid-divider uk-flex uk-flex-middle" data-uk-grid>
                        <div class="uk-width-expand uk-text-small">
                          <button type="button" onclick='showmethemodal(this, {{$evento->id}})' class="uk-button uk-button-default uk-button-small" name="info"><span uk-icon="icon:expand; ratio: .8" style="position: relative; bottom: 1px;"></span> Info</button>
                        </div>
                        <div class="uk-width-auto">
                          @auth {{-- Show more options to the event (such as edit and delete) --}}
                            <span style="cursor: pointer" class="uk-icon-button uk-alert-muted" uk-icon="more-vertical"></span>
                            <div class="uk-background-default uk-border-rounded" uk-dropdown="mode: click; animation: uk-animation-slide-bottom-small; duration: 150" >
                              <small class="uk-text-bold ">Mais Opções: </small><br>
                              <div class="uk-grid uk-grid-small uk-light" uk-grid>
                                <div>
                                  <a href="{{route('eventos.index')}}/{{$evento->id}}/edit" class="uk-icon-button uk-background-secondary uk-button-secondary" uk-icon="pencil"></a>
                                </div>
                                <div>
                                  {!!Form::open(['action' => ['EventosController@destroy', $evento->id], 'method'=>'POST'])!!}
                                    {{Form::hidden('_method', 'DELETE')}}
                                    <button type="submit" class="uk-icon-button uk-text-danger uk-button-secondary" uk-icon="trash"></button>
                                    {{-- {{Form::submit('Apagar', ['class'=> 'uk-button uk-margin-top uk-button-danger'])}} --}}
                                  {!!Form::close()!!}
                                </div>
                              </div>

                            </div>
                          @endauth
                        </div>
                      </div>
                    </div>
    							</div>
    						</div>
    						<!-- /card -->
            @endforeach
          </div>


          {{-- EVENT INFORMATION MODAL (WITH IMAGE) --}}
          {{-- <a class="uk-button uk-button-default" href="#show" uk-toggle>Open</a> --}}
          <div id="showwithimage" class="uk-modal-full" uk-modal>
              <div class="uk-modal-dialog">
                  <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>
                  <div uk-lightbox class="uk-grid-collapse uk-child-width-1-2@s uk-flex-middle " uk-grid> {{-- style="background-image: url({{asset('images/conselhodirectivo/1.jpg')}});" --}}
                      <a href="#" id="modal_image_a" class="uk-inline-clip uk-transition-toggle">
                        <div id="modal_image" class="uk-background-cover uk-placeholder" uk-height-viewport></div>
                        <div class="uk-transition-fade uk-position-cover uk-position-small uk-overlay-primary uk-flex uk-flex-middle uk-flex-center uk-text-bold">
                          <p class="uk-h4 uk-margin-remove">Ver a Imagem</p>
                        </div>
                      </a>
                      <div class="uk-padding-large uk-text-center" uk-height-viewport>
                          <h1 id="modal_title"></h1>
                          <a href="#" id="modal_image_b" class="uk-button uk-button-primary uk-float-bottom" name="button"><i style="position: relative; bottom: 1px" uk-icon="image"></i>&nbsp Ver a Imagem</a>

                          <p id="modal_description"></p>
                          <ul id="modal_list" class="uk-alert-primary uk-dark uk-padding-small uk-list"></ul>
                      </div>
                  </div>
              </div>
          </div>

          {{-- EVENT INFORMATION MODAL (W/OUT IMAGE) --}}
          <div id="showwithoutimage" class="uk-flex-top" uk-modal>
              <div class="uk-modal-dialog uk-padding-large uk-modal-body uk-margin-auto-vertical">
                  <button class="uk-modal-close-default" type="button" uk-close></button>

                  <h2 id="modal_title_second" class="uk-h2 uk-text-bold"></h2>
                  <p id="modal_description_second" class="uk-text-muted uk-text-justify" ></p>
                  <ul id="modal_list_second" class="uk-background-muted uk-dark uk-padding-small uk-list uk-list-divide"></ul>
              </div>
          </div>

          <div class="uk-container uk-container-small uk-margin-medium">
            {{$eventos->links()}}
          </div>
        </div>

    </section>
@endsection
