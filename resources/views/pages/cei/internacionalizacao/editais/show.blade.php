@extends('layouts.app')

@section('title')
    UP Maxixe –> CEI -> Editais -> ...
@endsection

@php
  /*
    Must init this variable..
    highlight @section('[mobile]menus')
  */
  $isActive = 'cei';
  $isActiveSub = 'cei.int';
  $isActiveSubSub = 'cei.int.editais';

@endphp

@section('menus')
  @include('inc.menus')
  @include('inc.mobilemenus')
@endsection

@section('navbar-right-content')
  @auth
    @include('inc.navbar-user')
  @endauth
@endsection

@section('headcsslink')
  {{ Html::script('../node_modules/@ckeditor/ckeditor5-build-decoupled-document/build/ckeditor.js') }}   

  <style>
    table {
      border-collapse: collapse;
      border-spacing: 0;
      border: 1px double #b3b3b3;
    } 

    tr {
      display: table-row;
      vertical-align: inherit;
      border-color: inherit;
    }

    table td, table th {
      min-width: 2em;
      padding: .4em;
      text-align: center;
      border: 1px solid #d9d9d9;
    }



    blockquote {
      overflow: hidden;
      padding-right: 1.5em;
      padding-left: 1.5em;
      margin-left: 0;
      font-style: italic;
      border-left: 5px solid #ccc;
      font-size: 1.25rem;
      line-height: 1.5;
      color: #333;
      font-family: "Roboto","Helvetica","Arial",sans-serif;
      position: relative;
      font-size: 24px;
      font-weight: 300;
      letter-spacing: .08em;
      display: block;

      -webkit-margin-before: 1em;
      -webkit-margin-after: 1em;
      -webkit-margin-start: 40px;
      -webkit-margin-end: 40px;
    }
  </style>
    
@endsection

{{--  Body  --}}
@section('body')


    {{-- <div class="uk-container uk-visible@m">
        <hr class="uk-margin-remove">
    </div> --}}
    <section class="uk-section uk-margin-remove-bottom uk-section-small uk-text-center">
        <div class="uk-container uk-container-small">
            <!--Message-->
              @include('inc.messages')
            <!--/Message-->
            <a style="position: relative; bottom: 2px;" href="{{route('editais.index')}}" class="uk-button uk-button-default uk-button-primary uk-text-bold uk-border-rounded uk-dark"> <span style="position: relative; bottom: 1px" uk-icon='arrow-left'></span> Voltar àos Editais</a>
        </div>
    </section>

    <!--ARTICLE-->
    <section class="uk-section uk-article uk-section-muted">
        <div class="uk-container uk-container-small">
          <h2 class="uk-text-bold uk-margin-remove-bottom uk-h2">{{$edital->title}}</h2>
          <small class="uk-text-muted uk-h7 uk-margin-remove-adjacent uk-margin-remove-top uk-margin-remove-bottom">
             <a class="uk-button-text uk-link-reset" href="{{route('editais.index')}}">Editais</a>
              / <i>
                    <span style="position: relative; bottom: 1px" uk-icon="calendar"></span>
                    Publicado as {{$edital->created_at->format('H:i\h')}}, no dia {{$edital->created_at->day}} de {{$ptmonth}} de {{$edital->created_at->year}}
                    @auth –– <a href="{{$edital->id}}/edit" class="uk-button uk-button-text uk-text-muted uk-text-bold" style="text-transform: none; position: relative; bottom: 1px">Editar</a> @endauth
                </i>
          </small>
        </div>

        <!-- body -->
        <div class="uk-container uk-container-small">
            <div style="overflow-wrap: break-word">
              <p>{!! $edital->body !!}</p>
            </div>
        </div>
        <!-- /body -->
    </section>

@endsection
