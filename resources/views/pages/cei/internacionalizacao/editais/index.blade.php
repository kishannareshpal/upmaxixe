@extends('layouts.app')

@section('title')
    UP Maxixe –> CEI -> Editais
@endsection

@php
  /*
    Must init this variable..
    highlight @section('[mobile]menus')
  */
  $isActive = 'cei';
  $isActiveSub = 'cei.int';
  $isActiveSubSub = 'cei.int.editais';

@endphp

@section('menus')
  @include('inc.menus')
  @include('inc.mobilemenus')
@endsection


{{--  The right side of the navbar  --}}
@section('navbar-right-content')
  @auth
    @include('inc.navbar-user')
  @endauth
@endsection


{{--  Body  --}}
@section('body')
  <section class="uk-section uk-section-small">
    <div class="uk-container uk-container-small">
        <h5 class="uk-margin-remove-bottom uk-text-bold uk-h5 uk-margin-remove-adjacent uk-text-muted uk-margin-remove-top">Cooperação e Internacionalização</h5>
        <h1 class="uk-margin-small-bottom uk-text-bold uk-h1 uk-margin-remove-adjacent uk-margin-remove-top">
          Editais
          @auth
            <a href="{{route('editais.create')}}" class="hvr-grow-shadow uk-dark uk-button uk-button-primary uk-border-rounded uk-box-shadow-small uk-float-right" style="position: relative; top: 6.4px;" uk-icon='plus' uk-tooltip="title: Publicar Novo Edital; delay: 300"></a>
          @endauth
        </h1>
        @include('inc.messages')

    </div>
  </section>

  {{-- Apresentação da Repartição --}}
  <section class="uk-section uk-section-muted">
    <div class="uk-container uk-container-small">

      @if (count($editais) > 0)
        @foreach ($editais as $edital)

          @php
            # localize month name to pt.. not the best way, i know... but whatever!
              // retrieve raw edital month
              $monthRaw = $edital->created_at->month;
              // finally translate that shit
              $ptmonth = $months[$monthRaw];
          @endphp


          <article class="uk-section uk-section-small uk-padding-remove-top">
            <header>
              <h2 class="uk-margin-remove-adjacent uk-text-bold uk-margin-small-bottom"><a title="Continuar a ler: '{{$edital->title}}'" class="uk-link-heading" href="{{route('editais.index')}}/{{$edital->id}}">{{$edital->title}}</a></h2>
              <p class="uk-article-meta">
                <span style="position: relative; bottom: 1px" uk-icon="calendar"></span>
                Postado as {{$edital->created_at->format('H:i A')}}, no dia {{$edital->created_at->day}} de {{$ptmonth}} de {{$edital->created_at->year}}
                @if ($edital->created_at->diffInDays(Carbon\Carbon::now()) < 90)
                  <span title="Postado a menos de 3 meses!" style="position: relative; bottom: 1px" class="uk-label uk-label-success uk-border-rounded"><img src="{{ asset('images/icons/blue_dot.svg') }}" style="position: relative; bottom: 1px" uk-svg width="15" height="15"> Recente</span>
                  
                @endif
              </p>
            </header>

            <p style="overflow-wrap: break-word">{!! str_limit(strip_tags($edital->body), $limit = 300, $end = ' [...] ') !!}</p>
            <a class="uk-button uk-button-default uk-button-small hvr-fade" style="border: 1px solid" href="{{route('editais.index')}}/{{$edital->id}}" title="Saber Mais...">Saber mais...</a>
            @auth {{-- Show more options to the event (such as edit and delete) --}}
              <div class="uk-inline uk-padding-small">
                  <span style="cursor: pointer" class="uk-icon-button uk-background-default" uk-icon="more-vertical"></span>
                  <div class="uk-background-default uk-border-rounded" uk-dropdown="mode: click; animation: uk-animation-slide-bottom-small; duration: 150" >
                    <small class="uk-text-bold uk-text-muted">Mais Opções: </small><br>
                    <div class="uk-grid uk-grid-small uk-light" uk-grid>
                      <div>
                        <a href="{{route('editais.index')}}/{{$edital->id}}/edit" class="uk-icon-button uk-background-secondary uk-button-secondary" uk-icon="pencil"></a>
                      </div>

                      <div>
                        {!!Form::open(['action' => ['IntEditaisController@destroy', $edital->id], 'method'=>'POST'])!!}
                          {{Form::hidden('_method', 'DELETE')}}
                          <button type="submit" class="uk-icon-button uk-text-danger uk-button-secondary" uk-icon="trash"></button>
                          {{-- {{Form::submit('Apagar', ['class'=> 'uk-button uk-margin-top uk-button-danger'])}} --}}
                        {!!Form::close()!!}
                      </div>
                    </div>

                  </div>
              </div>
            @endauth
            <hr>
          </article>
        @endforeach
        
      @else
        <div class="uk-margin">
          <div class="uk-card uk-card-small uk-card-default uk-card-body">
            <h6 class="uk-h6 uk-text-muted">
              Nenhum edital postado até então.
            </h6>
          </div>
        </div>
      @endif
    </div>
  </section>
@endsection



