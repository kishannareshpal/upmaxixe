@extends('layouts.app')

@section('title')
    UP Maxixe –> CEI -> Apresentação
@endsection

@php
  /*
    Must init this variable..
    highlight @section('[mobile]menus')
  */
  $isActive = 'cei';
  $isActiveSub = 'cei.coop';
  $isActiveSubSub = 'cei.coop.nacional';

@endphp

@section('menus')
  @include('inc.menus')
  @include('inc.mobilemenus')
@endsection


{{--  The right side of the navbar  --}}
@section('navbar-right-content')
  @auth
    @include('inc.navbar-user')
  @endauth
@endsection


{{--  Body  --}}
@section('body')