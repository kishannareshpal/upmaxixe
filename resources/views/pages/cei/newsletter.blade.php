@extends('layouts.app')

@section('title')
    UP Maxixe –> CEI -> Apresentação
@endsection

@php
  /*
    Must init this variable..
    highlight @section('[mobile]menus')
  */
  $isActive = 'cei';
  $isActiveSub = 'cei.newsletter';

  /*
    Must init this variable.
    Previous and Next Page.
  */
  $previousPage = "Informação";
  // $nextPage = "" -> NO NEXT PAGE!!
@endphp

@section('menus')
  @include('inc.menus')
  @include('inc.mobilemenus')
@endsection


{{--  The right side of the navbar  --}}
@section('navbar-right-content')
  @auth
    @include('inc.navbar-user')
  @endauth
@endsection


{{--  Body  --}}
@section('body')



  {{-- Next and Previous Buttons --}}  
  @include('inc.prev_next')


@endsection