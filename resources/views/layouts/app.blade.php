<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
	<!-- Standard Meta -->
	<meta charset="utf-8">
	<meta name="author" content="Universidade Pedagógica da Maxixe">
	<meta name="creator" content="Kishan Nareshpal Jadav">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="creator-website" content="https://kishannareshpal.github.io">
	<meta name="copyright" content="Universidade Pedagógica da Maxixe">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<title>@yield('title')</title>

	<!-- Add to homescreen for Chrome on Android -->
	<meta name="mobile-web-app-capable" content="yes">
	<link rel="icon" sizes="86x86" href="{{ asset('favicon.png') }}">

	<!-- Add to homescreen for Safari on iOS -->
	<meta name="apple-mobile-web-app-capable" content="yes">
	<!-- Depprecated -->
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-title" content="@yield('title')">
	<link rel="apple-touch-icon-precomposed" href="{{ asset('favicon.png') }}">


	<!-- IE Meta -->
	<meta name="application-name" content="UP Maxixe"/>
	<meta name="msapplication-tooltip" content="Universidade Pedagógica"/>
	  <!-- Tile icon for Win8 (144x144 + tile color) -->
	  <meta name="msapplication-TileImage" content="{{ asset('favicon.png') }}">
	  <meta name="msapplication-TileColor" content="#3372DF">


	<link rel="shortcut icon" href="{{ asset('favicon.png') }}">
	<!-- CSRF Token -->

	<!-- CSS FILES -->
	<noscript>
			Para a completa funcionalidade do site da UP Maxixe, é necessário habilitar o JavaScript. Aqui estão as
			<a href="https://www.enable-javascript.com/pt/" target="_blank"> instruções de como habilitar o JavaScript no seu navegador</a>.
	</noscript>

	<!-- Styles -->
	<link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.indigo-pink.min.css">
	{{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}
	<link href="{{ asset('css/app.css') }}" rel="stylesheet">
	<link rel="stylesheet" href="{{ asset('css/app-k.css') }}">
	@yield('headcsslink')

</head>

<body>

		{{-- ANALYTICS --}}
		<script>
			window.fbAsyncInit = function() {
				FB.init({
					appId      : "{{ env('FACEBOOK_APP_ID', null) }}",
					cookie     : true,
					xfbml      : true,
					version    : "{{ env('FACEBOOK_DEFAULT_GRAPH_VERSION', 'v3.1') }}"
				});
					
				FB.AppEvents.logPageView();   
					
			};
		
			(function(d, s, id){
					var js, fjs = d.getElementsByTagName(s)[0];
					if (d.getElementById(id)) {return;}
					js = d.createElement(s); js.id = id;
					js.src = "https://connect.facebook.net/en_US/sdk.js";
					fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
		</script>
		{{-- ./ANALYTICS --}}


	<div id="app" class="uk-offcanvas-content">
		@include('inc.nav')

		@yield('menus')

		@yield('body')

		@yield('prev_next')

		@include('inc.footer')

		<!-- OFFCANVAS -->
		{{-- @include('inc.mobilemenus') --}}
		<!-- /OFFCANVAS -->
	</div>

	<!-- Scripts -->
	<script src="https://code.getmdl.io/1.3.0/material.min.js"></script>
	<script src="{{ asset('js/app.js') }}"></script>
	@yield('jscript')
</body>

</html>
