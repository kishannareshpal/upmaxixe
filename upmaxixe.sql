-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 12, 2018 at 11:25 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `upmaxixe`
--

-- --------------------------------------------------------

--
-- Table structure for table `centros__nucleos`
--

CREATE TABLE `centros__nucleos` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `centros__nucleos`
--

INSERT INTO `centros__nucleos` (`id`, `title`, `body`, `created_at`, `updated_at`) VALUES
(1, 'Núcleo de Estudos de Desenvolvimento Comunitário e Ambiente  NEDECA', '<p>A UP-Maxixe em parceria com o Polit&eacute;cnico de Mil&atilde;o rubricaram um acordo em Fevereiro de 2017,com vista o desenvolvimento de algumas actividades para constru&ccedil;&atilde;o de infraestruturas resilientes aos eventos naturais extremos e apoiando o projecto Parque ecol&oacute;gico municipal: uma simbiose entre a preserva&ccedil;&atilde;o dos valores culturais e da biodiversidade local (NEDECA), que est&aacute; na sua fase explorat&oacute;ria na recolha de informa&ccedil;&otilde;es preliminares.</p>\r\n\r\n<p>De referir que at&eacute; ao momento as actividades desenvolvidas de levantamentos topogr&aacute;ficos, invent&aacute;rios entre outros, para produ&ccedil;&atilde;o de mapas tem&aacute;ticos da &aacute;rea em estudo, s&atilde;o de presta&ccedil;&atilde;o de servi&ccedil;os a pedido do Polit&eacute;cnico de Mil&atilde;o</p>', '2018-05-30 12:27:48', '2018-05-30 12:27:48'),
(2, 'Amazfit Stratos', '<p>The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog.</p>', '2018-05-30 15:11:12', '2018-05-30 15:11:12');

-- --------------------------------------------------------

--
-- Table structure for table `destaques`
--

CREATE TABLE `destaques` (
  `id` int(11) NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `docentes`
--

CREATE TABLE `docentes` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cadeiras` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `biography` longtext COLLATE utf8mb4_unicode_ci,
  `email` longtext COLLATE utf8mb4_unicode_ci,
  `celular` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `docentes`
--

INSERT INTO `docentes` (`id`, `title`, `name`, `cadeiras`, `photo`, `biography`, `email`, `celular`) VALUES
(1, 'licenciado', 'Kishan', 'Portugues', NULL, 'chskdchjdkscdskhkdhs', 'kishan_jadav@hotmail.com', '+258');

-- --------------------------------------------------------

--
-- Table structure for table `eventos`
--

CREATE TABLE `eventos` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `local` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `capacidade` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `precoindividual` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imageurl` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `horario_do_inicio` datetime NOT NULL,
  `horario_do_fim` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `eventos`
--

INSERT INTO `eventos` (`id`, `name`, `local`, `capacidade`, `precoindividual`, `imageurl`, `description`, `horario_do_inicio`, `horario_do_fim`, `created_at`, `updated_at`) VALUES
(25, 'Festa dos Caloiros', 'Campus 2 – UP Maxixe', 'unlimited', '250,00', 'festadoscaloiros_1528394418.jpg', 'Entradas: Pré venda 200,00MTS- Público em geral\r\nNo local: 250,00MTS - Público em geral\r\nBilhetes à 100,00MTS para os estudantes da UP Maxixe.\r\n\r\nEsperamos por si', '2018-03-24 22:00:00', '2018-03-25 06:00:00', '2018-06-07 18:00:18', '2018-06-07 18:00:18'),
(27, 'Conferência Internacional \"Personalismo\"', 'Campus 1 – UP Maxixe', 'unlimited', 'free', '20746286_10155785109464653_563753709229950897_o_1528394710.jpg', 'No âmbito da concretização dos seus propósitos, enquanto instituição de pesquisa e extensão, a Direcção para Pós-graduação, Pesquisa e Extensão vai realizar de 02 a 03 de Novembro de 2017 em Maxixe – Campus 1 da Universidade Pedagógica, Delegação de Maxixe, a Conferência Internacional sobre \"O Personalismo\".\r\nAtendendo que na actualidade cientistas, sociedade civil e ONG`s têm estado a discutir, nos fóruns nacionais e internacionais, a pessoa numa perspectiva pluridimensional ao nível global e local e sua influência no desenvolvimento no âmbito educacional, politico, social, cultural, estético e literário, espera-se que este evento científico sob o lema Humanizando a ciência em prol do Desenvolvimento Integral da Pessoa poderá proporcionar debate e intercâmbio entre especialistas sobre as temáticas propostas.\r\nA Conferência incluirá duas mesas-redondas nas quais serão debatidas as questões sobre o Personalismo – valorizando a pessoa na perspectiva pluridimensional.', '2017-11-02 08:00:00', '2017-11-02 17:00:00', '2018-06-07 18:05:10', '2018-06-07 18:05:10'),
(28, 'Aula Inaugural – 2017', 'UP Maxixe', 'unlimited', 'free', '16665735_10155149362874653_4843630343785662747_o_1528396664.jpg', NULL, '2017-03-22 09:00:00', '2017-03-22 00:00:00', '2018-06-07 18:07:29', '2018-06-07 18:37:44'),
(29, 'Abertura do Ano Lectivo – 2017', 'UP Maxixe', 'unlimited', 'free', NULL, 'SÃO TODOS CONVIDADOS A PARTICIPAR NA ABERTURA DO ANO ACADÉMICO 2017.\r\n\r\nCURSO DIURNO E PÓS-LABORAL.\r\n\r\nNOTA: A sessão de abertura acontecerá em três momentos, sendo o primeiro as 8:hrs e 30min para os estudantes de manhã, 14:00 horas para os de tarde, e finalmente 18:00 horas para os de Pós Laboral.\r\nLocal: Sala Magna da UP-Maxixe.\r\n\r\nA Cerimónia tem como pontos de Agenda:\r\n> Entoação do Hino Nacional;\r\n>Animação Cultural;\r\n>Intervenção do Diretor da Delegação;\r\n>Intervenção do Senhor Administrador do Distrito de Maxixe;\r\n>Intervenção do Presidente do Conselho Municipal de Maxixe;\r\n>Intervenção do Director Provincial de Educação e Desenvolvimento Humano;\r\n>Informe da Área Pedagógica;\r\n>Intervenção do Registo Académico;\r\n>Intervenção da Pós-graduação, Pesquisa e Extensão;\r\n>Intervenção da área Administrativa;\r\n>Intervenção dos Estudantes;\r\n>Diversos;\r\n>Encerramento', '2017-02-13 09:00:00', '2017-02-13 18:00:00', '2018-06-07 18:09:56', '2018-06-07 18:11:39'),
(30, 'Palestras do Prof. Doutor Didier Moureau', 'UP Maxixe', 'unlimited', 'free', '14202518_10154613848739653_5488481022621644764_n_1528395556.jpg', NULL, '2017-09-08 08:30:00', '2017-09-08 15:00:00', '2018-06-07 18:19:16', '2018-06-07 18:19:16'),
(31, 'ULTIMA JORNADA - CAMPEONATO PROVINCIAL FUTEBOL', 'UP Maxixe', 'unlimited', 'free', '11792103_10153556035379653_6222825121275566527_o_1528395789.jpg', 'UP MAXIXE  X  FERROVIÁRIO DE INHAMBANE', '2016-07-26 09:00:00', '2016-07-26 09:00:00', '2018-06-07 18:21:17', '2018-06-07 18:23:46'),
(32, '12ª JORNADA - CAMPEONATO PROVINCIAL FUTEBOL', 'Quissico – Moçambique', 'unlimited', 'free', '11703203_10153540007009653_3529425202708229492_n_1528396636.jpg', 'Quissico Futebol Clube X UP Maxixe \r\nDelegado: Paulo Custódio', '2015-07-18 14:30:00', '2015-07-18 16:30:00', '2018-06-07 18:25:41', '2018-06-07 18:37:16'),
(33, 'LIGA UNISAF 2015', 'Maxixe – Moçambique', 'unlimited', 'free', NULL, 'O Campeonato Interno da UP- Maxixe prestes a arrancar. \r\nParticipe a apoia a sua turma', '2015-03-28 14:30:00', '2015-03-28 16:30:00', '2018-06-07 18:27:36', '2018-06-07 18:27:36'),
(34, 'SIMPÓSIO SOBRE A PEDAGOGIA DE PAULA ISABEL CERIOLI', 'UP Maxixe – Campus 1 – Sala Magna', 'unlimited', 'free', '1410773_10151976233584653_614025822_o_1528396192.jpg', 'A UP-Maxixe (UniSaF) na ocasião do 150º aniversário de fundação da Congregação da Sagrada Família (CSF), celebra um Simpósio sobre a Pedagogia da Paula Isabel Cerioli, Fundadora da CSF. De manhã das 8:30 às 11:00 h. haverá palestras e comunicações. A tarde às 16:00 h. haverá a abertura da exposição e premiação das obras que participaram no concursos de arte africana \"Mãe de muitos filhos\".', '2013-11-01 08:30:00', '2013-11-01 11:00:00', '2018-06-07 18:29:52', '2018-06-07 18:36:56'),
(35, 'Seminários de Filosofia Ética', 'UP Maxixe – Campus 1 – Sala Magna', 'unlimited', 'free', NULL, 'Administrados pelo Prof. Doutor José Henrique Silveira de Brito, da Universidade Católica de Braga', '2013-04-02 10:30:00', '2018-04-02 13:25:00', '2018-06-07 18:32:00', '2018-06-07 18:32:00'),
(36, 'ANO DA FÉ', 'UP Maxixe – Campus 1 – Sala Magna', 'unlimited', 'free', NULL, NULL, '2012-10-16 15:00:00', '2012-10-16 18:00:00', '2018-06-07 18:33:48', '2018-06-07 18:34:50'),
(37, 'Teste', 'UP Maxixe', 'unlimited', 'free', NULL, 'Teste 123', '2018-06-16 09:00:00', '2018-06-16 10:00:00', '2018-06-15 16:07:37', '2018-06-15 16:07:37'),
(38, 'Teste', 'UP Maxixe', 'unlimited', 'free', 'Mojave_1529138011.jpg', NULL, '2018-06-16 09:00:00', '2018-06-16 15:00:00', '2018-06-16 08:33:31', '2018-06-16 08:38:58');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `noticia_id` int(11) NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`noticia_id`, `url`) VALUES
(11, 'abertura-ano2018_1525774979.jpg'),
(1, 'IMG_2506_1527662820.JPG'),
(1, 'IMG_2509_1527662820.JPG'),
(1, 'IMG_2512_1527662820.JPG'),
(1, 'IMG_2508_1527662826.JPG'),
(1, 'IMG_2507_1527662826.JPG'),
(1, 'IMG_2511_1527662827.JPG'),
(1, 'IMG_2510_1527662827.JPG'),
(1, 'IMG_2505_1527662827.JPG'),
(2, 'sdo_1528398094.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2018_04_17_152458_create_noticias_table', 1),
(3, '2018_04_23_142934_create_images_table', 1),
(4, '2018_05_01_171125_create_destaques_table', 2),
(10, '2018_05_10_175617_create_obras_table', 6),
(11, '2018_05_10_173018_create_docentes_table', 7),
(13, '2018_05_04_164736_create_eventos_table', 8),
(14, '2018_05_30_090316_create_centros__nucleos_table', 9);

-- --------------------------------------------------------

--
-- Table structure for table `noticias`
--

CREATE TABLE `noticias` (
  `poster_id` int(11) NOT NULL,
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imageurl` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subtitle` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `noticias`
--

INSERT INTO `noticias` (`poster_id`, `id`, `title`, `imageurl`, `subtitle`, `body`, `created_at`, `updated_at`) VALUES
(1, 2, 'MESTRADOS EM DIREITO CIVIL E EM ADMINISTRAÇÃO PÚBLICA', NULL, NULL, '<p>A Delega&ccedil;&atilde;o da UP Maxixe/ UniSaF, para al&eacute;m de oferecer v&aacute;rios cursos de licenciatura tem posto &agrave; disposi&ccedil;&atilde;o da sociedade cursos de p&oacute;s-gradua&ccedil;&atilde;o, tendo arrancado com o Curso de Mestrado em Administra&ccedil;&atilde;o e Gest&atilde;o Escolar (AGE) em 2012 e o Curso de Mestrado em Educa&ccedil;&atilde;o com Especialidades , em 2014. Todos estudantes do primeiro grupo j&aacute; defenderam. Muitos dos estudantes do segundo grupo tamb&eacute;m j&aacute; defenderam as suas disserta&ccedil;&otilde;es, sendo poucos que est&atilde;o na fase de culmina&ccedil;&atilde;o; finalmente, o terceiro grupo com cursos de Mestrados em Filosofia e em Psicologia Educacional est&aacute; no processo de elabora&ccedil;&atilde;o das Disserta&ccedil;&otilde;es. Assim, findas as tr&ecirc;s edi&ccedil;&otilde;es anteriores, a UP Maxixe/ UniSaF coloca &agrave; disposi&ccedil;&atilde;o da sociedade e toda comunidade acad&eacute;mica dois novos mestrados: (i) Mestrado em Direito Civil em colabora&ccedil;&atilde;o com a Faculdade de Direito da Universidade de Lisboa e o (ii) Mestrado em Administra&ccedil;&atilde;o P&uacute;blica. Neste &acirc;mbito, torna p&uacute;blico que, entre os dias 04 e 30 de Junho de 2018, decorrer&aacute; o processo de submiss&atilde;o de candidaturas para os interressados.</p>', '2018-06-07 18:50:21', '2018-06-07 18:50:21'),
(1, 3, 'Noticia', NULL, 'aosjd', '<p>sfsdfsdfsccsfsdfs dfsccsfsdfs dfsccsfsdfs dfsccsfsdfsdfsccsfs dfsdfsccsfsdfsdfscc sfsdfsdfsccsfs dfsdfsccsf sdfsdfsccsf sdfsdfsccsfsdfs dfsccsfsdfsdfsccsfs dfsdfsccsfsdfsd fsccsfsdfsdfsc csfsdfsdfscc &nbsp;</p>', '2018-06-16 08:34:46', '2018-07-10 20:28:34');

-- --------------------------------------------------------

--
-- Table structure for table `obras`
--

CREATE TABLE `obras` (
  `docente_id` int(11) NOT NULL,
  `referencia` longtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Kishan Nareshpal Jadav', 'kishan_jadav@hotmail.com', '$2y$10$lFSpNl7w1hA95pND5H987uscuSu507dKrS7Fkm28Li8lARRgPvW5K', 'bLszIcWPKuBFmTmQexTtDt2JlLLOciZ2DDrbq1tapPyvPFeVLYPpp3FgtL1d', '2018-04-27 03:10:12', '2018-04-27 03:10:12');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `centros__nucleos`
--
ALTER TABLE `centros__nucleos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `docentes`
--
ALTER TABLE `docentes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eventos`
--
ALTER TABLE `eventos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `centros__nucleos`
--
ALTER TABLE `centros__nucleos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `docentes`
--
ALTER TABLE `docentes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `eventos`
--
ALTER TABLE `eventos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `noticias`
--
ALTER TABLE `noticias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
