<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class Noticia extends Model
{
    protected $primaryKey = 'id';


    public function scopeLimitChars($query, $field, $numberOfCharacters = 500)
    {
        return $query->select(DB::raw("LEFT({$field}, {$numberOfCharacters}) as {$field}"));
    }

    # all images related to the post
    public function images(){
      return $this->hasMany('App\Image');
    }

    # one image for the cover
    public function firstimage(){
      return $this->hasOne('App\Image');
    }

    # get newer than current post
    public function newer(){
        return Noticia::where('id', '>', $this->id)->orderBy('id','asc')->first();
    }

    # get older thant current post
    public  function older(){
        return Noticia::where('id', '<', $this->id)->orderBy('id','desc')->first();
    }
}
