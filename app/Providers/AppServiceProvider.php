<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
// use Facebook\HttpClients\FacebookStreamHttpClient;
/*
    i've increased the timeout of facebook request on this class here:
    // Facebook\HttpClients\FacebookStreamHttpClient

*/


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
      // For facebook sdk
      $this->app->singleton(Facebook::class, function ($app) {
          return new Facebook(config('facebook.config'));
      });
    }

    
}
