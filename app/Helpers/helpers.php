<?php

// Sort an array Ascendingly by the key.
if (!function_exists('sortAscFacebookArrayBy')) {

  function sortAscFacebookArrayBy($array, $key) {
    // Refer to http://www.johnmorrisonline.com/how-to-sort-multidimensional-arrays-using-php/ for more info.
    foreach($array as $value) {
      
      if ($value[$key] instanceof DateTime) {
        $b[] = date_timestamp_get($value[$key]);
      } else {
        $b[] = strtolower($value[$key]);
      }

    }
    
    /** Source: https://www.w3schools.com/php/php_arrays_sort.asp
     * 
     * sort() - sort arrays in ascending order
     * rsort() - sort arrays in descending order
     * ----- We should use one of the mentioned bellow for this use case:
     * asort() - sort associative arrays in ascending order, according to the value 
     * ksort() - sort associative arrays in ascending order, according to the key
     * arsort() - sort associative arrays in descending order, according to the value
     * krsort() - sort associative arrays in descending order, according to the key
     */

    asort($b);

    foreach($b as $k=>$v) {
      $c[] = $array[$k];
    }
    
    return $c;
  }

}



// Remove URLs from string
if (!function_exists('stripURL')){

  /** Source
   * @desc Remove URLs from string
   * 
   * @param string $og_string - the string containing the url, to be processed
   * @param string $replacement - what should the links be replaced with (might be a button html or something)
   * 
   * @return string - the same string but without url.
   * @source https://gist.github.com/madeinnordeste/e071857148084da94891
   */

   
  function stripURL(string $og_string, $replacement = ''){
    $string = preg_replace('/\b((https?|ftp|file):\/\/|www\.)[-A-Z0-9+&@#\/%?=~_|$!:,.;]*[A-Z0-9+&@#\/%=~_|$]/i', $replacement, $og_string);
    return $string;
  }

}

