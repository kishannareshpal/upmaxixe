<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Destaque extends Model
{
  protected $primaryKey = 'id';
  public $timestamps = false;
  
}
