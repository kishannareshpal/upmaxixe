<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    //
    protected $primaryKey = 'notcia_id';
    public $timestamps = false;

    public function noticias() {
      return $this->belongsTo('App\Noticia');
    }


}
