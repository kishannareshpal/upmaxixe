<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Obra extends Model
{
  public $timestamps = false;

  public function docentes() {
    return $this->belongsTo('App\Docente');
  }
}
