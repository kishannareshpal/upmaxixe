<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\IntEdital;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class IntEditaisController extends Controller
{

     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except'=>['index', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $editais = IntEdital::orderBy('created_at', 'desc')->get();

        $months = [
            '0','Janeiro', 'Fevereiro',
            'Março','Abril','Maio','Junho',
            'Julho','Agosto','Setembro',
            'Outubro', 'Novembro', 'Dezembro'
        ];
  
        $data = [
            'editais' => $editais,
            'months' => $months
        ];


        return view('pages.cei.internacionalizacao.editais.index')->with($data);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.cei.internacionalizacao.editais.create');        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required'
        ]);

        # Create the actual post or event, whatever
        $edital = new IntEdital;
        $edital->title = $request->input('title');
        $edital->body = $request->input('body');
        $edital->save();

        return redirect(route('editais.index'))->with('success', 'Edital "'.$request->input('title').'" postado com sucesso.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $edital = IntEdital::find($id);

        # localize month name to pt.. not the best way, i know... but whatever!
          $months = [
                       '0','Janeiro', 'Fevereiro',
                       'Março','Abril','Maio','Junho',
                       'Julho','Agosto','Setembro',
                       'Outubro', 'Novembro', 'Dezembro'
          ];
          $monthRaw = $edital->updated_at->month;
          $ptmonth = $months[$monthRaw];

        # arguments to pass into the view:
          $data = [
              'edital'  => $edital,
              'ptmonth' => $ptmonth,
          ];

        // return $post->images;
        return view('pages.cei.internacionalizacao.editais.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edital = IntEdital::find($id);

        # localize month name to pt.. not the best way, i know... but whatever!
          $months = [
                       '0','Janeiro', 'Fevereiro',
                       'Março','Abril','Maio','Junho',
                       'Julho','Agosto','Setembro',
                       'Outubro', 'Novembro', 'Dezembro'
          ];
          $monthRaw = $edital->updated_at->month;
          $ptmonth = $months[$monthRaw];

        # arguments to pass into the view:
          $data = [
              'edital'  => $edital,
              'ptmonth' => $ptmonth
          ];

        return view('pages.cei.internacionalizacao.editais.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required'
        ]);

        # Create the actual post or event, whatever
        $edital = IntEdital::find($id);
        $edital->title = $request->input('title');
        $edital->body = $request->input('body');
        $edital->save();

        return redirect(route('editais.index'))->with('success', 'Edital "'.$request->input('title').'" editado com sucesso.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $edital = IntEdital::find($id);
        $title = $edital->title;
        $edital->delete();
        return redirect(route('editais.index'))->with('success', 'Edital "'.$title.'" removido com sucesso.');
    }
}
