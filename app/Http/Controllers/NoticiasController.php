<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Noticia;
use App\Image;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;


class NoticiasController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except'=>['index', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $posts = Noticia::orderBy('created_at', 'desc')->paginate(5);

      $months = [
        '0','Janeiro', 'Fevereiro',
        'Março','Abril','Maio','Junho',
        'Julho','Agosto','Setembro',
        'Outubro', 'Novembro', 'Dezembro'
      ];

      $data = [
        'posts' => $posts,
        'months' => $months
      ];
      return view('pages.noticias.index')->with($data);
    }








    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $last_post = DB::table('noticias')->orderBy('created_at', 'desc')->first();
      return view('pages.noticias.create')->with('last_post', $last_post);
    }







    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required'
        ]);

        # Create the actual post or event, whatever
        $post = new Noticia;
        $post->title = $request->input('title');
        if ($request->has('subtitle')) {
          $post->subtitle = $request->input('subtitle');
        }
        $post->body = $request->input('body');
        $post->image_urls = $request->input('image_urls');
        $post->poster_id = auth()->user()->id;
        $post->save();

        return redirect('/noticias')->with('success', 'Noticia "'.$request->input('title').'" publicada com sucesso.');

    }









    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Noticia::find($id);

        $newer_post = $post->newer();
        $older_post = $post->older();

        # localize month name to pt.. not the best way, i know... but whatever!
          $months = [
                       '0','Janeiro', 'Fevereiro',
                       'Março','Abril','Maio','Junho',
                       'Julho','Agosto','Setembro',
                       'Outubro', 'Novembro', 'Dezembro'
          ];
          $monthRaw = $post->updated_at->month;
          $ptmonth = $months[$monthRaw];

        # arguments to pass into the view:
          $data = [
              'post'  => $post,
              'ptmonth' => $ptmonth,
              'newer_post' => $newer_post,
              'older_post' => $older_post
          ];

        // return $post->images;
        return view('pages.noticias.show')->with($data);
    }











    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Noticia::find($id);

        # localize month name to pt.. not the best way, i know... but whatever!
          $months = [
                       '0','Janeiro', 'Fevereiro',
                       'Março','Abril','Maio','Junho',
                       'Julho','Agosto','Setembro',
                       'Outubro', 'Novembro', 'Dezembro'
          ];
          $monthRaw = $post->updated_at->month;
          $ptmonth = $months[$monthRaw];

        # arguments to pass into the view:
          $data = [
              'post'  => $post,
              'ptmonth' => $ptmonth
          ];

        return view('pages.noticias.edit')->with($data);
    }











    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
        'title' => 'required',
        'body' => 'required'
      ]);
      
      $post = Noticia::find($id);
      $post->title = $request->input('title');
      if ($request->has('subtitle')) {
        $post->subtitle = $request->input('subtitle');
      }
      $post->body = $request->input('body');
      $post->image_urls = $request->input('image_urls');
      $post->poster_id = auth()->user()->id;
      $post->save();

      return redirect('/noticias')->with('success', 'Noticia "'.$request->input('title').'" editada com sucesso.');
    }



////////////////////////////////////////////////////////////////////////////////
// Edit images (Upload Image)
public function ui(Request $request, $id){

  if ($request->hasFile('qqfile')) {
    $file = $request->file('qqfile');
    // $file_count = count($files);

    // foreach ($files as $file) {
      // get filename with the extension:
    $filenameWithExt = $file->getClientOriginalName();
    // without the ext:
    $filenameWithoutTheExt = pathinfo($filenameWithExt, PATHINFO_FILENAME);
    // get just the ext:
    $filenameExtensionOnly = $file->getClientOriginalExtension();
    // the actual filename that is going to get stored in memory:
    $filename = $filenameWithoutTheExt.'_'.time().'.'.$filenameExtensionOnly;

    // put the filenam in the images table
    $imagepost = new Image;
    $imagepost->url = $filename;
    $imagepost->noticia_id = $id;
    $imagepost->save();

    // upload the image now, i guess:
    $path = $file->storeAs('public/noticias_image/'.$id, $filename);

    $resp = [
      'success'=>true
    ];

    return json_encode($resp);

  } else {
    $resp = [
      'success'=>false
    ];
    return $resp;
  }

}


///////////
// Remove image (Delete Image)
public function di($id, $imageName){
  $post = Noticia::find($id);
  $dbDelete = Image::where('url', $imageName)->delete();

  // dd($image);
  $localDelete = Storage::delete('public/noticias_image/'.$id.'/'.$imageName);
  // return public_path('/storage/noticias_image'.'/'.$id.'/'.$imageName);

  // check for deletion error
  if (!$dbDelete && !$localDelete) {
      /*
        Database      : Failed.
        Local Storage : Failed.
      */
      $msg = [
        'failed'=>'Oops, ocorreu o ERRO 501 ao apagar a imagem. Por favor informe ao Kishan.'
      ];


    } elseif (!$dbDelete && $localDelete) {
      /*
        Database      : Failed.
        Local Storage : Success.
      */
      $msg = [
        'failed'=>'Oops, ocorreu o ERRO 502 ao apagar a imagem. Por favor informe ao Kishan.'
      ];


    } elseif ($dbDelete && !$localDelete) {
      /*
        Database      : Success.
        Local Storage : Failed.
      */
      $msg = [
        'failed'=>'Oops, ocorreu o ERRO 502 ao apagar a imagem. Por favor informe ao Kishan.'
      ];


    } else {
      /*
        Database      : Success.
        Local Storage : Success.
      */
      $msg = [
        'success'=>'Imagem apagada com sucesso!'
      ];
    }

  return redirect(route('noticias.index').'/'.$id.'/edit')->with($msg);
}

////////////////////////////////////////////////////////////////////////////////











    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Noticia::find($id);
        $title = $post->title;
        $post->delete();
        return redirect(route('noticias.index'))->with('success', 'Noticia "'.$title.'" removida com sucesso.');
    }
}
