<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Evento;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use DateTime;
use Illuminate\Support\Facades\Storage;
use DateTimezone;
use Facebook\Facebook;


class EventosController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except'=>['index', 'show', 'showmodal', 'todos']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fb = new Facebook;

        try {
          // Returns a `FacebookFacebookResponse` object
          $response = $fb->get(
            '/me?fields=events{id,name,description,cover,start_time,end_time,place}',
            env('FACEBOOK_UNISAF_API_KEY', null)
          );

        } catch(FacebookExceptionsFacebookResponseException $e) {
          echo 'Graph returned an error: ' . $e->getMessage();
          exit;
        } catch(FacebookExceptionsFacebookSDKException $e) {
          echo 'Facebook SDK returned an error: ' . $e->getMessage();
          exit;
        }

        $graphNode = $response->getGraphNode();
        $eventos = $graphNode->getField('events')->asArray();
  
        // $future_eventos = $eventos->where('horario_do_inicio', '>', Carbon::now('Africa/Maputo'))->take(5);
        // $old_eventos = $eventos->where('horario_do_inicio', '<', Carbon::now('Africa/Maputo'))->take(10);

        
        // for carbon date localization
        $months = [
           '0','Janeiro', 'Fevereiro',
           'Março','Abril','Maio','Junho',
           'Julho','Agosto','Setembro',
           'Outubro', 'Novembro', 'Dezembro'
        ];

        $shortmonths = [
          '0','JAN', 'FEV',
          'MAR','ABR','MAI','JUN',
          'JUL','AGO','SET',
          'OUT', 'NOV', 'DEZ'
       ];
  

        $shortweeks = ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb"];

        $weeks = [
           'Domingo','Segunda-Feira', 'Terça-Feira',
           'Quarta-Feira','Quinta-Feira','Sexta-Feira','Sábado'
        ];

        $data = [
          // 'future_eventos'=>$future_eventos,
          // 'old_eventos'=>$old_eventos,
          'all'=>$eventos,

          'months'=>$months,
          'weeks'=>$weeks,
          'shortweeks'=>$shortweeks,
          'shortmonths'=>$shortmonths

        ];

        return view('pages.eventos.index')->with($data);
    }







    /**
     * Display all of the resources.
     *
     * @return \Illuminate\Http\Response
     */
    public function todos(){
      $eventos = Evento::orderBy('horario_do_inicio', 'desc')->paginate(9);

      $months = [
         '0','Janeiro', 'Fevereiro',
         'Março','Abril','Maio','Junho',
         'Julho','Agosto','Setembro',
         'Outubro', 'Novembro', 'Dezembro'
      ];

      $shortweeks = ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb"];

      $weeks = [
         'Domingo','Segunda-Feira', 'Terça-Feira',
         'Quarta-Feira','Quinta-Feira','Sexta-Feira','Sábado'

      ];

      $data = [
        'eventos'=>$eventos,
        'months'=>$months,
        'weeks'=>$weeks
      ];

      return view('pages.eventos.todos')->with($data);
    }







    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.eventos.create');
    }





    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
          'nome' => 'required',
          'local' => 'required',
          'capacidade' => 'required',
          'precoindividual' => 'required',
          'horario_do_inicio' => 'required',
          'horario_do_fim' => 'required',
          'image' => 'image|nullable',

      ]);

      # Create the actual post or event, whatever
      $evento = new Evento;
      $evento->name = $request->input('nome'); // nome do evento
      $evento->local = $request->input('local'); // local do evento
      $evento->capacidade = $request->input('capacidade'); // local do evento
      $evento->precoindividual = $request->input('precoindividual'); // local do evento

      if ($request->has('description')) {
        $evento->description = $request->input('description'); // descrição (opcional)
      }
      // horarios
      $evento->horario_do_inicio = new DateTime($request->input('horario_do_inicio'), new DateTimezone('Africa/Maputo'));


      if (!($request->input('horario_do_fim') == "")) {
        // $evento->horario_do_fim = new Carbon(); // do fim (opcional)
        $evento->horario_do_fim = new DateTime($request->input('horario_do_fim'), new DateTimezone('Africa/Maputo'));
      }

      $evento->save();

      // Check if image is being uploaded
       // if an image already exists, overwrite that one
      if ($request->hasFile('image')) {
        $file = $request->file('image');
        // get filename with the extension:
        $filenameWithExt = $file->getClientOriginalName();
        // without the ext:
        $filenameWithoutTheExt = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        // get just the ext:
        $filenameExtensionOnly = $file->getClientOriginalExtension();
        // the actual filename that is going to get stored in memory:
        $filename = $filenameWithoutTheExt.'_'.time().'.'.$filenameExtensionOnly;

        $evento->imageurl = $filename;
        $id = $evento->id;

        // upload the image now, i guess:
        $path = $file->storeAs('public/eventos_image/'.$id, $filename);

        $evento->save();
      }

      return redirect(route('eventos.index'))->with('success', 'Evento "'.$request->input('nome').'" publicado com sucesso.');

    }






    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $fb = new Facebook;
      
      try {
        // Returns a `FacebookFacebookResponse` object
        $response = $fb->get(
          '/'.$id.'?fields=id,name,description,cover,start_time,end_time,place',
          env('FACEBOOK_UNISAF_API_KEY', null)
        );

      } catch(FacebookExceptionsFacebookResponseException $e) {
        echo 'Graph returned an error: ' . $e->getMessage();
        exit;
      } catch(FacebookExceptionsFacebookSDKException $e) {
        echo 'Facebook SDK returned an error: ' . $e->getMessage();
        exit;
      }

      $evento = $response->getGraphNode()->asArray();

      $months = [
        '0','Janeiro', 'Fevereiro',
        'Março','Abril','Maio','Junho',
        'Julho','Agosto','Setembro',
        'Outubro', 'Novembro', 'Dezembro'
     ];

     
     $weeks = [
        'Domingo','Segunda-Feira', 'Terça-Feira',
        'Quarta-Feira','Quinta-Feira','Sexta-Feira','Sábado'
     ];

      $data = [
        'evento' => $evento,
        'months' => $months,
        'weeks' => $weeks,
      ];
      
      return view('pages.eventos.show')->with($data);
    }







    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $evento = Evento::find($id);

      # localize month name to pt.. not the best way, i know... but whatever!
      $months = [
                   '0','Janeiro', 'Fevereiro',
                   'Março','Abril','Maio','Junho',
                   'Julho','Agosto','Setembro',
                   'Outubro', 'Novembro', 'Dezembro'
      ];

      $monthRaw = $evento->updated_at->month;
      $ptmonth = $months[$monthRaw];

      # arguments to pass into the view:
      $data = [
        'evento'=>$evento,
        'ptmonth'=>$ptmonth
      ];

      return view('pages.eventos.edit')->with($data);
    }







    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
      $this->validate($request, [
          'nome' => 'required',
          'local' => 'required',
          'capacidade' => 'required',
          'precoindividual' => 'required',
          'horario_do_inicio' => 'required',
          'horario_do_fim' => 'required',
          'image' => 'image|nullable',
      ]);

      # Edit the required or whatever
      $evento = Evento::find($id);
      $evento->name = $request->input('nome');                       // nome do evento
      $evento->local = $request->input('local');                     // local do evento
      $evento->capacidade = $request->input('capacidade');           // capacidade
      $evento->precoindividual = $request->input('precoindividual'); // preço individual

      if ($request->has('description')) {
        $evento->description = $request->input('description'); // descrição (opcional)
      }
      // horarios
      $evento->horario_do_inicio = new DateTime($request->input('horario_do_inicio'), new DateTimezone('Africa/Maputo'));


      if (!($request->input('horario_do_fim') == "")) {
        // $evento->horario_do_fim = new Carbon(); // do fim (opcional)
        $evento->horario_do_fim = new DateTime($request->input('horario_do_fim'), new DateTimezone('Africa/Maputo'));
      }

      // If a new image has been selected, replace the old one
      if ($request->hasFile('image')) {

        $file = $request->file('image');

        // get filename with the extension:
        $filenameWithExt = $file->getClientOriginalName();

        // without the ext:
        $filenameWithoutTheExt = pathinfo($filenameWithExt, PATHINFO_FILENAME);

        // get just the ext:
        $filenameExtensionOnly = $file->getClientOriginalExtension();

        // delete the old image from the storage, if exists:
        if (!empty($evento->imageurl)) {
          $imageName = $evento->imageurl;
          if (Storage::exists('public/eventos_image/'.$id.'/'.$imageName)) {
            $localDelete = Storage::delete('public/eventos_image/'.$id.'/'.$imageName);
          };
        }

        // store the newly selected image
        $filename = $filenameWithoutTheExt.'_'.time().'.'.$filenameExtensionOnly;
        $evento->imageurl = $filename;
        $path = $file->storeAs('public/eventos_image/'.$id, $filename);
      }

      $evento->save();

      return redirect(route('eventos.index'))->with('success', 'Evento "'.$request->input('nome').'" editado com sucesso.');
    }







    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $evento = Evento::find($id);
      $title = $evento->name;

      // delete the uploaded image from storage
      if (!empty($evento->imageurl)) {
        $imageName = $evento->imageurl;
        if (Storage::exists('public/eventos_image/'.$id.'/'.$imageName)) {
          $localDelete = Storage::delete('public/eventos_image/'.$id.'/'.$imageName);
        };
      }

      // delete the record from database
      $evento->delete();

      return redirect(route('eventos.index'))->with('success', 'Evento "'.$title.'" removido com sucesso.');
    }






    /**
     * Show modal to the user when asked.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showmodal($id){
      $evento = Evento::find($id);
      return $evento;
    }








    public function di($id, $imageName){
      $evento = Evento::find($id);
      $evento->imageurl = null;
      $evento->save();

      // dd($image);
      $localDelete = Storage::delete('public/eventos_image/'.$id.'/'.$imageName);
      // return public_path('/storage/noticias_image'.'/'.$id.'/'.$imageName);

      $msg = [
        'success'=>'Imagem apagada com sucesso!'
      ];

      return redirect(route('eventos.index').'/'.$id.'/edit')->with($msg);
    }

}
