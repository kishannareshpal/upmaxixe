<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CooperacaoController extends Controller
{
# ---- COOPERAÇÃO E INTERNACIONALIZAÇÃO TAB --------------------------------------------------- #
    # -> Apresentação
    public function apresentacao(){
        return view('pages.cei.apresentacao');
    }


        # -> Cooperação - Nacional
        public function cooperacaonacional(){
            return view('pages.cei.cooperacao.nacional');
        }


        # -> Cooperação - Internacional
        public function cooperacaointernacional(){
            return view('pages.cei.cooperacao.internacional');
        }


        # -> Internacionalização - Visiting
        public function visiting(){
            return view('pages.cei.internacionalizacao.visiting');
        }


        # -> Internacionalização - Professores Convidados
        public function professoresconvidados(){
            return view('pages.cei.internacionalizacao.professoresconvidados');
        }


        # -> Internacionalização - Editais
        // public function editais(){
        //     return view('pages.cei.internacionalizacao.editais');
        // }

    # -> Informação
    public function informacao(){
        return view('pages.cei.informacao.index');
    }

    # -> Newsletter
    public function newsletter(){
        return view('pages.cei.newsletter');
    }

}
