<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Destaque;
use App\Docente;
use App\Evento;
use App\Noticia;
use App\Obra;
use App\Centros_Nucleo;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Facebook\Facebook;

  # Facebook UNISAF PAGE API (Never Expires):
  // Actual -> EAAfyIQIUlmwBACblvzVPgewumV9NGVmuSSR94k1dCW0XUMz8qByDpTxdKBapk2RcecMo7h3ZBZBcmDZCO6fn77K1RUnZCu1OSs4UQGjowMKmZCZASHnsRKl6I58N0j417NHaxvqqEEUVQPjeaS0pkkZAwtubHYwGAZBaQhn9ItBFIwZDZD
  // Old without some permissions -> EAAfyIQIUlmwBAAZAUrUdpueHPeb0DZAdXrYfbASxK6H4X5RDDZCYREPqdM3qYSi3d3BJIPZA2AaKyYcGW3ZCvOWc5SZCZA2tyNAKGVAg9i3OrsUfYLkZCWSu1tBau9ZBfUZAYeHj794ZCXGUw5Ps46R8oepo6NtU8duWKcuiSreyJ2QgwZDZD
class PagesController extends Controller
{
    # Home Page
    public function home(){
      $fb = new Facebook;

      try {
        // Returns a \Facebook `FacebookResponse` object
        $response = $fb->get(
          '/me?fields=posts.limit(6){id,message,full_picture,type,link,permalink_url.limit(1)},events.limit(3){id,name,description,cover,start_time,end_time}',
          env('FACEBOOK_UNISAF_API_KEY', null)
        );

      } catch(FacebookExceptionsFacebookResponseException $e) {
        echo 'Graph returned an error: ' . $e->getMessage();
        exit;
      } catch(FacebookExceptionsFacebookSDKException $e) {
        echo 'Facebook SDK returned an error: ' . $e->getMessage();
        exit;
      }

      $graphNode = $response->getGraphNode();
      
      $posts = $graphNode->getField('posts')->asArray();
      
      $eventos = $graphNode->getField('events')->asArray();
      // THIS K-FUNCTION COMES FROM app/helpers/helpers.php
      $eventos = sortAscFacebookArrayBy($eventos, 'start_time');


      $destaques = Destaque::all();
      // $eventos = Evento::orderBy('horario_do_inicio', 'desc')->take(4)->get();
      $noticias = Noticia::orderBy('id', 'desc')->take(4)->get();

      $months = [
         '0','Janeiro', 'Fevereiro',
         'Março','Abril','Maio','Junho',
         'Julho','Agosto','Setembro',
         'Outubro', 'Novembro', 'Dezembro'
      ];

      $weeks = [
         'Domingo','Segunda-Feira', 'Terça-Feira',
         'Quarta-Feira','Quinta-Feira','Sexta-Feira','Sábado'
      ];

      // // MOCK
      // $eventos = [];
      // $posts = [];

      $data = [
        "weeks"=>$weeks,
        "months"=>$months,

        "posts"=>$posts,
        "eventos"=>$eventos,
        
        "destaques"=>$destaques,
        "noticias"=>$noticias,
        // "eventos"=>$eventos,
      ];

      return view('pages.home')->with($data);
    }


# ---- UNIVERSIDADES TAB --------------------------------------------------- #
      # -> Quem Somos
      public function quemsomos(){
        return view('pages.universidade.quemsomos');
      }

      # -> Missão e Valores
      public function missaoevalores(){
        return view('pages.universidade.missaoevalores');
      }

      # -> Organigrama
      public function organigrama(){
        return view('pages.universidade.organigrama');
      }

      # -> Orgãos Superiores
      public function orgaossuperiores(){
        return view('pages.universidade.orgaossuperiores');
      }

      # -> Conselho Directivo
      public function conselhodirectivo(){
        return view('pages.universidade.conselhodirectivo');
      }

      # -> Docentes
      public function docentes(){
        $docentes = Docente::all();
        $licenciados = $docentes->where('title', 'licenciado');
        $mestres = $docentes->where('title', 'mestre');
        $doutores = $docentes->where('title', 'doutor');

        $data = [
          'docentes'=>$docentes,
          'licenciados'=>$licenciados,
          'mestres'=>$mestres,
          'doutores'=>$doutores
        ];

        return view('pages.universidade.docentes')->with($data);
      }



          # -> Docentes -> Show the Profile
          public function docentesshow($id){
            $docente = Docente::find($id);

            $data = [
              'docente'=>$docente,
            ];

            return view('pages.universidade.docentes.show')->with($data);
          }

          # -> Docentes -> Manage
          public function docentesmanage(){
            $docentes = Docente::all();

            $data = [
              'docentes'=>$docentes
            ];

            return view('pages.universidade.docentes.add')->with($data);
          }

              # --> Docentes - Update/Edit Logic
              public function updateDocente(Request $request, $id){
                $this->validate($request, [
                    'nome' => 'required',
                    'cadeiras' => 'required',
                    'biografia' => 'required',
                    'foto' => 'image|nullable'
                ]);

                # Update
                $docente = Docente::find($id);
                $docente->name = $request->input('nome'); // nome completo
                $docente->title = $request->input('titulo'); // titulo academico
                $docente->cadeiras = $request->input('cadeiras'); // cadeira(s) que lecciona

                if ($request->has('biografia')) {
                  $docente->biography = $request->input('biografia'); // pequena biografia (opcional)
                }

                # Contacto
                  // Email
                  if ($request->has('email')) {
                    $docente->email = $request->input('email'); // email (opcional)
                  }

                  // Phone Number
                  if ($request->has('celular')) {
                    $docente->celular = $request->input('celular'); // número de celular (opcional)
                  }

                // Check if image is being uploaded; if it is, then upload the image after deleting the old one
                if ($request->hasFile('foto')) {
                  // if (!empty($imageName)) {
                  //   $imageName = $docente->photo;
                  //   if (Storage::exists('public/docentes_image/'.$imageName)) {
                  //     $localDelete = Storage::delete('public/docentes_image/'.$imageName);
                  //   };
                  // }

                  $file = $request->file('foto');
                  // get filename with the extension:
                  if (empty($imageName)) {
                    $imageName = $file->getClientOriginalName();
                  }

                  // without the ext:
                  $filenameWithoutTheExt = pathinfo($imageName, PATHINFO_FILENAME);

                  // get just the ext:
                  $filenameExtensionOnly = $file->getClientOriginalExtension();

                  // the actual filename that is going to get stored in memory:
                  // if (empty($docente->photo)) {
                  //   $filename = $filenameWithoutTheExt.'_'.time().'.'.$filenameExtensionOnly;
                  // } else {
                  //   $filename = $filenameWithoutTheExt.'.'.$filenameExtensionOnly;
                  // }

                  if (!empty($docente->photo)) {
                    $imageName = $docente->photo;
                    if (Storage::exists('public/docentes_image/'.$imageName)) {
                      $localDelete = Storage::delete('public/docentes_image/'.$imageName);
                    };
                  }

                  $filename = $filenameWithoutTheExt.'_'.time().'.'.$filenameExtensionOnly;
                  $docente->photo = $filename;
                  $path = $file->storeAs('public/docentes_image', $filename);
                }

                $docente->save();


                // If obras literarias was added
                if ($request->has('obras')) {
                  foreach ($request->obras as $obra) {
                    $obras_table = new Obra;
                    $obras_table->docente_id = $docente->id;
                    $obras_table->referencia = $obra;
                    $obras_table->save();
                  }
                }

                return redirect(route('docentes'))->with('success', 'Docente "'.$docente->name.'" editando com sucesso.');
              }

              # --> Docentes - Add Logic
              public function addDocente(Request $request){
                $this->validate($request, [
                    'nome' => 'required',
                    'cadeiras' => 'required',
                    'biografia' => 'required',
                    'foto' => 'image|nullable'
                ]);

                # Create the actual post or event, whatever
                $docente = new Docente;
                $docente->name = $request->input('nome'); // nome completo
                $docente->title = $request->input('titulo'); // titulo academico
                $docente->cadeiras = $request->input('cadeiras'); // cadeira(s) que lecciona

                if ($request->has('biografia')) {
                  $docente->biography = $request->input('biografia'); // pequena biografia (opcional)
                }

                # Contacto
                  // Email
                  if ($request->has('email')) {
                    $docente->email = $request->input('email'); // email (opcional)
                  }

                  // Phone Number
                  if ($request->has('celular')) {
                    $docente->celular = $request->input('celular'); // número de celular (opcional)
                  }

                // Check if image is being uploaded; if it has, then upload the image
                if ($request->hasFile('foto')) {
                  $file = $request->file('foto');
                  // get filename with the extension:
                  $filenameWithExt = $file->getClientOriginalName();
                  // without the ext:
                  $filenameWithoutTheExt = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                  // get just the ext:
                  $filenameExtensionOnly = $file->getClientOriginalExtension();
                  // the actual filename that is going to get stored in memory:
                  $filename = $filenameWithoutTheExt.'_'.time().'.'.$filenameExtensionOnly;

                  // upload the image now:
                  $docente->photo = $filename;
                  $path = $file->storeAs('public/docentes_image', $filename);
                }

                $docente->save();


                // If obras literarias was added
                if ($request->has('obras')) {
                  foreach ($request->obras as $obra) {
                    $obras_table = new Obra;
                    $obras_table->docente_id = $docente->id;
                    $obras_table->referencia = $obra;
                    $obras_table->save();
                  }
                }

                return redirect(route('docentes'))->with('success', 'Docente "'.$request->input('nome').'" adicionado com sucesso.');
              }

              # --> Docentes - Show Edit Form
              public function editDocente($id){
                $docente = Docente::find($id);

                $data = [
                  'docente'=>$docente,
                ];

                return view('pages.universidade.docentes.edit')->with($data);
              }

              # --> Docentes - Remove/Destroy/Delete
              public function deleteDocente($id){
                $docente = Docente::find($id);
                $name = $docente->name;
                $profilepicture = $docente->photo;
                $docente->delete();

                if (!empty($profilepicture)) {
                  if (Storage::exists('public/docentes_image/'.$profilepicture)) {
                    Storage::delete('public/docentes_image/'.$profilepicture);
                  }
                }

                $obras_do_docente = Obra::find($id);
                $obras_do_docente->delete();

                return redirect(view('pages.universidade.docentes'))->with('success', 'Docente "'.$name.'" removido(a) com sucesso.');
              }



# ---- DIRECÇÃO TAB -------------------------------------------------------- #
      # -> O Director
      public function odirector(){
        return view('pages.direccao.odirector');
      }

      # -> Gabinete do Director
      public function gabinetedodirector(){
        return view('pages.direccao.gabinetedodirector');
      }

      # -> Secretaria Geral
      public function secretariageral(){
        return view('pages.direccao.secretariageral');
      }

      # -> Gabinete do Director
      public function registoacademico(){
        return view('pages.direccao.registoacademico');
      }



# ---- FACULDADES TAB -------------------------------------------------------- #
      # -> Faculdades
      public function faculdades(){
        return view('pages.faculdades.faculdades');
      }


# ---- PESQUISA E EXTENSÃO TAB -------------------------------------------------------- #
      # -> Centros e Núcleos –––– NOTE: Logic MOVED TO "Centros_NucleosController"
      // public function centrosenucleos(){
      //
      // }

      # -> Projectos de Pesquisa
      public function projectosdepesquisa(){
        return view('pages.pesquisaeextensao.projectosdepesquisa');
      }

      # -> Revistas e Artigos
      public function revistaseartigos(){
        return view('pages.pesquisaeextensao.revistaseartigos');
      }

      # -> Escolinhas
      public function escolinhas(){
        return view('pages.pesquisaeextensao.escolinhas');
      }


}
