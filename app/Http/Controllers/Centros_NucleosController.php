<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Centros_Nucleo;

class Centros_NucleosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $nucleos = Centros_Nucleo::orderBy('created_at', 'desc')->paginate(5);
      return view('pages.pesquisaeextensao.centrosenucleos')->with('nucleos', $nucleos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.pesquisaeextensao.centrosenucleos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required'
        ]);

        # Create the actual post or event, whatever
        $nucleo = new Centros_Nucleo;
        $nucleo->title = $request->input('title');
        $nucleo->body = $request->input('body');
        $nucleo->save();

        return redirect('/centrosenucleos')->with('success', 'Núcleo "'.$request->input('title').'" publicado com sucesso.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $nucleo = Centros_Nucleo::find($id);

      # localize month name to pt.. not the best way, i know... but whatever!
        $months = [
                     '0','Janeiro', 'Fevereiro',
                     'Março','Abril','Maio','Junho',
                     'Julho','Agosto','Setembro',
                     'Outubro', 'Novembro', 'Dezembro'
        ];
        $monthRaw = $nucleo->updated_at->month;
        $ptmonth = $months[$monthRaw];

      # arguments to pass into the view:
        $data = [
            'nucleo'  => $nucleo,
            'ptmonth' => $ptmonth
        ];

      return view('pages.pesquisaeextensao.centrosenucleos.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // return 'working...';
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required',
            // 'images' => 'image|nullable|max:1999'
        ]);

        # Edit the actual post or event, whatever
        $nucleo = Centros_Nucleo::find($id);
        $nucleo->title = $request->input('title');
        $nucleo->body = $request->input('body');
        $nucleo->save();

        return redirect('/centrosenucleos')->with('success', 'Núcleo "'.$request->input('title').'" editada com sucesso.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function showmodal($id){
      $nucleo = Centros_Nucleo::find($id);
      return $nucleo;
    }
}
