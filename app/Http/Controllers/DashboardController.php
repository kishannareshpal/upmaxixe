<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Destaque;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;


class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.dashboard.index');
    }


    /**
     * Show the Home options.
     *
     * @return \Illuminate\Http\Response  <- Kishan, please research 'bout this later//
     */
    public function destaques()
    {
        $last_image = DB::table('destaques')->orderBy('id', 'desc')->first();
        $images = Destaque::all();

        $data = [
          'last_image'=>$last_image,
          'images'=>$images
        ];

        return view('pages.dashboard.destaques')->with($data);
    }


          # Upload images
          public function ui($id, Request $request){
            if ($request->hasFile('file')) {
              $file = $request->file('file');

              # Check if it is an image
              if(substr($file->getMimeType(), 0, 5) == 'image') {

                  // get just the ext:
                  $filenameExtensionOnly = $file->getClientOriginalExtension();
                  // the actual filename that is going to get stored in memory:
                  $filename = 'up-maxixe_destaque_'.time().'.'.$filenameExtensionOnly;

                  // Get the image from the requested id
                  $imagepost = Destaque::find($id);

                  // now if the row with that id already exists
                  if (!empty($imagepost)) {
                    if (!empty($imagepost->url)) {
                      if (Storage::exists('public/destaques_image/'.$id.'/'.$imagepost->url)) {
                        $localDelete = Storage::delete('public/destaques_image/'.$id.'/'.$imagepost->url);
                      }
                      // if the image is already uploaded
                      $imagepost->url = $filename;
                      $imagepost->save();
                    }

                  } else {
                    $imagepost = new Destaque;
                    $imagepost->id = $id;
                    $imagepost->url = $filename;
                    $imagepost->save();
                  }


                  // upload the image now, i guess:
                  $path = $file->storeAs('public/destaques_image/'.$id, $filename);
                  return 'success';

              } else {
                return 'file_unsupported';
              }

            } else {
              return 'request_failed';
            }
          }

          # Show the edit modal
          public function showmodal($id){
            $images = Destaque::find($id);
            return $images;
          }


          # Save Changes
          public function save($id, Request $request){
            $imagepost = Destaque::find($id);
            // $imagepost = new Destaque;
            // $imagepost->id = $id;
              $description = $request->input('description');
            $imagepost->description = $description;
            $imagepost->save();

            return redirect(route('dashboard.destaques'))->with('success', 'Guardado com sucesso.');
          }


          # Delete stuff
          public function delete($id, Request $request){
            $imagepost = Destaque::find($id);
            $localDelete = Storage::deleteDirectory('public/destaques_image/'.$id);
            $imagepost->delete();

            return redirect(route('dashboard.destaques'))->with('success', 'Apagado com sucesso.');
          }

}
