<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PosGraduacaoController extends Controller
{
    // Matches the url: /dppe
    function dppeapresentacao(){}
    // Matches the url: /dppe/organigrama
    function organigrama(){}
    // Matches the url: /dppe/planoestrategico
    function planoestrategico(){}
    // Matches the url: /dppe/regulamento
    function regulamento(){}
    // Matches the url: /dppe/boletim
    function boletimdodppe(){}
    // Matches the url: /actasdeconferencia
    function publactasdeconferencia(){}
    // Matches the url: /livros
    function publlivros(){}
    // Matches the url: /revistasdeposgraduacao
    function publrevistasdeposgrad(){}


    // Matches the url: /dppe/dpp
    function dppapresentacao(){}
    // Matches the url: /dppe/dpp/projectos
    function dppprojectos(){}
    // Matches the url: /dppe/dpp/linhasdepesquisa
    function dpplinhasdepesquisa(){}
    

    // Matches the url: /dppe/dpi
    function dpiapresentacao(){}
    // Matches the url: /dppe/dpi/projectos
    function dpiprojectos(){}
    // Matches the url: /dppe/dpi/escolinhas
    function dpiescolinhas(){}

        
    // Matches the url: /dppe/dfp
    function dfpapresentacao(){}
    // Matches the url: /dppe/dfp/efectivodedocente
    function dfpefectivodedocente(){}
    // Matches the url: /dppe/dfp/docentesporformacao
    function dfpdocentesporformacao(){}
}
