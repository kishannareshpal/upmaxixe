<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocentesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('docentes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');  // título académico // is he(she) a Dr(a).? MA?, etc
            $table->string('name'); // full name
            $table->longText('cadeiras'); // full name
            $table->string('photo')->nullable(); // profile picture
            $table->longText('biography')->nullable(); // pequena biografia
            $table->longText('email')->nullable(); // email
            $table->longText('celular')->nullable(); // email
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('docentes');
    }
}
