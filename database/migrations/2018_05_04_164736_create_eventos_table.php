<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('eventos', function (Blueprint $table) {
        $table->increments('id');
        $table->string('name');
        $table->string('local');
        $table->string('capacidade');
        $table->string('precoindividual');
        $table->string('imageurl')->nullable();
        $table->longText('description')->nullable();
        $table->dateTime('horario_do_inicio');
        $table->dateTime('horario_do_fim')->nullable();
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eventos');
    }
}
